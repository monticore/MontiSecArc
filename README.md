<!-- (c) https://github.com/MontiCore/monticore -->
# MonitSecArc
MontiSecArc (MSA) is a Security Architecture Description Language. This repository contains tooling based on MontiCore for Security Architecture Analysis and development of generators to derive code from MSA models.

# Content Overview
Folders in this repository:
* core     - the MontiSecArc language and analysis tools source code
* doc      - documentation for MontiSecArc
* examples - example projects for the MontiSecArc tools  <------------ start here

# Getting Started
Try out the pre-compiled Architecture Analysis by running:
```
cd examples/montiSecArcAnalysis/
./startExample.sh
```

# Build
Make sure you have access to MontiCore version 3. Then run the maven build:
```
cd core
mvn clean install
```

## Further Information

* [Project root: MontiCore @github](https://github.com/MontiCore/monticore)
* [MontiCore documentation](http://www.monticore.de/)
* [**List of languages**](https://github.com/MontiCore/monticore/blob/dev/docs/Languages.md)
* [**MontiCore Core Grammar Library**](https://github.com/MontiCore/monticore/blob/dev/monticore-grammar/src/main/grammars/de/monticore/Grammars.md)
* [Best Practices](https://github.com/MontiCore/monticore/blob/dev/docs/BestPractices.md)
* [Publications about MBSE and MontiCore](https://www.se-rwth.de/publications/)
* [Licence definition](https://github.com/MontiCore/monticore/blob/master/00.org/Licenses/LICENSE-MONTICORE-3-LEVEL.md)

