/* (c) https://github.com/MontiCore/monticore */

package secarc;


import com.google.inject.Inject;

import interfaces2.language.ILanguage;
import interfaces2.language.ModelingLanguage;
import mc.umlp.arcd.ToolConstants;
import secarc._tool.MontiSecArcRoot;
import secarc.ets.check.MontiSecArcAnalysisCreator;
import secarc.ets.check.MontiSecArcAnalysisWorkflow;
import secarc.ets.transform.criticalport.AnalysisCriticalPortTransformationWorkflow;
import secarc.ets.transform.trustlevel.AnalysisTrustlevelTransformationWorkflow;
import secarc.prettyprint.MontiSecArcPrettyPrintWorkflow;

/**
 * As subclass of {@link ModelingLanguage}, this class configures the technical
 * aspects of MontiSecArcAnalysis.
 * 
 * 
 */
public final class MontiSecArcAnalysis extends MontiSecArcLanguage {
  
  /**
   * Factory method for {@link MontiSecArcAnalysis}.
   */
  public static MontiSecArcAnalysis newLanguage(ILanguage component, String analysisParameter, String analysisConfPath) {
    return new MontiSecArcAnalysis(component, analysisParameter, analysisConfPath);
  }
  
  /**
   * Constructor for secarc.MontiSecArcAnalysis
   */
  @Inject
  public MontiSecArcAnalysis(final ILanguage component, String analysisParameter, String analysisConfPath) {
    super(component);
    
    /* Execution Units */
    
    //WORKFLOWS
        
    //Workflow for analysis with trustlevel
    this.addFilterTrustlevelWorkflow(analysisParameter, analysisConfPath);
    
    //Workflow for analysis with critical ports
    this.addFilterCriticalPortWorkflow(analysisConfPath);
    
    //Workflow for analysis
    this.addAnalysisWorkflow(analysisConfPath);
    
    // PrettyPrinter (can be called by "print" on the command line)
    addExecutionUnit(ToolConstants.WF_PRETTY_PRINT, new MontiSecArcPrettyPrintWorkflow(LANGUAGE_ROOT));
  }
  
  /**
   * Adds analysis workflow
   * 
   * @param analysisConfPath Configuration file path
   * @param analysisParameter Referenced components for what if analysis
   */
  private void addAnalysisWorkflow(String analysisConfPath) {
	  	//Workflow for Analysis
	    MontiSecArcAnalysisWorkflow<MontiSecArcRoot> analysisWorkflow = new MontiSecArcAnalysisWorkflow<MontiSecArcRoot>(LANGUAGE_ROOT);
	    //AnalysisCreator 
	    analysisWorkflow.setAnalysis(MontiSecArcAnalysisCreator.createAnalysis(analysisConfPath));
	    //AnalysisConfiguratin
	    if(analysisConfPath != null && !analysisConfPath.isEmpty()) {
	    	analysisWorkflow.setAnalysisConfiguration(MontiSecArcAnalysisCreator.createConfig(analysisConfPath));
	    } else {
	    	analysisWorkflow.setAnalysisConfiguration(MontiSecArcAnalysisCreator.createConfig());
	    }
	    //Add Workflow for Analysis for advanced users
	    analysisWorkflow.setAdvanced(false);
	    //Add Workflow for Analysis for beginners
	    addExecutionUnit(MontiSecArcConstants.ANALYSIS_WORKFLOW_BEGINNERS, analysisWorkflow);
	    
	    //Add Workflow for Analysis for advanced users
	    analysisWorkflow.setAdvanced(true);
	    addExecutionUnit(MontiSecArcConstants.ANALYSIS_WORKFLOW_ADVANCED, analysisWorkflow);
  }
  
  /**
   * Adds what if workflow for critical ports
   * 
   * @param analysisConfPath Configuration file path
   */
  private void addFilterCriticalPortWorkflow(String analysisConfPath) {
		AnalysisCriticalPortTransformationWorkflow<MontiSecArcRoot> criticalPortTransformationWorkflow = new AnalysisCriticalPortTransformationWorkflow<MontiSecArcRoot>(
				MontiSecArcRoot.class);
		addExecutionUnit(MontiSecArcConstants.FILTER_CRITICAL_PORT, criticalPortTransformationWorkflow);  
  }
  
  /**
   * Adds what if workflow for trustlevel 
   * 
   * @param analysisParameter Referenced components for what if analysis
   * @param analysisConfPath Configuration file path
   */
  private void addFilterTrustlevelWorkflow(String analysisParameter, String analysisConfPath) {
	  AnalysisTrustlevelTransformationWorkflow<MontiSecArcRoot> trustlevelTransformationWorkflow = new AnalysisTrustlevelTransformationWorkflow<MontiSecArcRoot>(MontiSecArcRoot.class);
	  trustlevelTransformationWorkflow.setAnalysisParameter(analysisParameter);
	  addExecutionUnit(MontiSecArcConstants.FILTER_TRUSTLEVEL, trustlevelTransformationWorkflow);
  }
  
}
