/* (c) https://github.com/MontiCore/monticore */
package secarc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.inject.Guice;
import com.google.inject.Injector;

import interfaces2.language.ETSTool;
import interfaces2.language.LanguageFamily;
import mc.ProblemReport.Type;
import mc.umlp.arc.MontiArcLanguageFamilyFactory;
import mc.umlp.arcd.InjectorProvider;
import secarc.ets.check.MontiSecArcAnalysisCreator;
import secarc.ets.check.MontiSecArcContextConditionCreator;

/**
 * Tool for {@link MontiSecArcAnalysis}.
 * 
 * 
 */
public class MontiSecArcAnalysisTool extends ETSTool {
	
	/**
	 * Path to config file
	 * 
	 * @param arguments
	 * @return String path to config file
	 */
	protected static String getAnalysisConfPath(String[] arguments) {
		boolean confpathExists = false;
		for(String argument : arguments) {
			if(argument.equals(MontiSecArcConstants.ARG_ANALYSISCONFPATH)) {
				confpathExists = true;
				continue;
			}
			if(confpathExists) {
				return argument;
			}
		}
		return "";
	}
	
	/**
	 * Searches for analysis parameters 
	 * 
	 * @param arguments
	 * @return List with parameters for analysis
	 */
	protected static List<String> getAnalysisParameter(String[] arguments) {
		boolean isAnalysisParameter = false; 
		List<String> analysisParameter = new ArrayList<String>();
		for(String argument : arguments) {
			//Search for beginning of parameters
			if(argument.equals(MontiSecArcConstants.FILTER_TRUSTLEVEL)) {
				isAnalysisParameter = true;
				continue;
			}
			
			if(isAnalysisParameter) {
				//New parameter
				if(argument.contains("-") || argument.contains("/")) {
					break;
				}
				analysisParameter.add(argument);
			}
			
		}
		
		return analysisParameter;
	}
	
	/**
	 * Cleans argument list form analysis parameters
	 * 
	 * @param arguments
	 * @return cleaned list
	 */
	protected static String[] cleanArgumentList(String[] arguments) {
		List<String> cleanedArguments = new ArrayList<String>();
		boolean isAnalysisParameter = false;
		boolean isAnalysisConfPath = false;
		for(String argument : arguments) {	
			//Filter parameter
			if(argument.equals(MontiSecArcConstants.FILTER_TRUSTLEVEL)) {
				isAnalysisParameter = true;
				cleanedArguments.add(argument);
				continue;
			}
			
			//Filter config path
			if(argument.equals(MontiSecArcConstants.ARG_ANALYSISCONFPATH)) {
				isAnalysisConfPath = true;
				continue;
			}
			
			//Filter config path
			if(isAnalysisConfPath) {
				isAnalysisConfPath = false;
				continue;
			}
			
			//Filter parameter
			if(isAnalysisParameter) {
				//New parameter
				if(argument.contains("-") || argument.contains("/")) {
					isAnalysisParameter = false;
					cleanedArguments.add(argument);
				}
				continue;
			}
			
			cleanedArguments.add(argument);
		}
		
		return cleanedArguments.toArray(new String[cleanedArguments.size()]);
	}

	/**
	* Constructor for secarc.MontiSecArcAnalysisTool
	* 
	* @param arguments
	*/
	public MontiSecArcAnalysisTool(String[] arguments, String analysisParameter, String analysisConfPath) {
		super(arguments);
	    
	    Injector injector = Guice.createInjector(new MontiSecArcDefaultModule());
	    InjectorProvider.setInjector(injector);
	    
	    LanguageFamily languageFamily = MontiArcLanguageFamilyFactory.create(MontiSecArcAnalysis.newLanguage(injector.getInstance(MontiSecArcComponent.class), analysisParameter, analysisConfPath));
	    setLanguages(languageFamily);
		
		// set default CoCos
	    Map<String, Type> cocoConfig = MontiSecArcContextConditionCreator.createConfig();
	    if(analysisConfPath != null && !analysisConfPath.isEmpty()) {
	    	cocoConfig.putAll(MontiSecArcAnalysisCreator.createConfig(analysisConfPath));
	    } else {
	    	cocoConfig.putAll(MontiSecArcAnalysisCreator.createConfig());
	    }
	    
	    setCocoConfiguration(cocoConfig);
	}
	
	/**
	* Constructor for secarc.MontiSecArcAnalysisTool
	* 
	* @param arguments
	*/
	public MontiSecArcAnalysisTool(String[] arguments, List<String> analysisParameter) {
		super(arguments);
	    Injector injector = Guice.createInjector(new MontiSecArcDefaultModule());
	    InjectorProvider.setInjector(injector);
	    
	    LanguageFamily languageFamily = new LanguageFamily();
	    languageFamily.addLanguage(MontiSecArcPreAnalysis.newLanguage(injector.getInstance(MontiSecArcComponent.class), analysisParameter));
	    setLanguages(languageFamily);
		
		// set default CoCos
	    Map<String, Type> cocoConfig = MontiSecArcContextConditionCreator.createConfig();
	    cocoConfig.putAll(MontiSecArcAnalysisCreator.createConfig());
	    
	    setCocoConfiguration(cocoConfig);
	}

}
