/* (c) https://github.com/MontiCore/monticore */
package secarc;


import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;

import secarc.ets.check.MontiSecArcAnalysisConstants;

import mc.MCG;

/**
 * Tool for {@link MontiSecArcAnalysis}.
 * For terminal controlled session
 * 
 * 
 */
public class MontiSecArcAnalysisToolWrapper extends MontiSecArcAnalysisTool {
	
	/**
     * Class of the dsl tool that is used.
     */
    protected static Class<MontiSecArcAnalysisToolWrapper> toolClass = MontiSecArcAnalysisToolWrapper.class;
	
	/**
     * Generate test output to this folder. 
     */
    public static final String OUTPUT_TEST_FOLDER = "target/testOut";
    
    /**
     * Test symbol table directory.
     */
    public static final String SYMTAB_FOLDER = "target/symtab";

    /**
     * constructor for preparing parameter
     * 
     * @param arguments
     * @param analysisParameter
     */
	public MontiSecArcAnalysisToolWrapper(String[] arguments, List<String> analysisParameter) {
		super(arguments, analysisParameter);
	}
	
	/**
	 * constructor for analysis
	 * 
	 * @param arguments
	 * @param analysisParameter
	 * @param analysisPath
	 */
	public MontiSecArcAnalysisToolWrapper(String[] arguments, String analysisParameter, String analysisPath) {
		super(arguments, analysisParameter, analysisPath);
	}
	
	/**
	* The main method of this tool. Creates, initializes and starts
	* {@link MontiSecArcAnalysisTool}. Takes a list of {@link DSLTool} arguments.
    * 
	* @param a list of {@link DSLTool} arguments.
	*/
	public static void main(String[] arguments) {
		
		MontiSecArcAnalysisTool tool = null;
		
		//Start Checking for parameters if parameters exist
		if(getAnalysisParameter(arguments).size() > 0) {
			System.out.println("\nStart: Checking parameter...\n");
			
			MCG.initMonticoreGlobals();
			tool = preCreateToolWithoutJava(arguments);
	
			tool.init();
			tool.run();
		}
		
		//Analysis
		
		//Analysis for every parameter
		if(MontiSecArcAnalysisConstants.EXISTING_PARAMETER.size() > 0) {

			for(String analysisParameter : MontiSecArcAnalysisConstants.EXISTING_PARAMETER) {
				System.out.println("\nStart: Analysis for " + analysisParameter);
				MCG.initMonticoreGlobals();
				tool = createToolWithoutJava(arguments, analysisParameter);
		
				tool.init();
				tool.run();
			}
		} else {
			//Analysis without parameter
			System.out.println("\nStart analysis\n");
			
			MCG.initMonticoreGlobals();
			tool = createToolWithoutJava(arguments, "");
	
			tool.init();
			tool.run();
		}
		
		System.out.println("\nDone");

	}
	
	/**
	 * Use start script for arguments 
	 * 
	 * @param arguments
	 * @return
	 */
	protected static MontiSecArcAnalysisToolWrapper preCreateToolWithoutJava(String[] arguments) {        
        MontiSecArcAnalysisToolWrapper tool = null;
        List<String> argsAsList = new LinkedList<String>();
        boolean next = false;
        for (String mp : arguments) {
        	if(!next){
        		argsAsList.add(mp);
        	}
        	if(mp.equals(MontiSecArcConstants.PREPARE_PARAMETER)) {
        		next = true;
        	}
        	//Search for files 
        	if(next && mp.contains("/")) {
        		argsAsList.add(mp);
        	}
        }
        try {
            Constructor<MontiSecArcAnalysisToolWrapper> constructor = toolClass.getConstructor(String[].class, List.class);
            String[] args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) cleanArgumentList(args), (Object) getAnalysisParameter(arguments));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
	
	/**
	 * Use start script for arguments
	 * 
	 * @param arguments
	 * @param analysisParameter
	 * @return
	 */
	protected static MontiSecArcAnalysisToolWrapper createToolWithoutJava(String[] arguments, String analysisParameter) {
        MontiSecArcAnalysisToolWrapper tool = null;
        List<String> argsAsList = new LinkedList<String>();
        for (int i = 0; i < arguments.length; i++) {
        	if(arguments[i].equals(MontiSecArcConstants.PREPARE_PARAMETER)) {
        		argsAsList.remove(i-1);
        		argsAsList.remove(i-2);
        		continue;
        	}
            argsAsList.add(arguments[i]);
        }
        try {
            Constructor<MontiSecArcAnalysisToolWrapper> constructor = toolClass.getConstructor(String[].class, String.class, String.class);
            String[] args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) cleanArgumentList(args), (Object) analysisParameter, (Object) getAnalysisConfPath(args));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }

}
