/* (c) https://github.com/MontiCore/monticore */

package secarc;

import java.util.List;

import com.google.inject.Inject;

import interfaces2.language.ILanguage;
import interfaces2.language.ModelingLanguage;
import secarc._tool.MontiSecArcRoot;
import secarc.ets.check.MontiSecArcAnalysisCreator;
import secarc.ets.transform.trustlevel.PreAnalysisTrustlevelTransformationWorkflow;

/**
 * As subclass of {@link ModelingLanguage}, this class configures the technical
 * aspects of MontiSecArcAnalysis.
 * 
 * Checks if the parameter for what if trustlevel exist
 * 
 * 
 */
public final class MontiSecArcPreAnalysis extends MontiSecArcLanguage {
  
  /**
   * Factory method for {@link MontiSecArcPreAnalysis}.
   */
  public static MontiSecArcPreAnalysis newLanguage(ILanguage component, List<String> analysisParameter) {
    return new MontiSecArcPreAnalysis(component, analysisParameter);
  }

  /**
   * Constructor for secarc.MontiSecArcPreAnalysis
   */
  @Inject
  public MontiSecArcPreAnalysis(final ILanguage component, List<String> analysisParameter) {
    super(component);
    
    /* Execution Units */
    
    //WORKFLOWS
        
    // PrettyPrinter (can be called by "print" on the command line)
    PreAnalysisTrustlevelTransformationWorkflow<MontiSecArcRoot> pretransfomration = new PreAnalysisTrustlevelTransformationWorkflow<MontiSecArcRoot>(MontiSecArcRoot.class);
    //AnalysisCreator 
    pretransfomration.setAnalysis(MontiSecArcAnalysisCreator.createPreAnalysis());
    pretransfomration.setAnalysisConfiguration(MontiSecArcAnalysisCreator.createPreAnalysisConfig());
    pretransfomration.setAnalysisParameter(analysisParameter);
    addExecutionUnit(MontiSecArcConstants.PREPARE_PARAMETER, pretransfomration);
  }
  
}
