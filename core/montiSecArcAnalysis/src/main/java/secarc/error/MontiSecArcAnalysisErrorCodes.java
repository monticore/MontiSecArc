/* (c) https://github.com/MontiCore/monticore */
package secarc.error;

import mc.IErrorCode;

/**
 * Enumeration containing the MontiSecArcAnalysis error codes.
 *
 *
 */
public enum MontiSecArcAnalysisErrorCodes implements IErrorCode {
	
	/**
	 * Lists all filters
	 */
	ListFilters,
	
	/**
	 * Checks if a input is filter before it is used
	 */
	TaintTracking,
	
	/**
	 * Lists all incoming ports of the system
	 */
	ListIncomingtPorts,
	
	/**
	 * Lists all outgoing ports of the system
	 */
	ListOutgoingPorts,
	
	/**
	 * Checks if a configuration is reviewed
	 */
	ReviewedConfiguration,
	
	/**
	 * Checks if an encrypted path has an unencrypted path
	 */
	EncryptedPathWithUnencryptedPart,
	
	/**
	 * Checks if encrypted path ends in low trustlevel 
	 */
	EncryptedPathEndInLowTrustlevel,
	
	/**
	 * Checks if unencrypted connector through low trustlevel
	 */
	UnencryptedConnectorThroughLowTrustlevel,
	
	/**
	 * Derives all roles for a third party component
	 */
	DerivedRolesThirdParty,
	
	/**
	 * Lists all critical ports
	 */
	ListAllCriticalPorts,
	
	/**
	 * List all roles for a component
	 */
	DerivedRolesComponent,
	
	/**
	 * Identity link is used with encryption
	 */
	IdentityWithEncryption,
	
	/**
	 * Trustlevel of encrypted path is higher than the environment
	 */
	TrustlevelPathHigherThanEnvironment,
	
	/**
	 * Derives all roles for ports
	 */
	DerivedRolesPort,
	
	/**
	 * Access for roles
	 */
	RoleAccess,
	
	/**
	 * If a trustlevel differs more then 2 form the trustlevel of the supercomponent a reason is expected
	 */
	ReasonDifferingTrustlevel,
	
	/**
	 * Derived turstlevel
	 */
	DerivedTrustlevel,
	
	/**
	 * A trustlevel of a server should be higher than
	 * the trustlevel of a client 
	 */
	TrustlevelClientServerIdentity,
	
	/**
	 * A input port should avoid the type String. 
	 */
	AvoidInputString,
	
	/**
	 * Lists all third party components
	 */
	ListThirdPartyComponents,
	
	/**
	 * The encrypted path continues unencrypted. Checks which component can read the data
	 */
	EncryptedPathContinuesUnencrypted,
	
	/**
	 * Filters have a higher trust than other components
	 */
	FilterWithHigherTrust,
	
	/**
	 * For a trustlevel relation should exist trustlevels
	 */
	TrustlevelForTrustlevelrelation,
	
	/**
	 * If an identity link is defined, the target should define roles
	 */
	IdentityWithoutRoles,
	
	/**
	 * The referenced component for the what if analysis does not exist
	 */
	ReferencedComponentWhatIfDoNotExist,
}
