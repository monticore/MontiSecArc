/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.coco.ContextCondition;

/**
 * Analysis are special context conditions
 * 
 * 
 * 
 */
public abstract class Analysis extends ContextCondition {

	public Analysis(String checkedProperty) {
		super(checkedProperty);
	}

}
