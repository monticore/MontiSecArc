/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import interfaces2.STEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

public final class AnalysisHelper {

	private AnalysisHelper() {
		
	}
	
	/**
	 * Checks if the port is the beginning of a path
	 * 
	 * @param entry
	 * @param graph
	 * @return
	 */
	public static ConnectorEntry isPortBenningOfPath(PortEntry entry, ArchitectureGraph graph) {
		Vertex<PortEntry> portVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getReversedRawGraph(), portVertex);
		
		//First element is not needed
		iterator.next();
		
		STEntry element = null;
		
		while(iterator.hasNext()) {
			element = iterator.next().getArchitectureElement();
			if(element instanceof ConnectorEntry) {
				return (ConnectorEntry) element;
			}
		}
		
		return null;
	}
	
}
