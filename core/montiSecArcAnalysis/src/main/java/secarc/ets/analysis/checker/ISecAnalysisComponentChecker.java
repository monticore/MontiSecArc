/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import secarc.ets.entries.SecComponentEntry;
import mc.umlp.arcd._ast.ASTArcComponent;
import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;

/**
 * Analysis checker interface for checking component
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisComponentChecker {

	/**
	 * Checks analysis for components
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @return factor of analysis
	 * @throws AmbigousException
	 * @throws EntryLoadingErrorException 
	 */
	int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced) throws AmbigousException, EntryLoadingErrorException;
	
}
