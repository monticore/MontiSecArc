/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import secarc._ast.ASTSecArcConfiguration;
import secarc.ets.entries.ConfigurationEntry;

/**
 * Analysis checker interface for checking configuration
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisConfigurationChecker {

	/**
	 * Checks analysis of configuration
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @return factor of analysis
	 */
	int check(ASTSecArcConfiguration node, ConfigurationEntry entry, boolean advanced);
	
}
