/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.graph.ArchitectureGraph;
import interfaces2.resolvers.AmbigousException;

/**
 * Analysis checker interface for checking connector
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisConnectorChecker {

	/**
	 * Checks analysis of connector 
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @param graph architecture graph with connectors as edge
	 * @return factor of analysis
	 * @throws AmbigousException
	 */
	int check(ASTArcConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException;
	
	/**
	 * Checks analysis of simple connector 
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @param graph architecture graph with connectors as edge
	 * @return factor of analysis
	 * @throws AmbigousException
	 */
	int check(ASTArcSimpleConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException;
	
}
