/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcFilter;
import secarc.ets.entries.FilterEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Analysis checker interface for checking filter
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisFilterChecker {

	/**
	 * Checks analysis of filter 
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @param graph architecture graph with connectors as edge
	 * @return factor of analysis
	 * @throws AmbigousException
	 */
	int check(ASTSecArcFilter node, FilterEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException;
	
}
