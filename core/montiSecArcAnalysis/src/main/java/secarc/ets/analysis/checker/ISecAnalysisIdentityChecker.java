/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Analysis checker interface for checking identity
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisIdentityChecker {

	/**
	 * Checks analysis of identity links
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param graphConnectorEdges graph with connectors as edges
	 * @param graphIdentityEdges graph with identity links as edges
	 * @param advanced detailness of output  
	 * @return factor of analysis
	 * @throws AmbigousException
	 */
	int check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graphConnectorEdges, ArchitectureGraph graphIdentityEdges, boolean advanced) throws AmbigousException;
	
}
