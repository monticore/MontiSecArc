/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.resolvers.AmbigousException;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;

/**
 * Analysis checker interface for checking parameter
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisParameterChecker {

	/**
	 * Checks analysis for parameter
	 * 
	 * @param parameter Referenced component
	 * @param node ASTNode to check
	 * @throws AmbigousException
	 */
	void check(ASTMCCompilationUnit node, String parameter) throws AmbigousException;
	
}
