/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.resolvers.AmbigousException;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;
import mc.umlp.arcd._ast.ASTArcPort;

/**
 * Analysis checker interface for checking port
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisPortChecker {

	/**
	 * analysis of port
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param graph graph with connectors as edges
	 * @param advanced detailness of output
	 * @return factor of analysis
	 * @throws AmbigousException
	 */
	int check(ASTArcPort node, SecPortEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException;
	
}
