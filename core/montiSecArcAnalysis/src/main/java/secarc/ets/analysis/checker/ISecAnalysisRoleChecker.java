/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc.ets.entries.RoleEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Analysis checker interface for checking role
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisRoleChecker {

	/**
	 * Analysis of roles
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param graph graph with connectors as edges
	 * @throws AmbigousException
	 */
	int check(ASTSecArcRole node, RoleEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException;
	
	/**
	 * Analysis for referenced roles
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param graph graph with connectors as edges
	 * @param advanced output for advanced users
	 * @return
	 * @throws AmbigousException
	 */
	int check(ASTSecArcRefRole node, RoleEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException;
	
}
