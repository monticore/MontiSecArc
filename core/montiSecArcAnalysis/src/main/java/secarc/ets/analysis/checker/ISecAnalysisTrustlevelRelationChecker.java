/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.checker;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.ets.entries.TrustlevelRelationEntry;

/**
 * Analysis checker interface for checking trustlevelrelation
 * related analysis
 * 
 * 
 * 
 */
public interface ISecAnalysisTrustlevelRelationChecker {

	/**
	 * Analysis of trustlevel 
	 * 
	 * @param node ast node to check
	 * @param entry entry of ast node
	 * @param advanced detailness of output
	 * @return factor of analysis
	 * @throws AmbigousException
	 */
	int check(ASTSecArcTrustlevelRelation node, TrustlevelRelationEntry entry, boolean advanced) throws AmbigousException;
	
}
