/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.component;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;

/**
 * L06: Lists all third party components with configuration and cpe
 * 
 * 
 * 
 */
public class ListThirdPartyComponents extends Analysis implements
		ISecAnalysisComponentChecker {
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ListThirdPartyComponents(int factor) {
		super(MontiSecArcAnalysisConstants.LIST_THIRD_PARTY_COMPONENTS);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		if(entry.getConfiguration() != null && entry.getCPE() != null) {
			addReport("The third party component " + NameHelper.getSimplenameFromComplexname(entry.getName()) + " has the cpe " + entry.getCPE().getCPE() + " and configuration " + entry.getConfiguration().getName() + ".", node.get_SourcePositionStart());
			return factor;
		}
		
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents;
	}

}
