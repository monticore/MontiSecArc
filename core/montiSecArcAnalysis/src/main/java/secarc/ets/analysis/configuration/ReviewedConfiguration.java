/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.configuration;

import mc.IErrorCode;
import secarc._ast.ASTSecArcConfiguration;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisConfigurationChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.ConfigurationEntry;

/**
 * L07: Checks if a configuration is reviewed.
 * Information gathering:
 *  - Testing for Web application fingerpring
 *  - Application discovery
 *  - Analysis of error codes
 * Configuration management testing
 * 
 * 
 * 
 */
public class ReviewedConfiguration extends Analysis implements
		ISecAnalysisConfigurationChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ReviewedConfiguration(int factor) {
		super(MontiSecArcAnalysisConstants.REVIEWED_CONFIGURATION);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecConfigurationChecker#check(secarc._ast.ASTSecArcConfiguration, secarc.ets.entries.ConfigurationEntry)
	 */
	public int check(ASTSecArcConfiguration node, ConfigurationEntry entry, boolean advanced) {
		String name = entry.getName();
		String standardOutput = "The configuration " + name + " is not reviewed, yet.";
		if(!name.endsWith("_reviewed")) {
			if(advanced) {
				addReport(standardOutput + " After the configuration is reviewed, a \"_reviewed\" must be added to the configuration's name.", node.get_SourcePositionStart());
			} else {
				addReport(standardOutput, node.get_SourcePositionStart());
			}
			return factor;
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ReviewedConfiguration;
	}

}
