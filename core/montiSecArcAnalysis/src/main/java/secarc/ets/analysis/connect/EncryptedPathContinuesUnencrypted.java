/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.connect;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.AnalysisHelper;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

/**
 * E03: Checks if an encrypted path continues unencrypted. 
 * Then the message is readable for the following components
 * 
 * 
 * 
 */
public class EncryptedPathContinuesUnencrypted extends Analysis implements
		ISecAnalysisPortChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public EncryptedPathContinuesUnencrypted(int factor) {
		super(MontiSecArcAnalysisConstants.ENCRYPTED_PATH_CONTINUES_UNENCRYPTED);
		this.factor = factor;		
	}

	@Override
	public int check(ASTArcPort node, SecPortEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		
		//Checks if the port is a target of an encrypted connection
		ConnectorEntry connectorEntry = AnalysisHelper.isPortBenningOfPath(entry, graph);
		//No encryption before
		if(connectorEntry == null || ((SecConnectorEntry) connectorEntry).isUnencrypted()) {
			return 0;
		}
		
		Vertex<PortEntry> portVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getRawGraph(), portVertex);
		
		//First element is not needed
		iterator.next();
		
		//Unencrypted path starts
		boolean unencryptedEnds = false;
		
		//output needed
		boolean outputNeeded = false;
		
		//Current element
		STEntry element = null;
		
		int count = 0;
		
		//Build Message		
		StringBuilder sbuilder = new StringBuilder("The port " + entry.getName() + " is the end of an encrypted path. The path continues unencrypted: ");
				
		//Run over paths
		while(iterator.hasNext()) {
			//Next element from graph
			element = iterator.next().getArchitectureElement();
			
			//Next path
			if(element.equals(entry)) {
				unencryptedEnds = false;
				sbuilder.append(", ");
				count = 0;
			}
			
			//Continues unencrypted
			if(element instanceof ConnectorEntry && ((SecConnectorEntry) element).isUnencrypted() && !unencryptedEnds) {
				if(count > 0) {
					sbuilder.append(" -> ");
				}
				sbuilder.append(element);
				outputNeeded = true;
				count++;
			} else if(element instanceof ConnectorEntry && ((SecConnectorEntry) element).isEncrypted()) {
				//Continues encrypted -> the rest is not needed because the analysis for the port will manage this
				unencryptedEnds = true;
			}
			
		}
		
		if(outputNeeded) {
			addReport(sbuilder.toString(), node.get_SourcePositionStart());
			return factor;
		}
		return 0;
	}

	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.EncryptedPathContinuesUnencrypted;
	}

}
