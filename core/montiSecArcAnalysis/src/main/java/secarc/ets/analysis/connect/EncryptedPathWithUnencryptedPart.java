/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.connect;


import java.util.ArrayList;
import java.util.List;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.AnalysisHelper;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;
import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * E01: Checks if a path consists of encrypted and unencrypted parts
 * 
 * - SSL/TLS Testing
 * 
 * 
 * 
 */
public class EncryptedPathWithUnencryptedPart extends Analysis
		implements ISecAnalysisPortChecker { 
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public EncryptedPathWithUnencryptedPart(int factor) {
		super(MontiSecArcAnalysisConstants.ENCRYPTED_PATH_WITH_UNENCRYPTED_PART);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecPortChecker#check(mc.umlp.arcd._ast.ASTArcPort, secarc.ets.entries.SecPortEntry)
	 */
	public int check(ASTArcPort node, SecPortEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException{
		
		//Checks if the path starts with this port
		if(AnalysisHelper.isPortBenningOfPath(entry, graph) != null) {
			return 0;
		}
		
		
		Vertex<PortEntry> portVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getRawGraph(), portVertex);
		//Saves path while checking 
		List<SecConnectorEntry> path = new ArrayList<SecConnectorEntry>();
		//Boolean for encrypted element
		boolean encrypted = false;
		//Boolean for unencrypted element
		boolean unencryted = false;
		
		STEntry element = null;
		
		//First element is not needed
		iterator.next();
		
		//Run over paths
		while(iterator.hasNext()) {
			//Next element from graph
			element = iterator.next().getArchitectureElement();
			
			//Run over path
			if(element instanceof SecConnectorEntry) {
				
				//Search for encrypted parts
				if(((SecConnectorEntry) element).isEncrypted()) {
					encrypted = true;
				}
				
				//Search for unencrypted parts
				if(!((SecConnectorEntry) element).isEncrypted()) {
					unencryted = true;
				}
				
				//Save path
				path.add((SecConnectorEntry) element);
				
			}
				
			//New path starts
			if(element.equals(entry) || !iterator.hasNext()) {
				//Check if info is needed
				if(!path.isEmpty()) {
					//Check for mixed path
					if(encrypted && unencryted) {
						StringBuilder sBuilder = new StringBuilder("The following path consists of encrypted and unencrypted parts: ");
						if(path.get(0).isUnencrypted()) {
							sBuilder.append("'" + path.get(0) + "*");
						} else {
							sBuilder.append("'" + path.get(0) + "'");
						}
						path.remove(0);
						for(SecConnectorEntry pathElement : path) {
							sBuilder.append(" -> ");
							if(pathElement.isUnencrypted()) {
								sBuilder.append("'" + pathElement + "'*");
							} else {
								sBuilder.append("'" + pathElement + "'");
							}
						}
						sBuilder.append(". The unecrypted parts are marked by *. It is likely that an encryption is missing.");
						addReport(sBuilder.toString(), node.get_SourcePositionStart());
						return factor;
						
					}
				}
				path.clear();
				encrypted = false;
				unencryted = false;
			}
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.EncryptedPathWithUnencryptedPart;
	}
	
}
