/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.connect;

import mc.IErrorCode;
import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import interfaces2.resolvers.AmbigousException;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.ISecAnalysisConnectorChecker;
import secarc.ets.analysis.trustlevel.DerivedTrustlevel;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecSubComponentEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * E04: Checks if an unencrypted connector is embedded in a component with a low trustlevel
 * 
 * - SSL/TLS Testing
 * 
 * 
 * 
 */
public class UnencryptedConnectorThroughLowTurstlevel extends DerivedTrustlevel
		implements ISecAnalysisConnectorChecker {
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public UnencryptedConnectorThroughLowTurstlevel(int factor) {
		super(MontiSecArcAnalysisConstants.UNENCRYPTED_CONNECTOR_THROUGH_LOW_TRUSTLEVEL);
		this.factor = factor;
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisConnectorChecker#check(cc.clarc.lang.architecture._ast.ASTArcConnector, secarc.ets.entries.SecConnectorEntry)
	 */
	@Override
	public int check(ASTArcConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		return innerCheck(node, entry, node.getMainParent(), advanced);
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisConnectorChecker#check(mc.umlp.arcd._ast.ASTArcSimpleConnector, secarc.ets.entries.SecConnectorEntry)
	 */
	@Override
	public int check(ASTArcSimpleConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		return innerCheck(node, entry, node.getMainParent(), advanced);
	}
	
	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	protected int innerCheck(ASTNode node, SecConnectorEntry entry, ASTArcComponent nodeParent, boolean advanced) throws AmbigousException {
		if(entry.isEncrypted()) {
			return 0;
		}
		
		SecComponentEntry parentComp = (SecComponentEntry) resolver.resolve(nodeParent.getName(), ComponentEntry.KIND, getNameSpaceFor(nodeParent));
		//Component source
		String sourceRef = entry.getSource();
		SecSubComponentEntry sourceSubComp = null;
		SecComponentEntry sourceComp = null;
		//Find source component
		if(sourceRef.contains(".")) {
			String nameSourceComp = sourceRef.substring(0, sourceRef.indexOf(".")); 
			sourceSubComp = (SecSubComponentEntry) resolver.resolve(nameSourceComp, SubComponentEntry.KIND, getNameSpaceFor(nodeParent));
			//Check in a coco
			if(sourceSubComp != null) {
				sourceComp = (SecComponentEntry) sourceSubComp.getComponentType().getBestKnownVersion();
			}
		} else {
			sourceComp = parentComp;
		}
		
		//Component target
		String targetRef = entry.getTarget();
		SecSubComponentEntry targetSubComp = null;
		SecComponentEntry targetComp = null;
		//Find traget component
		if(targetRef.contains(".")) {
			String nameTargetComp = targetRef.substring(0, targetRef.indexOf("."));
			targetSubComp = (SecSubComponentEntry) resolver.resolve(nameTargetComp, SubComponentEntry.KIND, getNameSpaceFor(nodeParent));
			//Check in a coco
			if(targetSubComp != null) {
				targetComp = (SecComponentEntry) targetSubComp.getComponentType().getBestKnownVersion();
			}
		} else {
			targetComp = parentComp;
		}
		
		//Ports are always in different components
		//if both ports have qualified name, in supercomponent
		if(!(sourceRef.contains(".") && targetRef.contains(".")) && (parentComp != null && parentComp.isInnerComponent())) {
			parentComp = (SecComponentEntry) resolver.resolve(nodeParent.getMainParent().getName(), ComponentEntry.KIND, getNameSpaceFor(nodeParent.getMainParent()));
		}
		
		String parentTrustlevel = "-2";
		int parentTrustlevelCompare = -1;
		//Check if Trustlevel for parent exists
		if(parentComp != null && parentComp.getTrustlevel().isPresent()) {
			parentTrustlevel = CoCoHelper.getTrustlevelAsString(parentComp);
			parentTrustlevelCompare = CoCoHelper.getTrustlevelAsInteger(parentComp);
		} else {
			//Derive trustlevel
			parentTrustlevel = CoCoHelper.getTrustlevelAsString(getDerivedTrustlevel(nodeParent));
			parentTrustlevelCompare = CoCoHelper.getTrustlevelAsInteger(getDerivedTrustlevel(nodeParent));
		}
		
		int sourceTrustlevelCompare = -1;
		//Check if Trustlevel for source exists
		if(sourceSubComp != null && sourceComp.getTrustlevel().isPresent()) {
			sourceTrustlevelCompare = CoCoHelper.getTrustlevelAsInteger(sourceComp);
		} else {
			//Do not have a own trustlevel, derive trustlevel
			sourceTrustlevelCompare = parentTrustlevelCompare;
		}
		
		int targetTrustlevelCompare = -1;
		//Check if Trustlevel for target exists
		if(targetSubComp != null && targetComp.getTrustlevel().isPresent()) {
			targetTrustlevelCompare = CoCoHelper.getTrustlevelAsInteger(targetComp);
		} else {
			//Do not have a own trustlevel, derive trustlevel
			targetTrustlevelCompare = parentTrustlevelCompare;
		}
		
		String standardOutput = "The unencrypted connector " + entry + " is embedded in a component with a low trustlevel: " + parentTrustlevel + ".";
		
		//The trustlevel of the supercomponent is low if it is lower than the trustlevel of the source or target
		if(parentTrustlevelCompare < sourceTrustlevelCompare || parentTrustlevelCompare < targetTrustlevelCompare) {
			if(advanced) {
				addReport(standardOutput, node.get_SourcePositionStart());
			} else {
				addReport(standardOutput + 
						" A trustlevel is defined as low if the trustlevel of the super component is lower than the trustlevel of the source or target component.", node.get_SourcePositionStart());
			}
			return factor;
		}
		
		return 0;
		
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.UnencryptedConnectorThroughLowTrustlevel;
	}

}
