/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.filter;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.AnalysisHelper;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * L05: String is a very unspecific input type. Therfore, it should be avoided
 * 
 * 
 * 
 */
public class AvoidInputString extends Analysis implements
		ISecAnalysisPortChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public AvoidInputString(int factor) {
		super(MontiSecArcAnalysisConstants.AVOID_INPUT_STRING);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisPortChecker#check(mc.umlp.arcd._ast.ASTArcPort, secarc.ets.entries.SecPortEntry, secarc.ets.graph.ArchitectureGraph)
	 */
	@Override
	public int check(ASTArcPort node, SecPortEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		if(AnalysisHelper.isPortBenningOfPath(entry, graph) == null
				&& entry.getTypeReference().getExtendedName().equals("java.lang.String")) {
			addReport("The input port " + entry.getName() + " has the type " + entry.getTypeReference() + ". This type is not specific enough.", node.get_SourcePositionStart());
			return factor;
		}
		
		return 0;

	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() { 
		return MontiSecArcAnalysisErrorCodes.AvoidInputString;
	}

}
