/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.filter;



import java.util.ArrayList;
import java.util.List;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import mc.IErrorCode;
import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd.ets.entries.PortEntry;
import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.AnalysisHelper;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.analysis.trustlevel.DerivedTrustlevel;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

/**
 * E05: Checks if an encrypted path ends in a low trustlevel
 * 
 * - SSL/TLS Testing
 * 
 * 
 * 
 */
public class EncryptedPathEndInLowTrustlevel extends DerivedTrustlevel implements
		ISecAnalysisPortChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public EncryptedPathEndInLowTrustlevel(int factor) {
		super(MontiSecArcAnalysisConstants.ENRYPTED_PATH_END_IN_LOW_TRUSTLEVEL);
		this.factor = factor;
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisPortChecker#check(mc.umlp.arcd._ast.ASTArcPort, secarc.ets.entries.SecPortEntry, secarc.ets.graph.ArchitectureGraph)
	 */
	@Override
	public int check(ASTArcPort node, SecPortEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		
		if(AnalysisHelper.isPortBenningOfPath(entry, graph) != null) {
			return 0;
		}
		
		
		Vertex<PortEntry> portVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getRawGraph(), portVertex);
		//Saves path while checking 
		List<SecConnectorEntry> path = new ArrayList<SecConnectorEntry>();
		//Boolean for encrypted element
		boolean encrypted = false;
		//Boolean for unencrypted element
		boolean unencryted = false;
		//only new component, if a new connector is found
		boolean newComponent = false;
		//Trustlevel of the found componet
		String trustlevel = "-2";
		int trustlevelCompare = -1;
		//Trustlevel of the super component
		int trustlevelSuperComponentCompare = -1;
		//Supercomponent entry
		SecComponentEntry superComponentEntry = null;
		
		STEntry element = null;
		SecComponentEntry lastComponent = null;
		
		while(iterator.hasNext()) {
			element = iterator.next().getArchitectureElement();
			
			if(element instanceof SecConnectorEntry) {
				
				newComponent = true;
				
				if(((SecConnectorEntry) element).isEncrypted()) {
					encrypted = true;
				}
				
				if(!((SecConnectorEntry) element).isEncrypted()) {
					unencryted = true;
				}
				
				//Save path
				path.add((SecConnectorEntry) element);
				
			}
			
			//Save subcomponent for trustlevel
			if(element instanceof SecComponentEntry && newComponent) {
				lastComponent = ((SecComponentEntry) element);
				newComponent = false;
			}
				
			//New path starts or next iteration for breath first search starting with other elements
			if(element.equals(entry) || !iterator.hasNext()) {
				//Check if info is needed
				if(!path.isEmpty()) {
					//Check just encrypted
					if(encrypted && !unencryted) {
						trustlevel = "-2";
							trustlevelCompare = -1;
						//component has a trustlevel
						if(lastComponent != null && lastComponent.getTrustlevel().isPresent()) {
							
							trustlevel = CoCoHelper.getTrustlevelAsString(lastComponent);
							trustlevelCompare = CoCoHelper.getTrustlevelAsInteger(lastComponent);
							
							//Trustlevel of supercomponent
							superComponentEntry = getDerivedTrustlevel((ASTArcComponent) lastComponent.getBestKnownVersion().getNode());
							//If no trustlevel in supercomponent exists, take the default trustlevel of the environment
							if(superComponentEntry != null && superComponentEntry.getTrustlevel().isPresent()) {
								trustlevelSuperComponentCompare = CoCoHelper.getTrustlevelAsInteger(superComponentEntry);
							} else {
								trustlevelSuperComponentCompare = -1;
							}
							
						} else {
							
							//Search for trustlevel -> derived trustlevel
							lastComponent = getDerivedTrustlevel((ASTArcComponent) lastComponent.getBestKnownVersion().getNode());
							//No trustlevel in super components defined, take the default trustlevel of the environment
							if(lastComponent.getTrustlevel().get() == null) {
								trustlevelCompare = -1;
								trustlevelSuperComponentCompare = -2;
							} else {
								//Trustlevel of supercomponent
								superComponentEntry = getDerivedTrustlevel((ASTArcComponent) lastComponent.getBestKnownVersion().getNode());
								//If no trustlevel in supercomponent exists, take the default trustlevel of the environment
								if(superComponentEntry == null || !superComponentEntry.getTrustlevel().isPresent()) {
									trustlevelSuperComponentCompare = -1;
								} else {
									trustlevelSuperComponentCompare = CoCoHelper.getTrustlevelAsInteger(superComponentEntry);
								}
							}
							
						}
						
						//If the trustlevel of the current component is lower than the trustlevel of the super component, warning
						if(trustlevelCompare < trustlevelSuperComponentCompare) {
							printWarningMessage(path, trustlevel, node);
							return factor;
						}
						
					}
				}
				path.clear();
				encrypted = false;
				unencryted = false;
			}

			
		}
		
		return 0;
		
	}
	
	/**
	 * Print the warining messagen for the analysis
	 * 
	 * @param path
	 * @param trustlevel
	 * @param node
	 */
	private void printWarningMessage(List<SecConnectorEntry> path, String trustlevel, ASTNode node) {
		StringBuilder builder = new StringBuilder("The following encrypted path ends in the low trustlevel " + trustlevel  + ": " + path.get(0));
		path.remove(0);
		for(SecConnectorEntry pathElement : path) {
			builder.append(" -> ");
			builder.append(pathElement);
		}
		addReport(builder.toString(), node.get_SourcePositionStart());
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.EncryptedPathEndInLowTrustlevel;
	}

}
