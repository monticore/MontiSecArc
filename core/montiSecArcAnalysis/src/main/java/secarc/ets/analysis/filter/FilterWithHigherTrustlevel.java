/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.filter;

import com.google.common.base.Optional;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import secarc._ast.ASTSecArcFilter;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.ISecAnalysisFilterChecker;
import secarc.ets.analysis.trustlevel.DerivedTrustlevel;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * F02: A filter should be more trustful than a 'normal' component
 * 
 * 
 * 
 */
public class FilterWithHigherTrustlevel extends DerivedTrustlevel implements
		ISecAnalysisFilterChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public FilterWithHigherTrustlevel(int factor) {
		super(MontiSecArcAnalysisConstants.FILTER_WITH_HIGHER_TRUST);
		this.factor = factor;
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		return 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisFilterChecker#check(secarc._ast.ASTSecArcFilter, secarc.ets.entries.FilterEntry, secarc.ets.graph.ArchitectureGraph)
	 */
	@Override
	public int check(ASTSecArcFilter node, FilterEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		ASTArcComponent parentNode = null;
		
		if(node.getMainParent() instanceof ASTArcComponent) {
			parentNode = (ASTArcComponent) node.getMainParent();
		} else {
			parentNode = (ASTArcComponent) ((ASTArcPort) node.getMainParent()).getMainParent();
		}
		
		SecComponentEntry componentEntry = (SecComponentEntry) resolver.resolve(parentNode.getName(), ComponentEntry.KIND, getNameSpaceFor(parentNode));
		SecComponentEntry parentComponentEntry = getDerivedTrustlevel(parentNode);
		
		Optional<TrustlevelEntry> trustlevelFilterEntry = componentEntry.getTrustlevel();
		
		if(!trustlevelFilterEntry.isPresent()) {
			if(parentComponentEntry.getFilter() != null) {
				//Check the same for the super filter component
				return 0;
			} else {
				addReport("The component with the filter " + entry.getName() + " has the same trustlevel as the supercomponent. A filter should have a higher trustlevel than the surrounding component.", node.get_SourcePositionStart());
				return factor;
			}
		} else {
			if(parentComponentEntry.getFilter() != null) {
				//Check the same for the super filter component
				return 0;
			} else {
				int filterTrustlevel = CoCoHelper.getTrustlevelAsInteger(componentEntry);
				int parentTrustlevel = -1;
				if(parentComponentEntry != null) {
					parentTrustlevel = CoCoHelper.getTrustlevelAsInteger(parentComponentEntry);
				}
				
				if(filterTrustlevel <= parentTrustlevel) {
					addReport("The component with the filter " + entry.getName() + " has the same trustlevel which is the same or lower than the trustlevel of the super component. A filter should have a higher trustlevel than the surrounding component.", node.get_SourcePositionStart());
					return factor;
				}
				
			}
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.trustlevel.DerivedTrustlevel#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.FilterWithHigherTrust;
	}

}
