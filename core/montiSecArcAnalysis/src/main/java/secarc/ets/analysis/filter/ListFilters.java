/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.filter;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcPort;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcFilter;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisFilterChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.FilterEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * L01: Lists all used filters
 * 
 * 
 * 
 */
public class ListFilters extends Analysis implements ISecAnalysisFilterChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ListFilters(int factor) {
		super(MontiSecArcAnalysisConstants.LIST_FILTER);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecFilterChecker#check(secarc._ast.ASTSecArcFilter, secarc.ets.entries.FilterEntry)
	 */
	public int check(ASTSecArcFilter node, FilterEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		if(node.get_Parent() instanceof ASTArcComponent) {
			addReport("Component filter: " + entry.getName(), node.get_SourcePositionStart());
			return factor;
		} else if(node.get_Parent() instanceof ASTArcPort) {
			addReport("Port filter: " + entry.getName(), node.get_SourcePositionStart());
			return factor;
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ListFilters;
	}

}
