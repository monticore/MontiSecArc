/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.filter;


import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisConnectorChecker;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecSubComponentEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;
import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

/**
 * F01: Checks the path to the filter
 * 
 * 
 * 
 */
public class TaintTracking extends Analysis implements ISecAnalysisConnectorChecker {
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public TaintTracking(int factor) {
		super(MontiSecArcAnalysisConstants.TAINT_TRACKING);
		this.factor = factor;
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisConnectorChecker#check(mc.umlp.arcd._ast.ASTArcConnector, secarc.ets.entries.SecConnectorEntry)
	 */
	@Override
	public int check(ASTArcConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		return innerCheck(node, node.getMainParent(), entry, graph);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisConnectorChecker#check(mc.umlp.arcd._ast.ASTArcSimpleConnector, secarc.ets.entries.SecConnectorEntry)
	 */
	@Override
	public int check(ASTArcSimpleConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		return innerCheck(node, node.getMainParent(), entry, graph);
	}
	
	protected int innerCheck(ASTNode node, ASTArcComponent nodeParent, SecConnectorEntry entry, ArchitectureGraph graph) throws AmbigousException {
		
		if(!checkForTrustlevelRelation(nodeParent, entry)) {
			return 0;
		}
		
		//Start check for filtering
		Vertex<ConnectorEntry> connectorVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getRawGraph(), connectorVertex);
		
		STEntry element = null;
			
		StringBuilder sBuilder = new StringBuilder("Taint tracking: The trustlevel becomes higher with the transition " + entry + ". Before the input is used, it must be filtered. The following pass is used: '" + entry);
		
		//Already used
		iterator.next();
		
		while(iterator.hasNext()) {
			element = iterator.next().getArchitectureElement();
			
			if(element.equals(entry)) {
				sBuilder.append(", '" + entry);
			}
			
			if(element instanceof ConnectorEntry) {
				sBuilder.append("' -> '" + element);
			} 
			
			if(element instanceof FilterEntry) {
				sBuilder.append(" " + element.getName() + "*");
			}
			
		}
		
		sBuilder.append("'. The filter is marked with a *. If the filter is missing, the input cannot be used.");
		
		addReport(sBuilder.toString(), node.get_SourcePositionStart());
		return factor;
	}
	
	/**
	 * Checks if the trustlevel becomes higher on the path
	 * 
	 * @param nodeParent
	 * @param entry
	 * @return
	 * @throws AmbigousException
	 */
	private boolean checkForTrustlevelRelation(ASTArcComponent nodeParent, SecConnectorEntry entry) throws AmbigousException {
		
		SecComponentEntry parentComp = (SecComponentEntry) resolver.resolve(nodeParent.getName(), ComponentEntry.KIND, getNameSpaceFor(nodeParent));
		
		// Component source
		String sourceRef = entry.getSource();
		SecSubComponentEntry sourceSubComp = null;
		SecComponentEntry sourceComp = null;
		// Find source component
		if (sourceRef.contains(".")) {
			String nameSourceComp = sourceRef.substring(0, sourceRef.indexOf("."));
			sourceSubComp = (SecSubComponentEntry) resolver.resolve(nameSourceComp, SubComponentEntry.KIND, getNameSpaceFor(nodeParent));
			// Check in a coco
			if (sourceSubComp != null) {
				sourceComp = (SecComponentEntry) sourceSubComp.getComponentType().getBestKnownVersion();
			}
		} else {
			sourceComp = parentComp;
		}
		
		if(sourceComp == null) {
			return false;
		}

		// Component target
		String targetRef = entry.getTarget();
		SecSubComponentEntry targetSubComp = null;
		SecComponentEntry targetComp = null;
		// Find traget component
		if (targetRef.contains(".")) {
			String nameTargetComp = targetRef.substring(0, targetRef.indexOf("."));
			targetSubComp = (SecSubComponentEntry) resolver.resolve(nameTargetComp, SubComponentEntry.KIND,
					getNameSpaceFor(nodeParent));
			// Check in a coco
			if (targetSubComp != null) {
				targetComp = (SecComponentEntry) targetSubComp.getComponentType().getBestKnownVersion();
			}
		} else {
			targetComp = parentComp;
		}
		
		int targetTrustlevel = -1;
		int sourceTrustlevel = -1;
		
		if(sourceComp.getTrustlevel().isPresent()) {
			sourceTrustlevel = CoCoHelper.getTrustlevelAsInteger(sourceComp);
		} else {
			SecComponentEntry parentTurstlevelSource = getTrustlevel((ASTArcComponent) sourceComp.getBestKnownVersion().getNode());
			if(parentTurstlevelSource != null) {
				sourceTrustlevel = CoCoHelper.getTrustlevelAsInteger(parentTurstlevelSource);
			}
		}
		
		if(targetComp == null) {
			return false;
		}
		
		if(targetComp.getTrustlevel().isPresent()) {
			targetTrustlevel = CoCoHelper.getTrustlevelAsInteger(targetComp);
		} else {
			SecComponentEntry parentTurstlevelTarget = getTrustlevel((ASTArcComponent) targetComp.getBestKnownVersion().getNode());
			if(parentTurstlevelTarget != null) {
				targetTrustlevel = CoCoHelper.getTrustlevelAsInteger(parentTurstlevelTarget);
			}
		}
		
		//If the trustlevel is the lower, the input do not need to be checked again
		//if the trustlevel is the same, then check it for the transition before
		if(sourceTrustlevel < targetTrustlevel) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Search for trustlevel in super components 
	 * @param node
	 * @return trustlevel
	 * @throws AmbigousException
	 */
	private SecComponentEntry getTrustlevel(ASTArcComponent node) throws AmbigousException {
		ASTArcComponent parent = node.getMainParent();
		if(parent != null) {
			SecComponentEntry componentParent = (SecComponentEntry) resolver.resolve(parent.getName(), ComponentEntry.KIND, getNameSpaceFor(parent));
			if(componentParent.getTrustlevel().isPresent()) {
				return componentParent;
			} else {
				return getTrustlevel(parent);
			}
		} else {
			return null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.TaintTracking;
	}

}
