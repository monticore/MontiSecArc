/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.filter;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import mc.umlp.common._ast.UMLPNode;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.ISecAnalysisConnectorChecker;
import secarc.ets.analysis.trustlevel.DerivedTrustlevel;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * E06: Checks if the trustlevel of the encrypted path is higher than the environment
 * 
 * - SSL/TLS Testing
 * 
 * 
 * 
 */
public class TrustlevelConnectorHigherThanEnvironment extends DerivedTrustlevel implements
		ISecAnalysisConnectorChecker {
 
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public TrustlevelConnectorHigherThanEnvironment(int factor) {
		super(MontiSecArcAnalysisConstants.TRUSTLEVEL_PATH_HIGHER_THAN_ENVIRONMENT);
		this.factor = factor;
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisConnectorChecker#check(mc.umlp.arcd._ast.ASTArcConnector, secarc.ets.entries.SecConnectorEntry)
	 */
	@Override
	public int check(ASTArcConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		return innerCheck(node, entry, node.getMainParent());
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisConnectorChecker#check(mc.umlp.arcd._ast.ASTArcSimpleConnector, secarc.ets.entries.SecConnectorEntry)
	 */
	@Override
	public int check(ASTArcSimpleConnector node, SecConnectorEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		return innerCheck(node, entry, node.getMainParent());
	}
	
	protected int innerCheck(UMLPNode node, SecConnectorEntry entry, ASTArcComponent component) throws AmbigousException {
		String refTarget = entry.getTarget();
		String refSource = entry.getSource();
		SubComponentEntry subComponentSource = null;
		SecComponentEntry componentSource = null;
		SubComponentEntry subComponentTarget = null;
		SecComponentEntry componentTarget = null;
		SecComponentEntry componentParent = null;
		
		
		if(refSource.contains(".")) {
			//Is in another namespace
			subComponentSource = (SubComponentEntry) resolver.resolve(refSource.substring(0, refSource.indexOf(".")), SubComponentEntry.KIND, getNameSpaceFor(component));
			//Checked in coco
			if(subComponentSource == null) {
				return 0;
			} else {
				componentSource = (SecComponentEntry) subComponentSource.getComponentType();
			}
		} else {
			//Is in the same namespace
			componentSource = (SecComponentEntry) resolver.resolve(component.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
		}
		
		if(refTarget.contains(".")) {
			//Is in another namespace
			subComponentTarget = (SubComponentEntry) resolver.resolve(refTarget.substring(0, refTarget.indexOf(".")), SubComponentEntry.KIND, getNameSpaceFor(component));
			//Checked in CoCo
			if(subComponentTarget == null) {
				return 0;
			} else {
				componentTarget = (SecComponentEntry) subComponentTarget.getComponentType();
			}
		} else {
			//Is in the same namespace
			componentTarget = (SecComponentEntry) resolver.resolve(component.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
		}
		
		if((refTarget.contains(".") && refSource.contains(".")) || component.getMainParent() == null) {
			//In the same namesparce
			componentParent = (SecComponentEntry) resolver.resolve(component.getName(), ComponentEntry.KIND, getNameSpaceFor(component));
		} else {
			//In a higher namespace
			componentParent = (SecComponentEntry) resolver.resolve(component.getMainParent().getName(), ComponentEntry.KIND, getNameSpaceFor(component.getMainParent()));
		}		
		
		//Is checked in another coco
		if(componentSource == null || componentTarget == null  || componentParent == null) {
			return 0;
		}
		
		int trustlevelSource = -2;
		boolean trustlevelSourceMissing = false;
		int trustlevelTarget = -2;
		boolean trustlevelTargetMissing = false;
		int trustlevelParent = -2;
		
		//Trustlevel source
		if(componentSource.getTrustlevel().isPresent()) {
			trustlevelSource = CoCoHelper.getTrustlevelAsInteger(componentSource);
		} else {
			//Gets trustlevel of the super component (derived)
			trustlevelSourceMissing = true;
		}
		
		//Trustlevel target
		if(componentTarget.getTrustlevel().isPresent()) {
			trustlevelTarget = CoCoHelper.getTrustlevelAsInteger(componentTarget);
		} else {
			//Gets trustlevel of the super component (derived)
			trustlevelTargetMissing = true;
		}
		
		//Source and Target do not have a trustlevel and therefore they get the one from the parent
		if(trustlevelSourceMissing && trustlevelTargetMissing) {
			return 0;
		}
		
		//Trustlevel parent
		if(componentParent.getTrustlevel().isPresent()) {
			trustlevelParent = CoCoHelper.getTrustlevelAsInteger(componentParent);
		} else {
			//Look for trustlevel in super components
			//Trustlevel of supercomponent
			componentParent = getDerivedTrustlevel((ASTArcComponent) componentParent.getBestKnownVersion().getNode());
			//If no trustlevel in supercomponent exists, take the default trustlevel of the environment
			if(componentParent.getTrustlevel().get() == null) {
				trustlevelParent = -1;
			} else {
				trustlevelParent = CoCoHelper.getTrustlevelAsInteger(componentParent);
			}
		}
		
		if(trustlevelSourceMissing) {
			trustlevelSource = trustlevelParent;
		} else if(trustlevelTargetMissing){
			trustlevelTarget = trustlevelParent;
		}
		
		//Compare Trustlevel
		if(trustlevelSource < trustlevelParent && trustlevelTarget < trustlevelParent) {
			addReport("The trustlevel of the source and target component of connector " + entry + " is lower than the trustlevel of the environment.", node.get_SourcePositionStart());
			return factor;
		} else if(trustlevelSource >= trustlevelParent && trustlevelTarget < trustlevelParent) {
			addReport("The trustlevel of the target component of connector " + entry + " is lower than the trustlevel of the environment.", node.get_SourcePositionStart());
			return factor;
		} else if(trustlevelSource < trustlevelParent && trustlevelTarget >= trustlevelParent) {
			addReport("The trustlevel of the source component of connector " + entry + " is lower than the trustlevel of the environment.", node.get_SourcePositionStart());
			return factor;
		}
		return 0;
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.TrustlevelPathHigherThanEnvironment;
	}

}
