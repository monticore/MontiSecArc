/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.identity;

import java.util.List;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.ISecAnalysisIdentityChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.cocos.common.ComponentExistence;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

/**
 * I01: Checks if a encrypted connection exists between the components of the identity link
 * 
 * 
 * 
 */
public class IdentityWithEncryption extends ComponentExistence  implements ISecAnalysisIdentityChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public IdentityWithEncryption(int factor) {
		super(MontiSecArcAnalysisConstants.IDENTITY_WITH_ENCRYPTION);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.common.ComponentExistence#check(secarc._ast.ASTSecArcIdentity, secarc.ets.entries.IdentityEntry, secarc.ets.graph.ArchitectureGraph)
	 */
	@Override
	public int check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graphConnectorEdges, ArchitectureGraph graphIdentityEdges, boolean advanced)
			throws AmbigousException {
		
		SubComponentEntry targetSubComponentEntry = innerCheck(node, node.getMainParent(), entry.getTarget());
		
		SubComponentEntry sourceSubComponentEntry = innerCheck(node, node.getMainParent(), entry.getSource());
		//Is check in a coco
		if(targetSubComponentEntry == null || sourceSubComponentEntry == null) {
			return 0;
		}
		
		SecComponentEntry targetComponentEntry = (SecComponentEntry) targetSubComponentEntry.getComponentType().getBestKnownVersion();
		
		SecComponentEntry sourceComponentEntry = (SecComponentEntry) sourceSubComponentEntry.getComponentType().getBestKnownVersion();
		
		List<PortEntry> incomingPorts = targetComponentEntry.getIncomingPorts();
		
		Vertex<PortEntry> portVertex = null;
		
		STEntry element = null;
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator;
		
		boolean encrypted = true;
		
		boolean pathfound = false;
		
		//Search for every port from target component
		//at least one connection must be encrypted
		for(PortEntry port : incomingPorts) {
			
			portVertex = Vertex.of(port);
			
			iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graphConnectorEdges.getReversedRawGraph(), portVertex);
			
			//First element is not needed
			element = iterator.next().getArchitectureElement();
			
			//Search for encrypted path
			while(iterator.hasNext()) {
				element = iterator.next().getArchitectureElement();
				
				//Search for source component of the identity link
				if((element instanceof ComponentEntry &&
						((ComponentEntry) element).equals(sourceComponentEntry) &&
						encrypted) ||
						(!iterator.hasNext() &&
								encrypted)) {
					pathfound = true;
					//There is an encrypted path, break
					break;
				}
				
				//Check if the connection is encrypted
				if(element instanceof ConnectorEntry) {
					if(((SecConnectorEntry) element).isUnencrypted()) {
						encrypted = false;
					}
				}
				
				//New path
				if(element.equals(port)) {
					encrypted = true;
				}
			}
			
			//There is an encrypted path, break
			if(pathfound) {
				break;
			}
			
		}
		
		if(!pathfound) {
			addReport("There is an identity link between the components " + entry.getSource() + " and " + entry.getTarget() + ", but an encrypted connection is missing.", node.get_SourcePositionStart());
			return factor;
		}
		return 0;
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.common.ComponentExistence#check(secarc._ast.ASTSecArcTrustlevelRelation, secarc.ets.entries.TrustlevelRelationEntry)
	 */
	@Override
	public void check(ASTSecArcTrustlevelRelation node,
			TrustlevelRelationEntry entry) throws AmbigousException {
		
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.common.ComponentExistence#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.IdentityWithEncryption;
	}

}
