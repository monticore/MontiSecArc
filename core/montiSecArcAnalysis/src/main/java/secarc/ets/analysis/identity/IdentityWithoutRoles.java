/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.identity;

import java.util.List;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import secarc._ast.ASTSecArcIdentity;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisIdentityChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

/**
 * I03: Checks if in the target component roles exist
 * 
 * 
 * 
 */
public class IdentityWithoutRoles extends Analysis implements
		ISecAnalysisIdentityChecker {
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public IdentityWithoutRoles(int factor) {
		super(MontiSecArcAnalysisConstants.IDNETITY_WITHOUT_ROLES);
		this.factor = factor;
	}

	@Override
	public int check(ASTSecArcIdentity node, IdentityEntry entry,
			ArchitectureGraph graphConnectorEdges, ArchitectureGraph graphIdentityEdges, boolean advanced) throws AmbigousException {

		Vertex<IdentityEntry> identityVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graphIdentityEdges.getRawGraph(), identityVertex);
		
		STEntry element = null;
		
		//first element is not needed
		iterator.next();
		
		if(iterator.hasNext()) {
			element = iterator.next().getArchitectureElement();
		} 
		
		//Error in graph
		if(!(element instanceof ComponentEntry)) {
			return 0;
		}
		
		List<RoleEntry> roles = ((SecComponentEntry) element).getRoles();
		
		if(roles == null || roles.isEmpty()) {
			addReport("The identity link " + entry.toString() + " references the component " + element.getName() + ", but there are no roles defined which have to be authenticated.", node.get_SourcePositionStart());
			return factor;
		}
		
		return 0;
		
	}

	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.IdentityWithoutRoles;
	}

}
