/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.identity;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.ISecAnalysisIdentityChecker;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.cocos.common.ComponentExistence;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * I02: Check if the trustlevel for the client is lower then the trustlevel for the server
 * 
 * 
 * 
 */
public class TrustlevelClientServer extends ComponentExistence implements ISecAnalysisIdentityChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public TrustlevelClientServer(int factor) {
		super(MontiSecArcAnalysisConstants.TRUSTLEVEL_CLIENT_SERVER_IDENTITY);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecIdentityChecker#check(secarc._ast.ASTSecArcIdentity, secarc.ets.entries.IdentityEntry)
	 */
	@Override
	public int check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graphConnectorEdges, ArchitectureGraph graphIdentityEdges, boolean advanced) throws AmbigousException {
		String refSource = entry.getSource();
		String refTarget = entry.getTarget();
		int trustlevelSource = -1;
		int trustlevelTarget = -1;

		SubComponentEntry sourceSubComponent = innerCheck(node, node.getMainParent(), entry.getSource());
		
		SubComponentEntry targetSubComponent = innerCheck(node, node.getMainParent(), entry.getTarget());

		//Existence is checked in other coco
		if(sourceSubComponent == null || targetSubComponent == null) {
			return 0;
		}

		SecComponentEntry sourceComp = (SecComponentEntry) sourceSubComponent.getComponentType().getBestKnownVersion();
		
		SecComponentEntry targetComp = (SecComponentEntry) targetSubComponent.getComponentType().getBestKnownVersion();
		
		if(!sourceComp.getTrustlevel().isPresent()) {
			sourceComp = getTrustlevel((ASTArcComponent) sourceComp.getNode());
		}
		
		if(!targetComp.getTrustlevel().isPresent()) {
			targetComp = getTrustlevel((ASTArcComponent) targetComp.getNode());
		}
		
		if(sourceComp != null) {
			trustlevelSource = CoCoHelper.getTrustlevelAsInteger(sourceComp);
		}
		
		if(targetComp != null) {
			trustlevelTarget = CoCoHelper.getTrustlevelAsInteger(targetComp);
		}
		
		//Is the Trustelevel of the target higher then the trustlevel of the source
		if(trustlevelSource >= trustlevelTarget) {
			addReport("The trustlevel of the server " + refTarget + " should be higher than the trustlevel of the client " + refSource + ".", node.get_SourcePositionStart());
			return factor;
		}
		
		return 0;
				
	}
	
	/**
	 * Search for trustlevel in super components 
	 * @param node
	 * @return trustlevel
	 * @throws AmbigousException
	 */
	private SecComponentEntry getTrustlevel(ASTArcComponent node) throws AmbigousException {
		ASTArcComponent parent = node.getMainParent();
		if(parent != null) {
			SecComponentEntry componentParent = (SecComponentEntry) resolver.resolve(parent.getName(), ComponentEntry.KIND, getNameSpaceFor(parent));
			if(componentParent.getTrustlevel().isPresent()) {
				return componentParent;
			} else {
				return getTrustlevel(parent);
			}
		} else {
			return null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker#check(secarc._ast.ASTSecArcTrustlevelRelation, secarc.ets.entries.TrustlevelRelationEntry)
	 */
	@Override
	public void check(ASTSecArcTrustlevelRelation node,
			TrustlevelRelationEntry entry) throws AmbigousException {
		
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.TrustlevelClientServerIdentity;
	}

}
