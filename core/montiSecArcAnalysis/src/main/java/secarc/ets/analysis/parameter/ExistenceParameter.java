/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.parameter;

import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisParameterChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;

/**
 * Check if the referenced components are exist
 * 
 * 
 * 
 */
public class ExistenceParameter extends Analysis implements
		ISecAnalysisParameterChecker {

	public ExistenceParameter() {
		super(MontiSecArcAnalysisConstants.REFERENCED_COMPONENT_WHATIF_DO_NOT_EXIST);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisParameterChecker#check(mc.umlp.arcd._ast.ASTMCCompilationUnit, java.lang.String)
	 */
	@Override
	public void check(ASTMCCompilationUnit node, String parameter) throws AmbigousException {

		String workingParameter = parameter;
		if(workingParameter.contains("*")) {
			workingParameter = workingParameter.substring(0, workingParameter.lastIndexOf("."));
		}
		
		ComponentEntry entry = existComponent(workingParameter, getNameSpaceFor(node));
		
		if(entry == null) {
			addReport("The referenced component " + workingParameter + " does not exist.", node.get_SourcePositionStart());
		} else {
			MontiSecArcAnalysisConstants.EXISTING_PARAMETER.add(workingParameter);
			if(parameter.contains("*")) {
				addSubComponents(entry);
			}
		}
		
	}
	
	protected ComponentEntry existComponent(String parameter, NameSpace np) throws AmbigousException {
		ComponentEntry entry = (ComponentEntry) resolver.resolve(parameter, ComponentEntry.KIND, np);
		if(entry != null) {
			return entry;
		} else {
			for(NameSpace npChild : np.getChildren()) {
				entry = existComponent(parameter, npChild);
				if(entry != null) {
					return entry;
				}
			}
		}
		return null;
	}
	
	protected void addSubComponents(ComponentEntry entry) {
		for(SubComponentEntry subComponentEntry : entry.getSubComponents()) {
			MontiSecArcAnalysisConstants.EXISTING_PARAMETER.add(subComponentEntry.getComponentType().getName());
			addSubComponents(subComponentEntry.getComponentType());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ReferencedComponentWhatIfDoNotExist;
	}

}
