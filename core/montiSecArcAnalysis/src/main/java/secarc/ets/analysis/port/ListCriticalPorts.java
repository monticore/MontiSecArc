/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.port;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * L02: Lists all critical ports of the system
 * 
 * 
 * 
 */
public class ListCriticalPorts extends Analysis implements
		ISecAnalysisPortChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ListCriticalPorts(int factor) {
		super(MontiSecArcAnalysisConstants.LIST_CRITICAL_PORTS);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisPortChecker#check(mc.umlp.arcd._ast.ASTArcPort, secarc.ets.entries.SecPortEntry, secarc.ets.graph.ArchitectureGraph)
	 */
	@Override
	public int check(ASTArcPort node, SecPortEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		
		if(entry.isCritical()) {
			addReport("The port " + entry.getName() + " is critical.", node.get_SourcePositionEnd());
			return factor;
		}
		
		return 0;
		
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ListAllCriticalPorts;
	}

}
