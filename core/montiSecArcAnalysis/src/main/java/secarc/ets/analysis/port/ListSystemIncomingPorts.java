/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.port;

import java.util.List;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.PortEntry;
import interfaces2.resolvers.AmbigousException;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;

/**
 * L03: Lists all incoming ports of the system
 * - Incoming ports are the entry ports of the system. A potential
 * 	 attacker can collect information.
 * - Information gathering is prevented 
 * 
 * 
 * 
 */
public class ListSystemIncomingPorts extends Analysis implements
		ISecAnalysisComponentChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ListSystemIncomingPorts(int factor) {
		super(MontiSecArcAnalysisConstants.LIST_INCOMING_PORTS);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		int count = 0;
		List<PortEntry> incomingPorts = entry.getIncomingPorts();
		if(!entry.isInnerComponent() && incomingPorts != null && !incomingPorts.isEmpty()) {
			for(PortEntry entryPort : incomingPorts) {
				if(entryPort.isIncoming()) {
					addReport("The port " + entryPort.getName() + " with the type " + entryPort.getTypeReference().getName()  + " is an incoming port of the system. Please check if the entry port is necessary.", entryPort.getNode().get_SourcePositionStart());
					count += factor;
				}
			}
		}
		return count;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ListIncomingtPorts;
	}

}
