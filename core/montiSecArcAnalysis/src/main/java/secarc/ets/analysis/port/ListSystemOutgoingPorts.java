/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.port;

import java.util.List;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.PortEntry;
import interfaces2.resolvers.AmbigousException;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;

/**
 * L04: Lists all outgoing ports of the system
 * - Outgoing ports are the interfaces which send information to a potential
 * 	 attacker 
 * - Information gathering is prevented 
 * 
 * 
 * 
 */
public class ListSystemOutgoingPorts extends Analysis implements
		ISecAnalysisComponentChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ListSystemOutgoingPorts(int factor) {
		super(MontiSecArcAnalysisConstants.LIST_OUTGOING_PORTS);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		int count = 0;
		List<PortEntry> outgoingPorts = entry.getOutgoingPorts();
		if(!entry.isInnerComponent() && outgoingPorts != null && !outgoingPorts.isEmpty()) {
			for(PortEntry entryPort : outgoingPorts) {
				if(entryPort.isOutgoing()) {
					addReport("The port " + entryPort.getName() + " with the type " + entryPort.getTypeReference().getName()  + " is an outgoing port of the system. Please check if the user needs this output.", entryPort.getNode().get_SourcePositionStart());
					count += factor;
				}
			}
		}
		return count;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ListOutgoingPorts;
	}

}
