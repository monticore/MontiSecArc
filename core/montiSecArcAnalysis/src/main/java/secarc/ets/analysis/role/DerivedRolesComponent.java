/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.role;

import java.util.ArrayList;
import java.util.List;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecPortEntry;

/**
 * D01: Lists all roles for a component
 * - Testing for privilege escalation
 * 
 * 
 * 
 */
public class DerivedRolesComponent extends Analysis implements
		ISecAnalysisComponentChecker {
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	/**
	 * Roles of component
	 */
	List<String> roleEntries;

	public DerivedRolesComponent(int factor) {
		super(MontiSecArcAnalysisConstants.DERIVED_ROLES_COMPONENT);
		this.factor = factor;
	}

	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		
		//Check if component is not a 3rd party component
		if(entry.getConfiguration() != null || entry.getCPE() != null) {
			return 0;
		}
		
		roleEntries = new ArrayList<String>();
		
		addSubcomponentRoles(entry);
		
		addPortRoles(entry);
		
		if(roleEntries.size() > 0) {
			StringBuilder sBuilder = new StringBuilder("The component " + NameHelper.getSimplenameFromComplexname(entry.getName()) + " has the following roles: " + roleEntries.get(0));
			roleEntries.remove(0);
			
			for(String name : roleEntries) {
				sBuilder.append(", ");
				sBuilder.append(name);
			}
			if(advanced) {
				addReport(sBuilder.toString(), node.get_SourcePositionStart());
			} else {
				addReport(sBuilder.append(". The roles of the current component and its sub components are considered.").toString(), node.get_SourcePositionStart());
			}
			return factor;
		}
		return 0;
	}
	
	public void addSubcomponentRoles(SecComponentEntry entry) {
		if(entry.getRoles() != null) {
			for(RoleEntry role : entry.getRoles()) {
				if(!roleEntries.contains(role.getName())) {
					roleEntries.add(role.getName());
				}
			}
		}
		
		for(SubComponentEntry subComponentEntry : entry.getSubComponents()) {
			addSubcomponentRoles((SecComponentEntry) subComponentEntry.getComponentType().getBestKnownVersion());
		}
	}
	
	private void addPortRoles(SecComponentEntry entry) {
		List<PortEntry> ports = entry.getAllPorts(loader, deserializers);
		if(ports != null) {
			for(PortEntry port : ports) {
				if(((SecPortEntry) port).getRoles() != null) {
					for(RoleEntry role : ((SecPortEntry) port).getRoles()) {
						if(!roleEntries.contains(role.getName())) {
							roleEntries.add(role.getName());
						}
					}
				}
			}
		}
		
	}

	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.DerivedRolesComponent;
	}

}
