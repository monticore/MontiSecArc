/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.role;

import java.util.ArrayList;
import java.util.List;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * D02: Lists all roles for a port
 * - Testing for privilege escalation
 * 
 * 
 * 
 */
public class DerivedRolesPort extends Analysis implements
		ISecAnalysisPortChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	/**
	 * Roles of component
	 */
	List<String> roleEntries;
	
	public DerivedRolesPort(int factor) {
		super(MontiSecArcAnalysisConstants.DERIVE_ROLES_PORT);
		this.factor = factor;
	}

	@Override
	public int check(ASTArcPort node, SecPortEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		roleEntries = new ArrayList<String>();
		
		SecComponentEntry componentEntry = (SecComponentEntry) resolver.resolve(node.getMainParent().getName(), ComponentEntry.KIND, getNameSpaceFor(node));
		
		//Add all roles of the component 
		for(RoleEntry role : ((SecPortEntry) entry).getRoles()) {
			roleEntries.add(role.getName());
		}
		
		//If nothing is specified for the port, roles of the the component are used
		if(roleEntries.isEmpty()) {
			addSubcomponentRoles(componentEntry);
			
			addPortRoles(componentEntry);
		}
		
		StringBuilder sBuilder = new StringBuilder("The port " + entry.getName() + " has the following roles: ");
		
		if(roleEntries.size() > 0) {
			sBuilder.append(roleEntries.get(0));
			roleEntries.remove(0);
		} else {
			return 0;
		}
		
		for(String name : roleEntries) {
			sBuilder.append(", ");
			sBuilder.append(name);
		}
		
		addReport(sBuilder.toString(), node.get_SourcePositionStart());
		
		return factor;
		
	}
	
	public void addSubcomponentRoles(SecComponentEntry entry) {
		if(entry.getRoles() != null) {
			for(RoleEntry role : entry.getRoles()) {
				if(!roleEntries.contains(role.getName())) {
					roleEntries.add(role.getName());
				}
			}
		}
		
		for(SubComponentEntry subComponentEntry : entry.getSubComponents()) {
			addSubcomponentRoles((SecComponentEntry) subComponentEntry.getComponentType().getBestKnownVersion());
		}
	}
	
	private void addPortRoles(SecComponentEntry entry) {
		List<PortEntry> ports = entry.getAllPorts(loader, deserializers);
		if(ports != null) {
			for(PortEntry port : ports) {
				if(((SecPortEntry) port).getRoles() != null) {
					for(RoleEntry role : ((SecPortEntry) port).getRoles()) {
						if(!roleEntries.contains(role.getName())) {
							roleEntries.add(role.getName());
						}
					}
				}
			}
		}
		
	}

	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.DerivedRolesPort;
	}

}
