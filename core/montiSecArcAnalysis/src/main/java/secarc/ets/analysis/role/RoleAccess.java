/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.role;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jgrapht.alg.DirectedNeighborIndex;
import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import interfaces2.STEntry;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.ast.ASTNode;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisRoleChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

/**
 * WI01: Lists role access
 * - Testing for privilege escalation
 * 
 * 
 * 
 */
public class RoleAccess extends Analysis implements ISecAnalysisRoleChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public RoleAccess(int factor) {
		super(MontiSecArcAnalysisConstants.ROLE_ACCESS);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisRoleChecker#check(secarc._ast.ASTSecArcRole, secarc.ets.entries.RoleEntry)
	 */
	@Override
	public int check(ASTSecArcRole node, RoleEntry entry, ArchitectureGraph graph, boolean advanced)
			throws AmbigousException {
		return innerCheck(node, graph, entry);
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisRoleChecker#check(secarc._ast.ASTSecArcRefRole, secarc.ets.entries.RoleEntry, secarc.ets.graph.ArchitectureGraph, boolean)
	 */
	@Override
	public int check(ASTSecArcRefRole node, RoleEntry entry,
			ArchitectureGraph graph, boolean advanced) throws AmbigousException {
		return innerCheck(node, graph, entry);
	}
	
	protected int innerCheck(ASTNode node, ArchitectureGraph graph, RoleEntry entry) {
		//Get all roles with the same name
		Set<Vertex<? extends STEntry>> vertexSet = graph.getRawGraph().vertexSet();
				
		Iterator<Vertex<? extends STEntry>> it = vertexSet.iterator();
		STEntry element = null;
				
		//Result set of role with the given name of the entry
		List<RoleEntry> resultSet = new ArrayList<RoleEntry>();
				
		while(it.hasNext()) {
			element = it.next().getArchitectureElement();
		 	if(element instanceof RoleEntry && element.getName().equals(entry.getName())) {
				resultSet.add((RoleEntry) element);
			}
		}
				
		DirectedNeighborIndex<Vertex<? extends STEntry>, Edge> directedNeighborIndex = new DirectedNeighborIndex<Vertex<? extends STEntry>, Edge>(graph.getRawGraph());
				
				
		List<Vertex<? extends STEntry>> allPredescessor = new ArrayList<Vertex<? extends STEntry>>();
				
		for(RoleEntry role : resultSet) {
			allPredescessor.addAll(directedNeighborIndex.predecessorListOf(Vertex.of(role)));
		}
		
		//If a role has access to a port, it has also access to the component behind the port
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = null;
		List<ComponentEntry> components = new ArrayList<ComponentEntry>();
		for(Vertex<? extends STEntry> vertex : allPredescessor) {
			if(!(vertex.getArchitectureElement() instanceof PortEntry)) {
				continue;
			}
			
			iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getRawGraph(), vertex);
			while(iterator.hasNext()) {
				element = iterator.next().getArchitectureElement();
				if(element instanceof SecComponentEntry) {
					components.add((ComponentEntry) element);
					break;
				}
			}
		}
				
		StringBuilder sBuilder = new StringBuilder("The role " + entry.getName() + " has access to the following ports and components: ");
				
		if(!allPredescessor.isEmpty()) {
			sBuilder.append(allPredescessor.get(0).getArchitectureElement().getName());
			allPredescessor.remove(0);
		}
				
		for(Vertex<? extends STEntry> vertex : allPredescessor) {
			sBuilder.append(", "); 
			sBuilder.append(vertex.getArchitectureElement().getName());
		}
		
		for(STEntry component : components) {
			sBuilder.append(", "); 
			sBuilder.append(component.getName());
		}
			
		addReport(sBuilder.toString(), node.get_SourcePositionStart());
				
		return factor;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.RoleAccess;
	}

}
