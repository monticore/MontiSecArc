/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.trustlevel;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;

/**
 * D04: Derives trustlevel for components without trustlevel
 * 
 * 
 * 
 */
public class DerivedTrustlevel extends Analysis implements
		ISecAnalysisComponentChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public DerivedTrustlevel(int factor) {
		super(MontiSecArcAnalysisConstants.DERIVED_TRUSTLEVEL);
		this.factor = factor;
	}
	
	public DerivedTrustlevel(String checkedProperty) {
		super(checkedProperty);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.analysis.checker.ISecAnalysisComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced)
			throws AmbigousException {
		if(entry.getTrustlevel().isPresent()) {
			return 0;
		}
		
		SecComponentEntry trustlevelEntry = getDerivedTrustlevel(node);
		String trustlevel = CoCoHelper.getTrustlevelAsString(trustlevelEntry);
		
		addReport("The component " + entry.getName() + " has the derived trustlevel " + trustlevel, node.get_SourcePositionStart());
		return factor;
	}
	
	/**
	 * Search for trustlevel in super components 
	 * @param node
	 * @return trustlevel
	 * @throws AmbigousException
	 */
	protected SecComponentEntry getDerivedTrustlevel(ASTArcComponent node) throws AmbigousException {
		ASTArcComponent parent = node.getMainParent();
		if(parent != null) {
			SecComponentEntry componentParent = (SecComponentEntry) resolver.resolve(parent.getName(), ComponentEntry.KIND, getNameSpaceFor(parent));
			if(componentParent.getTrustlevel().isPresent()) {
				return componentParent;
			} else {
				return getDerivedTrustlevel(parent);
			}
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.DerivedTrustlevel;
	}

}
