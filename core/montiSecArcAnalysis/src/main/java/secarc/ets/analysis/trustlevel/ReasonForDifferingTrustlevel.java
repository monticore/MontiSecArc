/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.trustlevel;

import java.util.List;

import com.google.common.base.Optional;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.Analysis;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelEntry;

/**
 * T01: Checks if the trustlevel differs more then 2 from the supercomponent. Than 
 * a reason is expected
 * 
 * 
 * 
 */
public class ReasonForDifferingTrustlevel extends Analysis implements
ISecAnalysisComponentChecker{
	
	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public ReasonForDifferingTrustlevel(int factor) {
		super(MontiSecArcAnalysisConstants.REASON_DIFFERING_TRUSTLEVEL);
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public int check(ASTArcComponent node, SecComponentEntry entry, boolean advanced) {
		//Check trustlevel for all innercomponents
		List<SubComponentEntry> subComponents = entry.getSubComponents();
		List<ComponentEntry> innerComponents = entry.getInnerComponents();
		Optional<TrustlevelEntry> trustlevelEntry = entry.getTrustlevel();
		
		//if no trustlevel and no innercomponents, nothing to check
		if((subComponents != null || innerComponents != null) && trustlevelEntry.isPresent()) {
			
			int valueTrustlevel = trustlevelEntry.get().getValue();
			int innerTrustlevel = -2;
			
			if(trustlevelEntry.get().isNegative()) {
				valueTrustlevel *= -1;
			}
			
			for(SubComponentEntry innerComponent : subComponents) {
				innerComponents.add(innerComponent.getComponentType());
			}
			
			//Check difference for every innercomponent
			for(ComponentEntry innerComponent : innerComponents) {
				
				Optional<TrustlevelEntry> innerTrustlevelEntry = ((SecComponentEntry) innerComponent).getTrustlevel();
				
				String message = "The trustlevel of the component " + innerComponent.getName() + " differs more than 2 from its super component and a reason is missing.";
				
				//if no trustlevel, nothing to check 
				//Is checked in TrustlevelForComponents
				if(innerTrustlevelEntry.isPresent()) {
					
					innerTrustlevel = innerTrustlevelEntry.get().getValue();
					if(innerTrustlevelEntry.get().isNegative()) {
						innerTrustlevel *= -1;
					}
					
					//If both are positive or negative
					if((valueTrustlevel>=0 && innerTrustlevel>=0) || (valueTrustlevel<0 && innerTrustlevel<0)) {
						
						if(Math.abs(valueTrustlevel - innerTrustlevel) > 2 && innerTrustlevelEntry.get().getReason() == null) {
							if(innerComponent.getNode() != null) {
								addReport(message, innerComponent.getNode().get_SourcePositionStart());
							} else {
								addReport(message, node.get_SourcePositionStart());
							}
							return factor;
						}
						
					} else {
						
						if((Math.abs(valueTrustlevel) + Math.abs(innerTrustlevel)) > 2 && innerTrustlevelEntry.get().getReason() == null) {
							if(innerComponent.getNode() != null) {
								addReport(message, innerComponent.getNode().get_SourcePositionStart());
							} else {
								addReport(message, innerComponent.getNode().get_SourcePositionStart());
							}
							return factor;
						}
						
					}
				}
			}
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.ReasonDifferingTrustlevel;
	}

}
