/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.analysis.trustlevelrelation;

import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.analysis.checker.ISecAnalysisTrustlevelRelationChecker;
import secarc.ets.check.MontiSecArcAnalysisConstants;
import secarc.ets.cocos.common.ComponentExistence;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * T02: Checks if for the referenced components a trustlevel exist
 * 
 * 
 * 
 */
public class TrustlevelForTrustlevelRelation extends ComponentExistence implements ISecAnalysisTrustlevelRelationChecker {

	/**
	 * Factor rating of analysis
	 */
	private int factor = 1;

	public TrustlevelForTrustlevelRelation(int factor) {
		super(MontiSecArcAnalysisConstants.TRUSTLEVEL_FOR_TRUSTLEVELRELATION);
		this.factor = factor;
	}
	
	@Override
	public void check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graph)
			throws AmbigousException {
		
	}

	@Override
	public int check(ASTSecArcTrustlevelRelation node,
			TrustlevelRelationEntry entry, boolean advanced) throws AmbigousException {
		
		String refLowerTrustlevelComponent = entry.getComponentWithLowerTrustlevel();
		String refHigherTrustlevelComponent = entry.getComponentWithHigherTrustlevel();
		
		SubComponentEntry lowerTrustlevelComoponent = innerCheck(node, node.getMainParent(), refLowerTrustlevelComponent);
		
		SubComponentEntry higherTrustlevelComoponent = innerCheck(node, node.getMainParent(), refHigherTrustlevelComponent);
		
		//Checked in coco
		if(lowerTrustlevelComoponent == null || higherTrustlevelComoponent == null) {
			return 0;
		}
		
		if(!((SecComponentEntry) lowerTrustlevelComoponent.getComponentType()).getTrustlevel().isPresent() || !((SecComponentEntry) higherTrustlevelComoponent.getComponentType()).getTrustlevel().isPresent()) {
			addReport("A trustlevelrelation is defined between the components " + lowerTrustlevelComoponent.getName() + " and " + higherTrustlevelComoponent.getName() + ". Therefore a trustlevel should be defined for both.", node.get_SourcePositionStart());
			return factor;
		}
		return 0;
	}

	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcAnalysisErrorCodes.TrustlevelForTrustlevelrelation;
	}

}
