/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Analysis utility class.
 *
 *
 */
public final class MontiSecArcAnalysisConstants {

	private MontiSecArcAnalysisConstants() {
		
	}
	
	//Description for analysis
	public static final String ALL_ANALYSES = "Checks all Analysis of MontiSecArc.";
	
	public static final String ALL_ENCRYPTED_CONNECT = "Checks all analysis related to encrypted connections.";
	
	public static final String ENCRYPTED_PATH_WITH_UNENCRYPTED_PART = "Checks if a encrypted path as a part which is unencrypted.";
	
	public static final String ENRYPTED_PATH_END_IN_LOW_TRUSTLEVEL = "Checks if a encrypted path ends in a low trustlevel.";
	
	public static final String UNENCRYPTED_CONNECTOR_THROUGH_LOW_TRUSTLEVEL = "Checks if an unencrypted connector is embedded in a component with a low trustlevel.";
	
	public static final String ENCRYPTED_PATH_CONTINUES_UNENCRYPTED = "Checks if an encrypted path continues unencrypted and lists which components can read the data.";
	
	public static final String TRUSTLEVEL_PATH_HIGHER_THAN_ENVIRONMENT = "Checks if the trustlevels of an encrypted path should be higher than the trustlevel of the environment.";
	
	public static final String ALL_FILTER_ANALYSIS = "Checks all analysis related to filters.";
	
	public static final String TAINT_TRACKING = "Checks if a input is filter before it is used.";
	
	public static final String LIST_FILTER = "Lists all filters.";
	
	public static final String FILTER_WITH_HIGHER_TRUST = "Checks if the filter has a higher trustlevel than the super component.";
	
	public static final String AVOID_INPUT_STRING = "Checks if a input port has the type String.";
	
	public static final String ALL_PORT_ANALYSIS = "Checks all analysis related to ports.";
	
	public static final String LIST_INCOMING_PORTS = "Lists all incoming ports of the system.";
	
	public static final String LIST_OUTGOING_PORTS = "Lists all outgoing ports of the system.";
	
	public static final String LIST_CRITICAL_PORTS = "Lists all critical ports of the system.";
	
	public static final String ALL_CONFIGURATION_ANALYSIS = "Checks all analysis related to configuration.";
	
	public static final String REVIEWED_CONFIGURATION = "Checks if a configuration is reviewed.";
	
	public static final String ALL_ROLE_ANALYSIS = "Checks all analysis related to roles.";
	
	public static final String DERIVED_ROLES_THIRD_PARTY = "Derives all roles for a 3rd party component from the ports.";
	
	public static final String DERIVED_ROLES_COMPONENT = "Derives all roles for components.";
	
	public static final String DERIVE_ROLES_PORT = "Derives all roles for ports.";
	
	public static final String ROLE_ACCESS = "Lists access for roles";
	
	public static final String ALL_IDENTITY = "Checks all analysis related to identity links.";
	
	public static final String IDNETITY_WITHOUT_ROLES = "Checks if the target component defines roles.";
	
	public static final String TRUSTLEVEL_CLIENT_SERVER_IDENTITY = "Checks if the trustlevel of a server is higher then the trustlevel of a client.";
	
	public static final String IDENTITY_WITH_ENCRYPTION = "Checks if the communication between two components is encrypted when an identity link is used.";
	
	public static final String ALL_TRUSTLEVEL = "Checks all analysis related to trustlevel.";

	public static final String REASON_DIFFERING_TRUSTLEVEL = "Checks if the trustlevel differs more then 2 from expected level.";
	
	public static final String DERIVED_TRUSTLEVEL = "Drives turstlevel for components which do not have a trustlevel.";
	
	public static final String ALL_COMPONENT = "Checks all analysis related to components";
	
	public static final String LIST_THIRD_PARTY_COMPONENTS = "Lists all third party components of the system.";
	
	public static final String ALL_TRUSTLEVEL_RELATION = "Checks all analysis related to trustlevel relation.";
	
	public static final String TRUSTLEVEL_FOR_TRUSTLEVELRELATION = "Checks if a trustlevel exists for a trustlevel relation.";
	
	public static final String ALL_PARAMETER = "Checks all analysis related to analysis parameter.";
	
	public static final String REFERENCED_COMPONENT_WHATIF_DO_NOT_EXIST = "Checks if the referenced components exists for the wath if analysis.";
	
	//names for analysis
	public static final String L01 = "L01-List-Filters";
	
	public static final String L02 = "L02-List-Critical-Ports";
	
	public static final String L03 = "L03-List-Incoming-Ports";
	
	public static final String L04 = "L04-List-Outgoing-Ports";
	
	public static final String L05 = "L05-List-Ports-String";
	
	public static final String L06 = "L06-List-Third-Party-Components";
	
	public static final String L07 = "L07-List-Not-Reviewed-Configuration";
	
	public static final String D01 = "D01-Derive-Role-Component";
	
	public static final String D02 = "D02-Derive-Role-Port";
	
	public static final String D03 = "D03-Derive-Role-Third-Party-Component";
	
	public static final String D04 = "D04-Derive-Trustlevel-Component";
	
	public static final String E01 = "E01-Unencrypted-Connector-In-Path";
	
	public static final String E02 = "E02-Unencrypted-Connector-Low-Trustlevel";
	
	public static final String E03 = "E03-Encrypted-Path-Continues-Unencrypted";
	
	public static final String E04 = "E04-Unencrypted-Path-End-In-Low-Trustlevel";
	
	public static final String E05 = "E05-Trustlevel-Connector-Higher";
	
	public static final String I01 = "I01-Encrypted-Connector";
	
	public static final String I02 = "I02-Trustlevel-Client-Server";
	
	public static final String I03 = "I03-Roles-Defined";
	
	public static final String F01 = "F01-Taint-Tracking";
	
	public static final String F02 = "F02-High-Trustlevel";
	
	public static final String T01 = "T01-Reason-Trustlevel";
	
	public static final String T02 = "T02-Relation-Trustlevel";
	
	public static final String WI01 = "WI01-Access-Roles";
	
	public static final String WI02 = "WI02-Third-Party-Component-Security";
	
	public static final String WI03 = "WI03-Low-Trustlevel";
	
	public static final String WI04 = "WI04-Critical-Ports";
	
	//Parameter for what if trustlevel analysis
	public static final List<String> EXISTING_PARAMETER = new ArrayList<String>();
	
}
