/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

import interfaces2.coco.AbstractContextCondition;
import interfaces2.coco.CompositeContextCondition;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import secarc.ets.analysis.component.ListThirdPartyComponents;
import secarc.ets.analysis.configuration.ReviewedConfiguration;
import secarc.ets.analysis.connect.EncryptedPathContinuesUnencrypted;
import secarc.ets.analysis.connect.EncryptedPathWithUnencryptedPart;
import secarc.ets.analysis.connect.UnencryptedConnectorThroughLowTurstlevel;
import secarc.ets.analysis.filter.AvoidInputString;
import secarc.ets.analysis.filter.EncryptedPathEndInLowTrustlevel;
import secarc.ets.analysis.filter.FilterWithHigherTrustlevel;
import secarc.ets.analysis.filter.ListFilters;
import secarc.ets.analysis.filter.TaintTracking;
import secarc.ets.analysis.filter.TrustlevelConnectorHigherThanEnvironment;
import secarc.ets.analysis.identity.IdentityWithEncryption;
import secarc.ets.analysis.identity.IdentityWithoutRoles;
import secarc.ets.analysis.identity.TrustlevelClientServer;
import secarc.ets.analysis.parameter.ExistenceParameter;
import secarc.ets.analysis.port.ListCriticalPorts;
import secarc.ets.analysis.port.ListSystemIncomingPorts;
import secarc.ets.analysis.port.ListSystemOutgoingPorts;
import secarc.ets.analysis.role.DerivedRolesComponent;
import secarc.ets.analysis.role.DerivedRolesPort;
import secarc.ets.analysis.role.DerivedRolesThirdParty;
import secarc.ets.analysis.role.RoleAccess;
import secarc.ets.analysis.trustlevel.DerivedTrustlevel;
import secarc.ets.analysis.trustlevel.ReasonForDifferingTrustlevel;
import secarc.ets.analysis.trustlevelrelation.TrustlevelForTrustlevelRelation;


import mc.MCG;
import mc.ProblemReport;
import mc.ProblemReport.Type;

/**
 * 
 * Serves default analysis for MontiSecArc.
 *
 *
 */
public final class MontiSecArcAnalysisCreator {

	/**
	 * Singleton instance
	 */
	private static MontiSecArcAnalysisCreator creator = null;
	
	/**
	 * Roote context condition
	 */
	private CompositeContextCondition analysis;
	
	/**
	 * Default configuration
	 */
	private Map<String, ProblemReport.Type> analysisConfiguration;
	
	/**
	 * 
	 * @return singleton instance
	 */
	private static MontiSecArcAnalysisCreator getInstance() {
		if (creator == null) {
			creator = new MontiSecArcAnalysisCreator();
		}
		return creator;
	}
	
	/**
	 * Private default constructor
	 */
	private MontiSecArcAnalysisCreator() {
		
	}
	
	/**
	 * 
	 * @return default MontiSecArcAnaylsis context condition configuration
	 */
	public static Map<String, ProblemReport.Type> createConfig() {
		return getInstance().doCreateConfiguration();
	}
	
	/**
	 * 
	 * @return configuration for pre analysis parameter checking
	 */
	public static Map<String, ProblemReport.Type> createPreAnalysisConfig() {
		return getInstance().doCreatePreAnalysisConfiguration();
	}
	
	/**
	 * Configuration for analysis
	 * 
	 * @param pathConfiguration Path for used anylsis
	 * @return MontiSecArcAnaylsis context condition configuration
	 */
	public static Map<String, ProblemReport.Type> createConfig(String pathConfiguration) {
		return getInstance().doCreateConfiguration(pathConfiguration);
	}
	
	/**
	 * 
	 * @return default MontiSecArcAnalysis context conditions
	 */
	public static AbstractContextCondition createAnalysis(String pathConfiguration) {
		return getInstance().doCreateAnalysis(pathConfiguration);
	}
	
	/**
	 * 
	 * @return context conditions for checking parameter
	 */
	public static AbstractContextCondition createPreAnalysis() {
		return getInstance().doCreatePreAnalysis();
	}
	
	/**
	 * Creates MontiSecArcAnalsis context conditions
	 * 
	 * @param pathConfiguration Path for analysis configuration file with factors for analysis
	 * @return default MontiSecArcAnalsis context conditions
	 */
	private AbstractContextCondition doCreateAnalysis(String pathConfiguration) {
		if(analysis == null) {
			analysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_ANALYSES);
			analysis.setLevel(Type.WARNING);
			
			//Analysis for connetors
			CompositeContextCondition connectorAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_ENCRYPTED_CONNECT);
			
			//Find all path from a port which has at least one encrypted connection
			connectorAnalysis.addChild(new EncryptedPathWithUnencryptedPart(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.E01)));
			
			//Find paths which end in low trustlevel
			connectorAnalysis.addChild(new EncryptedPathEndInLowTrustlevel(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.E04)));
			
			//Finds connector through low trustlevel
			connectorAnalysis.addChild(new UnencryptedConnectorThroughLowTurstlevel(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.E02)));
			
			//Trustlevel of environment higher than encrypted path
			connectorAnalysis.addChild(new TrustlevelConnectorHigherThanEnvironment(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.E05)));
			
			//An encrypted path continues unencrypted
			connectorAnalysis.addChild(new EncryptedPathContinuesUnencrypted(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.E03)));
			
			//Analysis for filters
			CompositeContextCondition filterAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_FILTER_ANALYSIS);
			
			//List all filters
			filterAnalysis.addChild(new ListFilters(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L01)));
			
			//Taint tracking
			filterAnalysis.addChild(new TaintTracking(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.F01)));
			
			//Filter with higher trustlevel
			filterAnalysis.addChild(new FilterWithHigherTrustlevel(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.F02)));
			
			//Analysis for ports
			CompositeContextCondition portAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_PORT_ANALYSIS);
			
			//List all incoming ports of the system
			portAnalysis.addChild(new ListSystemIncomingPorts(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L03)));
			
			//List all outgoing ports
			portAnalysis.addChild(new ListSystemOutgoingPorts(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L04)));
			
			//List all ciritcal ports
			portAnalysis.addChild(new ListCriticalPorts(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L02)));
			
			//Avoid input type String
			portAnalysis.addChild(new AvoidInputString(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L05)));
			
			//Analysis for configurations
			CompositeContextCondition configurationAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_CONFIGURATION_ANALYSIS);
			
			//Checks if configurations are reviewed
			configurationAnalysis.addChild(new ReviewedConfiguration(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L07)));
			
			//Analysis for roles
			CompositeContextCondition roleAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_ROLE_ANALYSIS);
			
			//Derives roles for thrid party components
			roleAnalysis.addChild(new DerivedRolesThirdParty(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.D03)));
			
			//Derives roles for "normal " components
			roleAnalysis.addChild(new DerivedRolesComponent(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.D01)));
			
			//Derives roles for ports
			roleAnalysis.addChild(new DerivedRolesPort(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.D02)));
			
			//Access for roles
			roleAnalysis.addChild(new RoleAccess(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.WI01)));
		
			//Analysis for identity
			CompositeContextCondition identityAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_IDENTITY);
			
			//identity link needs an encrypted connection
			identityAnalysis.addChild(new IdentityWithEncryption(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.I01)));
			
			//The trustlevel of client should be lower than the one of a server
			identityAnalysis.addChild(new TrustlevelClientServer(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.I02)));
			
			//if an identity link is used, roles should be defined in the target component
			identityAnalysis.addChild(new IdentityWithoutRoles(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.I03)));
			
			//Analysis for trustlevel
			CompositeContextCondition trustlevelAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_TRUSTLEVEL);
			
			//Trustlevel differs more than 2 from relative trustevel
			trustlevelAnalysis.addChild(new ReasonForDifferingTrustlevel(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.T01)));
			
			//Dervied trustlevel for components without trustlevel
			trustlevelAnalysis.addChild(new DerivedTrustlevel(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.D04)));
			
			//Analysis for components
			CompositeContextCondition componentAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_COMPONENT);
			
			//Lists all third party componnts
			componentAnalysis.addChild(new ListThirdPartyComponents(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.L06)));
			
			//Analysis for trustlevel relation
			CompositeContextCondition trustlevelRelationAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_TRUSTLEVEL_RELATION);
			
			//If a relation is defined, define trustlevel
			trustlevelRelationAnalysis.addChild(new TrustlevelForTrustlevelRelation(getFactorFromCofigFile(pathConfiguration, MontiSecArcAnalysisConstants.T02)));
			
			//Analysis for analysis parameter
			CompositeContextCondition parameterAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_PARAMETER);
			
			//Existence of analysis parameterrl
			parameterAnalysis.addChild(new ExistenceParameter());
			
			analysis.addChild(connectorAnalysis);
			analysis.addChild(filterAnalysis);
			analysis.addChild(portAnalysis);
			analysis.addChild(configurationAnalysis);
			analysis.addChild(roleAnalysis);
			analysis.addChild(identityAnalysis);
			analysis.addChild(trustlevelAnalysis);
			analysis.addChild(componentAnalysis);
			analysis.addChild(trustlevelRelationAnalysis);
			analysis.addChild(parameterAnalysis);
			
		}
		return analysis;
	}
	
	/**
	 * 
	 * @return context condition for parameter
	 */
	private AbstractContextCondition doCreatePreAnalysis() {
		if(analysis == null) {
			analysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_ANALYSES);
			analysis.setLevel(Type.WARNING);
			
			//Analysis for analysis parameter
			CompositeContextCondition parameterAnalysis = new CompositeContextCondition(MontiSecArcAnalysisConstants.ALL_PARAMETER);
			
			//Existence of analysis parameter
			parameterAnalysis.addChild(new ExistenceParameter());
			
			analysis.addChild(parameterAnalysis);
			
		}
		return analysis;
	}
	
	/**
	 * Factor for metric 
	 * 
	 * @param analysisConfFile Path to config file
	 * @param name Name of analysis
	 * @return factor Factor of analysis
	 */
	private int getFactorFromCofigFile(String analysisConfFile, String name) {
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileReader = new FileReader(analysisConfFile);
			bufferedReader = new BufferedReader(fileReader);
		} catch (FileNotFoundException e){
			MCG.getLogger().info(e.getMessage());
		}
		
		String zeile = "";
		int value = 1;
		//Can't read file and use default value
		if(bufferedReader == null) {
			return value;
		}
		try {
			while((zeile = bufferedReader.readLine()) != null) {
				if(zeile.contains(name)) {
					if(zeile.contains(" ")) {
						String svalue = zeile.split("\\s")[1];
						bufferedReader.close();
						fileReader.close();
						return Integer.parseInt(svalue);
					}
				}
			}
			bufferedReader.close();
			fileReader.close();
		} catch (IOException | NumberFormatException e) {
			//Error while reading
			MCG.getLogger().info(e.getMessage());
		} 
		return value;
	}
	
	/**
	 * Checks if an analysis exists in the config file
	 * 
	 * @param analysisConfFile File with configuration
	 * @param name Name of analysis
	 * @return boolean true if analysis exists
	 */
	private boolean existsAnalysisInFile(String analysisConfFile, String name) {
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileReader = new FileReader(analysisConfFile);
			bufferedReader = new BufferedReader(fileReader);
		} catch (FileNotFoundException e){
			MCG.getLogger().info(e.getMessage());
		}
		
		String zeile = "";
		boolean value = false;
		//Can't read file and use default value
		if(bufferedReader == null) {
			return value;
		} 
		try {
			while((zeile = bufferedReader.readLine()) != null) {
				if(zeile.contains(name)) {
					bufferedReader.close();
					fileReader.close();
					return true;
				}
			}
			bufferedReader.close();
			fileReader.close();
		} catch (IOException | NumberFormatException e) {
			//Error while reading
			MCG.getLogger().info(e.getMessage());
		} 
		
		return value;
	}
	
	/**
	 * 
	 * @return default MontiSecArc context conditions configuration
	 */
	private Map<String,ProblemReport.Type> doCreateConfiguration() {
		if(analysisConfiguration == null) {
			analysisConfiguration = new HashMap<String, Type>(); 
		
			analysisConfiguration.put(MontiSecArcAnalysisConstants.ALL_ANALYSES, Type.WARNING);
			
		}
		
		return analysisConfiguration;
	}
	
	/**
	 * 
	 * @return configuration for pre analysis parameter checking
	 */
	private Map<String,ProblemReport.Type> doCreatePreAnalysisConfiguration() {
		if(analysisConfiguration == null) {
			analysisConfiguration = new HashMap<String, Type>(); 
		
			analysisConfiguration.put(MontiSecArcAnalysisConstants.ALL_PARAMETER, Type.WARNING);
			
		}
		
		return analysisConfiguration;
	}
	
	/**
	 * Creates a mapping between analysis names and description for configuration
	 * 
	 * @return Mapping between analysis names and description
	 */
	private Map<String, String> createMappingNameConstant() {
		Map<String, String> mapping = new HashMap<String, String>();
		
		//Analysis which list information
		mapping.put(MontiSecArcAnalysisConstants.L01, MontiSecArcAnalysisConstants.LIST_FILTER);
		mapping.put(MontiSecArcAnalysisConstants.L02, MontiSecArcAnalysisConstants.LIST_CRITICAL_PORTS);
		mapping.put(MontiSecArcAnalysisConstants.L03, MontiSecArcAnalysisConstants.LIST_INCOMING_PORTS);
		mapping.put(MontiSecArcAnalysisConstants.L04, MontiSecArcAnalysisConstants.LIST_OUTGOING_PORTS);
		mapping.put(MontiSecArcAnalysisConstants.L05, MontiSecArcAnalysisConstants.AVOID_INPUT_STRING);
		mapping.put(MontiSecArcAnalysisConstants.L06, MontiSecArcAnalysisConstants.LIST_THIRD_PARTY_COMPONENTS);
		mapping.put(MontiSecArcAnalysisConstants.L07, MontiSecArcAnalysisConstants.REVIEWED_CONFIGURATION);
		
		//Analysis which derive information
		mapping.put(MontiSecArcAnalysisConstants.D01, MontiSecArcAnalysisConstants.DERIVED_ROLES_COMPONENT);
		mapping.put(MontiSecArcAnalysisConstants.D02, MontiSecArcAnalysisConstants.DERIVE_ROLES_PORT);
		mapping.put(MontiSecArcAnalysisConstants.D03, MontiSecArcAnalysisConstants.DERIVED_ROLES_THIRD_PARTY);
		mapping.put(MontiSecArcAnalysisConstants.D04, MontiSecArcAnalysisConstants.DERIVED_TRUSTLEVEL);
		
		//Analysis for encrypted/unencrypted connections
		mapping.put(MontiSecArcAnalysisConstants.E01, MontiSecArcAnalysisConstants.ENCRYPTED_PATH_WITH_UNENCRYPTED_PART);
		mapping.put(MontiSecArcAnalysisConstants.E02, MontiSecArcAnalysisConstants.UNENCRYPTED_CONNECTOR_THROUGH_LOW_TRUSTLEVEL);
		mapping.put(MontiSecArcAnalysisConstants.E03, MontiSecArcAnalysisConstants.ENCRYPTED_PATH_CONTINUES_UNENCRYPTED);
		mapping.put(MontiSecArcAnalysisConstants.E04, MontiSecArcAnalysisConstants.ENRYPTED_PATH_END_IN_LOW_TRUSTLEVEL);
		mapping.put(MontiSecArcAnalysisConstants.E05, MontiSecArcAnalysisConstants.TRUSTLEVEL_PATH_HIGHER_THAN_ENVIRONMENT);
		
		//Analysis for identity links
		mapping.put(MontiSecArcAnalysisConstants.I01, MontiSecArcAnalysisConstants.IDENTITY_WITH_ENCRYPTION);
		mapping.put(MontiSecArcAnalysisConstants.I02, MontiSecArcAnalysisConstants.TRUSTLEVEL_CLIENT_SERVER_IDENTITY);
		mapping.put(MontiSecArcAnalysisConstants.I03, MontiSecArcAnalysisConstants.IDNETITY_WITHOUT_ROLES);
		
		//Analysis for filters
		mapping.put(MontiSecArcAnalysisConstants.F01, MontiSecArcAnalysisConstants.TAINT_TRACKING);
		mapping.put(MontiSecArcAnalysisConstants.F02, MontiSecArcAnalysisConstants.FILTER_WITH_HIGHER_TRUST);
		
		//Analysis for trustlevel
		mapping.put(MontiSecArcAnalysisConstants.T01, MontiSecArcAnalysisConstants.REASON_DIFFERING_TRUSTLEVEL);
		mapping.put(MontiSecArcAnalysisConstants.T02, MontiSecArcAnalysisConstants.TRUSTLEVEL_FOR_TRUSTLEVELRELATION);
		
		//Analysis for roles
		mapping.put(MontiSecArcAnalysisConstants.WI01, MontiSecArcAnalysisConstants.ROLE_ACCESS);	
		
		return mapping;
	}
	
	/**
	 * 
	 * @return default MontiSecArc context conditions configuration
	 */
	private Map<String,ProblemReport.Type> doCreateConfiguration(String pathConfiguration) {
		Map<String, String> mapping = createMappingNameConstant();
		if(analysisConfiguration == null) {
			analysisConfiguration = new HashMap<String, Type>();
		
			//Add analysis from configuration file			 
			for(Entry<String, String> it : mapping.entrySet()) {
				if(existsAnalysisInFile(pathConfiguration, it.getKey())) {
					analysisConfiguration.put(it.getValue(), Type.WARNING);
				}
			}
				
			//Add analysis for parameter
			analysisConfiguration.put(MontiSecArcAnalysisConstants.ALL_PARAMETER, Type.WARNING);
			
		}
		return analysisConfiguration;
	}
	

}
