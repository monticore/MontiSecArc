/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

import interfaces2.coco.ContextCondition;
import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;
import interfaces2.workflows.CheckWorkflowClient;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mc.MCG;
import mc.helper.NameHelper;
import mc.types._ast.ASTQualifiedName;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;

import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.ets.analysis.checker.ISecAnalysisComponentChecker;
import secarc.ets.analysis.checker.ISecAnalysisConfigurationChecker;
import secarc.ets.analysis.checker.ISecAnalysisConnectorChecker;
import secarc.ets.analysis.checker.ISecAnalysisFilterChecker;
import secarc.ets.analysis.checker.ISecAnalysisIdentityChecker;
import secarc.ets.analysis.checker.ISecAnalysisPortChecker;
import secarc.ets.analysis.checker.ISecAnalysisRoleChecker;
import secarc.ets.analysis.checker.ISecAnalysisTrustlevelRelationChecker;
import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.ArchitectureGraphBuilder;

/**
 * 
 * Visitor for MontiSecArc that checks analysis.
 *
 *
 */
public class MontiSecArcAnalysisVisitor extends CheckWorkflowClient {
	
	/**
	 * Architecture graph for analysis with connectors as edges
	 */
	protected ArchitectureGraph graphConnectorEdges;
	
	/**
	 * Architecture graph for analysis with identity links as edges
	 */
	protected ArchitectureGraph graphIdentityEdges;
	
	/**
	 * Analysis for filters
	 */
	protected Set<ISecAnalysisFilterChecker> analysisFilterChecker;
	
	/**
	 * Analysis for configurations
	 */
	protected Set<ISecAnalysisConfigurationChecker> analysisConfigurationChecker;
	
	/**
	 * Analysis for Ports
	 */
	protected Set<ISecAnalysisPortChecker> analysisPortChecker;
	
	/**
	 * Analysis for connectors
	 */
	protected Set<ISecAnalysisConnectorChecker> analysisConnectorChecker;
	
	/**
	 * Analysis for components
	 */
	protected Set<ISecAnalysisComponentChecker> analysisComponentChecker;
	
	/**
	 * Analysis for identity
	 */
	protected Set<ISecAnalysisIdentityChecker> analysisIdentityChecker;
	
	/**
	 * Analysis for roles
	 */
	protected Set<ISecAnalysisRoleChecker> analysisRoleChecker;
	
	/**
	 * Analysis for trustlevel relation
	 */
	protected Set<ISecAnalysisTrustlevelRelationChecker> analysisTrustlevelRelationChecker;
	
	/**
	 * The detailness of the warinings depend of this paramenter
	 * beginner vs advanced
	 */
	private boolean advanced = false;
	
	/**
	 * The sum of all factors results in the metric
	 */
	private int metricSum = 0;
	
	/**
	 * default constructor
	 */
	public MontiSecArcAnalysisVisitor() {
		analysisFilterChecker = new HashSet<ISecAnalysisFilterChecker>();
		analysisConfigurationChecker = new HashSet<ISecAnalysisConfigurationChecker>();
		analysisPortChecker = new HashSet<ISecAnalysisPortChecker>();
		analysisConnectorChecker = new HashSet<ISecAnalysisConnectorChecker>();
		analysisComponentChecker = new HashSet<ISecAnalysisComponentChecker>();
		analysisIdentityChecker = new HashSet<ISecAnalysisIdentityChecker>();
		analysisRoleChecker = new HashSet<ISecAnalysisRoleChecker>();
		analysisTrustlevelRelationChecker = new HashSet<ISecAnalysisTrustlevelRelationChecker>();
	}
	
	/**
	 * Define if the output is for advanced users or beginners
	 * 
	 * @param advanced
	 */
	public void setAdvanced(boolean advanced) {
		this.advanced = advanced;
	}
	
	/**
	 * Value for metric 
	 * 
	 * @return Sum of all factors
	 */
	public int getMetricSum() {
		return metricSum;
	}
	
	/**
	 * Enabled analysis for supergrammars and MontiSecArc
	 * 
	 * @param enabledAnalysis: Enabled analysis
	 */
	@Override
	public void setEnabledConditions(Collection<ContextCondition> enabledAnalysis) {
		super.setEnabledConditions(enabledAnalysis);
		createUsefulCollections();
	}
	
	/**
	 * Puts enabled context conditions and analysis in the corresponding sets
	 */
	protected void createUsefulCollections() {
		for(ContextCondition coco : getEnabledConditions()) {
			if(coco instanceof ISecAnalysisFilterChecker) {
				analysisFilterChecker.add((ISecAnalysisFilterChecker) coco);
			}
			if(coco instanceof ISecAnalysisConfigurationChecker) {
				analysisConfigurationChecker.add((ISecAnalysisConfigurationChecker) coco);
			}
			if(coco instanceof ISecAnalysisPortChecker) {
				analysisPortChecker.add((ISecAnalysisPortChecker) coco);
			}
			if(coco instanceof ISecAnalysisConnectorChecker) {
				analysisConnectorChecker.add((ISecAnalysisConnectorChecker) coco);
			}
			if(coco instanceof ISecAnalysisComponentChecker) {
				analysisComponentChecker.add((ISecAnalysisComponentChecker) coco);
			}
			if(coco instanceof ISecAnalysisIdentityChecker) {
				analysisIdentityChecker.add((ISecAnalysisIdentityChecker) coco);
			}
			if(coco instanceof ISecAnalysisRoleChecker) {
				analysisRoleChecker.add((ISecAnalysisRoleChecker) coco);
			}
			if(coco instanceof ISecAnalysisTrustlevelRelationChecker) {
				analysisTrustlevelRelationChecker.add((ISecAnalysisTrustlevelRelationChecker) coco);
			}
		}
	}
	
	/**
	 * Visits compilation unit and builds the architecture graph
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTMCCompilationUnit node) {
		SecComponentEntry componentEntry = null;
		
		try {
			//root component for architecture graph
			componentEntry = (SecComponentEntry) resolver.resolve(node.getType().printName(), ComponentEntry.KIND, getNameSpaceFor(node));
		} catch (AmbigousException | NullPointerException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
		//Create graph from AST
		if(componentEntry != null) {
			graphConnectorEdges = ArchitectureGraphBuilder.forArchitecture(componentEntry).buildGraphConnectorEdge();
			graphIdentityEdges = ArchitectureGraphBuilder.forArchitecture(componentEntry).buildGraphIdentityEdge();
		}
		
	}
	
	/**
	 * Visits filters
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcFilter node) {
		try {
			FilterEntry entry = (FilterEntry) resolver.resolve(node.getName(), FilterEntry.KIND, getNameSpaceFor(node));
			if(entry != null) {
				for (ISecAnalysisFilterChecker cc : analysisFilterChecker) {
					metricSum += cc.check(node, entry, graphConnectorEdges, advanced);
				}
			}
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
	}
	
	/**
	 * Visits configurations
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcConfiguration node) {
		try {
			ConfigurationEntry entry = (ConfigurationEntry) resolver.resolve(node.getName(), ConfigurationEntry.KIND, getNameSpaceFor(node));
			if(entry != null) {
				for (ISecAnalysisConfigurationChecker cc : analysisConfigurationChecker) {
					metricSum += cc.check(node, entry, advanced);
				}
			}
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
	}
	
	/**
	 * Visits ports
	 */
	public void visit(ASTArcPort node) {
		try {
			SecPortEntry entry = (SecPortEntry) resolver.resolve(node.printName(), PortEntry.KIND, getNameSpaceFor(node));
			if(entry != null) {
				for(ISecAnalysisPortChecker cc : analysisPortChecker) {
					metricSum += cc.check(node, entry, graphConnectorEdges, advanced);
				}
			}
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
	}
	
	/**
	 * Visits connectors
	 */
	public void visit(ASTArcConnector node) {
		for (ASTQualifiedName name : node.getTargets()) {
            try {
                SecConnectorEntry entry = (SecConnectorEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(name.getParts()), ConnectorEntry.KIND,
                        getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecAnalysisConnectorChecker cc : analysisConnectorChecker) {
                    	metricSum += cc.check(node, entry, graphConnectorEdges, advanced);
                    }
                }
            } catch (AmbigousException e) {
                // not handled here
                MCG.getLogger().info(e.getMessage());
            }
        }
	}
	
	/**
	 * Visits simple connectors
	 */
	public void visit(ASTArcSimpleConnector node) {
		for (ASTQualifiedName name : node.getTargets()) {
            try {
                SecConnectorEntry entry = (SecConnectorEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(name.getParts()), ConnectorEntry.KIND,
                        getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecAnalysisConnectorChecker cc : analysisConnectorChecker) {
                    	metricSum += cc.check(node, entry, graphConnectorEdges, advanced);
                    }
                }
            } catch (AmbigousException e) {
                // not handled here
                MCG.getLogger().info(e.getMessage());
            }
        }
	}
	
	/**
	 * Visits Components
	 */
	public void visit(ASTArcComponent node) {
		try {
			SecComponentEntry entry = (SecComponentEntry) resolver.resolve(node.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
			if(entry != null) {
				for(ISecAnalysisComponentChecker cc : analysisComponentChecker) {
					metricSum += cc.check(node, entry, advanced);
				}
			}
		} catch (AmbigousException | EntryLoadingErrorException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
	}
	
	/**
	 * Visits identity links
	 */
	public void visit(ASTSecArcIdentity node) {
		for (ASTQualifiedName name : node.getTargets()) {
            try {
            	IdentityEntry entry = (IdentityEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(name.getParts()), IdentityEntry.KIND,
                        getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecAnalysisIdentityChecker cc : analysisIdentityChecker) {
                    	metricSum += cc.check(node, entry, graphConnectorEdges, graphIdentityEdges, advanced);
                    }
                }
            } catch (AmbigousException e) {
                // not handled here
                MCG.getLogger().info(e.getMessage());
            }
        }
	}
	
	/**
	 * Visits roles
	 */
	public void visit(ASTSecArcRole node) {
		for (String name : node.getRoles()) {
            try {
            	RoleEntry entry = (RoleEntry) resolver.resolve(name, RoleEntry.KIND, getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecAnalysisRoleChecker cc : analysisRoleChecker) {
                    	metricSum += cc.check(node, entry, graphConnectorEdges, advanced);
                    }
                }
            } catch (AmbigousException e) {
                // not handled here
                MCG.getLogger().info(e.getMessage());
            }
        }
	}
	
	/**
	 * Visits referenced roles
	 */
	public void visit(ASTSecArcRefRole node) {
		for (String name : node.getRoles()) {
            try {
            	RoleEntry entry = (RoleEntry) resolver.resolve(name, RoleEntry.KIND, getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecAnalysisRoleChecker cc : analysisRoleChecker) {
                    	metricSum += cc.check(node, entry, graphConnectorEdges, advanced);
                    }
                }
            } catch (AmbigousException e) {
                // not handled here
                MCG.getLogger().info(e.getMessage());
            }
        }
	}
	
	/**
	 * Visits trustlevel relation
	 */
	public void visit(ASTSecArcTrustlevelRelation node) {
		try {
			String name = "";
			//The component with the higher trustlevel is used as name
			if(node.isGT()) {
				name = NameHelper.dotSeparatedStringFromList(node.getClient().getParts());
			} else {
				name = NameHelper.dotSeparatedStringFromList(node.getServer().getParts());
			}
			TrustlevelRelationEntry entry = (TrustlevelRelationEntry) resolver.resolve(name, TrustlevelRelationEntry.KIND, getNameSpaceFor(node));
			if(entry != null) {
				for(ISecAnalysisTrustlevelRelationChecker cc : analysisTrustlevelRelationChecker) {
					metricSum += cc.check(node, entry, advanced);
				}
			}
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
	}
	
}
