/* (c) https://github.com/MontiCore/monticore */

package secarc.ets.check;

import interfaces2.EtsErrorCode;
import interfaces2.Interfaces2Constants;
import interfaces2.coco.AbstractContextCondition;
import interfaces2.coco.ContextCondition;
import interfaces2.loaders.STEntryDeserializer;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.Resolver;

import java.util.Collection;
import java.util.Map;

import mc.DSLRoot;
import mc.DSLWorkflow;
import mc.ProblemReport;
import mc.ProblemReport.Type;
import mc.ast.ASTNode;
import mc.ast.IVisitor;
import mc.ast.MultiInheritanceVisitor;

/**
 * A workflow that executes a {@link MontiSecArcAnalysisVisitor} on the given AST
 * and the resulting graph.
 * The setting is very close to the CheckWorkflow.
 * 
 * 
 */
public final class MontiSecArcAnalysisWorkflow<Root extends DSLRoot<?>> extends
		DSLWorkflow<Root> {

	/**
	 * Constructor for MontiSecArcAnalysisWorkflow
	 * 
	 * @param responsibleClass  LANGUAGE_ROOT
	 */
	public MontiSecArcAnalysisWorkflow(Class<? extends Root> responsibleClass) {
		super(responsibleClass);
	}

	/**
	 * Analysis are static analysis such as contextconditions
	 */
	protected AbstractContextCondition analysis;

	/**
	 * Configuration for analysis
	 */
	protected Map<String, ProblemReport.Type> analysisConfiguration;
	
	/**
	 * Output for beginner or advanced users
	 */
	protected boolean advanced = false;
	
	/**
	 * 
	 * @return analysis static analysis for AST/Graph
	 */
	public AbstractContextCondition getAnalysis() {
		return analysis;
	}

	/**
	 * 
	 * @param analysis static analysis for AST/Graph
	 */
	public void setAnalysis(AbstractContextCondition analysis) {
		this.analysis = analysis;
	}

	/**
	 * 
	 * @return analysisConfiguration configuration for analysis
	 */
	public Map<String, ProblemReport.Type> getAnalysisConfiguration() {
		return analysisConfiguration;
	}

	/**
	 * 
	 * @param analysisConfiguration configuration for analysis
	 */
	public void setAnalysisConfiguration(
			Map<String, ProblemReport.Type> analysisConfiguration) {
		this.analysisConfiguration = analysisConfiguration;
	}
	
	/**
	 * 
	 * @param advanced
	 */
	public void setAdvanced(boolean advanced) {
		this.advanced = advanced;
	}
		
	/*
	 * (non-Javadoc)
	 * @see mc.DSLWorkflow#run(mc.DSLRoot)
	 */
	@Override
	public void run(Root dslroot) {
		if (dslroot.hasFatalError()) {
			return;
		}

		Map<NameSpace, ASTNode> nameSpacesToNodes = (Map<NameSpace, ASTNode>) dslroot.getAnnotation(Interfaces2Constants.NAME_SPACE_TO_NODES_ID);
		Map<ASTNode, NameSpace> nodesToNameSpaces = (Map<ASTNode, NameSpace>) dslroot.getAnnotation(Interfaces2Constants.NODES_TO_NAME_SPACE_ID);

		Collection<STEntryDeserializer> set = (Collection<STEntryDeserializer>) dslroot.getAnnotation(Interfaces2Constants.DESERIALIZER_ID);

		Resolver resolver = (Resolver) dslroot.getAnnotation(Interfaces2Constants.RESOLVER_ID);

		NameSpace topLevelNameSpace = (NameSpace) dslroot.getAnnotation(Interfaces2Constants.NAME_SPACE_ID);

		if (dslroot.hasFatalError()) {
			dslroot.getErrorDelegator()
					.addInfo(
							"0xA2040 CheckWorkflow can't be executed because of fatal errors",
							EtsErrorCode.ResolvingFailed);
			return;
		}

		if (resolver == null) {
			dslroot.getErrorDelegator()
					.addErrorToCurrentResource(
							"0xA0015 CheckWorkflow can't be executed: Resolver wasn't set",
							EtsErrorCode.ResolvingFailed);
			return;
		}

		IVisitor v = new MultiInheritanceVisitor();
		//initialize visitor
		//Visitor for Analysis
	    MontiSecArcAnalysisVisitor analysisVisitor = new MontiSecArcAnalysisVisitor();
	    //Advanced or beginner messages
	    analysisVisitor.setAdvanced(advanced);
	    //Parameter for what if trustlevel analysis
		
	    analysisVisitor.setDelegator(dslroot.getErrorDelegator());
	    analysisVisitor.setDeserializers(set);
	    analysisVisitor.setLoader(dslroot.getModelInfrastructureProvider().getModelloader());
	    analysisVisitor.setNameSpacesToNodes(nameSpacesToNodes);
	    analysisVisitor.setNodesToNameSpaces(nodesToNameSpaces);
	    analysisVisitor.setResolver(resolver);
	    analysisVisitor.setTopLevelNameSpace(topLevelNameSpace);

		if (getAnalysis() != null) {
			getAnalysis().resetConfiguration();
			getAnalysis().setConfiguration(getAnalysisConfiguration());
			Collection<ContextCondition> enabled = getAnalysis().getEnabledConditions();
			for (ContextCondition con : enabled) {
				con.setDelegator(dslroot.getErrorDelegator());
				con.setDeserializers(set);
				con.setLoader(dslroot.getModelInfrastructureProvider()
						.getModelloader());
				con.setResolver(resolver);
				con.setNodes2namesSpace(nodesToNameSpaces);
			}
			if (analysisVisitor.getEnabledConditions() == null
					|| analysisVisitor.getEnabledConditions().isEmpty()) {
				analysisVisitor.setEnabledConditions(enabled);
			}
		}

		v.addClient(analysisVisitor);
		

		v.startVisit((ASTNode) dslroot.getAst());
		
		//Add output for metric to report
		ProblemReport rep = new ProblemReport(Type.WARNING, "Regarding the configuration the result of the metrik is: " + analysisVisitor.getMetricSum() , 0, 0);
		dslroot.getErrorDelegator().addProblemReport(rep);

	}

}
