/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.criticalport;

import interfaces2.ISTEntry;
import interfaces2.STEntry;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AmbigousException;
import interfaces2.resolvers.Resolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jgrapht.DirectedGraph;
import org.jgrapht.traverse.DepthFirstIterator;

import secarc.MontiSecArcConstants;
import secarc._ast.ASTSecArcCPE;
import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcPEP;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcTrustLevel;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.ets.entries.CPEEntry;
import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.ArchitectureGraphBuilder;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;
import mc.MCG;
import mc.ast.ASTNode;
import mc.ast.ConcreteVisitor;
import mc.helper.NameHelper;
import mc.types._ast.ASTQualifiedName;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.common._ast.UMLPNode;

/**
 * 
 * Visitor for transformation with critical ports
 * Saves every element which have to be deleted from the ast
 *
 *
 */
public class AnalysisCriticalPortTransformationVisitor extends ConcreteVisitor {

	/**
	 * Entries which are needed for the analysis
	 */
	private List<STEntry> entries;
	
	/**
	 * Nodes which will be deleted in the next step
	 */
	private List<UMLPNode> removeNodes = new ArrayList<UMLPNode>();
	
	/**
	 * Resolver for entries
	 */
	private Resolver resolver;
	
	/**
	 * Namespace for nodes
	 */
	protected Map<ASTNode, NameSpace> nodesToNameSpaces;
	
	/**
	 * Architecture graph for analysis with identity links as edges
	 */
	private ArchitectureGraph graphIdentityEdges;
	
	/**
     * 
     * @param dslroot dsl root to use
     */
	public AnalysisCriticalPortTransformationVisitor() {
		super();
	}
	
	/**
	 * 
	 * @param resolver Resolver for entries
	 */
	public void setResolver(Resolver resolver) {
    	this.resolver = resolver; 
    }
    
	/**
	 * 
	 * @param nodesToNameSpaces Namespace for nodes
	 */
    public void setNodesToNameSpaces(Map<ASTNode, NameSpace> nodesToNameSpaces) {
    	this.nodesToNameSpaces = nodesToNameSpaces;
    }
    
    /**
     * 
     * @param entries Entries which are needed in the next steps
     */
    public void setEntries(List<STEntry> entries) {
    	this.entries = entries;
    }
    
    /**
     * 
     * @return Nodes which will be deleted in the next step
     */
    public List<UMLPNode> getRemoveNodes() {
    	return removeNodes;
    }
    
    /**
	 * Visits compilation unit and builds the architecture graph
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTMCCompilationUnit node) {
		SecComponentEntry componentEntry = null;
		
		try {
			//Root element for graph
			componentEntry = (SecComponentEntry) resolver.resolve(node.getType().getName(), ComponentEntry.KIND, getNameSpaceFor(node));
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
		//Create graph from AST
		if(componentEntry != null) {
			graphIdentityEdges = ArchitectureGraphBuilder.forArchitecture(componentEntry).buildGraphIdentityEdge();
		}
		
	}
    
	/**
	 * Visits role
	 * 
	 * @param node ast node to visit
	 */
    public void visit(ASTSecArcRole node) {
    	try {
	    	for(String role : node.getRoles()) {
	    		RoleEntry entry = (RoleEntry) resolver.resolve(role, RoleEntry.KIND, getNameSpaceFor(node));
	    		if(entry != null && !this.entries.contains(entry)) {
	    			removeNodes.add(node);
	    		}
	    	}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits configuration
     * 
     * @param node ast node to visit
     */
    public void visit(ASTSecArcConfiguration node) {
    	try {
    		ConfigurationEntry entry = (ConfigurationEntry) resolver.resolve(node.getName(), ConfigurationEntry.KIND, getNameSpaceFor(node));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits pep
     * 
     * @param node ast node to visit
     */
    public void visit(ASTSecArcPEP node) {
    	try {
    		PEPEntry entry = (PEPEntry) resolver.resolve(MontiSecArcConstants.PEP_NAME, PEPEntry.KIND, getNameSpaceFor(node));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits trustlevel 
     * 
     * @param node ast node to visit
     */
    public void visit(ASTSecArcTrustLevel node) {
    	try {
    		TrustlevelEntry entry = (TrustlevelEntry) resolver.resolve(MontiSecArcConstants.TRUSTLEVEL_NAME, TrustlevelEntry.KIND, getNameSpaceFor(node));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits cpe
     * 
     * @param node ast node to visit
     */
    public void visit(ASTSecArcCPE node) {
    	try {
    		CPEEntry entry = (CPEEntry) resolver.resolve(MontiSecArcConstants.CPE_NAME, CPEEntry.KIND, getNameSpaceFor(node));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits connector
     * 
     * @param node ast node to visit
     */
    public void visit(ASTArcConnector node) {
    	try {
    		for(ASTQualifiedName target : node.getTargets()) {
    			ConnectorEntry entry = (ConnectorEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(target.getParts()), ConnectorEntry.KIND, getNameSpaceFor(node));
    			if(entry != null && !this.entries.contains(entry)) {
    				removeNodes.add(node);
        		}
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits simple connector
     * 
     * @param node ast node to visit
     */
    public void visit(ASTArcSimpleConnector node) {
    	try {
    		for(ASTQualifiedName target : node.getTargets()) {
    			ConnectorEntry entry = (ConnectorEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(target.getParts()), ConnectorEntry.KIND, getNameSpaceFor(node));
    			if(entry != null && !this.entries.contains(entry)) {
    				removeNodes.add(node);
        		}
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits port
     * 
     * @param node ast node to visit
     */
    public void visit(ASTArcPort node) {
    	try {
    		PortEntry entry = (PortEntry) resolver.resolve(node.printName(), PortEntry.KIND, getNameSpaceFor(node));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits component and deletes identity links
     * 
     * @param node ast node to visit
     */
    public void visit(ASTArcComponent node) {
    	try {
    		ComponentEntry entry = (ComponentEntry) resolver.resolve(node.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    			
    			//Delete Identity link, if the component is a target or source
        		Vertex<ComponentEntry> componentVertex = Vertex.of(entry);
        		DirectedGraph<Vertex<? extends STEntry>, Edge> reversedRawGraph = graphIdentityEdges.getReversedRawGraph();
        		DirectedGraph<Vertex<? extends STEntry>, Edge> rawGraph = graphIdentityEdges.getRawGraph();
        		
        		//Check for target
    			DepthFirstIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(reversedRawGraph, componentVertex);
    			iterator.next();
        		if(iterator.hasNext()) {
        			Vertex<? extends STEntry> identity = iterator.next();
        			ISTEntry identityEntry = identity.getArchitectureElement().getBestKnownVersion();
        			if(identityEntry instanceof IdentityEntry && !removeNodes.contains(identityEntry.getNode())) {
        				removeNodes.add((UMLPNode) identityEntry.getNode());
        			}
        		}
        		//Check for source
        		iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(rawGraph, componentVertex);
        		iterator.next();
        		if(iterator.hasNext()) {
        			Vertex<? extends STEntry> identity = iterator.next();
        			ISTEntry identityEntry = identity.getArchitectureElement().getBestKnownVersion();
        			if(identityEntry instanceof IdentityEntry && !removeNodes.contains(identityEntry.getNode())) {
        				removeNodes.add((UMLPNode) identityEntry.getNode());
        			}
        		}
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Visits trustlevel relation
     * 
     * @param node ast node to visit
     */
    public void visit(ASTSecArcTrustlevelRelation node) {
    	try {
    		TrustlevelRelationEntry entry = (TrustlevelRelationEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(node.getServer().getParts()), TrustlevelRelationEntry.KIND, getNameSpaceFor(node.getMainParent()));
    		if(entry != null && !this.entries.contains(entry)) {
    			removeNodes.add(node);
    		}
    	} catch (AmbigousException e) {
    		// not checked here
            MCG.getLogger().info(e.getMessage());
    	}
    }
    
    /**
     * Analysis which is the namespace a node is in.
     * 
     * @param n Node to be checked.
     * @return Namespace the given node is in.
     */
    public NameSpace getNameSpaceFor(ASTNode n) {
      if (nodesToNameSpaces != null) {
        ASTNode curr = n;
        while (curr != null) {
          NameSpace nsp = nodesToNameSpaces.get(curr);
          if (nsp != null) {
            return nsp;
          }
          curr = curr.get_Parent();
        }
      }
      return null;
    }
	
}
