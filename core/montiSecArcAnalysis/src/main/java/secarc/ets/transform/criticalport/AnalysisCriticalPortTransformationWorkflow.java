/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.criticalport;

import interfaces2.EtsErrorCode;
import interfaces2.Interfaces2Constants;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.Resolver;

import java.util.Map;

import secarc._ast.ASTMCCompilationUnit;
import secarc._tool.MontiSecArcRoot;
import mc.DSLWorkflow;
import mc.ast.ASTNode;
import mc.ast.InheritanceVisitor;
import mc.umlp.common._ast.UMLPNode;

/**
 * 
 * Transformation workflow for what if analysis with critical ports
 *
 *
 */
public class AnalysisCriticalPortTransformationWorkflow<T extends MontiSecArcRoot> extends DSLWorkflow<T> {
	
	/**
     * @param responsibleClass class of the dsl root
     */
    public AnalysisCriticalPortTransformationWorkflow(final Class<T> responsibleClass) {
        super(responsibleClass);
    }
    
    /*
     * (non-Javadoc)
     * @see mc.DSLWorkflow#run(mc.DSLRoot)
     */
    @Override
    public void run(MontiSecArcRoot dslroot) {
    	Map<ASTNode, NameSpace> nodesToNameSpaces = (Map<ASTNode, NameSpace>) dslroot.getAnnotation(Interfaces2Constants.NODES_TO_NAME_SPACE_ID);
    	
    	Resolver resolver = (Resolver) dslroot.getAnnotation(Interfaces2Constants.RESOLVER_ID);
    	
    	if (dslroot.hasFatalError()) {
			dslroot.getErrorDelegator()
					.addInfo(
							"0xA2040 CheckWorkflow can't be executed because of fatal errors",
							EtsErrorCode.ResolvingFailed);
			return;
		}

		if (resolver == null) {
			dslroot.getErrorDelegator()
					.addErrorToCurrentResource(
							"0xA0015 CheckWorkflow can't be executed: Resolver wasn't set",
							EtsErrorCode.ResolvingFailed);
			return;
		}
    	
		//Elements which are needed for analysis
    	PreAnalysisCriticalPortTransformationVisitor preVisitor = new PreAnalysisCriticalPortTransformationVisitor();
    	preVisitor.setResolver(resolver);
    	preVisitor.setNodesToNameSpaces(nodesToNameSpaces);
    	
    	//Copy AST for this workflow
    	ASTMCCompilationUnit astCopy = dslroot.getAst().deepClone();
    	
        InheritanceVisitor.run(preVisitor, dslroot.getAst());
        
        //Delete Elements which are not needed
        if(preVisitor.getEntries() != null) {
	        AnalysisCriticalPortTransformationVisitor visitorTransform = new AnalysisCriticalPortTransformationVisitor();
	        visitorTransform.setResolver(resolver);
	        visitorTransform.setNodesToNameSpaces(nodesToNameSpaces);
	        visitorTransform.setEntries(preVisitor.getEntries());
	        
	        InheritanceVisitor.run(visitorTransform, dslroot.getAst());
	        
	        //Delete all nodes which are not needed
	        for(UMLPNode node : visitorTransform.getRemoveNodes()) {
	        	node.delete();
//	        	resolver.resolve(node.get, expectedKind, nsp)
	        }
        }

    }
	
}
