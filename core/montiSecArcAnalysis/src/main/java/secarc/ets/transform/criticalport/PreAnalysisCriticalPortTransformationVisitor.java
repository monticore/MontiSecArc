/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.criticalport;

import interfaces2.STEntry;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AmbigousException;
import interfaces2.resolvers.Resolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.GraphIterator;

import secarc.ets.analysis.checker.AnalysisHelper;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.ArchitectureGraphBuilder;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;
import mc.MCG;
import mc.ast.ASTNode;
import mc.ast.ConcreteVisitor;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * 
 * Visitor for transformation with critical ports
 * Saves every path with a critical port
 *
 *
 */
public class PreAnalysisCriticalPortTransformationVisitor extends ConcreteVisitor {

	/**
	 * Architecture graph for analysis with connectors as edges
	 */
	private ArchitectureGraph graphConnectorEdges;
	
	/**
	 * Entries which are needed 
	 */
	private List<STEntry> entries = new ArrayList<STEntry>();
	
	/**
	 * Resolver for entries
	 */
	private Resolver resolver;
	
	/**
	 * Namespace for nodes
	 */
	protected Map<ASTNode, NameSpace> nodesToNameSpaces;
	
	/**
     * 
     * @param dslroot dsl root to use
     */
    public PreAnalysisCriticalPortTransformationVisitor() {
    	super();
    }
    
    /**
     * 
     * @param resolver Resolver for entries
     */
    public void setResolver(Resolver resolver) {
    	this.resolver = resolver;
    }
    
    /**
     * 
     * @param nodesToNameSpaces Namespace for nodes
     */
    public void setNodesToNameSpaces(Map<ASTNode, NameSpace> nodesToNameSpaces) {
    	this.nodesToNameSpaces = nodesToNameSpaces;
    }
    
    /**
     * 
     * @return Entries which are needed for the next steps
     */
    public List<STEntry> getEntries() {
    	return this.entries;
    }
    
    /**
	 * Visits compilation unit and builds the architecture graph
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTMCCompilationUnit node) {
		SecComponentEntry componentEntry = null;
		
		try {
			//Root element for graph
			componentEntry = (SecComponentEntry) resolver.resolve(node.getType().getName(), ComponentEntry.KIND, getNameSpaceFor(node));
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
		//Create graph from ASTl
		if(componentEntry != null) {
			graphConnectorEdges = ArchitectureGraphBuilder.forArchitecture(componentEntry).buildGraphConnectorEdge();
		}
	}
    
    /**
     * Find paths with critical ports 
     * 
     * @param node
     */
    public void visit(ASTArcPort node) {
    	PortEntry entry = null;
		try {
			entry = (PortEntry) resolver.resolve(node.printName(), PortEntry.KIND, getNameSpaceFor(node));
			if(entry == null) {
				return;
			}
		} catch (AmbigousException e) {
			// not checked here
			e.printStackTrace();
		}
    	
    	if(AnalysisHelper.isPortBenningOfPath(entry, graphConnectorEdges) != null) {
    		return;
    	}
    	
    	//Entries of a path
    	List<STEntry> pathEntries = new ArrayList<STEntry>();
    	
    	//Decides if a path is saved or not
    	boolean hasCriticalPort = false;
    	
    	//Current entry
    	STEntry pathEntry = null;
    	
    	//Beginning of path
    	Vertex<PortEntry> portVertex = Vertex.of(entry);
		
		//Look for paths with port as beginning
		GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new DepthFirstIterator<Vertex<? extends STEntry>, Edge>(graphConnectorEdges.getRawGraph(), portVertex);
		
		//Loop over all path
		while(iterator.hasNext()) {			
			pathEntry = iterator.next().getArchitectureElement();
			
			//Path ends
			if(pathEntry.equals(entry) || !iterator.hasNext()) {
				//If the path has a critical port, save all element
				if(hasCriticalPort) {
					this.entries.addAll(pathEntries);
					this.entries.add(pathEntry);
				}
				//preparing for next path
				pathEntries.clear();
				hasCriticalPort = false;
			}
			
			//Save current element
			pathEntries.add(pathEntry);
			
			//Checks if path has a critical port
			if(pathEntry instanceof PortEntry && ((SecPortEntry) pathEntry).isCritical()) {
				hasCriticalPort = true;
			}
		}
    	
    }
    
    /**
     * Analysis which is the namespace a node is in.
     * 
     * @param n Node to be checked.
     * @return Namespace the given node is in.
     */
    public NameSpace getNameSpaceFor(ASTNode n) {
      if (nodesToNameSpaces != null) {
        ASTNode curr = n;
        while (curr != null) {
          NameSpace nsp = nodesToNameSpaces.get(curr);
          if (nsp != null) {
            return nsp;
          }
          curr = curr.get_Parent();
        }
      }
      return null;
    }
	
}
