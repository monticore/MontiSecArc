/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.trustlevel;

import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AmbigousException;
import interfaces2.resolvers.Resolver;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Optional;


import secarc._ast.ASTSecArcFilterComponent;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelEntry;
import mc.ast.ASTNode;
import mc.ast.ConcreteVisitor;
import mc.umlp.arcd.ets.entries.ComponentEntry;

/**
 * 
 * Transformation visitor for what if analysis with trustlevel -1
 * Changes trustlevel of referenced components to -1
 *
 *
 */
public class AnalysisTrustlevelTransformationVisitor extends ConcreteVisitor {

	/**
	 * Parameters with referenced components
	 */
	private String analysisParameter;
	
	/**
	 * Resolver for entries
	 */
	protected Resolver resolver;
	
	/**
	 * Namespace for nodes
	 */
	protected Map<ASTNode, NameSpace> nodesToNameSpaces;
	
	/**
	 * Saves original trustlevel of components
	 */
	protected Map<String, TrustlevelEntry> actualTrustlevels = new HashMap<String, TrustlevelEntry>();
	
	/**
	 * Decides, if the trustlevel is set to -1 or to the original value
	 */
	protected boolean reverse = false;
	
	/**
	 * default constructor
	 */
    public AnalysisTrustlevelTransformationVisitor() {
    	super();
    }
    
    /**
     * 
     * @param analysisParameter Parameters with referenced components
     */
    public void setAnalysisParameter(String analysisParameter) {
		this.analysisParameter = analysisParameter;
	}
    
    /**
     * 
     * @param actualTrustlevels Original trustlevels
     */
    public void setActualTrustlevel(Map<String, TrustlevelEntry> actualTrustlevels) {
    	this.actualTrustlevels = actualTrustlevels;
    }
    
    /**
     * 
     * @return Original trustlevels
     */
    public Map<String, TrustlevelEntry> getActualTrustlevel() {
    	return this.actualTrustlevels;
    }
    
    /**
     * 
     * @param reverse Set trustlevel to -1 or to original value
     */
    public void isReverse(boolean reverse) {
    	this.reverse = reverse;
    }
    
    /**
     * Visits components and changes trustlevel
     * 
     * @param node ASTNode to visit
     */
    public void visit(ASTSecArcFilterComponent node) {
    	ComponentEntry compEntry = null;
    	try {
			compEntry = (ComponentEntry) resolver.resolve(node.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
			if(compEntry == null) {
				return;
			}
		} catch (AmbigousException e) {
			// not checked here
			e.printStackTrace();
		}
    	
    	/**
    	 * Checks if the trustlevel has to be changed
    	 */
    	if(analysisParameter.equals(compEntry.getName())) {
    		Optional<TrustlevelEntry> trustlevel = ((SecComponentEntry) compEntry).getTrustlevel();
    		TrustlevelEntry value = null;
    		//Set original trustlevel
    		if(reverse) {
    			//A trustlevel exists
    			if(actualTrustlevels.get(compEntry.getName()) != null) {
    				value = trustlevel.get();
    				value.setValue(actualTrustlevels.get(compEntry.getName()).getValue());
    				value.setRelative(actualTrustlevels.get(compEntry.getName()).isPositive());
    			} else {
    				((SecComponentEntry) compEntry).setTrustlevel(null);
    			}
    		} else {
    			//Set trustlevel to -1
	    		if(trustlevel.isPresent()) {
	    			value = trustlevel.get();
	    			//Save original value
	    			actualTrustlevels.put(compEntry.getName(), new TrustlevelEntry(value.getValue(), value.isPositive()));
	    		} else {
	    			//trustlevel does not exist. new trustlevel
	    			value = new TrustlevelEntry();
					((SecComponentEntry) compEntry).setTrustlevel(value);
	    		}
	    		
	    		value.setValue(1);
				value.setRelative(false);
    		}
    	}
    }
    
    /**
     * 
     * @param resolver Resolver for entries
     */
    public void setResolver(Resolver resolver) {
    	this.resolver = resolver;
    }
    
    /**
     * 
     * @param nodesToNameSpaces Namespace for nodes
     */
    public void setNodesToNameSpaces(Map<ASTNode, NameSpace> nodesToNameSpaces) {
    	this.nodesToNameSpaces = nodesToNameSpaces;
    }
    
    /**
     * Analysis which is the namespace a node is in.
     * 
     * @param n Node to be checked.
     * @return Namespace the given node is in.
     */
    public NameSpace getNameSpaceFor(ASTNode n) {
      if (nodesToNameSpaces != null) {
        ASTNode curr = n;
        while (curr != null) {
          NameSpace nsp = nodesToNameSpaces.get(curr);
          if (nsp != null) {
            return nsp;
          }
          curr = curr.get_Parent();
        }
      }
      return null;
    }
	
}
