/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.trustlevel;

import interfaces2.EtsErrorCode;
import interfaces2.Interfaces2Constants;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.Resolver;

import java.util.Map;

import mc.DSLWorkflow;
import mc.ast.ASTNode;
import mc.ast.InheritanceVisitor;
import secarc._tool.MontiSecArcRoot;

/**
 * 
 * Transformation workflow for what if analysis with parameter and trustlevel -1 
 *
 *
 */
public class AnalysisTrustlevelTransformationWorkflow<T extends MontiSecArcRoot> extends DSLWorkflow<T> {

	/**
	 * Parameter with referenced components for what if analysis
	 */
	private String analysisParameter;
	
	/**
     * @param responsibleClass class of the dsl root
     */
    public AnalysisTrustlevelTransformationWorkflow(final Class<T> responsibleClass) {
        super(responsibleClass);
    }
    
    /*
     * (non-Javadoc)
     * @see mc.DSLWorkflow#run(mc.DSLRoot)
     */
    @Override
    public void run(MontiSecArcRoot dslroot) {
    	Map<ASTNode, NameSpace> nodesToNameSpaces = (Map<ASTNode, NameSpace>) dslroot.getAnnotation(Interfaces2Constants.NODES_TO_NAME_SPACE_ID);
    	
    	Resolver resolver = (Resolver) dslroot.getAnnotation(Interfaces2Constants.RESOLVER_ID);
    	
    	if (dslroot.hasFatalError()) {
			dslroot.getErrorDelegator()
					.addInfo(
							"0xA2040 CheckWorkflow can't be executed because of fatal errors",
							EtsErrorCode.ResolvingFailed);
			return;
		}

		if (resolver == null) {
			dslroot.getErrorDelegator()
					.addErrorToCurrentResource(
							"0xA0015 CheckWorkflow can't be executed: Resolver wasn't set",
							EtsErrorCode.ResolvingFailed);
			return;
		}
    	
    	//Transform entries
		AnalysisTrustlevelTransformationVisitor visitor = new AnalysisTrustlevelTransformationVisitor();
    	visitor.setAnalysisParameter(analysisParameter);
    	visitor.setResolver(resolver);
    	visitor.setNodesToNameSpaces(nodesToNameSpaces); 
    	visitor.isReverse(false);
    	
        InheritanceVisitor.run(visitor, dslroot.getAst());
        
    }
    
    public void setAnalysisParameter(String analysisParameter) {
    	this.analysisParameter = analysisParameter;
    }
	
}
