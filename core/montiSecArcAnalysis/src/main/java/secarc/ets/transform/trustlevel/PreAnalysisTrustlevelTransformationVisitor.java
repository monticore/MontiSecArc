/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.trustlevel;

import interfaces2.coco.ContextCondition;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AmbigousException;
import interfaces2.resolvers.Resolver;
import interfaces2.workflows.CheckWorkflowClient;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import secarc.ets.analysis.checker.ISecAnalysisParameterChecker;

import mc.MCG;
import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;

public class PreAnalysisTrustlevelTransformationVisitor extends CheckWorkflowClient {

	/**
	 * Saves names of components for what if analysis
	 */
	private List<String> analysisParameter;
	
	protected Collection<ContextCondition> enabledConditions;
	
	/**
	 * Analysis for parameter
	 */
	protected Set<ISecAnalysisParameterChecker> analysisParameterChecker;
	
	
	public PreAnalysisTrustlevelTransformationVisitor() {
		analysisParameterChecker = new HashSet<ISecAnalysisParameterChecker>();
	}
	
	/**
	 * Enabled analysis for supergrammars and MontiSecArc
	 * 
	 * @param enabledAnalysis: Enabled analysis
	 */
	@Override
	public void setEnabledConditions(Collection<ContextCondition> enabledAnalysis) {
		super.setEnabledConditions(enabledAnalysis);
		createUsefulCollections();
	}
	
	/**
	 * Puts enabled context conditions and analysis in the corresponding sets
	 */
	protected void createUsefulCollections() {
		for(ContextCondition coco : this.getEnabledConditions()) {
			if(coco instanceof ISecAnalysisParameterChecker) {
				analysisParameterChecker.add((ISecAnalysisParameterChecker) coco);
			}
		}
	}
	
	/**
	 * Analysis parameter for what if trustlevel analysis
	 * 
	 * @param analysisParameter
	 */
	public void setAnalysisParameter(List<String> analysisParameter) {
		this.analysisParameter = analysisParameter;
	}
	
	/**
	 * Visits compilation unit and builds the architecture graph
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTMCCompilationUnit node) {
		// Check if analysis parameter existiert
		if (analysisParameter != null) {
			for (String parameter : analysisParameter) {
				try {
					for (ISecAnalysisParameterChecker cc : analysisParameterChecker) {
						cc.check(node, parameter);
					}
				} catch (AmbigousException e) {
					// not checked here
					MCG.getLogger().info(e.getMessage());
				}
			}
		}
	}
	
	/**
     * 
     * @param resolver Resolver for entries
     */
    public void setResolver(Resolver resolver) {
    	this.resolver = resolver;
    }
    
    /**
     * 
     * @param nodesToNameSpaces Namespace for nodes
     */
    public void setNodesToNameSpaces(Map<ASTNode, NameSpace> nodesToNameSpaces) {
    	this.nodesToNameSpaces = nodesToNameSpaces;
    }
	
}
