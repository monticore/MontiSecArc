/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform.trustlevel;

import interfaces2.EtsErrorCode;
import interfaces2.Interfaces2Constants;
import interfaces2.coco.AbstractContextCondition;
import interfaces2.coco.ContextCondition;
import interfaces2.loaders.STEntryDeserializer;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.Resolver;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import mc.DSLWorkflow;
import mc.ProblemReport;
import mc.ast.ASTNode;
import mc.ast.IVisitor;
import mc.ast.MultiInheritanceVisitor;
import secarc._tool.MontiSecArcRoot;

public class PreAnalysisTrustlevelTransformationWorkflow<T extends MontiSecArcRoot> extends DSLWorkflow<T> {

	/**
	 * Parameter with referenced components for what if analysis
	 */
	private List<String> analysisParameter;
	
	/**
	 * Analysis are static analysis such as contextconditions
	 */
	protected AbstractContextCondition analysis;
	
	/**
	 * Configuration for analysis
	 */
	protected Map<String, ProblemReport.Type> analysisConfiguration;
	
	/**
     * @param responsibleClass class of the dsl root
     */
    public PreAnalysisTrustlevelTransformationWorkflow(final Class<T> responsibleClass) {
        super(responsibleClass);
    }
    
    /**
	 * 
	 * @param analysisConfiguration configuration for analysis
	 */
	public void setAnalysisConfiguration(
			Map<String, ProblemReport.Type> analysisConfiguration) {
		this.analysisConfiguration = analysisConfiguration;
	}
	
	/**
	 * 
	 * @return analysisConfiguration configuration for analysis
	 */
	public Map<String, ProblemReport.Type> getAnalysisConfiguration() {
		return analysisConfiguration;
	}
    
    /**
	 * 
	 * @param analysis static analysis for AST/Graph
	 */
	public void setAnalysis(AbstractContextCondition analysis) {
		this.analysis = analysis;
	}
	
	/**
	 * 
	 * @return analysis static analysis for AST/Graph
	 */
	public AbstractContextCondition getAnalysis() {
		return analysis;
	}
    
    /*
     * (non-Javadoc)
     * @see mc.DSLWorkflow#run(mc.DSLRoot)
     */
    @Override
    public void run(MontiSecArcRoot dslroot) {
    	if (dslroot.hasFatalError()) {
			return;
		}

		Map<NameSpace, ASTNode> nameSpacesToNodes = (Map<NameSpace, ASTNode>) dslroot.getAnnotation(Interfaces2Constants.NAME_SPACE_TO_NODES_ID);
		Map<ASTNode, NameSpace> nodesToNameSpaces = (Map<ASTNode, NameSpace>) dslroot.getAnnotation(Interfaces2Constants.NODES_TO_NAME_SPACE_ID);

		Collection<STEntryDeserializer> set = (Collection<STEntryDeserializer>) dslroot.getAnnotation(Interfaces2Constants.DESERIALIZER_ID);

		Resolver resolver = (Resolver) dslroot.getAnnotation(Interfaces2Constants.RESOLVER_ID);

		NameSpace topLevelNameSpace = (NameSpace) dslroot.getAnnotation(Interfaces2Constants.NAME_SPACE_ID);
    	
    	if (dslroot.hasFatalError()) {
			dslroot.getErrorDelegator()
					.addInfo(
							"0xA2040 CheckWorkflow can't be executed because of fatal errors",
							EtsErrorCode.ResolvingFailed);
			return;
		}

		if (resolver == null) {
			dslroot.getErrorDelegator()
					.addErrorToCurrentResource(
							"0xA0015 CheckWorkflow can't be executed: Resolver wasn't set",
							EtsErrorCode.ResolvingFailed);
			return;
		}
		
		IVisitor v = new MultiInheritanceVisitor();
		//initialize visitor
		//Transform entries
		PreAnalysisTrustlevelTransformationVisitor visitor = new PreAnalysisTrustlevelTransformationVisitor();
		visitor.setAnalysisParameter(analysisParameter);
	    //Parameter for what if trustlevel analysis
		
		visitor.setDelegator(dslroot.getErrorDelegator());
		visitor.setDeserializers(set);
		visitor.setLoader(dslroot.getModelInfrastructureProvider().getModelloader());
		visitor.setNameSpacesToNodes(nameSpacesToNodes);
		visitor.setNodesToNameSpaces(nodesToNameSpaces);
		visitor.setResolver(resolver);
	    visitor.setTopLevelNameSpace(topLevelNameSpace);

		if (getAnalysis() != null) {
			getAnalysis().resetConfiguration();
			getAnalysis().setConfiguration(getAnalysisConfiguration());
			Collection<ContextCondition> enabled = getAnalysis().getEnabledConditions();
			for (ContextCondition con : enabled) {
				con.setDelegator(dslroot.getErrorDelegator());
				con.setDeserializers(set);
				con.setLoader(dslroot.getModelInfrastructureProvider()
						.getModelloader());
				con.setResolver(resolver);
				con.setNodes2namesSpace(nodesToNameSpaces);
			}
			if (visitor.getEnabledConditions() == null
					|| visitor.getEnabledConditions().isEmpty()) {
				visitor.setEnabledConditions(enabled);
			}
		}

		v.addClient(visitor);
		

		v.startVisit((ASTNode) dslroot.getAst());
        
    }
    
    public void setAnalysisParameter(List<String> analysisParameter) {
    	this.analysisParameter = analysisParameter;
    }
	
}
