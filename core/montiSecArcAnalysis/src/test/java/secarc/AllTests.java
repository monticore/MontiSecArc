/* (c) https://github.com/MontiCore/monticore */
package secarc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MontiSecArcAnalysisTest.class,
		MontiSecArcAnalysisTransformationCriticalPortTest.class,
		MontiSecArcAnalysisTransformationTrustlevelTest.class,
		MontiSecArcAnalysisWhatIfCiritcalPortTest.class,
		MontiSecArcAnalysisWhatIfCiritcalPortTrustlevelTest.class,
		MontiSecArcAnalysisWhatIfTrustlevelTest.class })
public class AllTests {

}
