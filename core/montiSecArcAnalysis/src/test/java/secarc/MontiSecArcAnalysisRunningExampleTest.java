/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mc.dsltool.AmbigousException;
import mc.dsltool.CircluarDependencyException;

public class MontiSecArcAnalysisRunningExampleTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool>   {

	public MontiSecArcAnalysisRunningExampleTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Test for Running Example in MontiSecArc with Analysis
	 * @throws AmbigousException 
	 * @throws CircluarDependencyException  
	 */
	@Test
	public void testSecRunningExample() throws CircluarDependencyException, AmbigousException {
		MontiSecArcAnalysisTestTool tool = createTestToolWithAdditionalParameters(new String[] {"src/test/resources/secarc/seccds"}, new String[] {"src/test/resources"});
		tool.init();
		
		assertTrue(tool.run());
		
	}
	
	/**
	 * Test only path for Running Example in MontiSecArc with Analysis
	 * @throws AmbigousException 
	 * @throws CircluarDependencyException  
	 */
	@Test
	public void testSecRunningExamplePath() throws CircluarDependencyException, AmbigousException {
		MontiSecArcAnalysisTestTool tool = createTestToolWithAdditionalParameters(new String[] {"src/test/resources/secarc/seccdsE/CashDeskSystem.secarc"}, new String[] {"src/test/resources"});
		tool.init();
		
		assertTrue(tool.run());
		
	}
		
	/**
	 * Test only filter for Running Example in MontiSecArc with Analysis
	 * @throws AmbigousException 
	 * @throws CircluarDependencyException  
	 */
	@Test
	public void testSecRunningExampleFilter() throws CircluarDependencyException, AmbigousException {
		MontiSecArcAnalysisTestTool tool = createTestToolWithAdditionalParameters(new String[] {"src/test/resources/secarc/seccdsF/CashDeskSystem.secarc"}, new String[] {"src/test/resources"});
		tool.init();
		
		assertTrue(tool.run());
		
	}
}
