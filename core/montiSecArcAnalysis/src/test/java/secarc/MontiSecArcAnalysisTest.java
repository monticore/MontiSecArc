/* (c) https://github.com/MontiCore/monticore */

package secarc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import mc.ProblemReport;

import org.junit.Test;

import secarc.error.MontiSecArcAnalysisErrorCodes;


/**
 * Test for @{MontiSecArcAnalysisTool}.
 * 
 * 
 */
public class MontiSecArcAnalysisTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool> {

	/**
     * 
     */
	public MontiSecArcAnalysisTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}

	/**
	 * Tests for Connetors
	 */
	@Test
	public void testConnector() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListIncomingtPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.EncryptedPathEndInLowTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(4, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second file
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("EncryptedPathWithLowerTrustlevel.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathWithLowerTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Third file
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("EncryptedPathWithUnencryptedPart.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathWithUnencryptedPart.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		assertEquals(8, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Fourth file
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("UnencryptedPathThroughLowTrustlevel.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/connector/UnencryptedPathThroughLowTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
	}
	
	/**
	 * Test for filters
	 */
	@Test
	public void testFilter() {
		//First file
		System.out.println("FilterComponent.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/filter/FilterComponent.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListFilters);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.UnencryptedConnectorThroughLowTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.FilterWithHigherTrust);

        assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
	    //Second file
	    handler.getWarnings().clear();
	    errorCodes.clear();
	    System.out.println("FilterPort.secarc");
	    tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/filter/FilterPort.secarc" });
	    tool.init();
	    assertTrue(tool.run());
		
		assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Third file
	    handler.getWarnings().clear();
	    errorCodes.clear();
	    System.out.println("FilterWithLowerTrustlevel.secarc");
	    tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/filter/FilterWithLowerTrustlevel.secarc" });
	    tool.init();
	    assertTrue(tool.run());
		
		assertEquals(4, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
      	//Forth file
	    handler.getWarnings().clear();
	    errorCodes.clear();
	    System.out.println("InputString.secarc");
	    tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/filter/InputString.secarc" });
	    tool.init();
	    assertTrue(tool.run());
		
		assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
		
	}
	
	/**
	 * Test for configurations
	 */
	@Test
	public void testConfiguration() {
		//First File
		System.out.println("ConfigurationNotReviewed.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/configuration/ConfigurationNotReviewed.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ReviewedConfiguration);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("ConfigurationReviewed.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/configuration/ConfigurationReviewed.secarc" });
        tool.init();
		assertTrue(tool.run());
		
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for roles
	 */
	@Test
	public void testRole() {
		//First file
		System.out.println("DerivedThirdPartyRoles.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/role/DerivedThirdPartyRoles.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesThirdParty);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesComponent);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.IdentityWithEncryption);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesPort);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.RoleAccess);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(12, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("RoleAccessMultipleComponents.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/role/RoleAccessMultipleComponents.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(10, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for ports
	 */
	@Test
	public void testPort() {
		//First file
		System.out.println("CriticalPort.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/port/CriticalPort.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListIncomingtPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListOutgoingPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListAllCriticalPorts);

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IncomingPort.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/port/IncomingPort.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("OutgoingPort.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/port/OutgoingPort.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for identity
	 */
	@Test
	public void testIdentity() {
		//First file
		System.out.println("IdentityWithEncryption.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithEncryption.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.IdentityWithEncryption);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesComponent);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesPort);		
		errorCodes.add(MontiSecArcAnalysisErrorCodes.RoleAccess);

        assertEquals(6, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IdentityWithoutConnector.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithoutConnector.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(5, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Third File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IdentityWithoutRoles.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithoutRoles.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Fourth File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IdentityWithoutEncryption.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithoutEncryption.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(8, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Fifth File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("TrustlevelClientServerWrong.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/identity/TrustlevelClientServerWrong.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(8, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }	
	}
	
	/**
	 * Test for trustlevel
	 */
	@Test
	public void testTrustlevel() {
		//First file
		System.out.println("DerivedTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevel/DerivedTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ReasonDifferingTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedTrustlevel);

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("ReasonForDifferingTrustlevel.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevel/ReasonForDifferingTrustlevel.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Third File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("ReasonDifferingTrustlevelDifferentSign.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevel/ReasonDifferingTrustlevelDifferentSign.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Forth File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("DerivedTrustlevelMoreSteps.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevel/DerivedTrustlevelMoreSteps.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for trustlevel relation
	 */
	@Test
	public void testTrustlevelRelation() {
		//First file
		System.out.println("TrustlevelRelationWithoutTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevelrelation/TrustlevelRelationWithoutTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
			
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.TrustlevelForTrustlevelrelation);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedTrustlevel);

		assertEquals(4, handler.getWarnings().size());
		for(ProblemReport error : handler.getErrors()) {
			assertTrue(errorCodes.contains(error.getErrorcode()));
		}
		
		//Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("TrustlevelRelationWithTrustlevel.secarc");
        tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevelrelation/TrustlevelRelationWithTrustlevel.secarc" });
        tool.init();
		assertTrue(tool.run());

        assertEquals(1, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
}
