/* (c) https://github.com/MontiCore/monticore */
package secarc;

import java.util.List;

import mc.DSLRoot;
import mc.test.helpers.TestErrorHandler;

/**
 * Tool for {@link MontiSecArcAnalysis}.
 * 
 * 
 */
public class MontiSecArcAnalysisTestTool extends MontiSecArcAnalysisTool {

	public MontiSecArcAnalysisTestTool(String[] arguments, List<String> analysisParameter) {
		super(arguments, analysisParameter);
		this.init();
	}
	
	public MontiSecArcAnalysisTestTool(String[] arguments, String analysisParameter, String analysisPath) {
		super(arguments, analysisParameter, analysisPath);
		this.init();
	}
    
	/*
	 * (non-Javadoc)
	 * @see interfaces2.language.ETSTool#init()
	 */
    @Override
    public void init() {
        super.init();
        this.addErrorHandler(TestErrorHandler.getInstance());
    }

    /**
     * @param string
     * @return
     */
    public DSLRoot<?> getRootByName(String filename) {
        return super.getModelloader().getRootByFileName(filename);
    }

}
