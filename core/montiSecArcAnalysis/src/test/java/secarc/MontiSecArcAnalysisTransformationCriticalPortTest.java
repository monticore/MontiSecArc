/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import secarc._tool.MontiSecArcRoot;

public class MontiSecArcAnalysisTransformationCriticalPortTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool> {

	/**
     * 
     */
	public MontiSecArcAnalysisTransformationCriticalPortTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Test if the transformation for critical ports is correct 
	 */
	@Test
	public void testCriticalPortTransformationWithoutCriticalPort() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		//Without Transformation
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		MontiSecArcRoot root= (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel");
		
		//With Transformation
		MontiSecArcAnalysisTestTool toolTransformation = createTestToolWithoutJavaWhatIfCiritcalPort(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" });
		toolTransformation.init();
		assertTrue(toolTransformation.run());
		
		MontiSecArcRoot rootTransformed = (MontiSecArcRoot) initSymtabForRoot(toolTransformation, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel");
		if(!root.getAst().deepEquals(rootTransformed.getAst())){
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}
	
	/**
	 * Test if the transformation for critical ports is correct 
	 */
	@Test
	public void testCriticalPortTransformationWithCriticalPort() {
		//First file
		System.out.println("CriticalPort.secarc");
		//Without Transformation
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/criticalport/CriticalPort.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		MontiSecArcRoot root= (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.criticalport.CriticalPort");
		
		//With Transformation
		MontiSecArcAnalysisTestTool toolTransformation = createTestToolWithoutJavaWhatIfCiritcalPort(new String[] { "src/test/resources/secarc/analysis/criticalport/CriticalPort.secarc" });
		toolTransformation.init();
		assertTrue(toolTransformation.run());
		
		MontiSecArcRoot rootTransformed = (MontiSecArcRoot) initSymtabForRoot(toolTransformation, "secarc.analysis.criticalport.CriticalPort");
		if(!root.getAst().deepEquals(rootTransformed.getAst())){
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}
	
}
