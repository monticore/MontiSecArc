/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AmbigousException;

import mc.MCG;
import mc.ast.ASTNode;
import mc.ast.InheritanceVisitor;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;

import org.junit.Test;

import secarc._tool.MontiSecArcRoot;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.transform.trustlevel.AnalysisTrustlevelTransformationVisitor;

public class MontiSecArcAnalysisTransformationTrustlevelTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool> {

	/**
     * 
     */
	public MontiSecArcAnalysisTransformationTrustlevelTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Test if parameter exists
	 */
	@Test
	public void testParameterExistence() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		String parameter = "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel.TargetHelp";
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" }, parameter);
		tool.init();
		assertTrue(tool.run());
		MontiSecArcRoot root = (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel");
		
		ComponentEntry entry = null;
		for(ASTNode child : root.getAst().get_Children()) {
			if(child instanceof ASTArcComponent) {
				try {
					entry = existComponent(parameter, getNameSpaceFor(child));
					break;
				} catch (AmbigousException e) {
					// not checked here
		            MCG.getLogger().info(e.getMessage());
				}
			}
		}
		if(entry != null) {
			assertEquals(true, ((SecComponentEntry) entry).getTrustlevel().isPresent());
			assertEquals(1, ((SecComponentEntry) entry).getTrustlevel().get().getValue());
			assertEquals(false, ((SecComponentEntry) entry).getTrustlevel().get().isPositive());
		}

		assertEquals(5, handler.getWarnings().size());
	}
	
	/**
	 * Test if parameter exists
	 */
	@Test
	public void testPreParameterExistence() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		String parameter = "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel.TargetHelp";
		List<String> parameters = new ArrayList<String>();
		parameters.add(parameter);
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaPreTrustlevel("src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc", parameters);
		tool.init();
		assertTrue(tool.run());
	}
	
	/**
	 * Test if parameter exists with not existing trustlevel
	 */
	@Test
	public void testParameterExistenceNotExistingTrustlevel() {
		//First file with existing trustlevel
		System.out.println("DerivedTrustlevel.secarc");
		String parameter = "secarc.analysis.trustlevel.DerivedTrustlevel.Help";
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/trustlevel/DerivedTrustlevel.secarc" }, parameter);
		tool.init();
		assertTrue(tool.run());
		MontiSecArcRoot root = (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.trustlevel.DerivedTrustlevel");
		
		ComponentEntry entry = null;
		for(ASTNode child : root.getAst().get_Children()) {
			if(child instanceof ASTArcComponent) {
				try {
					entry = existComponent(parameter, getNameSpaceFor(child));
					break;
				} catch (AmbigousException e) {
					// not checked here
		            MCG.getLogger().info(e.getMessage());
				}
			}
		}
		if(entry != null) {
			assertEquals(true, ((SecComponentEntry) entry).getTrustlevel().isPresent());
			assertEquals(1, ((SecComponentEntry) entry).getTrustlevel().get().getValue());
			assertEquals(false, ((SecComponentEntry) entry).getTrustlevel().get().isPositive());
		}

		assertEquals(2, handler.getWarnings().size());
	}
	
	/**
	 * Test if parameter exists with existing trustelvel
	 */
	@Test
	public void testTranformationWithParameterExistenceExistingTrustlevel() {
		//First file with existing trustlevel
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		String parameter = "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel.TargetHelp";
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		MontiSecArcRoot root = (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel");
		
		//Transform entries
		AnalysisTrustlevelTransformationVisitor visitor = new AnalysisTrustlevelTransformationVisitor();
		visitor.setResolver(resolver);
		visitor.setNodesToNameSpaces(nodesToNameSpaces);
		visitor.isReverse(false);
		visitor.setAnalysisParameter(parameter);
		    	
		InheritanceVisitor.run(visitor, root.getAst());
		
		ComponentEntry entry = null;
		for(ASTNode child : root.getAst().get_Children()) {
			if(child instanceof ASTArcComponent) {
				try {
					entry = existComponent(parameter, getNameSpaceFor(child));
					break;
				} catch (AmbigousException e) {
					// not checked here
		            MCG.getLogger().info(e.getMessage());
				}
			}
		}
		if(entry != null) {
			assertEquals(true, ((SecComponentEntry) entry).getTrustlevel().isPresent());
			assertEquals(1, ((SecComponentEntry) entry).getTrustlevel().get().getValue());
			assertEquals(false, ((SecComponentEntry) entry).getTrustlevel().get().isPositive());
		}

		assertEquals(4, handler.getWarnings().size());
	}
	
	/**
	 * Test if parameter exists with existing trustelvel which does not exist
	 */
	@Test
	public void testTranformationWithParameterExistenceNotExistingTrustlevel() {
		//First file with existing trustlevel
		System.out.println("DerivedTrustlevel.secarc");
		String parameter = "secarc.analysis.trustlevel.DerivedTrustlevel.Help";
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/trustlevel/DerivedTrustlevel.secarc" });
		tool.init();
		assertTrue(tool.run());
		MontiSecArcRoot root = (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.trustlevel.DerivedTrustlevel");
		
		//Transform entries
		AnalysisTrustlevelTransformationVisitor visitor = new AnalysisTrustlevelTransformationVisitor();
		List<String> parameters = new ArrayList<String>();
		parameters.add(parameter);
		visitor.setResolver(resolver);
		visitor.setNodesToNameSpaces(nodesToNameSpaces);
		visitor.isReverse(false);
		visitor.setAnalysisParameter(parameter);
		    	
		InheritanceVisitor.run(visitor, root.getAst());
		
		ComponentEntry entry = null;
		for(ASTNode child : root.getAst().get_Children()) {
			if(child instanceof ASTArcComponent) {
				try {
					entry = existComponent(parameter, getNameSpaceFor(child));
					break;
				} catch (AmbigousException e) {
					// not checked here
		            MCG.getLogger().info(e.getMessage());
				}
			}
		}
		if(entry != null) {
			assertEquals(true, ((SecComponentEntry) entry).getTrustlevel().isPresent());
			assertEquals(1, ((SecComponentEntry) entry).getTrustlevel().get().getValue());
			assertEquals(false, ((SecComponentEntry) entry).getTrustlevel().get().isPositive());
		}

		assertEquals(2, handler.getWarnings().size());
	}
	
	/**
	 * Test if parameter do not exist
	 */
	@Test
	public void testWrongParameterExistence() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		String parameter = "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel.TargetHel";
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" }, parameter);
		tool.init();
		assertTrue(tool.run());
		MontiSecArcRoot root = (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel");
		
		ComponentEntry entry = null;
		for(ASTNode child : root.getAst().get_Children()) {
			if(child instanceof ASTArcComponent) {
				try {
					entry = existComponent(parameter, getNameSpaceFor(child));
					break;
				} catch (AmbigousException e) {
					// not checked here
		            MCG.getLogger().info(e.getMessage());
				}  
			}
		}
		assertEquals(null, entry);

		assertEquals(4, handler.getWarnings().size());
	}
	
	protected ComponentEntry existComponent(String parameter, NameSpace np) throws AmbigousException {
		ComponentEntry entry = (ComponentEntry) resolver.resolve(parameter, ComponentEntry.KIND, np);
		if(entry != null) {
			return entry;
		} else {
			for(NameSpace npChild : np.getChildren()) {
				if(existComponent(parameter, npChild) != null) {
					return entry;
				}
			}
		}
		return null;
	}
	
}
