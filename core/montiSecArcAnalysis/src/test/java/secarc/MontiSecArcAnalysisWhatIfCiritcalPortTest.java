/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import mc.ast.InheritanceVisitor;
import mc.umlp.common._ast.UMLPNode;

import org.junit.Test;

import secarc._ast.ASTArcComponent;
import secarc._tool.MontiSecArcRoot;
import secarc.ets.transform.criticalport.AnalysisCriticalPortTransformationVisitor;
import secarc.ets.transform.criticalport.PreAnalysisCriticalPortTransformationVisitor;

public class MontiSecArcAnalysisWhatIfCiritcalPortTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool> {

	/**
     * 
     */
	public MontiSecArcAnalysisWhatIfCiritcalPortTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Test preTransformation without a critical port
	 */
	@Test
	public void testPreTranformationCriticalPortWithoutCriticalPort() {
		//First File
		System.out.println("ConfigurationNotReviewed.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/configuration/ConfigurationNotReviewed.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		MontiSecArcRoot root= (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.configuration.ConfigurationNotReviewed");
		
		//Elements which are needed for analysis
    	PreAnalysisCriticalPortTransformationVisitor preVisitor = new PreAnalysisCriticalPortTransformationVisitor();
    	preVisitor.setResolver(resolver);
    	preVisitor.setNodesToNameSpaces(nodesToNameSpaces);
    	
        InheritanceVisitor.run(preVisitor, root.getAst());
		
		assertEquals(0, preVisitor.getEntries().size()); 
	}
	
	/**
	 * Test preTransformation with a critical port
	 */
	@Test
	public void testPreTranformationCriticalPortWithCriticalPort() {
		//First File
		System.out.println("CriticalPort.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/criticalport/CriticalPort.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		MontiSecArcRoot root= (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.criticalport.CriticalPort");
		
		//Elements which are needed for analysis
    	PreAnalysisCriticalPortTransformationVisitor preVisitor = new PreAnalysisCriticalPortTransformationVisitor();
    	preVisitor.setResolver(resolver);
    	preVisitor.setNodesToNameSpaces(nodesToNameSpaces);
    	
        InheritanceVisitor.run(preVisitor, root.getAst());
		
		assertEquals(4, preVisitor.getEntries().size()); 
	}
	
	/**
	 * Test Transformation with a critical port
	 */
	@Test
	public void testTranformationCriticalPortWithCriticalPort() {
		//First File
		System.out.println("CriticalPort.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/analysis/criticalport/CriticalPort.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		MontiSecArcRoot root = (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.criticalport.CriticalPort");
		
		//Elements which are needed for analysis
    	PreAnalysisCriticalPortTransformationVisitor preVisitor = new PreAnalysisCriticalPortTransformationVisitor();
    	preVisitor.setResolver(resolver);
    	preVisitor.setNodesToNameSpaces(nodesToNameSpaces);
    	
        InheritanceVisitor.run(preVisitor, root.getAst());
		
		assertEquals(4, preVisitor.getEntries().size()); 
		
		//Delete Elements which are not needed
        if(preVisitor.getEntries() != null) {
	        AnalysisCriticalPortTransformationVisitor visitorTransform = new AnalysisCriticalPortTransformationVisitor();
	        visitorTransform.setResolver(resolver);
	        visitorTransform.setNodesToNameSpaces(nodesToNameSpaces);
	        visitorTransform.setEntries(preVisitor.getEntries());
	        
	        InheritanceVisitor.run(visitorTransform, root.getAst());
	        
	        //Delete all entries
	        for(UMLPNode node : visitorTransform.getRemoveNodes()) {
	        	node.delete();
	        }
        }
        
        assertEquals(0, ((ASTArcComponent) root.getAst().getType()).getSubComponents().size());
	}
	
	/**
	 * Test preTransformation with a critical port
	 */
	@Test
	public void testAnalysisTransformationCriticalPortWithCriticalPort() {
		//First File
		System.out.println("CriticalPort.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfCiritcalPort(new String[] { "src/test/resources/secarc/analysis/criticalport/CriticalPort.secarc" });
		tool.init();
		assertTrue(tool.run());
		
		
		
	}
	
}
