/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import mc.ProblemReport;
import mc.ast.InheritanceVisitor;

import org.junit.Test;

import secarc._tool.MontiSecArcRoot;
import secarc.error.MontiSecArcAnalysisErrorCodes;
import secarc.ets.transform.criticalport.PreAnalysisCriticalPortTransformationVisitor;

public class MontiSecArcAnalysisWhatIfCiritcalPortTrustlevelTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool> {

	/**
     * 
     */
	public MontiSecArcAnalysisWhatIfCiritcalPortTrustlevelTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}
	
	
	/**
	 * Tests for Connetors
	 */
	@Test
	public void testConnectorWhatIf() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfCiritcalPortTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" }, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListIncomingtPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.EncryptedPathEndInLowTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ReasonDifferingTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.TaintTracking);

        assertEquals(1, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
	}
	
	/**
	 * Test preTransformation with a critical port
	 */
	@Test
	public void testPreTranformationCriticalPortWithCriticalPort() {
		//First File
		System.out.println("CriticalPort.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfCiritcalPortTrustlevel(new String[] { "src/test/resources/secarc/analysis/criticalport/CriticalPort.secarc" }, "secarc.analysis.criticalport.CriticalPort");
		tool.init();
		assertTrue(tool.run());
		
		MontiSecArcRoot root= (MontiSecArcRoot) initSymtabForRoot(tool, "secarc.analysis.criticalport.CriticalPort");
		
		//Elements which are needed for analysis
    	PreAnalysisCriticalPortTransformationVisitor preVisitor = new PreAnalysisCriticalPortTransformationVisitor();
    	preVisitor.setResolver(resolver);
    	preVisitor.setNodesToNameSpaces(nodesToNameSpaces);
    	
        InheritanceVisitor.run(preVisitor, root.getAst());
		
		assertEquals(4, preVisitor.getEntries().size()); 
	}
}
