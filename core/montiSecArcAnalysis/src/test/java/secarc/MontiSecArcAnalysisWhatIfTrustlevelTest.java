/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mc.ProblemReport;

import org.junit.Test;

import secarc.error.MontiSecArcAnalysisErrorCodes;

public class MontiSecArcAnalysisWhatIfTrustlevelTest extends TestWithSymtabAnalysis<MontiSecArcAnalysisTestTool> {

	/**
     * 
     */
	public MontiSecArcAnalysisWhatIfTrustlevelTest() {
		super(MontiSecArcAnalysisTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Tests for Connetors
	 */
	@Test
	public void testConnectorWhatIf() {
		//First file
		System.out.println("EncryptedPathEndInLowTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathEndInLowTrustlevel.secarc" }, "secarc.analysis.connector.EncryptedPathEndInLowTrustlevel.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListIncomingtPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.EncryptedPathEndInLowTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ReasonDifferingTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.TaintTracking);

        assertEquals(5, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second file
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("EncryptedPathWithLowerTrustlevel.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathWithLowerTrustlevel.secarc" }, "secarc.analysis.connector.EncryptedPathWithLowerTrustlevel.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Third file
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("EncryptedPathWithUnencryptedPart.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/EncryptedPathWithUnencryptedPart.secarc" }, "secarc.analysis.connector.EncryptedPathWithUnencryptedPart.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		assertEquals(9, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Fourth file
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("UnencryptedPathThroughLowTrustlevel.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/connector/UnencryptedPathThroughLowTrustlevel.secarc" }, "secarc.analysis.connector.UnencryptedPathThroughLowTrustlevel.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		assertEquals(4, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
	}
	
	/**
	 * Test for filters
	 */
	@Test
	public void testFilterWhatIf() {
		//First file
		System.out.println("FilterComponent.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/filter/FilterComponent.secarc" }, "secarc.analysis.filter.FilterComponent.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListFilters);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.UnencryptedConnectorThroughLowTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.FilterWithHigherTrust);

        assertEquals(5, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
	    //Fourth file
	    handler.getWarnings().clear();
	    errorCodes.clear();
	    System.out.println("FilterPort.secarc");
	    tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/filter/FilterPort.secarc" }, "secarc.analysis.filter.FilterPort.TargetHelp");
	    tool.init();
	    assertTrue(tool.run());
		
		assertEquals(5, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
		
	}
	
	/**
	 * Test for configurations
	 */
	@Test
	public void testConfigurationWhatIf() {
		//First File
		System.out.println("ConfigurationNotReviewed.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/configuration/ConfigurationNotReviewed.secarc" }, "secarc.analysis.configuration.ConfigurationNotReviewed.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ReviewedConfiguration);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(3, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("ConfigurationReviewed.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/configuration/ConfigurationReviewed.secarc" }, "secarc.analysis.configuration.ConfigurationReviewed.TargetHelp");
        tool.init();
		assertTrue(tool.run());
		
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for roles
	 */
	@Test
	public void testRole() {
		//First file
		System.out.println("DerivedThirdPartyRoles.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/role/DerivedThirdPartyRoles.secarc" }, "secarc.analysis.role.DerivedThirdPartyRoles.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesThirdParty);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesComponent);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.IdentityWithEncryption);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesPort);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.RoleAccess);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListThirdPartyComponents);

        assertEquals(15, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for ports
	 */
	@Test
	public void testPortWhatIf() {
		//First file
		System.out.println("CriticalPort.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/port/CriticalPort.secarc" }, "secarc.analysis.port.CriticalPort.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListIncomingtPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListOutgoingPorts);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ListAllCriticalPorts);

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IncomingPort.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/port/IncomingPort.secarc" }, "secarc.analysis.port.IncomingPort.TargetHelp");
        tool.init();
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("OutgoingPort.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/port/OutgoingPort.secarc" }, "secarc.analysis.port.OutgoingPort.TargetHelp");
        tool.init();
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for identity
	 */
	@Test
	public void testIdentityWhatIf() {
		//First file
		System.out.println("IdentityWithEncryption.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithEncryption.secarc" }, "secarc.analysis.identity.IdentityWithEncryption.TargetHelp");
		tool.init();
		assertTrue(tool.run());
		
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.IdentityWithEncryption);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesComponent);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedRolesPort);		
		errorCodes.add(MontiSecArcAnalysisErrorCodes.RoleAccess);

        assertEquals(7, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IdentityWithoutEncryption.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithoutConnector.secarc" }, "secarc.analysis.identity.IdentityWithoutConnector.TargetHelp");
        tool.init();
		assertTrue(tool.run());

        assertEquals(5, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
        //Second File
        handler.getWarnings().clear();
        errorCodes.clear();
        System.out.println("IdentityWithoutRoles.secarc");
        tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/identity/IdentityWithoutRoles.secarc" }, "secarc.analysis.identity.IdentityWithoutRoles.TargetHelp");
        tool.init();
		assertTrue(tool.run());

        assertEquals(4, handler.getWarnings().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}

	/**
	 * Test for what if
	 */
	@Test
	public void testTrustlevelWhatIf() {
		// First file
		System.out.println("DerivedTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(
				new String[] { "src/test/resources/secarc/analysis/trustlevel/DerivedTrustlevel.secarc" },
				"secarc.analysis.trustlevel.DerivedTrustlevel.Help");
		tool.init();
		assertTrue(tool.run());

		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.ReasonDifferingTrustlevel);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedTrustlevel);

		assertEquals(2, handler.getWarnings().size());
		for (ProblemReport error : handler.getErrors()) {
			assertTrue(errorCodes.contains(error.getErrorcode()));
		}

		// Second File
		handler.getWarnings().clear();
		errorCodes.clear();
		System.out.println("ReasonForDifferingTrustlevel.secarc");
		tool = createTestToolWithoutJavaWhatIfTrustlevel(
				new String[] { "src/test/resources/secarc/analysis/trustlevel/ReasonForDifferingTrustlevel.secarc" },
				"secarc.analysis.trustlevel.ReasonForDifferingTrustlevel.Help");
		tool.init();
		assertTrue(tool.run());

		assertEquals(2, handler.getWarnings().size());
		for (ProblemReport error : handler.getErrors()) {
			assertTrue(errorCodes.contains(error.getErrorcode()));
		}
	}
	
	/**
	 * Test for trustlevel relation
	 */
	@Test
	public void testTrustlevelRelationWhatIf() {
		//First file
		System.out.println("TrustlevelRelationWithoutTrustlevel.secarc");
		MontiSecArcAnalysisTestTool tool = createTestToolWithoutJavaWhatIfTrustlevel(new String[] { "src/test/resources/secarc/analysis/trustlevelrelation/TrustlevelRelationWithoutTrustlevel.secarc" }, "secarc.analysis.trustlevelrelation.TrustlevelRelationWithoutTrustlevel.Help");
		tool.init();
		assertTrue(tool.run());
			
		List<MontiSecArcAnalysisErrorCodes> errorCodes = new ArrayList<MontiSecArcAnalysisErrorCodes>();
		errorCodes.add(MontiSecArcAnalysisErrorCodes.TrustlevelForTrustlevelrelation);
		errorCodes.add(MontiSecArcAnalysisErrorCodes.DerivedTrustlevel);

		assertEquals(5, handler.getWarnings().size());
		for(ProblemReport error : handler.getErrors()) {
			assertTrue(errorCodes.contains(error.getErrorcode()));
		}
	}
	
}
