/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static mc.umlp.arcd.ToolConstants.ARG_ANALYSIS;
import static mc.umlp.arcd.ToolConstants.ARG_CONF;
import static mc.umlp.arcd.ToolConstants.ARG_MODELPATH;
import static mc.umlp.arcd.ToolConstants.ARG_OUT;
import static mc.umlp.arcd.ToolConstants.ARG_SYMTABDIR;
import static mc.umlp.arcd.ToolConstants.ARG_SYNTHESIS;
import static mc.umlp.arcd.ToolConstants.WF_CREATE_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_INIT_CHECK;
import static mc.umlp.arcd.ToolConstants.WF_INIT_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_PARSE;
import static mc.umlp.arcd.ToolConstants.WF_PRE_CHECK_TRAFO;
import static mc.umlp.arcd.ToolConstants.WF_RUN_CHECK;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import interfaces2.language.ETSTool;
import mc.Parameters;
import mc.ProblemReport.Type;
import mc.umlp.arcd.TestWithSymtab;
import mc.umlp.arcd.ToolConstants;

/**
 * Helper class to setup tests with a symbol table.
 * Extends TestWithSymtab with analysis workflow
 *
 *
 * @param <T>
 */
public class TestWithSymtabAnalysis<T extends ETSTool> extends
		TestWithSymtab<T> {

	public TestWithSymtabAnalysis(final Class<T> toolClass, final String[] symbolFolders) {
		super(toolClass, symbolFolders);
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.TestWithSymtab#createTestToolWithoutJava(java.lang.String[], java.lang.String[])
	 */
	@Override
    protected T createTestToolWithoutJava(String[] files, String... additionalModelPath) {
        String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK,
                //Uses analysis workflow
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.ANALYSIS_WORKFLOW_BEGINNERS,};
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String file : files) {
            argsAsList.add(file);
        }
        
        T tool = null;
        try {
            Constructor<T> constructor = toolClass.getConstructor(String[].class, String.class, String.class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args, (Object) "", (Object) "");
            tool.setErrorLevel(Type.WARNING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
	
	/**
     * Creates a DSL Tool from the type T that is configured to build the symbol
     * table for the given files being able to load models and symbols from the
     * src folder, the java bootstrap folder and the given additional model
     * paths.
     *
     * @param files files to process
     * @param additionalModelPath to expand the model path
     * @param additionalParameters add additional parameters here to configure the tool
     *
     * @return the created dsl tool
     */
	@Override
    protected T createTestToolWithAdditionalParameters(String[] files, String[] additionalModelPath, String... additionalParameters) {
		String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, "javadsl", "setname",
                ARG_ANALYSIS, "javadsl", "addImports",
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK,
                //Uses analysis workflow
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.ANALYSIS_WORKFLOW_BEGINNERS,};
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String file : files) {
            argsAsList.add(file);
        }
        for (String param : additionalParameters) {
            argsAsList.add(param);
        }
        
        T tool = null;
        try {
            Constructor<T> constructor = toolClass.getConstructor(String[].class, String.class, String.class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args, (Object) "", (Object) "/home/user/workspace/MA-Paff/03.Implementierung/montiSecArcAnalysis/src/main/conf/Analysis_Conf.txt");
            tool.setErrorLevel(Type.WARNING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
	
	/**
	 * test with filter turstlevel and parameters
	 * 
	 * @param files
	 * @param whatIfElement
	 * @param additionalModelPath
	 * @return tool for test
	 */
    protected T createTestToolWithoutJavaWhatIfTrustlevel(String[] files, String whatIfElement, String... additionalModelPath) {
        String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK,
                //Uses analysis workflow
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.FILTER_TRUSTLEVEL,
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.ANALYSIS_WORKFLOW_BEGINNERS,};
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String file : files) {
            argsAsList.add(file);
        }
        
        T tool = null;
        try {
            Constructor<T> constructor = toolClass.getConstructor(String[].class, String.class, String.class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args, (Object) whatIfElement, (Object) "");
            tool.setErrorLevel(Type.WARNING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
    
    /**
     * Test with ciritcal port
     * 
     * @param files
     * @param additionalModelPath
     * @return
     */
    protected T createTestToolWithoutJavaWhatIfCiritcalPort(String[] files, String... additionalModelPath) {
        String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK,
                //Uses analysis workflow
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.FILTER_CRITICAL_PORT,
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.ANALYSIS_WORKFLOW_BEGINNERS,};
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String file : files) {
            argsAsList.add(file);
        }
        
        T tool = null;
        try {
            Constructor<T> constructor = toolClass.getConstructor(String[].class, String.class, String.class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args, (Object) "", (Object) "");
            tool.setErrorLevel(Type.WARNING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
    
    /**
     * Test with trustlevel and critical port
     * 
     * @param files
     * @param whatIfElement
     * @param additionalModelPath
     * @return
     */
    protected T createTestToolWithoutJavaWhatIfCiritcalPortTrustlevel(String[] files, String whatIfElement, String... additionalModelPath) {
        String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK,
                //Uses analysis workflow
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.FILTER_TRUSTLEVEL,
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.FILTER_CRITICAL_PORT,
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.ANALYSIS_WORKFLOW_BEGINNERS,
                ARG_SYNTHESIS, "secarc", ToolConstants.WF_PRETTY_PRINT,};
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String file : files) {
            argsAsList.add(file);
        }
        
        T tool = null;
        try {
            Constructor<T> constructor = toolClass.getConstructor(String[].class, String.class, String.class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args, (Object) whatIfElement, (Object) "");
            tool.setErrorLevel(Type.WARNING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
    
    /**
     * Pre analysis parameter check
     * 
     * @param file
     * @param whatIfElement
     * @return
     */
    protected T createTestToolWithoutJavaPreTrustlevel(String file, List<String> whatIfElement) {
        String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK,
                //Prepare parameter
                ARG_SYNTHESIS, "secarc", MontiSecArcConstants.PREPARE_PARAMETER,
                file };
        
        T tool = null;
        try { 
            Constructor<T> constructor = toolClass.getConstructor(String[].class, List.class);
            tool = constructor.newInstance((Object) args, (Object) whatIfElement);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }

}
