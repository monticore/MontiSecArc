/* (c) https://github.com/MontiCore/monticore */
package secarc;

/**
 * Constants for the MontiSecArc generator.
 *
 *
 */
public final class MontiSecArcGeneratorConstants {

	/**
     * Empty private constructor to prevent constant class instantiation.
     */
    private MontiSecArcGeneratorConstants() {
        
    }
    
    /**
     * Turn this on to generate protocols.
     */
    protected static boolean generateProtocols = false;
    
    /**
     * Template name of the MontiSecArc component generator.
     */
    public static final String COMPONENT_GENERATOR = "mc.umlp.arc.ComponentMain";
    
    /**
     * Template name of the MontiSecArc factory generator.
     */
    public static final String FACTORY_GENERATOR = "mc.umlp.arc.ComponentFactoryMain";
    
    /**
     * Template name of the MontiSecArc interface generator. 
     */
    public static final String INTERFACE_GENERATOR = "mc.umlp.arc.ComponentInterfaceMain";
    
    /**
     * Contains all default generators.
     */
    public static final String[] DEFAULT_GENERATORS = new String[] {INTERFACE_GENERATOR, FACTORY_GENERATOR, COMPONENT_GENERATOR};
	
}
