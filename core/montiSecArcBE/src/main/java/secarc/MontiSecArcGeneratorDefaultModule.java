/* (c) https://github.com/MontiCore/monticore */
package secarc;

import interfaces2.language.ModelingLanguage;


/**
 * Default guice module for MontiSecArc generator.
 *
 *          $Date$
 *
 */
public class MontiSecArcGeneratorDefaultModule extends MontiSecArcDefaultModule {

  /*@Override
  protected void bindGenerator() {
    bind(ModelingLanguage.class).to(MontiSecArcGeneratorLanguage.class);
  }*/
	
}
