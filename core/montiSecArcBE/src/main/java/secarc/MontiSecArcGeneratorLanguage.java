/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static mc.umlp.arcd.ToolConstants.WF_CREATE_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_INIT_CHECK;
import static mc.umlp.arcd.ToolConstants.WF_PARSE;
import static mc.umlp.arcd.ToolConstants.WF_PRETTY_PRINT;
import static mc.umlp.arcd.ToolConstants.WF_PRE_CHECK_TRAFO;
import static mc.umlp.arcd.ToolConstants.WF_PRE_CODEGEN_TRAFO;
import secarc._tool.MontiSecArcParsingWorkflow;
import secarc._tool.MontiSecArcRoot;
import secarc._tool.MontiSecArcRootFactory;
import secarc.codegen.transform.PreCodegenTransformationWorkflow;
import secarc.ets.transform.PreCoCoCheckMontiSecArcTransformationWorkflow;
import secarc.prettyprint.MontiSecArcInterfacePrinterWorkflow;
import secarc.prettyprint.MontiSecArcPrettyPrintWorkflow;

import com.google.inject.Inject;

import interfaces2.language.ILanguage;
import interfaces2.workflows.CreateExportedInterfaceWorkflow;
import interfaces2.workflows.PrepareCheckWorkflow;
import mc.DSLRootFactory;
import mc.GenerationLanguage;
import mc.IErrorDelegator;
import mc.IModelInfrastructureProvider;

public class MontiSecArcGeneratorLanguage extends GenerationLanguage {

  /**
   * Factory method for {@link MontiSecArc}.
   */
  public static MontiSecArcGeneratorLanguage newLanguage(ILanguage component) {
    return new MontiSecArcGeneratorLanguage(component);
  }
	 /**
     * Creates a new {@link MontiArcGeneratorLanguage}.
     */
    @Inject
    public MontiSecArcGeneratorLanguage(final ILanguage component) {
        super();
        this.language = component;
        
        // 1) PARSING-SETUP:
        // Set Root-Class (can be called by "arc" on the command line)
        dslRootClassForUserNames.put(MontiSecArcConstants.FILE_ENDING, MontiSecArcRoot.class);
        setRootClass(MontiSecArcRoot.class);
        
        // Set file-extension
        setFileEnding(MontiSecArcConstants.FILE_ENDING);
        
        // 2) WORKFLOWS
        // Parse-Workflow (can be called by "parse" on the command line)
        addExecutionUnit(WF_PARSE, new MontiSecArcParsingWorkflow());
        
        // Creates exported interface (can be called by "createExported" on the
        // command line)
        addExecutionUnit(WF_CREATE_SYMTAB, new CreateExportedInterfaceWorkflow<MontiSecArcRoot>(MontiSecArcRoot.class, this));
        
        // prepares context checks (can be called by "prepareCheck" on the
        // command line)
        addExecutionUnit(WF_INIT_CHECK, new PrepareCheckWorkflow<MontiSecArcRoot>(MontiSecArcRoot.class, this));
        
        // Transformation-Workflow before coco checks (can be called by
        // "preCheckTransformation" on the command line)
        addExecutionUnit(WF_PRE_CHECK_TRAFO, new PreCoCoCheckMontiSecArcTransformationWorkflow<MontiSecArcRoot>(MontiSecArcRoot.class));
        // Transformation-Workflow after coco checks but before code generation
        // (can be called by "preCodegenTransformation" on the command line)
        addExecutionUnit(WF_PRE_CODEGEN_TRAFO, new PreCodegenTransformationWorkflow<MontiSecArcRoot>(MontiSecArcRoot.class));
        
        // PrettyPrinter (can be called by "print" on the command line)
        addExecutionUnit(WF_PRETTY_PRINT, new MontiSecArcPrettyPrintWorkflow(MontiSecArcRoot.class));
        
        // InterfacePrinter (can be called by "printInterface" on the command
        // line)
        addExecutionUnit("printInterface", new MontiSecArcInterfacePrinterWorkflow());
        
    }
    
    /*
     * (non-Javadoc)
     * @see mc.GenerationLanguage#getConstant(java.lang.String)
     */
    @Override
    public String getConstant(String key) {
        return MontiSecArcConstants.get(key);
    }
    
    /*
     * (non-Javadoc)
     * @see interfaces2.language.ModelingLanguage#getRootFactory(mc.
     * IModelInfrastructureProvider, mc.IErrorDelegator, java.lang.String)
     */
    @Override
    public DSLRootFactory<?> getRootFactory(IModelInfrastructureProvider infrastructureprovider, IErrorDelegator errorHandler, String encoding) {
        return new MontiSecArcRootFactory(infrastructureprovider, errorHandler, encoding);
    }
    
    /*
     * (non-Javadoc)
     * @see interfaces2.language.ModelingLanguage#initILanguage()
     */
    @Override
    protected ILanguage initILanguage() {
        return null;
    }
   

}
