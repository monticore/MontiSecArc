/* (c) https://github.com/MontiCore/monticore */
package secarc;


import secarc.ets.check.MontiSecArcContextConditionCreator;
import interfaces2.language.LanguageFamily;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import mc.GenerationTool;
import mc.GenerationToolParameters;
import mc.MCG;
import mc.Parameters;
import mc.umlp.arc.MontiArcConstants;
import mc.umlp.arc.MontiArcLanguageFamilyFactory;
import mc.umlp.arc.MontiArcTool;
import mc.umlp.arcd.InjectorProvider;
import mc.umlp.arcd.ToolConstants;

/**
 * MontiSecArc tool that is able to generate simulation components. 
 * 
 */
public class MontiSecArcGeneratorTool extends GenerationTool {

	public MontiSecArcGeneratorTool(final String[] args) {
        this(args, new MontiSecArcGeneratorDefaultModule());
    }
	
	/**
     * @param args tool parameters
     */
    public MontiSecArcGeneratorTool(final String[] args, AbstractModule bindings) {
        super(args);
        
        // Define tool components
        Injector injector = Guice.createInjector(bindings);
        InjectorProvider.setInjector(injector);
        
        
        LanguageFamily languageFamily = MontiArcLanguageFamilyFactory.create(MontiSecArcGeneratorLanguage.newLanguage(injector.getInstance(MontiSecArcComponent.class)));
        
        setLanguages(languageFamily);
        
        setCocoConfiguration(MontiSecArcContextConditionCreator.createConfig());
      
        init();
        
        initGraphLogger();
    }
	
    /**
     * Initializes and registers a {@link GMLLogger}, if protocol is enabled.
     */
    private void initGraphLogger() {
    	
    }
    
    /* (non-Javadoc)
     * 
     * @see interfaces2.language.ETSTool#initParameters(java.lang.String[]) */
    @Override
    protected void initParameters(String[] args) {
        if (args != null) {
            String[] passedArgs = MontiArcTool.addStandardParameters(args);
            MontiSecArcTool.replaceFileParameterValue(passedArgs, ToolConstants.ARG_CONF, MontiArcConstants.DEFAULT_MONTI_ARC_CONFIG_FILE, true);                
            
            super.initParameters(passedArgs);
            
            // generate Log?
            if (parameters.getOptionParameter(ToolConstants.PARAM_GENLOG) != null) {
                MCG.getLogger().info("Protocol enanbled");
                MontiSecArcGeneratorConstants.generateProtocols = true;
            }
        }
    }
    
    @Override
    protected void initGenerator() {
        
        // default cfg file
        if (parameters.getModelPathConfigFile() == null) {
            parameters.setModelPathConfigFile(parameters.getBaseDir() + "/" + MontiSecArcConstants.DEFAULT_MONTI_ARC_CONFIG_FILE);
        }
        
        // default execution units
        if (parameters.getAnalyses().isEmpty() && parameters.getSyntheses().isEmpty()) {
            parameters.addAnalysis(Parameters.ALL, ToolConstants.WF_PARSE);
            parameters.addAnalysis(ToolConstants.ROOT_NAME_JAVA, "setname");
            parameters.addAnalysis(ToolConstants.ROOT_NAME_JAVA, "addImports");
            parameters.addAnalysis(Parameters.ALL, ToolConstants.WF_INIT_SYMTAB);
            parameters.addAnalysis(Parameters.ALL, ToolConstants.WF_CREATE_SYMTAB);
            
            parameters.addSynthesis(Parameters.ALL, ToolConstants.WF_INIT_CHECK);
            parameters.addSynthesis(MontiSecArcConstants.MONTI_SEC_ARC_ROOT_NAME, ToolConstants.WF_PRE_CHECK_TRAFO);
            parameters.addSynthesis(MontiSecArcConstants.MONTI_SEC_ARC_ROOT_NAME, ToolConstants.WF_RUN_CHECK);
            parameters.addSynthesis(MontiSecArcConstants.MONTI_SEC_ARC_ROOT_NAME, ToolConstants.WF_PRE_CODEGEN_TRAFO);
            
        }
        
        // generator parameters
        if (parameters.getOptionParameter(GenerationToolParameters.GENERATOR.toString()) == null) {
            for (String generator : MontiSecArcGeneratorConstants.DEFAULT_GENERATORS) {
                parameters.addOptionParameter(GenerationToolParameters.GENERATOR.toString(),
                        new String[] { generator, "MontiSecArcComponent" });
            }
        }
        super.initGenerator();
    }
    
    /* (non-Javadoc)
     * 
     * @see mc.DSLTool#printHelpMessage() */
    @Override
    protected void printHelpMessage() {
        getErrorDelegator().addInfo("MontiSecArcGeneratorTool ");
        getErrorDelegator().addInfo(
                "-----------------------------------------------------------------------------");
        getErrorDelegator().addInfo("Specify MontiSecArc input files as command line arguments");
    }
    
    /**
     * Runs the MontiArc generator tool.
     * 
     * @param args the command line parameters
     */
    public static void main(String[] args) {
        // Init MontiCoreGlobals
        MCG.initMonticoreGlobals();
        
        // MontiArcGeneratorTool
        MontiSecArcGeneratorTool tool = new MontiSecArcGeneratorTool(args);
        
        // setup CoCos
        tool.setCocoConfiguration(MontiSecArcContextConditionCreator.createConfig());
        
        // Init tool-infrastructure
        tool.init();
        
        // run
        tool.run();
    }
    
}
