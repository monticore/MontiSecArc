/* (c) https://github.com/MontiCore/monticore */
package secarc.codegen.transform;

import mc.DSLRoot;
import mc.DSLWorkflow;
import mc.umlp.arc._ast.ASTMCCompilationUnit;

/**
 * 
 * Workflow for transformations that are applied before code generation.
 *
 *
 * @param <T> dsl root type
 */
public class PreCodegenTransformationWorkflow<T extends DSLRoot<? extends ASTMCCompilationUnit>> extends DSLWorkflow<T> {

	/**
     * @param responsibleClass responsible dsl root class
     */
	public PreCodegenTransformationWorkflow(Class<T> responsibleClass) {
		super(responsibleClass);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * @see mc.DSLWorkflow#run(mc.DSLRoot)
	 */
	@Override
	public void run(T arg0) {
		// TODO Auto-generated method stub
		
	}

}
