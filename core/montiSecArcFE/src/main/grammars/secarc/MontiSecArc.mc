/* (c) https://github.com/MontiCore/monticore */
package secarc;

version "0.0.3-SNAPSHOT";

/**
 * This grammar modifies and extends the common ArchitectureDiagram grammar
 * by adding cloud-specific concepts, most notably, replication and contexts. 
 *
 * Derived from mc.umlp.arcd.MontiArc
 *
 */
grammar MontiSecArc extends mc.umlp.arc.MontiArc {

  /* ======================================================================= */
  /* =============================== OPTIONS =============================== */
  /* ======================================================================= */

  options {
    compilationunit ArcComponent
    nostring
  }
  
  /* ======================================================================= */
  /* =============================== GRAMMAR =============================== */
  /* ======================================================================= */
  
  /**
  * SecArcFilter is used by components and ports in order to filter input data.
  */
  /SecArcFilter = 
  "(" "filter" Name ")";
  
   /**
   * SecArcTrustLevel defines the trust in a component. 
   *
   * {@attribute stereotype an optional stereotype}
   * {@attribute value an absolute trustlevel}
   * {@attribute reason an optional reason for the trustlevel}
   */
   /SecArcTrustLevel implements 
  	(Stereotype? "trustlevel") => ArcElement =
  	Stereotype?
  	"trustlevel" ( ["+"] | ["-"] ) value:IntLiteral reason:String? ";"; 
    
   /**
   * SecArcIdentity defines an Identity Link between two components
   */
    
   /SecArcIdentity implements
   	(Stereotype? "identity") => ArcElement =
   	Stereotype?
   	"identity" ( ["strong"] | ["weak"] ) source:QualifiedName "->" 
   	targets:QualifiedName ("," targets:QualifiedName)* ";";
   
    
   /**
   * SecArcConnector represents an encrypted connection between a source port
   * and a target port. It extends ArcConnector by adding a
   * an optional key word "encrypted". 
   */
   
   /SecArcConnector 
   	extends ArcConnector
   	implements (Stereotype? "connect" ("encrypted"|"unencrypted") QualifiedName "->")  => ArcElement =
   	Stereotype?
   	"connect" ( ["encrypted"] | ["unencrypted"] )? source:QualifiedName "->" 
   	targets:QualifiedName ("," targets:QualifiedName)* ";";
   
   abstract ArcConnector;
   
   /**
   * Simple way to connect ports encrypted.
   */
   
   /SecArcSimpleConnector extends ArcSimpleConnector=
   	Stereotype? ( ["encrypted"] | ["unencrypted"] )? source:QualifiedName "->" targets:QualifiedName
   											("," targets:QualifiedName)*;
   
   abstract ArcSimpleConnector;
   
  /**
  * SecMontiArcAutoConnect is used to connect ports automatically encrypted.
  */
  /SecMontiArcAutoConnect extends MontiArcAutoConnect = 
    "autoconnect" Stereotype? ( ["encrypted"] | ["unencrypted"] )?
    (["type"] | ["port"] | ["off"]) ";";
    
  abstract MontiArcAutoConnect;
  
  /**
  * SecArcFilterComponent filters dependent on the filter type. This kind of
  * filter is generic. If the filter is typed, you can use ports as filter.
  */
  /SecArcFilterComponent 
  extends ArcComponent
  implements (Stereotype? "component" SecArcFilter? Name Name? ArcComponentHead "{")  => ArcElement =
  Stereotype? 
  "component" SecArcFilter? Name (instanceName:Name)? 
  head:ArcComponentHead
  body:ArcComponentBody;
  
  abstract ArcComponent; 
  
  /**
  * SecArcFilterubComponent filters dependent on the filter type. This kind of
  * filter is generic. If the filter is typed, you can use ports as filter.
  */
  /SecArcFilterSubComponent 
  extends ArcSubComponent
  implements (Stereotype? "component" SecArcFilter? ReferenceType  
      ("(" | Name | ";" ))  => ArcElement =
    Stereotype?
    "component"
    SecArcFilter?
    type:ReferenceType 
    ("(" arguments:CVExpression
    ("," arguments:CVExpression)* ")" )?
    (instances:ArcSubComponentInstance 
    ("," instances:ArcSubComponentInstance)* )? ";";
  
  abstract ArcSubComponent; 
   
  /**
   * SecArcPort represents a port declaration. It extends ArcPort by adding a
   * key word "critical" and "filter" and the optional declaration of a critical port
   * and a filter port.
   */
  /SecArcPort
      extends (Stereotype? ("critical"|SecArcFilter)? ("in" | "out")) => ArcPort =
    Stereotype?
    (["critical"] | SecArcFilter)?
    (incoming:["in"] | outgoing:["out"])
    Type Name?;
    
   /**
   * SecArcRole represents a set of roles for components or ports. A component/role 
   * has a selection of roles which have access. Default is public
   */
   
   /SecArcRole 
   	implements (Stereotype? "access" Name) => ArcElement =
   	Stereotype?
   	"access" roles:Name ("," roles:Name)* ";";
   	
   /**
   * SecArcRefRole represents a set of roles for ports or components. A port/component
   * has a selection of roles which have access. You can reference the port/component
   */
   
   /SecArcRefRole =
   	Stereotype?
   	portName:Name "(" roles:Name ("," roles:Name)* ")";
   	
   /**
   * SecArcRoleList represents a set of roles for a list of ports or components. A port/component 
   * has a selection of roles which have access.
   */
   
   /SecArcRoleInterface
   	implements (Stereotype? "access" QualifiedName "(" Name) => ArcElement =
   	Stereotype?
   	"access" SecArcRefRole ("," SecArcRefRole)* ";";
   	
   /**
   * SecArcPEP is a super-class of roles which is more common. Somewhere 
   * in the given component must be policy enforced with roles. Default is off.
   */
   
   /SecArcPEP 
   	implements (Stereotype? "accesscontrol") => ArcElement = 
   	Stereotype?
   	"accesscontrol" ( ["on"] | ["off"] ) ";";
    
   /**
   * SecArcCPE represent the cpe of a 3rd party component.
   */
   /SecArcCPE 
   	implements(Stereotype? "cpe") => ArcElement =
   	Stereotype?
   	"cpe" nameCPE:StringLiteral ";";
   	
   /**
   * SecArcConfigration represent the configuration of a 3rd party component.
   */
   /SecArcConfiguration 
   	implements(Stereotype? "configuration" Name) => ArcElement =
   	Stereotype?
   	"configuration" name:Name ";"; 	
   	
   	/**
   	* SecArcTrustlevelRelation defines the trustlevel relation between two components.
   	* It supports the comparison.
   	*/
   	/SecArcTrustlevelRelation
   	implements(Stereotype? "trustlevelrelation" QualifiedName) => ArcElement =
   	Stereotype?
   	"trustlevelrelation" client:QualifiedName ([">"] | ["="] | ["<"]) server:QualifiedName ";";
   	
	/* ======================================================*/
	/* =================== ASTRULES =========================*/
	/* ======================================================*/
  	// replacement of ASTCNode with UMLPNode
  	ast SecArcFilter astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcTrustLevel astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcIdentity astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcRole astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcRefRole astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcRoleInterface astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcPEP astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcCPE astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcConfiguration astextends 
      	/mc.umlp.common._ast.UMLPNode;
    ast SecArcTrustlevelRelation astextends 
      	/mc.umlp.common._ast.UMLPNode;
      	
   
}
