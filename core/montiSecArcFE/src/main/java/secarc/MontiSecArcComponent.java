/* (c) https://github.com/MontiCore/monticore */
package secarc;

import com.google.inject.Inject;

import mc.umlp.arc.ets.check.MonitArcCheckSymbolsExistVisitor;
import mc.umlp.arc.ets.qualifier.MontiArcModelNameQualifierClient;
import mc.umlp.arcd.ets.deserializers.ArcdFieldEntryDeserializer;
import mc.umlp.arcd.ets.deserializers.ArcdTypeEntryDeserializer;
import mc.umlp.arcd.ets.deserializers.ArcdTypeReferenceEntryDeserializer;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import mc.umlp.arcd.ets.entries.QualifiedArcdTypeEntryCreator;
import mc.umlp.arcd.ets.entries.QualifiedArcdTypeReferenceEntryCreator;
import mc.umlp.arcd.ets.namespaces.ArcdInheritedEntriesCalculator;
import mc.umlp.arcd.ets.qualifier.ArcdModelNameQualifierClient;
import mc.umlp.arcd.ets.qualifier.ArcdTypeQualifier;
import mc.umlp.arcd.ets.qualifier.ComponentQualifier;
import mc.umlp.arcd.ets.qualifier.ReferenceQualifier;
import mc.umlp.arcd.ets.resolvers.ArcdFieldResolverClient;
import mc.umlp.arcd.ets.resolvers.ArcdTypeResolverClient;
import mc.umlp.arcd.ets.resolvers.ComponentResolverClient;
import mc.umlp.arcd.ets.resolvers.ConnectorResolverClient;
import mc.umlp.arcd.ets.resolvers.PortResolverClient;
import mc.umlp.arcd.ets.resolvers.SubComponentResolverClient;
import secarc.ets.check.MontiSecArcCheckVisitor;
import secarc.ets.check.MontiSecArcContextConditionCreator;
import secarc.ets.deserializers.ConfigurationEntryDeserializer;
import secarc.ets.deserializers.FilterEntryDeserializer;
import secarc.ets.deserializers.IdentityEntryDeserializer;
import secarc.ets.deserializers.MontiSecArcInterfaceExtender;
import secarc.ets.deserializers.PEPEntryDeserializer;
import secarc.ets.deserializers.RoleEntryDeserializer;
import secarc.ets.deserializers.SecComponentEntryDeserializer;
import secarc.ets.deserializers.SecConnectorEntryDeserializer;
import secarc.ets.deserializers.SecPortEntryDeserializer;
import secarc.ets.deserializers.SecSubComponentEntryDeserializer;
import secarc.ets.deserializers.TrustlevelEntryDeserializer;
import secarc.ets.deserializers.TrustlevelRelationEntryDeserializer;
import secarc.ets.deserializers.CPEEntryDeserializer;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.qualifier.MontiSecArcModelNameQualifierClient;
import secarc.ets.resolvers.RoleResolverClient;
import secarc.ets.resolvers.ConfigurationResolverClient;
import secarc.ets.resolvers.FilterResolverClient;
import secarc.ets.resolvers.IdentityResolverClient;
import secarc.ets.resolvers.PEPResolverClient;
import secarc.ets.resolvers.TrustlevelRelationResolverClient;
import secarc.ets.resolvers.TrustlevelResolverClient;
import secarc.ets.resolvers.CPEResolverClient;
import interfaces2.language.LanguageComponent;
import interfaces2.workflows.ConcreteASTAndNameSpaceVisitor;

/**
 * MonitSecArc language component
 * 
 * 
 * 
 */
public class MontiSecArcComponent extends LanguageComponent {
  
	/**
	 * Creates a new MontiSecArc Language component
	 */
	@Inject
	public MontiSecArcComponent(
            final ConcreteASTAndNameSpaceVisitor entryCreator, 
            final IArcdEntryFactory arcdFactory) {
		addNamespaceEstablishingClass(MontiSecArcConstants.NODES_WITH_NAMESPACE);
        
        addEntryCreator(entryCreator);
        
        addInheritedEntriesCalculator(new ArcdInheritedEntriesCalculator());
         
        //Deserializer from Arcd and MontiArc
        addDeserializer(new ArcdTypeEntryDeserializer());
        addDeserializer(new ArcdTypeReferenceEntryDeserializer(arcdFactory));
        addDeserializer(new ArcdFieldEntryDeserializer());
        //Deserializer from MontiSecArc
        addDeserializer(new PEPEntryDeserializer(arcdFactory));
        addDeserializer(new SecComponentEntryDeserializer(arcdFactory));
        addDeserializer(new ConfigurationEntryDeserializer(arcdFactory));
        addDeserializer(new FilterEntryDeserializer(arcdFactory));
        addDeserializer(new IdentityEntryDeserializer(arcdFactory));
        addDeserializer(new RoleEntryDeserializer(arcdFactory));
        addDeserializer(new SecConnectorEntryDeserializer(arcdFactory));
        addDeserializer(new SecPortEntryDeserializer(arcdFactory));
        addDeserializer(new SecSubComponentEntryDeserializer(arcdFactory));
        addDeserializer(new TrustlevelEntryDeserializer(arcdFactory));
        addDeserializer(new CPEEntryDeserializer(arcdFactory));
        addDeserializer(new TrustlevelRelationEntryDeserializer(arcdFactory));
        
        addQualifierClient(new ComponentQualifier());
        addQualifierClient(new ArcdTypeQualifier());
        addQualifierClient(new ReferenceQualifier());
        
        //Arcd Resolver
        addResolverClient(new PortResolverClient());
        addResolverClient(new ComponentResolverClient());
        addResolverClient(new SubComponentResolverClient());
        addResolverClient(new ArcdTypeResolverClient());
        addResolverClient(new ArcdFieldResolverClient());
        addResolverClient(new ConnectorResolverClient());
        //MontiSecArc Resolver
        addResolverClient(new IdentityResolverClient());
        addResolverClient(new RoleResolverClient());
        addResolverClient(new ConfigurationResolverClient());
        addResolverClient(new FilterResolverClient());
        addResolverClient(new CPEResolverClient());
        addResolverClient(new PEPResolverClient());
        addResolverClient(new TrustlevelResolverClient());
        addResolverClient(new TrustlevelRelationResolverClient());
        
        addParserExtender(new MontiSecArcInterfaceExtender());
        
        addModelNameQualifierClient(new MontiArcModelNameQualifierClient());
        addModelNameQualifierClient(new ArcdModelNameQualifierClient());
        addModelNameQualifierClient(new MontiSecArcModelNameQualifierClient());
        
        addCheckWorkflowClient(new MonitArcCheckSymbolsExistVisitor());
        addCheckWorkflowClient(new MontiSecArcCheckVisitor());
        
        //Context conditions MontiSecArc, MontiArc and Arcd
        setContextConditions(MontiSecArcContextConditionCreator.createConditions());
        
        QualifiedArcdTypeEntryCreator qtec = new QualifiedArcdTypeEntryCreator();
        addQualifiedEntryCreator(qtec);
        addQualifiedEntryCreator(new QualifiedArcdTypeReferenceEntryCreator(qtec));
        
        setExportedEntryKind(SecComponentEntry.KIND);
			
	}
	
}
