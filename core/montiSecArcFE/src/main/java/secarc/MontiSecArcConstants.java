/* (c) https://github.com/MontiCore/monticore */
package secarc;

import java.util.List;
import java.util.Map;

import secarc._ast.ASTSecArcCPE;
import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcFilterComponent;
import secarc._ast.ASTSecArcFilterSubComponent;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcPEP;
import secarc._ast.ASTSecArcPort;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcRoleInterface;
import secarc._ast.ASTSecArcTrustLevel;
import secarc._ast.ASTSecArcTrustlevelRelation;

import mc.ast.ASTCNode;
import mc.types._ast.ASTTypeParameters;
import mc.umlp.arc._ast.ASTMCCompilationUnit;
import mc.umlp.arc._ast.ASTMontiArcInvariant;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcComponentImplementation;
import mc.umlp.arcd._ast.ASTArcParameter;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTArcSubComponent;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import interfaces2.Interfaces2Constants;

/**
 * MonitSecArc constants
 * 
 * 
 * 
 */
public final class MontiSecArcConstants extends Interfaces2Constants {
	
	/** MontiSecArc language ID. */
    public static final String MONTI_SEC_ARC_LANGUAGE_ID = "secarc.MontiSecArc";
    
    /**
     * The descriptive name of this language.
     */
    public static final String LANGUAGE_NAME = "MontiSecArc";
    
    /**
     * The short name of this language.
     */
    public static final String LANGUAGE_SHORTNAME = "secarc";
    
    /**
     * The file ending of models of this language.
     */
    public static final String FILE_ENDING = LANGUAGE_SHORTNAME;
    
    /**
     * Name of the MontiSecArc roots.
     */
    public static final String MONTI_SEC_ARC_ROOT_NAME = FILE_ENDING;
    
    /**
     * Default name of the file to configure the MontiSecArc model path.
     */
    public static final String DEFAULT_MONTI_ARC_CONFIG_FILE = "ma.cfg";
    
    /**
     * Argument for Analysis configuration
     */
    public static final String ARG_ANALYSISCONFPATH = "-analysisconfpath";
    
    /**
     * name for pep entry
     */
    public static final String PEP_NAME = "pep";
    
    /**
     * name for trustlevel entry
     */
    public static final String TRUSTLEVEL_NAME = "trustlevel";
    
    /**
     * name for version entry
     */
    public static final String CPE_NAME = "version";
    
    /**
     * Checks, if the parameter exist
     */
    public static final String PREPARE_PARAMETER = "secPreParameter";
    
    /**
     * Workflow name for analysis as beginner
     */
    public static final String ANALYSIS_WORKFLOW_BEGINNERS = "secAnalysisBeginners";
    
    /**
     * Workflow name for trustevel filter
     */
    public static final String FILTER_TRUSTLEVEL = "secFilterTrustlevel";
    
    /**
     * Workflow name for critical port filter
     */
    public static final String FILTER_CRITICAL_PORT = "secFilterCriticalPort";
    
    /**
     * Workflow name for analysis as advanced user
     */
    public static final String ANALYSIS_WORKFLOW_ADVANCED = "secAnalysisAdvanced";
    
    /**
     * List of MontiSecArc AST nodes that have public visibility in the symbol table.
     */
    public static final List<Class<? extends ASTCNode>> MONTI_SEC_ARC_PUBLIC_NODES = ImmutableList.of(ASTArcComponent.class, ASTArcPort.class, 
                    ASTMCCompilationUnit.class, ASTTypeParameters.class, ASTArcParameter.class, ASTSecArcFilterComponent.class, ASTSecArcPort.class, 
                    ASTSecArcRole.class, ASTSecArcRefRole.class, ASTSecArcRoleInterface.class, ASTSecArcIdentity.class, ASTSecArcFilter.class, ASTSecArcConfiguration.class, ASTSecArcCPE.class,ASTSecArcPEP.class,
                    ASTSecArcTrustLevel.class, ASTSecArcTrustlevelRelation.class, ASTArcSubComponent.class, ASTSecArcFilterSubComponent.class);
    
    /**
     * Holds abbreviations mapped to AST-node names.
     */
    protected static final Map<String, String> GENERATOR_CONSTANTS = ImmutableMap.of("montisecarccomponent", ASTMCCompilationUnit.class.getName());
    
    /**
     * Nodes that build up name spaces.
     */
    static final Class<?>[] NODES_WITH_NAMESPACE = {ASTArcComponent.class, ASTMCCompilationUnit.class, 
        ASTArcComponentImplementation.class, ASTMontiArcInvariant.class, ASTSecArcPort.class, ASTSecArcFilterComponent.class,
        ASTArcPort.class, 
       };
    
    /**
     * Singleton instance of this class.
     */
    static MontiSecArcConstants instance = null;
    
    private MontiSecArcConstants() {
    	
    }
    
    /**
     * 
     * @param key abbreviation or qualified AST-node name
     * 
     * @return the AST-node mapped to the given key, the key itself, if no mapping exists.
     */
    public static String get(String key) {
      if (instance == null) {
        instance = new MontiSecArcConstants();
      }
      return instance.doGet(key);
    }
    
    /**
     * 
     * @param key abbreviation or qualified AST-node name
     * 
     * @return the AST-node mapped to the given key, the key itself, if no mapping exists.
     */
    protected String doGet(String key) {
      String result = key;
      String key2lower = key.toLowerCase();
      if (GENERATOR_CONSTANTS.containsKey(key2lower)) {
        result = GENERATOR_CONSTANTS.get(key2lower);
      }
      return result;
    }

}
