/* (c) https://github.com/MontiCore/monticore */
package secarc;

import secarc.ets.entries.ConfigurationEntryFactory;
import secarc.ets.entries.FilterEntryFactory;
import secarc.ets.entries.IConfigurationEntryFactory;
import secarc.ets.entries.IFilterEntryFactory;
import secarc.ets.entries.IIdentityEntryFactory;
import secarc.ets.entries.IPEPEntryFactory;
import secarc.ets.entries.IRoleEntryFactory;
import secarc.ets.entries.ITrustlevelEntryFactory;
import secarc.ets.entries.ITrustlevelRelationEntryFactory;
import secarc.ets.entries.ICPEEntryFactory;
import secarc.ets.entries.IdentityEntryFactory;
import secarc.ets.entries.PEPEntryFactory;
import secarc.ets.entries.RoleEntryFactory;
import secarc.ets.entries.SecComponentEntryFactory;
import secarc.ets.entries.SecConnectorEntryFactory;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.SecPortEntryFactory;
import secarc.ets.entries.SecSubComponentEntryFactory;
import secarc.ets.entries.TrustlevelEntryFactory;
import secarc.ets.entries.TrustlevelRelationEntryFactory;
import secarc.ets.entries.CPEEntryFactory;
import secarc.ets.visitor.MontiSecArcSymtabVisitor;
import interfaces2.language.ILanguage;
import interfaces2.language.LanguageComponent;
import interfaces2.workflows.ConcreteASTAndNameSpaceVisitor;
import mc.umlp.arc.CompositeMontiArcAndJavaLanguage;
import mc.umlp.arc.MontiArcDefaultModule;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import mc.umlp.arcd.ets.entries.IComponentEntryFactory;
import mc.umlp.arcd.ets.entries.IConnectorEntryFactory;
import mc.umlp.arcd.ets.entries.IPortEntryFactory;
import mc.umlp.arcd.ets.entries.ISubComponentEntryFactory;

/**
 * Default bindings for MontiSecArc.
 *
 *
 */
public class MontiSecArcDefaultModule extends MontiArcDefaultModule {

	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arc.MontiArcDefaultModule#bindComponentFactories()
	 */
	@Override
    protected void bindComponentFactories() {
        bind(IComponentEntryFactory.class).to(SecComponentEntryFactory.class);
        bind(ISubComponentEntryFactory.class).to(SecSubComponentEntryFactory.class);
    }
    
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.ArcdDefaultModule#bindConnectorFactory()
	 */
    @Override
    protected void bindConnectorFactory() {
        bind(IConnectorEntryFactory.class).to(SecConnectorEntryFactory.class);      
    }
    
    /* (non-Javadoc)
     * @see mc.umlp.arcd.ArcdDefaultModule#bindLanguageAndVisitor()
     */
    @Override
    protected void bindLanguageAndVisitor() {
      bind(ConcreteASTAndNameSpaceVisitor.class).to(MontiSecArcSymtabVisitor.class);
      bind(ILanguage.class).to(CompositeMontiArcAndJavaLanguage.class);
      bind(LanguageComponent.class).to(MontiSecArcComponent.class);
    }
    
    /*
     * (non-Javadoc)
     * @see mc.umlp.arcd.ArcdDefaultModule#bindMainFactory()
     */
    @Override
    protected void bindMainFactory() {
    	bind(IArcdEntryFactory.class).to(SecEntryFactory.class);
    }
    
    /*
     * (non-Javadoc)
     * @see mc.umlp.arcd.ArcdDefaultModule#bindPortFactory()
     */
    @Override
    protected void bindPortFactory() {
        bind(IPortEntryFactory.class).to(SecPortEntryFactory.class);      
    }
    
    /*
     * (non-Javadoc)
     * @see mc.umlp.arcd.ArcdDefaultModule#additionalBindings()
     */
    @Override
    protected void additionalBindings() {
      super.additionalBindings();
    	bind(IFilterEntryFactory.class).to(FilterEntryFactory.class);
    	bind(IPEPEntryFactory.class).to(PEPEntryFactory.class);
    	bind(ITrustlevelEntryFactory.class).to(TrustlevelEntryFactory.class);
    	bind(IConfigurationEntryFactory.class).to(ConfigurationEntryFactory.class);
    	bind(ICPEEntryFactory.class).to(CPEEntryFactory.class);
    	bind(IRoleEntryFactory.class).to(RoleEntryFactory.class);
    	bind(IIdentityEntryFactory.class).to(IdentityEntryFactory.class);
    	bind(ITrustlevelRelationEntryFactory.class).to(TrustlevelRelationEntryFactory.class);
    }
	
}
