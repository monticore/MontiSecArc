/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static mc.umlp.arcd.ToolConstants.ARG_ANALYSIS;
import static mc.umlp.arcd.ToolConstants.ARG_CONF;
import static mc.umlp.arcd.ToolConstants.ARG_MODELPATH;
import static mc.umlp.arcd.ToolConstants.ARG_OUT;
import static mc.umlp.arcd.ToolConstants.ARG_SYMTABDIR;
import static mc.umlp.arcd.ToolConstants.ARG_SYNTHESIS;
import static mc.umlp.arcd.ToolConstants.WF_CREATE_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_INIT_CHECK;
import static mc.umlp.arcd.ToolConstants.WF_INIT_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_PARSE;
import static mc.umlp.arcd.ToolConstants.WF_PRE_CHECK_TRAFO;
import static mc.umlp.arcd.ToolConstants.WF_RUN_CHECK;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import mc.MCG;
import mc.Parameters;

/**
 * Default tool for {@link MontiSecArcLanguage}.
 * Sets default parameter for input files.
 * 
 * 
 */
public class MontiSecArcDefaultTool extends MontiSecArcTool {

	public MontiSecArcDefaultTool(String[] arguments) {
		super(arguments);
	}

	/**
	 * The main method of this tool. Creates, initializes and starts
	 * {@link MontiSecArcTool}. Uses default arguments.
	 * 
	 * argument 0: Modelpath 
	 * argument 1: Path for the configuration 
	 * argument 2: output directory for tests target/testOut 
	 * argument 3: output directory for symboltables target/symtab 
	 * argument 4..n: input files
	 * 
	 * @param a list of {@link DSLTool} arguments.
	 */
	public static void main(String[] arguments) {

		String[] args = new String[] { 
				ARG_MODELPATH, arguments[0],
				ARG_CONF, arguments[1], 
				ARG_OUT, arguments[2], 
				ARG_SYMTABDIR, arguments[3], 
				ARG_ANALYSIS, Parameters.ALL, WF_PARSE, 
				ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB, 
				ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB, 
				ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK, 
				ARG_SYNTHESIS, "arcd", WF_PRE_CHECK_TRAFO, 
				ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK };
		List<String> argsAsList = new LinkedList<String>();
		argsAsList.addAll(Arrays.asList(args));
		// add input files
		for (int i = 4; i < arguments.length; i++) {
			argsAsList.add(arguments[i]);
		}

		MCG.initMonticoreGlobals();

		MontiSecArcTool tool = new MontiSecArcDefaultTool(
				argsAsList.toArray(new String[argsAsList.size()]));

		tool.init();
		tool.run();

	}

}
