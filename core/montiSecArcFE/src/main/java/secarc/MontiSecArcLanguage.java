/* (c) https://github.com/MontiCore/monticore */
package secarc;

import com.google.inject.Inject;

import interfaces2.language.ILanguage;
import interfaces2.language.ModelingLanguage;
import interfaces2.workflows.CreateExportedInterfaceWorkflow;
import interfaces2.workflows.PrepareCheckWorkflow;
import mc.DSLRootFactory;
import mc.IErrorDelegator;
import mc.IModelInfrastructureProvider;
import mc.umlp.arc.MontiArcLanguage;
import secarc._tool.MontiSecArcParsingWorkflow;
import secarc._tool.MontiSecArcRoot;
import secarc._tool.MontiSecArcRootFactory;
import secarc.ets.transform.PreCoCoCheckMontiSecArcTransformationWorkflow;

/**
 * As subclass of {@link ModelingLanguage}, this class configures the technical
 * aspects of MontiSecArc.
 * 
 * 
 */
public class MontiSecArcLanguage extends MontiArcLanguage {
      
  /**
   * The {@link mc.DSLRoot} class that encapsulates ASTs of this language.
   */
  public static final Class<? extends MontiSecArcRoot> LANGUAGE_ROOT = MontiSecArcRoot.class;
  
  /**
   * Factory method for {@link MontiSecArcLanguage}.
   */
  public static MontiSecArcLanguage newLanguage(ILanguage component) {
    return new MontiSecArcLanguage(component);
  }
  
  /**
   * Constructor for secarc.MontiSecArc
   */
  @Inject
  public MontiSecArcLanguage(final ILanguage component) {
    super(component);
	  
	// PARSING-SETUP
    setRootClass(LANGUAGE_ROOT);
    this.dslRootClassForUserNames.put(
        MontiSecArcConstants.LANGUAGE_SHORTNAME,
        LANGUAGE_ROOT);
    
    // Set file-extension
    setFileEnding(MontiSecArcConstants.FILE_ENDING);
    
    /* Execution Units */
    
    //WORKFLOWS
    
    addExecutionUnit("parse",
        new MontiSecArcParsingWorkflow());
    
    addExecutionUnit("createExported", new CreateExportedInterfaceWorkflow<MontiSecArcRoot>(LANGUAGE_ROOT, this));
    
    addExecutionUnit("prepareCheck", new PrepareCheckWorkflow<MontiSecArcRoot>(LANGUAGE_ROOT, this));
    
    //Transformation-Workflow before coco checks (can be called by "preCheckTransformation" on the command line)
    addExecutionUnit("preCheckTransformation", new PreCoCoCheckMontiSecArcTransformationWorkflow<MontiSecArcRoot>(LANGUAGE_ROOT));
    
  }
  
  /**
   * @see interfaces2.language.ModelingLanguage#getRootFactory(mc.IModelInfrastructureProvider,
   *      mc.IErrorDelegator, java.lang.String)
   */
  @Override
  public DSLRootFactory<MontiSecArcRoot> getRootFactory(IModelInfrastructureProvider mip,
      IErrorDelegator errorDelegator, String encoding) {
    return new MontiSecArcRootFactory(mip, errorDelegator, encoding);
  }
  
  /**
   * @see interfaces2.language.ModelingLanguage#initILanguage()
   */
  @Override
  protected ILanguage initILanguage() {
    return null;
  }
  
}
