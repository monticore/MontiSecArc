/* (c) https://github.com/MontiCore/monticore */
package secarc;

import java.io.File;

import com.google.inject.Guice;
import com.google.inject.Injector;

import secarc.ets.check.MontiSecArcContextConditionCreator;
import interfaces2.language.ETSTool;
import interfaces2.language.LanguageFamily;
import mc.MCG;
import mc.umlp.arc.MontiArcLanguageFamilyFactory;
import mc.umlp.arcd.InjectorProvider;

/**
 * Tool for {@link MontiSecArcLanguage}.
 * 
 * 
 */
public class MontiSecArcTool extends ETSTool {
  
  /**
   * The main method of this tool. Creates, initializes and starts
   * {@link MontiSecArcTool}. Takes a list of {@link DSLTool} arguments.
   * 
   * @param a list of {@link DSLTool} arguments.
   */
  public static void main(String[] arguments) {

    MCG.initMonticoreGlobals();
    
    MontiSecArcTool tool = new MontiSecArcTool(arguments);
    
    tool.init();
    tool.run();
    
  }
  
  /**
   * Constructor for secarc.MontiSecArcTool
   * 
   * @param arguments
   */
  public MontiSecArcTool(String[] arguments) {
    super(arguments);
    
    //Inject Entries
    Injector injector = Guice.createInjector(new MontiSecArcDefaultModule());
    InjectorProvider.setInjector(injector);
    
    LanguageFamily languageFamily = MontiArcLanguageFamilyFactory.create(MontiSecArcLanguage.newLanguage(injector.getInstance(MontiSecArcComponent.class)));
    setLanguages(languageFamily);
    
    // set default CoCos
    setCocoConfiguration(MontiSecArcContextConditionCreator.createConfig());
  }
  
  /**
   * Replaces values of parameters with one value in the given arguments.
   * 
   * @param args arguments to modify
   * @param parameter parameter to find
   * @param newValue value to set for the given parameter
   * @param checkExistence if this is true, it is first checked, if the old
   *            value exists as a file. If this file exists, the value is NOT
   *            replaced!
   */
  public static void replaceFileParameterValue(String[] args, String parameter, String newValue, boolean checkExistence) {
      int paramPosition = -1;
      
      for (int i = 0; i < args.length; i++) {
          String arg = args[i];
          if (arg.equals(parameter) && args.length > (i + 1)) {
              paramPosition = i + 1;
              break;
          }
      }
      if (paramPosition != -1) {
          boolean exists = false;
          
          if (checkExistence) {
              File cfgFile = new File(args[paramPosition]);
              exists = cfgFile.exists();
          }
          
          if (!exists) {
              String newConf = args[paramPosition];
              int pos = newConf.lastIndexOf(File.separator);
              if (pos == -1) {
                  pos = newConf.lastIndexOf('/');
              }
              if (pos != -1) {
                  newConf = newConf.substring(0, pos + 1) + newValue;
              }
              else {
                  newConf = newValue;
              }
              
              args[paramPosition] = newConf;
          }
      }
  }
  
}
