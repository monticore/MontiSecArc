/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcConfiguration extends PrototypeASTSecArcConfiguration {
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.common._ast.UMLPNode#getMainParent()
	 */
	@Override
    public ASTArcComponent getMainParent() {
        // ASTArcConfigurationList, ASTArcInterface, ASTArcInterfaceList, ASTArcComponentBody in between
        return (ASTArcComponent) _parent.get_Parent().get_Parent();
    }
	
}
