/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;
 
import java.util.List;
 
import mc.types._ast.ASTQualifiedName;

import com.google.common.collect.ImmutableList;
 
/**
 * {@link ASTNode} for the respective production in MontiSecARc.mc
 * 
 * 
 */
public class ASTSecArcConnector extends PrototypeASTSecArcConnector {

	/**
     * @return the list of names of the targets of this connector.
     */
    public List<String> printTargets() {
 
	    ImmutableList.Builder<String> targetNames = ImmutableList.builder();
	 
	    for (ASTQualifiedName targetName : getTargets()) {
	    	targetNames.add(targetName.toString());
	    }
	  
	   return targetNames.build();
   }
     
}
