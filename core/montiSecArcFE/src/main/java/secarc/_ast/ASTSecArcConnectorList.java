/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;
 
/**
 * {@link ASTList} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcConnectorList extends PrototypeASTSecArcConnectorList {

  /**
   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
   */
  public ASTSecArcConnectorList() {
	  super();
  }
 
  /**
   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
   */
   public ASTSecArcConnectorList(boolean strictlyOrdered) {
	   super(strictlyOrdered);
   }
     
}
