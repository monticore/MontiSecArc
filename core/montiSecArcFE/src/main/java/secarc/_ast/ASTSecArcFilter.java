/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.common._ast.UMLPNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcFilter extends PrototypeASTSecArcFilter {

	/*
	 * (non-Javadoc)
	 * @see mc.umlp.common._ast.UMLPNode#getMainParent()
	 */
    public UMLPNode getMainParent() {
        // ASTArcFilterList, ASTArcInterface, ASTArcInterfaceList, ASTArcComponentBody in between
		if(_parent instanceof ASTArcComponent) {
			return (ASTArcComponent) _parent;
		} else {
			return (ASTArcPort) _parent;
		}
    }
	
}
