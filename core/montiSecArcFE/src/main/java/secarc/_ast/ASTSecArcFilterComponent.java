/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

import java.util.ArrayList;
import java.util.List;

import mc.umlp.arcd._ast.ASTArcElement;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcFilterComponent extends PrototypeASTSecArcFilterComponent{
	
	/**
	 * Lazy calculated trustlevel for component
	 */
	protected ASTSecArcTrustLevel trustlevel;
	
	/**
	 * Lazy calculated configuratin for component
	 */
	protected ASTSecArcConfiguration configuration;
	
	/**
	 * Lazy calculated cpe for component
	 */
	protected ASTSecArcCPE cpe;
	
	/**
	 * Lazy calculated critical ports for component
	 */
	protected List<ASTArcPort> criticalPorts;
	
	/**
	 * Lazy calculated roles for component
	 */
	protected ASTSecArcRole roles;
	
	/**
	 * Lazy calculated filter for component
	 */
	protected ASTSecArcFilter filter;
	
	/**
	 * Lazy calculated encrypted connectors
	 */
	protected List<ASTArcConnector> encryptedConnectors;
	
	/**
	 * Lazy calculated encrypted simple connectors
	 */
	protected List<ASTArcSimpleConnector> encryptedSimpleConnectors;
	
	/**
	 * Lazy calculated unencrypted connectors
	 */
	protected List<ASTArcConnector> unencryptedConnectors;
	
	/**
	 * Lazy calculated unencrypted simple connectors
	 */
	protected List<ASTArcSimpleConnector> unencryptedSimpleConnectors;
	
	/**
	 * Lazy calculated pep 
	 */
	protected ASTSecArcPEP pep;
	
	/**
	 * Lazy calculated trustlevel relations
	 */
	protected List<ASTSecArcTrustlevelRelation> trustlevelRelations;
	
	/**
	 * Lazy calculated identity link
	 */
	protected List<ASTSecArcIdentity> identities;
	
	/**
	 * default constructor
	 */
	public ASTSecArcFilterComponent() {
		super();
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd._ast.ASTArcComponent#checkComponentType()
	 */
	@Override
	protected void checkComponentType() {
		componentTypeCalculated = false;
		
		super.checkComponentType();
		
		criticalPorts = new ArrayList<ASTArcPort>();
		encryptedConnectors = new ArrayList<ASTArcConnector>();
		unencryptedConnectors = new ArrayList<ASTArcConnector>();
		encryptedSimpleConnectors = new ArrayList<ASTArcSimpleConnector>();
		unencryptedSimpleConnectors = new ArrayList<ASTArcSimpleConnector>();
		trustlevelRelations = new ArrayList<ASTSecArcTrustlevelRelation>();
		identities = new ArrayList<ASTSecArcIdentity>();
		
		//Critical ports from ports
		for(ASTArcPort port : this.getPorts()) {
			if(port instanceof ASTSecArcPort && ((ASTSecArcPort) port).isCritical()) {
				criticalPorts.add(port);
			}
		}
		
		//encrypted connectors and unencrypted connectors from connectors
		for(ASTArcConnector connector : this.getConnectors()) {
			if(connector instanceof ASTSecArcConnector && ((ASTSecArcConnector) connector).isEncrypted()) {
				encryptedConnectors.add(connector);
			} else {
				unencryptedConnectors.add(connector);
			}
		}
		
		//encrypted simple connectors and unencrypted simple connectors from simple connectors
		for(ASTArcSimpleConnector simpleConnector : this.getSimpleConnectors()) {
			if(simpleConnector instanceof ASTSecArcSimpleConnector && ((ASTSecArcSimpleConnector) simpleConnector).isEncrypted()) {
				encryptedSimpleConnectors.add(simpleConnector);
			} else {
				unencryptedSimpleConnectors.add(simpleConnector);
			}
		}
		
		for (ASTArcElement elem : getBody().getArcElement()) {
			//check for roles
			if(elem instanceof ASTSecArcRole) {
				roles = (ASTSecArcRole) elem;
			}
			
			//check for trustlevelrelations
			if(elem instanceof ASTSecArcTrustlevelRelation) {
				trustlevelRelations.add((ASTSecArcTrustlevelRelation) elem);
			}
			//check for configuration
			if(elem instanceof ASTSecArcConfiguration) {
				configuration = (ASTSecArcConfiguration) elem;
			}
			
			//check for cpe
			if(elem instanceof ASTSecArcCPE) {
				cpe = (ASTSecArcCPE) elem;
			}
			
			//check for pep
			if(elem instanceof ASTSecArcPEP) {
				pep = (ASTSecArcPEP) elem;
			}
			
			//check for trustlevel
			if(elem instanceof ASTSecArcTrustLevel) {
				trustlevel = (ASTSecArcTrustLevel) elem;
			}
			
			//check for identities
			if(elem instanceof ASTSecArcIdentity) {
				identities.add((ASTSecArcIdentity) elem);
			}
			
		}
		
		//check for filter
		filter = this.getSecArcFilter();
		
		componentTypeCalculated = true;
	}

	/**
	 * Returns trustlevel of component
	 * 
	 * @return trustlevel
	 */
	public ASTSecArcTrustLevel getTrustlevel() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return trustlevel;
	}
	
	/**
	 * Returns configuration for component
	 * 
	 * @return configuration
	 */
	public ASTSecArcConfiguration getConfiguration() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
			
		return configuration;
	}
	
	/**
	 * Returns cpe for component
	 * 
	 * @return cpe
	 */
	public ASTSecArcCPE getCPE() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return cpe;
	}
	
	/**
	 * Returns critical ports for component
	 * 
	 * @return critical ports
	 */
	public List<ASTArcPort> getAllCriticalPorts() {
		if (!componentTypeCalculated) {
            checkComponentType();
        }
		
		return criticalPorts;
	}
	
	/**
	 * Returns roles for component
	 * 
	 * @return roles
	 */
	public ASTSecArcRole getAllRoles() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return roles;
	}
	
	/**
	 * Returns filter for component
	 * 
	 * @return filter
	 */
	public ASTSecArcFilter getFilter() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return filter;
	}
	
	/**
	 * Returns encrypted connectors for component
	 * 
	 * @return encrypted connectors
	 */
	public List<ASTArcConnector> getAllEncryptedConnectors() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return encryptedConnectors;
	}
	
	/**
	 * Returns unencrypted connectors for component
	 * 
	 * @return unencrypted connectors
	 */
	public List<ASTArcConnector> getAllUnencryptedConnectors() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return unencryptedConnectors;
	}
	
	/**
	 * Returns encrypted simple connectors for component
	 * 
	 * @return encrypted simple connectors
	 */
	public List<ASTArcSimpleConnector> getAllEncryptedSimpleConnectors() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return encryptedSimpleConnectors;
	}
	
	/**
	 * Returns unencrypted simple connectors for component
	 * 
	 * @return
	 */
	public List<ASTArcSimpleConnector> getAllUnencryptedSimpleConnectors() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return unencryptedSimpleConnectors;
	}
	
	/**
	 * Returns pep for component
	 * 
	 * @return pep
	 */
	public ASTSecArcPEP getPEP() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return pep;
	}
	
	/**
	 * Returns trustlevel relations for component
	 * 
	 * @return trustlevel relations
	 */
	public List<ASTSecArcTrustlevelRelation> getAllTrustlevelRelation() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return trustlevelRelations;
	}
	
	/**
	 * Returns identity links for component
	 * 
	 * @return identities
	 */
	public List<ASTSecArcIdentity> getAllIdentities() {
		if(!componentTypeCalculated) {
			checkComponentType();
		}
		
		return identities;
	}
	
}
