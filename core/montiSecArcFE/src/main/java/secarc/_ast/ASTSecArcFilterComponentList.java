/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcFilterComponentList extends PrototypeASTSecArcFilterComponentList {

	/**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterComponentList
	   */
	  public ASTSecArcFilterComponentList() {
		  super();
	  }
	 
	  /**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterComponentList
	   */
	   public ASTSecArcFilterComponentList(boolean strictlyOrdered) {
		   super(strictlyOrdered);
	   }
	
}
