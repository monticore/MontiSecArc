/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcFilterSubComponentList extends PrototypeASTSecArcFilterSubComponentList{
	
	/**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterSubComponentList
	   */
	  public ASTSecArcFilterSubComponentList() {
		  super();
	  }
	 
	  /**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcFilterSubComponentList
	   */
	   public ASTSecArcFilterSubComponentList(boolean strictlyOrdered) {
		   super(strictlyOrdered);
	   }

}
