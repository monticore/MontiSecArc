/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

import mc.ast.ASTNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcPEPList extends PrototypeASTSecArcPEPList {

	/**
	 * Default constructor
	 */
	public ASTSecArcPEPList() {
		super();
	}
	
	/**
     * Constructor for strictly ordered lists.
     * 
     * @param strictlyOrdered true, if this list should be strictly ordered.
     */
	public ASTSecArcPEPList(boolean strictlyOrdered) {
		super(strictlyOrdered);
	}
	
}
