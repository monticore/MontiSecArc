/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;
 

import java.util.ArrayList;
import java.util.List;

import mc.umlp.arcd._ast.ASTArcElement;

import de.se_rwth.commons.Names;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 */
public class ASTSecArcPort extends PrototypeASTSecArcPort {
	
	/**
     * Flags, if lazy informations have been calculated.
     */
    protected boolean portTypeCalculated;
    
    /**
     * Lazy calculated roles
     */
    protected List<String> roles;
    
	/**
     * The package name of this port, that is, the package name of the parent
     * component of this port.
     */
     private String packageName;
    
     /**
      * The simple type name of this port.
      */
     private String simpleTypeName;
    
     /**
      * @return the package of this port, that is, the package of the parent
      * component of this port.
      */
     public String printPackage() {
       if (this.packageName == null) {
         this.packageName = getMainParent().printPackage();
       }
       return this.packageName;
     }
     
     /**
      * @return the simple type name of this port.
      */
     public String printSimpleType() {
       
       if (this.simpleTypeName == null) {
         this.simpleTypeName = Names.getSimpleName(printType());
       }
       
       return this.simpleTypeName;
     }
     
     /**
      * calculates the role type and collects ArcElements in their specific
      * typed lists.
      */
     protected void checkComponentType() {
 		portTypeCalculated = false;
 		
 		roles = new ArrayList<String>();
 		
 		for (ASTArcElement elem : getMainParent().getBody().getArcElement()) {
 			if(elem instanceof ASTSecArcRoleInterface) {
 				ASTSecArcRefRoleList refRole = ((ASTSecArcRoleInterface) elem).getSecArcRefRole();
 				for(ASTSecArcRefRole role : refRole.getList()) {
 					if(role.getPortName().equals(getName())) {
 						roles.addAll(role.getRoles());
 					}
 				}
 			}
 		}
 		
 		portTypeCalculated = true;
 		
     }
     
     /**
      * Returns roles for port
      * 
      * @return roles
      */
     public List<String> getRoles() {
    	 if(!portTypeCalculated) {
    		 checkComponentType();
    	 }
    	 
    	 return roles;
     }
     
}
