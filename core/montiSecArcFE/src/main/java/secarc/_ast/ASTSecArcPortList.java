/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;
 
/**
 * {@link ASTList} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcPortList extends PrototypeASTSecArcPortList {
   
	/**
     * Constructor for cc.clarc.lang.architecture._ast.ASTClArcMessagePortList
     */
    public ASTSecArcPortList() {
      super();
    }
    
    /**
     * Constructor for cc.clarc.lang.architecture._ast.ASTClArcMessagePortList
     */
    public ASTSecArcPortList(boolean strictlyOrdered) {
      super(strictlyOrdered);
    }
     
}
