/* (c) https://github.com/MontiCore/monticore */
 package secarc._ast;

import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.common._ast.UMLPNode;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcRole extends PrototypeASTSecArcRole {

	/*
	 * (non-Javadoc)
	 * @see mc.umlp.common._ast.UMLPNode#getMainParent()
	 */
	@Override
    public UMLPNode getMainParent() {
        // (ASTArcPort, ASTArcPortList) | (ASTSecArcRefRole, ASTSecArcRefRoleList) | (ASTSecArcRoleInterface, SecArcRoleInterfaceList), 
		// ASTArcRoleList, ASTArcInterface, ASTArcInterfaceList, ASTArcComponentBody in between
		if(_parent.get_Parent().get_Parent() instanceof ASTArcComponent) {
			return (ASTArcComponent) _parent.get_Parent().get_Parent();
		} else {
			return (ASTArcPort) _parent;
		}
    }
	
}
