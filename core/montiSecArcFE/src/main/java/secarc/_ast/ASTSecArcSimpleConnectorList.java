/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;
 
/**
 * {@link ASTList} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecArcSimpleConnectorList extends PrototypeASTSecArcSimpleConnectorList {

  /**
   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
   */
  public ASTSecArcSimpleConnectorList() {
	  super();
  }
 
  /**
   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
   */
   public ASTSecArcSimpleConnectorList(boolean strictlyOrdered) {
	   super(strictlyOrdered);
   }
     
}
