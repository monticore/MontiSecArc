/* (c) https://github.com/MontiCore/monticore */
package secarc._ast;

/**
 * {@link ASTNode} for the respective production in MontiSecArc.mc
 * 
 * 
 */
public class ASTSecMontiArcAutoConnectList extends PrototypeASTSecMontiArcAutoConnectList {
	
	/**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
	   */
	  public ASTSecMontiArcAutoConnectList() {
		  super();
	  }
	 
	  /**
	   * Constructor for cc.clarc.lang.architecture._ast.ASTSecArcConnectorList
	   */
	   public ASTSecMontiArcAutoConnectList(boolean strictlyOrdered) {
		   super(strictlyOrdered);
	   }
	     

}
