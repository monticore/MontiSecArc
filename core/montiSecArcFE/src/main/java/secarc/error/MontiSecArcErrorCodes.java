/* (c) https://github.com/MontiCore/monticore */
package secarc.error;

import mc.IErrorCode;

/**
 * Enumeration containing the MontiSecArc error codes.
 *
 *
 */
public enum MontiSecArcErrorCodes implements IErrorCode {
	
	/**
	 * The main component do not use accesscontrol
	 */
	AccessControlOff,
	
	/**
	 * For a component exists a unique version
	 */
	UniqueCPE,
	
	/**
	 * For a component exists a unique trustlevel
	 */
	UniqueTrustlevel,
	
	/**
	 * For a component exists a unique pep
	 */
	UniquePEP,
	
	/**
	 * For a component exists a unique configuration
	 */
	UniqueConfiguration,
	
	/**
	 * For a component exists a unique role definition
	 */
	UniqueComponentRole,
	
	/**
	 * Unique definition of the Target
	 */
	UniqueDefinitionWithFullQualifiedName,
	
	/**
	 * If accesscontrol is on, the roles must be defined in
	 * the subcomponents.
	 */
	RolesDefinedInSubcomponentsPEP,
	
	/**
	 * A referenced port must exist.
	 */
	PortExistence,
	
	/**
	 * For a component exists a unique definition for roles.
	 */
	UniqueComponentRoleDefinition,
	
	/**
	 * The roles within a definition are unique
	 */
	UniqueRoleDefinitionForComponent,
	
	/**
	 * The roles within a definition are unique
	 */
	UniqueRoleDefinitionForPort,
	
	/**
	 * Roles must be authenticated by an identity link
	 */
	RoleWithIdentity,
	
	/**
	 * Third party components consist of version and configuration
	 */
	CompoletnessThirdPartyComponent,
	
	/**
	 * Naming conventions for configurations, roles and filters
	 */
	NamingConventions,
	
	/**
	 * The referenced components must exist
	 */
	ComponentExistence,
	
	/**
	 * Different source and target component
	 */
	DifferentSourceTarget,
	
	/**
	 * Checks if the components have the correct relation regarding the trustlevel
	 */
	CorrectRelationBetweenTrustlevel,
	
	/**
	 * Checks for cycle with identity links
	 */
	IdentityCycleCheck,
	
}
