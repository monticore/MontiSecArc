/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

import secarc.ets.entries.SecComponentEntry;


/**
 * Helper for CoCos
 *
 *          $Date:$
 *
 */
public final class CoCoHelper {
	
	private CoCoHelper() {
		
	}
	
	/**
	 * Trustlevel object in String
	 * 
	 * @param entry
	 * @return trustlevel
	 */
	public static String getTrustlevelAsString(SecComponentEntry entry) {
		//Trustlevel is not present
		if(entry == null) {
			return "-1";
		}
		
		String trustlevel = "" + entry.getTrustlevel().get().getValue();
		if(entry.getTrustlevel().get().isNegative()) {
			trustlevel = "-" + trustlevel;
		} else {
			trustlevel = "+" + trustlevel;
		}
		return trustlevel;
	}
	
	/**
	 * Trustlevel object in Integer
	 * 
	 * @param entry
	 * @return trustlevel
	 */
	public static int getTrustlevelAsInteger(SecComponentEntry entry) {
		if(entry == null) {
			return -1;
		}
		
		int trustlevel = entry.getTrustlevel().get().getValue();
		if(entry.getTrustlevel().get().isNegative()) {
			trustlevel *= -1;
		} 
		return trustlevel;
	}
	
}
