/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

import java.util.Collection;

import java.util.HashSet;
import java.util.Set;

import mc.MCG;
import mc.helper.NameHelper;
import mc.types._ast.ASTQualifiedName;
import mc.umlp.arc.ets.check.MontiArcCheckCoCoVisitor;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcComponentBody;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.PortEntry;

import secarc.MontiSecArcConstants;
import secarc._ast.ASTMCCompilationUnit;
import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc._ast.ASTSecArcPEP;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.cocos.checkers.ISecComponentBodyChecker;
import secarc.ets.cocos.checkers.ISecComponentChecker;
import secarc.ets.cocos.checkers.ISecConfigurationChecker;
import secarc.ets.cocos.checkers.ISecFilterChecker;
import secarc.ets.cocos.checkers.ISecIdentityChecker;
import secarc.ets.cocos.checkers.ISecPepChecker;
import secarc.ets.cocos.checkers.ISecPortChecker;
import secarc.ets.cocos.checkers.ISecRefRoleChecker;
import secarc.ets.cocos.checkers.ISecRoleChecker;
import secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker;
import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.ArchitectureGraphBuilder;

import interfaces2.coco.ContextCondition;
import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;

/**
 * 
 * Visitor for MontiArc that checks context conditions.
 *
 *
 */
public class MontiSecArcCheckVisitor extends MontiArcCheckCoCoVisitor {
	
	/**
	 * Architecture graph for analysis
	 */
	protected ArchitectureGraph graph;
	
	/**
	 * Cocos that check the component related conditions
	 */
	protected Set<ISecComponentChecker> secComponentChecker;
	
	/**
	 * Cocos that check the body of a component 
	 */
	protected Set<ISecComponentBodyChecker> secComponentBodyChecker;
	
	/**
	 * Cocos that check the identity related conditions
	 */
	protected Set<ISecIdentityChecker> secIdentityChecker;
	
	/**
	 * Cocos that check pep related conditions
	 */
	protected Set<ISecPepChecker> secPepChecker;
	
	/**
	 * Cocos that check port roles related conditions
	 */
	protected Set<ISecRefRoleChecker> secRefRoleChecker;
	
	/**
	 * Cocos that check port related conditions
	 */
	protected Set<ISecPortChecker> secPortChecker;
	
	/**
	 * Cocos that check configuration related conditions
	 */
	protected Set<ISecConfigurationChecker> secConfigurationChecker;
	
	/**
	 * Cocos that check role related conditions
	 */
	protected Set<ISecRoleChecker> secRoleChecker;
	
	/**
	 * Cocos that check filter related conditions
	 */
	protected Set<ISecFilterChecker> secFilterChecker;
	
	/**
	 * Cocos that check trustlevel relation related conditions
	 */
	protected Set<ISecTrustlevelRelationChecker> secTrustlevelRelationChecker;
	
	
	public MontiSecArcCheckVisitor() {
		secComponentChecker = new HashSet<ISecComponentChecker>();
		secComponentBodyChecker = new HashSet<ISecComponentBodyChecker>();
		secIdentityChecker = new HashSet<ISecIdentityChecker>();
		secPepChecker = new HashSet<ISecPepChecker>();
		secRefRoleChecker = new HashSet<ISecRefRoleChecker>();
		secPortChecker = new HashSet<ISecPortChecker>();
		secConfigurationChecker = new HashSet<ISecConfigurationChecker>();
		secRoleChecker = new HashSet<ISecRoleChecker>();
		secFilterChecker = new HashSet<ISecFilterChecker>();
		secTrustlevelRelationChecker = new HashSet<ISecTrustlevelRelationChecker>();
	}
	
	/**
	 * Enabled cocos for supergrammars and MontiSecArc
	 * 
	 * @param enabledCoditions: Enabled cocos
	 */
	@Override
	public void setEnabledConditions(Collection<ContextCondition> enabledCoditions) {
		super.setEnabledConditions(enabledCoditions);
		createUsefulCollections();
	}
	
	/**
	 * Puts enabled context conditions in the corresponding sets
	 */
	protected void createUsefulCollections() {
		super.createUsefulCollections();
		
		for(ContextCondition coco : getEnabledConditions()) {
			if(coco instanceof ISecComponentChecker) {
				secComponentChecker.add((ISecComponentChecker) coco);
			}
			if(coco instanceof ISecComponentBodyChecker) {
				secComponentBodyChecker.add((ISecComponentBodyChecker) coco);
			}
			if(coco instanceof ISecIdentityChecker) {
				secIdentityChecker.add((ISecIdentityChecker) coco);
			}
			if(coco instanceof ISecPepChecker) {
				secPepChecker.add((ISecPepChecker) coco);
			}
			if(coco instanceof ISecRefRoleChecker) {
				secRefRoleChecker.add((ISecRefRoleChecker) coco); 
			}
			if(coco instanceof ISecPortChecker) {
				secPortChecker.add((ISecPortChecker) coco);
			}
 			if(coco instanceof ISecConfigurationChecker) {
 				secConfigurationChecker.add((ISecConfigurationChecker) coco);
 			} 
 			if(coco instanceof ISecRoleChecker) {
 				secRoleChecker.add((ISecRoleChecker) coco);
 			}
 			if(coco instanceof ISecFilterChecker) {
 				secFilterChecker.add((ISecFilterChecker) coco);
 			}
 			if(coco instanceof ISecTrustlevelRelationChecker) {
 				secTrustlevelRelationChecker.add((ISecTrustlevelRelationChecker) coco);
 			}
		}
	}
	
	/**
	 * Visits compilation unit and builds the architecture graph
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTMCCompilationUnit node) {
        
        SecComponentEntry componentEntry = null;
		
		try {
			componentEntry = (SecComponentEntry) resolver.resolve(node.getType().getName(), ComponentEntry.KIND, getNameSpaceFor(node));
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
		//Create graph from AST
		if(componentEntry != null) {
			graph = ArchitectureGraphBuilder.forArchitecture(componentEntry).buildGraphIdentityEdge();
		}
		
	}
	
	/**
	 * Visits portroles
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcRefRole node) {
		try {
			for (ISecRefRoleChecker cc : secRefRoleChecker) {
				cc.check(node);
			}
		} catch (AmbigousException e) {
			// not checked here
            MCG.getLogger().info(e.getMessage());
		}
	}
	
	/**
	 * Visits peps
	 * 
	 * @param secArcPEP ast node to visit
	 */
	public void visit(ASTSecArcPEP node) {
		try {
        	PEPEntry entry = (PEPEntry) resolver.resolve(MontiSecArcConstants.PEP_NAME, PEPEntry.KIND, getNameSpaceFor(node.getMainParent()));
            if (entry != null) {
                for (ISecPepChecker cc : secPepChecker) {
                    cc.check(node, entry);
                }
            }
        } catch (AmbigousException | EntryLoadingErrorException e) {
            // not checked here
            MCG.getLogger().info(e.getMessage());
        }
	}
	
	/**
	 * Visits identity link
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcIdentity node) {
		for (ASTQualifiedName name : node.getTargets()) {
            try {
                IdentityEntry entry = (IdentityEntry) resolver.resolve(NameHelper.dotSeparatedStringFromList(name.getParts()), IdentityEntry.KIND, getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecIdentityChecker cc : secIdentityChecker) {
                        cc.check(node, entry, graph);
                    }
                }
            } catch (AmbigousException | EntryLoadingErrorException e) {
            	delegator.addError("The identity link with the target '" + name +  " is ambiguous.", MontiSecArcContextConditionConstants.UNIQUE_DEFINITION_WITH_FULL_QUALIFIED_NAME, MontiSecArcErrorCodes.UniqueDefinitionWithFullQualifiedName, node.get_SourcePositionStart());
            }
		}
	}
	
	/**
	 * Visits component body
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTArcComponentBody node) {		
		for (ISecComponentBodyChecker cc : secComponentBodyChecker) {
            cc.check(node);
        }
	}
	
	/**
     * Visits components
     * 
     * @param node component to visit
     */
	@Override
    public void visit(ASTArcComponent node) {
    	super.visit(node);
    	
        try {
        	SecComponentEntry entry = (SecComponentEntry) resolver.resolve(node.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
            if (entry != null) {
                for (ISecComponentChecker cc : secComponentChecker) {
                    cc.check(node, entry, graph);
                }
            }
        } catch (AmbigousException e) {
            // not checked here
            MCG.getLogger().info(e.getMessage());
        }
    }
	
	/**
	 * Visits ports
	 * 
	 * @param node port to visit
	 */
	@Override
	public void visit(ASTArcPort node) {
		super.visit(node);
    	
        try {
        	SecPortEntry entry = (SecPortEntry) resolver.resolve(node.printName(), PortEntry.KIND, getNameSpaceFor(node));
            if (entry != null) {
                for (ISecPortChecker cc : secPortChecker) {
                    cc.check(node, entry);
                }
            }
        } catch (AmbigousException e) {
            // not checked here
            MCG.getLogger().info(e.getMessage());
        }
	}
	
	/**
	 * Visits configuration
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcConfiguration node) {
		try {
			ConfigurationEntry entry = (ConfigurationEntry) resolver.resolve(node.getName(), ConfigurationEntry.KIND, getNameSpaceFor(node));
            if (entry != null) {
                for (ISecConfigurationChecker cc : secConfigurationChecker) {
                    cc.check(node, entry);
                }
            }
        } catch (AmbigousException e) {
            // not checked here
            MCG.getLogger().info(e.getMessage());
        }
	}
	
	/**
	 * Visits role
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcRole node) {
		for (String name : node.getRoles()) {
            try {
                RoleEntry entry = (RoleEntry) resolver.resolve(name, RoleEntry.KIND, getNameSpaceFor(node));
                if (entry != null) {
                    for (ISecRoleChecker cc : secRoleChecker) {
                        cc.check(node, entry);
                    }
                }
            } catch (AmbigousException e) {
                // not handled here
                MCG.getLogger().info(e.getMessage());
            }
        }
	}
	
	/**
	 * Visits filter
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcFilter node) {
		try {
			FilterEntry entry = (FilterEntry) resolver.resolve(node.getName(), FilterEntry.KIND, getNameSpaceFor(node));
            if (entry != null) {
                for (ISecFilterChecker cc : secFilterChecker) {
                    cc.check(node, entry);
                }
            }
        } catch (AmbigousException e) {
            // not checked here
            MCG.getLogger().info(e.getMessage());
        }
	}
	
	/**
	 * Visits trustlevel relation
	 */
	public void visit(ASTSecArcTrustlevelRelation node) {
		try {
			String name = "";
			if(node.isGT()) {
				name = NameHelper.dotSeparatedStringFromList(node.getClient().getParts());
			} else {
				name = NameHelper.dotSeparatedStringFromList(node.getServer().getParts());
			}
			TrustlevelRelationEntry entry = (TrustlevelRelationEntry) resolver.resolve(name, TrustlevelRelationEntry.KIND, getNameSpaceFor(node.getMainParent()));
            if (entry != null) {
                for (ISecTrustlevelRelationChecker cc : secTrustlevelRelationChecker) {
                    cc.check(node, entry);
                }
            }
		} catch (AmbigousException e) {
			delegator.addError("The trustlevelrelation with the referenced components '" + NameHelper.dotSeparatedStringFromList(node.getClient().getParts()) +
					" and " + NameHelper.dotSeparatedStringFromList(node.getServer().getParts()) + " is ambiguous.", MontiSecArcContextConditionConstants.UNIQUE_DEFINITION_WITH_FULL_QUALIFIED_NAME, MontiSecArcErrorCodes.UniqueDefinitionWithFullQualifiedName, node.get_SourcePositionStart());
		}
	}
}
