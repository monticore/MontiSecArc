/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

/**
 * 
 * Context condition utility class.
 *
 *
 */
public final class MontiSecArcContextConditionConstants {

	private MontiSecArcContextConditionConstants() {
		
	}
	
	public static final String ALL_COMMON = "Checks all properties related to more than one element.";
	
	public static final String ALL_PROPERTY = "Checks all properties of MontiSecArc.";
	
	public static final String ALL_UNIQUENESS = "Checks all uniqueness context conditions of MontiSecArc.";
	
	public static final String ALL_TRUSTLEVEL = "Checks all context conditions related to trustlevel in MontiSecArc.";
	
	public static final String ALL_IDENTITY = "Checks all context conditions related to identity link in MontiSecArc.";
	
	public static final String ALL_ROLES = "Checks all context conditions related to roles in MontiSecArc.";
	
	public static final String ALL_ACCESSCONTROL = "Checks all context conditions related to accesscontrol in MontiSecArc.";
	
	public static final String ALL_VERSION = "Checks all context conditions related to version in MontiSecArc.";
	
	public static final String ALL_CONFIGURATION = "Checks all context conditions related to configuration in MontiSecArc.";
	
	public static final String ALL_CRITICALPORT = "Checks all context conditions related to critical port in MontiSecArc.";
	
	public static final String ALL_COMPONENT = "Checks all context conditions related to components.";
	
	public static final String ALL_TRUSTLEVEL_RELATION = "Checks all context conditions related to trustlevel relation.";

	public static final String ALL_CONVENTION = "Checks all conventions of MontiSecArc.";
	
	public static final String ACCESS_CONTROL_ON = "Checks if accesscontrol is on.";
	
	public static final String UNIQUE_CPE = "Checks if for a component exists a unique version.";
	
	public static final String UNIQUE_TRUSTLEVEL = "Checks if for a component exists a unique trustlevel.";
	
	public static final String UNIQUE_PEP = "Checks if for a component exists a unique pep.";
	
	public static final String UNIQUE_CONFIGURATION = "Checks if for a component exists a unique configuration.";
	
	public static final String UNIQUE_COMPONENT_ROLE = "Checks if for a component exists a unique role definition.";
	
	public static final String UNIQUE_DEFINITION_WITH_FULL_QUALIFIED_NAME = "Checks if the defintion of an identity link is unique.";
	
	public static final String IDENTITY_CYCLE_CHECK = "Checks if the identity links build a cycle.";

	public static final String ROLES_IN_SUBCOMPONENT_PEP = "Checks if roles are defined in subcomponent.";
	
	public static final String PORT_EXISTENCE = "Checks if a referenced port exists.";
	
	public static final String UNIQUE_ROLE_DEFINITION_FOR_COMPONENT = "Checks if a role is unique within a definition.";
	
	public static final String UNIQUE_ROLE_DEFINTION_FOR_PORT = "Checks if a role is unique within a definition.";
	
	public static final String EXISTENCE_TARGET_IDENTITY = "Checks if the target of the identity link exists.";
	
	public static final String ROLE_WITH_IDENTITY = "Checks if the roles are authenticated by an identity link.";
	
	public static final String COMPLETNESS_THIRD_PARTY_COMPONENT = "Checks if a third party component cosists of version and configuration.";
	
	public static final String NAMING_CONVENTIONS = "Names from configurations, roles and filters should start with a small letter.";
	
	public static final String COMPONENT_EXISTENCE = "Checks if the referenced components exist.";
	
	public static final String DIFFERENT_SOURCE_TARGET = "Checks if the source and target is different.";
	
	public static final String CORRECT_RELATION_BETWEEN_TRUSTLEVEL = "Checks if the given components have the correct relation regarding the trustlevel.";
	
	
}
