/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.check;

import java.util.HashMap;
import java.util.Map;


import secarc.ets.cocos.common.ComponentExistence;
import secarc.ets.cocos.common.DifferentSourceAndTarget;
import secarc.ets.cocos.common.NamingConventions;
import secarc.ets.cocos.component.CompletenessThirdPartyComponent;
import secarc.ets.cocos.configuration.UniqueConfiguration;
import secarc.ets.cocos.cpe.UniqueCPE;
import secarc.ets.cocos.identity.IdentityCycleCheck;
import secarc.ets.cocos.pep.AccessControlCoCo;
import secarc.ets.cocos.pep.RoleDefintionInSubcomponents;
import secarc.ets.cocos.pep.UniquePEP;
import secarc.ets.cocos.role.RefPortExistence;
import secarc.ets.cocos.role.RoleWithIdentity;
import secarc.ets.cocos.role.UniqueComponentRole;
import secarc.ets.cocos.role.UniqueRoleDefinitionForComponent;
import secarc.ets.cocos.role.UniqueRoleDefinitionForPort;
import secarc.ets.cocos.trustlevel.UniqueTrustlevel;
import secarc.ets.cocos.trustlevelrelation.CorrectRelationBetweenTrustlevel;

import mc.ProblemReport;
import mc.ProblemReport.Type;
import mc.umlp.arc.ets.check.coco.MontiArcDefaultContextConditionCreator;

import interfaces2.coco.AbstractContextCondition;
import interfaces2.coco.CompositeContextCondition;
import interfaces2.coco.ContextCondition;

/**
 * 
 * Serves default Context Conditions for MontiSecArc.
 *
 *
 */
public final class MontiSecArcContextConditionCreator {

	/**
	 * Singleton instance
	 */
	private static MontiSecArcContextConditionCreator creator = null;
	
	/**
	 * Root context condition
	 */
	private CompositeContextCondition conditions;
	
	/**
	 * Default configuration
	 */
	private Map<String, ProblemReport.Type> cocoConfiguration;
	
	/**
	 * 
	 * @return singleton instance
	 */
	private static MontiSecArcContextConditionCreator getInstance() {
		if (creator == null) {
			creator = new MontiSecArcContextConditionCreator();
		}
		return creator;
	}
	
	/**
	 * Private default constructor
	 */
	private MontiSecArcContextConditionCreator() {
		
	}
	
	/**
	 * 
	 * @return default MontiSecArc context condition configuration
	 */
	public static Map<String, ProblemReport.Type> createConfig() {
		return getInstance().doCreateConfiguration();
	}
	
	/**
	 * 
	 * @return default MontiSecArc context conditions
	 */
	public static AbstractContextCondition createConditions() {
		return getInstance().doCreateCondition();
	}
	
	/**
	 * 
	 * @return default MontiSecArc context conditions
	 */
	private AbstractContextCondition doCreateCondition() {
		if(conditions == null) {
			conditions = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_PROPERTY);
			
			//MontiArc and ArchitectureDiagram context conditions
			AbstractContextCondition arcCond = MontiArcDefaultContextConditionCreator.createConditions();
			for (ContextCondition cc : arcCond.getLeafs()) {
                  conditions.addChild(cc);
            }
			
			//Context conditions for MontiSecArc
			CompositeContextCondition accessControlCoCo = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_ACCESSCONTROL);
			accessControlCoCo.setLevel(Type.ERROR);
			
			//Checks if accesscontrol is used
			accessControlCoCo.addChild(new AccessControlCoCo());
			
			CompositeContextCondition uniqueCoCo = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_UNIQUENESS);
			uniqueCoCo.setLevel(Type.ERROR);
			
			//Unique Version in AST
			uniqueCoCo.addChild(new UniqueCPE());
			
			//Unique Trustlevel in AST
			uniqueCoCo.addChild(new UniqueTrustlevel());
			
			//Unique PEP in AST
			uniqueCoCo.addChild(new UniquePEP());
			
			//Unique configuration in AST
			uniqueCoCo.addChild(new UniqueConfiguration());
			
			//Unique role definition for Component in AST
			uniqueCoCo.addChild(new UniqueRoleDefinitionForComponent());
			
			//Unique role definition for Port in AST
			uniqueCoCo.addChild(new UniqueRoleDefinitionForPort());
			
			//Unique ComponentRole defintion in AST
			uniqueCoCo.addChild(new UniqueComponentRole());
			
			CompositeContextCondition identityCoCos = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_IDENTITY);
			identityCoCos.setLevel(Type.ERROR);
			
			//Roles must be authenticated
			identityCoCos.addChild(new RoleWithIdentity());
			
			//No cycles allowed
			identityCoCos.addChild(new IdentityCycleCheck());
			
			CompositeContextCondition roleCoCos = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_ROLES);
			roleCoCos.setLevel(Type.ERROR);
			
			//If pep exists, roles must be defined in subcomponents
			roleCoCos.addChild(new RoleDefintionInSubcomponents());
			
			//The refereced port must exist
			roleCoCos.addChild(new RefPortExistence());
			
			//CoCos for components
			CompositeContextCondition componentCoCos = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_COMPONENT);
			componentCoCos.setLevel(Type.ERROR);
			
			//Third party components consist of version and configuration
			componentCoCos.addChild(new CompletenessThirdPartyComponent());
			
			//CoCos for components
			CompositeContextCondition conventionCoCos = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_CONVENTION);
			conventionCoCos.setLevel(Type.WARNING);
			
			//Checks naming conventions for filter, configuration and role
			conventionCoCos.addChild(new NamingConventions());
			
			//CoCos for more than one element
			CompositeContextCondition commonCoCos = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_COMMON);
			commonCoCos.setLevel(Type.ERROR);
			
			//Checks component existence for identity and trustlevel relation
			commonCoCos.addChild(new ComponentExistence());
			
			//Checks if the source and target of identity/trustlevelrelation are different
			commonCoCos.addChild(new DifferentSourceAndTarget());
			
			//CoCos for trustlevel relation
			CompositeContextCondition trustlevelRelationCoCos = new CompositeContextCondition(MontiSecArcContextConditionConstants.ALL_TRUSTLEVEL_RELATION);
			trustlevelRelationCoCos.setLevel(Type.ERROR);
			
			//Checks if relation between trustlevel is correct
			trustlevelRelationCoCos.addChild(new CorrectRelationBetweenTrustlevel());
			
			conditions.addChild(accessControlCoCo);
			conditions.addChild(uniqueCoCo);
			conditions.addChild(identityCoCos);
			conditions.addChild(roleCoCos);
			conditions.addChild(componentCoCos);
			conditions.addChild(conventionCoCos);
			conditions.addChild(commonCoCos);
			conditions.addChild(trustlevelRelationCoCos);
		}
		return conditions;
	}
	
	/**
	 * 
	 * @return default MontiSecArc context conditions configuration
	 */
	private Map<String,ProblemReport.Type> doCreateConfiguration() {
		if(cocoConfiguration == null) {
			cocoConfiguration = new HashMap<String, Type>();
			Map<String, Type> arcConfig = MontiArcDefaultContextConditionCreator.createDefaultConfig();
            for (Map.Entry<String, Type> s : arcConfig.entrySet()) {
            	cocoConfiguration.put(s.getKey(), s.getValue());
            }
            
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_PROPERTY, Type.WARNING);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_ACCESSCONTROL, Type.ERROR);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_UNIQUENESS, Type.ERROR);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_IDENTITY, Type.ERROR);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_ROLES, Type.ERROR);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_COMPONENT, Type.ERROR);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_CONVENTION, Type.WARNING);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_COMMON, Type.ERROR);
            cocoConfiguration.put(MontiSecArcContextConditionConstants.ALL_TRUSTLEVEL_RELATION, Type.ERROR);
            
			
		}
		return cocoConfiguration;
	}
	
}
