/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import mc.umlp.arcd._ast.ASTArcComponentBody;

/**
 * Context condition checker interface for checking ComponetBody
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecComponentBodyChecker {

	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related version entry to be checked
	 */
	void check(ASTArcComponentBody node);	
	
}
