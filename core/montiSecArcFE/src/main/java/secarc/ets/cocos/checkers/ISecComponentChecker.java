/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;
import mc.umlp.arcd._ast.ASTArcComponent;

/**
 * Context condition checker interface for checking SecComponent
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecComponentChecker {
	
	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related component entry to be checked
	 * @throws AmbigousException 
	 */
	void check(ASTArcComponent node, SecComponentEntry entry, ArchitectureGraph graph) throws AmbigousException;

}
