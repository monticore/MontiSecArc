/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcConfiguration;
import secarc.ets.entries.ConfigurationEntry;

/**
 * Context condition checker interface for checking configuration
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecConfigurationChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	void check(ASTSecArcConfiguration node, ConfigurationEntry entry) throws AmbigousException;	
	
}
