/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcFilter;
import secarc.ets.entries.FilterEntry;

/**
 * Context condition checker interface for checking filter
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecFilterChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	void check(ASTSecArcFilter node, FilterEntry entry) throws AmbigousException;
	
}
