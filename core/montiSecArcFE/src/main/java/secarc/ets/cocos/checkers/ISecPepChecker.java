/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcPEP;
import secarc.ets.entries.PEPEntry;

/**
 * Context condition checker interface for checking pep
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecPepChecker {

	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related pep to be checked
	 * @throws AmbigousException 
	 */
	void check(ASTSecArcPEP node, PEPEntry entry) throws AmbigousException, EntryLoadingErrorException;
	
}
