/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc.ets.entries.SecPortEntry;

/**
 * Context condition checker interface for checking port
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecPortChecker {

	/**
	 * 
	 * @param node ast node to be checked
	 * @param entry entry related port to be checked
	 * @throws AmbigousException 
	 */
	void check(ASTArcPort node, SecPortEntry entry) throws AmbigousException;
	
}
