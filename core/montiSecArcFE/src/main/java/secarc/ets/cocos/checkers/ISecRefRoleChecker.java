/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcRefRole;

/**
 * Context condition checker interface for checking port roles
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecRefRoleChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException 
	 */
	void check(ASTSecArcRefRole node) throws AmbigousException;
	
}
