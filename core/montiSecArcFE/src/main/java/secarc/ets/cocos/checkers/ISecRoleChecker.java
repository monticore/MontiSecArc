/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.checkers;

import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcRole;
import secarc.ets.entries.RoleEntry;

/**
 * Context condition checker interface for checking role
 * related context conditions
 * 
 * 
 * 
 */
public interface ISecRoleChecker {

	/**
	 * 
	 * @param node
	 * @param entry
	 * @throws AmbigousException
	 */
	void check(ASTSecArcRole node, RoleEntry entry) throws AmbigousException;
	
}
