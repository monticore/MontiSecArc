/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.common;

import mc.IErrorCode;
import mc.ast.ASTNode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import interfaces2.coco.ContextCondition;
import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecIdentityChecker;
import secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Checks if the components exist for identity link and trustlevel relation
 * 
 * 
 * 
 */
public class ComponentExistence extends ContextCondition implements
		ISecTrustlevelRelationChecker, ISecIdentityChecker {

	public ComponentExistence() {
		super(MontiSecArcContextConditionConstants.COMPONENT_EXISTENCE);
	}
	
	public ComponentExistence(String checkedProperty) {
		super(checkedProperty);
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecIdentityChecker#check(secarc._ast.ASTSecArcIdentity, secarc.ets.entries.IdentityEntry)
	 */
	@Override
	public void check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graph)
			throws AmbigousException, EntryLoadingErrorException {
		String refSource = entry.getSource();
		String refTarget = entry.getTarget();
		
		SubComponentEntry sourceSubComponent = innerCheck(node, node.getMainParent(), refSource);
		
		SubComponentEntry targetSubComponent = innerCheck(node, node.getMainParent(), refTarget);
		
		if(sourceSubComponent == null) {
			addReport("The refereced component " + refSource + " does not exist. Use full qualified names in order to reference a component.", node.get_SourcePositionStart());
		} 
		
		if(targetSubComponent == null) {
			addReport("The refereced component " + refTarget + " does not exist in the given namespace. Use full qualified names in order to reference a component.", node.get_SourcePositionStart());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker#check(secarc._ast.ASTSecArcTrustlevelRelation, secarc.ets.entries.TrustlevelRelationEntry)
	 */
	@Override
	public void check(ASTSecArcTrustlevelRelation node,
			TrustlevelRelationEntry entry) throws AmbigousException {
		
		String refLowerTrustlevelComponent = entry.getComponentWithLowerTrustlevel();
		String refHigherTrustlevelComponent = entry.getComponentWithHigherTrustlevel();
		
		SubComponentEntry lowerTrustlevelComoponent = innerCheck(node, node.getMainParent(), refLowerTrustlevelComponent);
		
		SubComponentEntry higherTrustlevelComoponent = innerCheck(node, node.getMainParent(), refHigherTrustlevelComponent);
		
		if(lowerTrustlevelComoponent == null) {
			addReport("The refereced component " + refLowerTrustlevelComponent + " does not exist in the given namespace.", node.get_SourcePositionStart());
		} 
		
		if(higherTrustlevelComoponent == null) {
			addReport("The refereced component " + refHigherTrustlevelComponent + " does not exist in the given namespace.", node.get_SourcePositionStart());
		}
	}
	
	/**
	 * Searchs for referenced Comonent
	 * 
	 * 
	 * @param node
	 * @param parent
	 * @param refComponent
	 * @return SecComponentEntry if it exists
	 * @throws AmbigousException
	 */
	protected SubComponentEntry innerCheck(ASTNode node, ASTArcComponent parent, String refComponent) throws AmbigousException {
		SubComponentEntry refSubcomponent = null;
		
		ComponentEntry rootSubComponentEntry = null;
		
		//It's the same component
		if(!refComponent.contains(".")) {
			refSubcomponent = (SubComponentEntry) resolver.resolve(refComponent, SubComponentEntry.KIND, getNameSpaceFor(parent));
			if(refSubcomponent == null && parent.getMainParent() != null) {
				refSubcomponent = (SubComponentEntry) resolver.resolve(refComponent, SubComponentEntry.KIND, getNameSpaceFor(parent.getMainParent()));
			}
		} else {
			//Full qualified name starts in supercomponent
			rootSubComponentEntry = (ComponentEntry) resolver.resolve(parent.getName(), ComponentEntry.KIND, getNameSpaceFor(node));
			
			if(rootSubComponentEntry != null) {
				refSubcomponent = findFullQulifiedSubComponentFromRoot(rootSubComponentEntry, refComponent);
			}
		}
		
		return refSubcomponent;
		
	}
	
	/**
	 * Starts in AST root element and searches for full qualified components  
	 * 
	 * @param parent
	 * @param fullQualifiedName
	 * @return
	 */
	private SubComponentEntry findFullQulifiedSubComponentFromRoot(ComponentEntry parent, String fullQualifiedName) {
		int firstDot = fullQualifiedName.indexOf(".");
		String firstPart = "";
		if(firstDot != -1) {
			firstPart = fullQualifiedName.substring(0, firstDot);
		} else {
			firstPart = fullQualifiedName;
			return findSubComponent(parent, firstPart);
		}
		
		SubComponentEntry subComponentEntry = findSubComponent(parent, firstPart);
		if(subComponentEntry != null) {
			return findFullQulifiedSubComponentFromRoot(subComponentEntry.getComponentType(), fullQualifiedName.substring(firstDot + 1));
		} else {
			return null;
		}
	}
	
	/**
	 * Searches in subcomponents for component with name
	 * 
	 * @param parent
	 * @param name
	 * @return
	 */
	private SubComponentEntry findSubComponent(ComponentEntry parent, String name) {
		for(SubComponentEntry subCompent : parent.getSubComponents()) {
			if(subCompent.getName().equals(name)) {
				return subCompent;
			}			
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.ComponentExistence;
	}

}
