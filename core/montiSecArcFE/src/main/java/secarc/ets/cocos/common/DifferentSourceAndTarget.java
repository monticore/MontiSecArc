/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.common;

import mc.IErrorCode;
import mc.ast.ASTNode;
import interfaces2.coco.ContextCondition;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecIdentityChecker;
import secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Checks if the source and target are different
 * 
 * 
 * 
 * 
 */
public class DifferentSourceAndTarget extends ContextCondition implements
		ISecIdentityChecker, ISecTrustlevelRelationChecker{

	public DifferentSourceAndTarget() {
		super(MontiSecArcContextConditionConstants.DIFFERENT_SOURCE_TARGET);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecIdentityChecker#check(secarc._ast.ASTSecArcIdentity, secarc.ets.entries.IdentityEntry)
	 */
	@Override
	public void check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graph)
			throws AmbigousException {
		innerCheck(entry.getSource(), entry.getTarget(), node);
	}
	
	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker#check(secarc._ast.ASTSecArcTrustlevelRelation, secarc.ets.entries.TrustlevelRelationEntry)
	 */
	@Override
	public void check(ASTSecArcTrustlevelRelation node,
			TrustlevelRelationEntry entry) throws AmbigousException {
		innerCheck(entry.getComponentWithLowerTrustlevel(), entry.getComponentWithHigherTrustlevel(), node);
	}
	
	/**
	 * Checks, if the referenced components are different
	 * @param refSource
	 * @param refTarget
	 * @param node
	 */
	protected void innerCheck(String refSource, String refTarget, ASTNode node) {
		if(refSource.equals(refTarget)) {
			addReport("The two used components must be different. " + refSource + " is equlas " + refTarget, node.get_SourcePositionStart());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.DifferentSourceTarget;
	}

}
