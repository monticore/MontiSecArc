/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.common;

import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcRole;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecConfigurationChecker;
import secarc.ets.cocos.checkers.ISecFilterChecker;
import secarc.ets.cocos.checkers.ISecRoleChecker;
import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.RoleEntry;
import mc.IErrorCode;
import mc.helper.NameHelper;
import interfaces2.coco.ContextCondition;
import interfaces2.resolvers.AmbigousException;

/**
 * Naming conventions for role filter and configuration
 * 
 * 
 * 
 */
public class NamingConventions extends ContextCondition implements ISecConfigurationChecker, ISecFilterChecker, ISecRoleChecker {

	public NamingConventions() {
		super(MontiSecArcContextConditionConstants.NAMING_CONVENTIONS);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecRoleChecker#check(secarc._ast.ASTSecArcRole, secarc.ets.entries.RoleEntry)
	 */
	@Override
	public void check(ASTSecArcRole node, RoleEntry entry)
			throws AmbigousException {
		String name = entry.getName();
		if (NameHelper.firstToUpper(name).equals(name)) {
            addReport("The name from the role '" + name + "' should start with a small letter.", node.get_SourcePositionStart());
        }
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecFilterChecker#check(secarc._ast.ASTSecArcFilter, secarc.ets.entries.FilterEntry)
	 */
	@Override
	public void check(ASTSecArcFilter node, FilterEntry entry)
			throws AmbigousException {
		String name = entry.getName();
		if (NameHelper.firstToUpper(name).equals(name)) {
            addReport("The name from the filter '" + name + "' should start with a small letter.", node.get_SourcePositionStart());
        }
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecConfigurationChecker#check(secarc._ast.ASTSecArcConfiguration, secarc.ets.entries.ConfigurationEntry)
	 */
	@Override
	public void check(ASTSecArcConfiguration node, ConfigurationEntry entry)
			throws AmbigousException {
		String name = entry.getName();
		if (NameHelper.firstToUpper(name).equals(name)) {
            addReport("The name from the configuration '" + name + "' should start with a small letter.", node.get_SourcePositionStart());
        }
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.NamingConventions;
	}

}
