/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.component;

import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import interfaces2.coco.ContextCondition;
import interfaces2.resolvers.AmbigousException;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentChecker;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * A third party component consists always of a configuration and a version
 * 
 * 
 * 
 */
public class CompletenessThirdPartyComponent extends ContextCondition implements
		ISecComponentChecker {
	
	/** Third party component definition literal. */
    private static final String THIRD_PARTY_COMPONET_DEFINTION = "A third party component consists of a cpe and configuration.";

	
	public CompletenessThirdPartyComponent() {
		super(MontiSecArcContextConditionConstants.COMPLETNESS_THIRD_PARTY_COMPONENT);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public void check(ASTArcComponent node, SecComponentEntry entry, ArchitectureGraph graph)
			throws AmbigousException {
		
		if(entry.getCPE() != null && entry.getConfiguration() == null) {
			addReport("The component " + NameHelper.getSimplenameFromComplexname(entry.getName()) + " has a version, but the configration is missing. " +
					THIRD_PARTY_COMPONET_DEFINTION, node.get_SourcePositionStart());
		}
		
		if(entry.getCPE() == null && entry.getConfiguration() != null) {
			addReport("The component " + NameHelper.getSimplenameFromComplexname(entry.getName()) + " has a configuration, but the cpe is missing." +
					THIRD_PARTY_COMPONET_DEFINTION, node.get_SourcePositionStart());
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.CompoletnessThirdPartyComponent;
	}

}
