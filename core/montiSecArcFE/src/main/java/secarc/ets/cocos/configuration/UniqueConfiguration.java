/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.configuration;

import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcComponentBody;
import mc.umlp.arcd._ast.ASTArcElement;
import interfaces2.coco.ContextCondition;
import secarc._ast.ASTSecArcConfiguration;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentBodyChecker;

/**
 * Check if the configuration within a component is unique
 * 
 * 
 * 
 */
public class UniqueConfiguration extends ContextCondition implements
		ISecComponentBodyChecker {

	public UniqueConfiguration() {
		super(MontiSecArcContextConditionConstants.UNIQUE_CONFIGURATION);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentBodyChecker#check(mc.umlp.arcd._ast.ASTArcComponentBody)
	 */
	@Override
	public void check(ASTArcComponentBody node) {
		int count = 0;
		
		for(ASTArcElement children : node.getArcElement()) {
			if(children instanceof ASTSecArcConfiguration) {
				count++;
			}
		}
		
		ASTArcComponent componentNode = (ASTArcComponent) node.get_Parent();
		
		if(count > 1) {
			addReport("The component " + NameHelper.getSimplenameFromComplexname(componentNode.getName()) + " has more than one configuration. A component has a unique configuration.", node.get_SourcePositionStart());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.UniqueConfiguration;
	}

}
