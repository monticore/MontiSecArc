/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.cpe;

import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcComponentBody;
import mc.umlp.arcd._ast.ASTArcElement;
import interfaces2.coco.ContextCondition;
import secarc._ast.ASTSecArcCPE;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentBodyChecker;

/**
 * Checks if the version definition is unique
 * 
 * 
 * 
 */
public class UniqueCPE extends ContextCondition implements ISecComponentBodyChecker {

	public UniqueCPE() {
		super(MontiSecArcContextConditionConstants.UNIQUE_CPE);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentBodyChecker#check(mc.umlp.arcd._ast.ASTArcComponentBody)
	 */
	@Override
	public void check(ASTArcComponentBody node) {
		int count = 0;
		
		for(ASTArcElement children : node.getArcElement()) {
			if(children instanceof ASTSecArcCPE) {
				count++;
			}
		}
		
		ASTArcComponent parent = (ASTArcComponent) node.get_Parent();
		
		if(count > 1) {
			addReport("The component " + NameHelper.getSimplenameFromComplexname(parent.getName()) + " has more than one cpe definition.", node.get_SourcePositionStart());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.UniqueCPE;
	}
	
}
