/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.identity;

import java.util.Set;

import org.jgrapht.alg.CycleDetector;

import mc.IErrorCode;
import interfaces2.STEntry;
import interfaces2.coco.ContextCondition;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecIdentityChecker;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

public class IdentityCycleCheck extends ContextCondition implements ISecIdentityChecker {

	public IdentityCycleCheck() {
		super(MontiSecArcContextConditionConstants.IDENTITY_CYCLE_CHECK);
	}

	@Override
	public void check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graph)
			throws AmbigousException {
		
		CycleDetector<Vertex<? extends STEntry>, Edge> cycleDetector = new CycleDetector<Vertex<? extends STEntry>, Edge>(graph.getRawGraph());
		
		Set<Vertex<? extends STEntry>> setCycle = cycleDetector.findCycles();
		
		if(setCycle.contains(Vertex.of(entry))) {
			addReport("There is a cycle of identity links starting in " + entry, node.get_SourcePositionStart());
		}
		
	}

	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.IdentityCycleCheck;
	}

}
