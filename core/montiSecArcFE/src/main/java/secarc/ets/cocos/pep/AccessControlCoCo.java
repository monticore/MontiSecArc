/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.pep;


import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import interfaces2.coco.ContextCondition;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentChecker;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Check if accesscontrol is used or roles are defined 
 * 
 * 
 * 
 */
public class AccessControlCoCo extends ContextCondition implements
		ISecComponentChecker {

	public AccessControlCoCo() {
		super(MontiSecArcContextConditionConstants.ACCESS_CONTROL_ON);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public void check(ASTArcComponent node, SecComponentEntry entry, ArchitectureGraph graph) {
		
		//check if roles exist for this component or ports
		//component also saves roles from components
		boolean existRolesEntry = entry.getRoles() == null || entry.getRoles().isEmpty();
		
		// Only for main component
		if (!entry.isInnerComponent()) {
			//No PEP, No Roles in this component
			if (entry.getPEP() == null && existRolesEntry) {
				addReport("The exterior component " + NameHelper.getSimplenameFromComplexname(entry.getName())
						+ " have to use access control or roles have to be defined.",
						node.get_SourcePositionStart());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.AccessControlOff;
	}

}
