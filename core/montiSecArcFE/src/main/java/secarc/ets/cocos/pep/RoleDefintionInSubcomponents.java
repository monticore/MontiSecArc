/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.pep;

import java.util.List;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import interfaces2.coco.ContextCondition;
import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcPEP;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecPepChecker;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.SecComponentEntry;

/**
 * Check if roles are defined in subcomponents
 * 
 * 
 * 
 */
public class RoleDefintionInSubcomponents extends ContextCondition implements
		ISecPepChecker {

	public RoleDefintionInSubcomponents() {
		super(MontiSecArcContextConditionConstants.ROLES_IN_SUBCOMPONENT_PEP);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecPepChecker#check(secarc._ast.ASTSecArcPEP, secarc.ets.entries.PEPEntry)
	 */
	@Override
	public void check(ASTSecArcPEP node, PEPEntry entry) throws AmbigousException, EntryLoadingErrorException {
		//This check is not needed if accesscontrol is off
		if(entry.isOff()) {
			return;
		}
		
		ASTArcComponent parent = node.getMainParent();
		SecComponentEntry compEntry = null;
		
		compEntry = (SecComponentEntry) resolver.resolve(parent.getName(), ComponentEntry.KIND, getNameSpaceFor(parent));
		
		//Checked in another CoCo
		if(compEntry == null) {
			return;
		}
		
		//check if roles are already in the main parent defined
		List<RoleEntry> roles = compEntry.getRoles();
		if(roles != null && !roles.isEmpty()) {
			return;
		} 
		
		List<SubComponentEntry> subComponentEntries = compEntry.getSubComponents();
		
		if(checkForRoles(subComponentEntries)){
			return; 
		}
		
		addReport("If accesscontrol is activated, the roles must be specified in the subcomponents.", node.get_SourcePositionStart());
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.RolesDefinedInSubcomponentsPEP;
	}

	/**
	 * Checks rekursive role existence
	 *  
	 * @param subComponentEntries
	 * @return boolean 
	 * @throws EntryLoadingErrorException 
	 */
	private boolean checkForRoles(List<SubComponentEntry> subComponentEntries) throws EntryLoadingErrorException {
		if(subComponentEntries == null || subComponentEntries.isEmpty()) {
			return false;
		} 
		for(SubComponentEntry subComponentEntry : subComponentEntries) {
			SecComponentEntry compType = (SecComponentEntry) subComponentEntry.getComponentType();
			
			if(compType == null) {
				return false;
			}
			//check for roles
			List<RoleEntry> roles = compType.getRoles();
			if(roles != null && !roles.isEmpty()) {
				return true;
			} else {
				if(checkForRoles(compType.getSubComponents())) {
					return true;
				}
			}
		}
		return false;
	}
	
}
