/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.role;

import mc.IErrorCode;
import mc.umlp.arcd.ets.entries.PortEntry;
import interfaces2.coco.ContextCondition;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcRefRole;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecRefRoleChecker;

/**
 * Checks if the referenced port/Role exists
 * 
 * 
 */
public class RefPortExistence extends ContextCondition implements
		ISecRefRoleChecker { 

	public RefPortExistence() {
		super(MontiSecArcContextConditionConstants.PORT_EXISTENCE);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecRefRoleChecker#check(secarc._ast.ASTSecArcRefRole)
	 */
	@Override
	public void check(ASTSecArcRefRole node) throws AmbigousException {
		PortEntry portEntry = null;
		String refElement = node.getPortName();
		
		//Port ref can be only defined in the same component
		portEntry = (PortEntry) resolver.resolve(refElement, PortEntry.KIND, getNameSpaceFor(node));
		
		if(portEntry == null) {
			addReport("The referenced port " + refElement + " does not exist in the given component", node.get_SourcePositionStart());
		} 
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.PortExistence;
	}

}
