/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.role;

import java.util.List;

import org.jgrapht.traverse.BreadthFirstIterator;
import org.jgrapht.traverse.GraphIterator;


import interfaces2.STEntry;
import interfaces2.coco.ContextCondition;
import interfaces2.resolvers.AmbigousException;
import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentChecker;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;
import secarc.ets.graph.Edge;
import secarc.ets.graph.Vertex;

/**
 * Checks if a role is authenticated
 * 
 * 
 */
public class RoleWithIdentity extends ContextCondition implements
		ISecComponentChecker {

	public RoleWithIdentity() {
		super(MontiSecArcContextConditionConstants.ROLE_WITH_IDENTITY);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public void check(ASTArcComponent node, SecComponentEntry entry, ArchitectureGraph graph) throws AmbigousException {
		List<RoleEntry> roles = entry.getRoles();
		
		if(roles != null && !roles.isEmpty()) {
			
			Vertex<ComponentEntry> componentVertex = Vertex.of(entry);
			
			//Look for paths with port as beginning
			GraphIterator<Vertex<? extends STEntry>, Edge> iterator = new BreadthFirstIterator<Vertex<? extends STEntry>, Edge>(graph.getReversedRawGraph(), componentVertex);
			
			STEntry element = null;
			
			boolean identityExistence = false;
			
			//first element is not needed
			iterator.next();
			
			while(iterator.hasNext()) {
				element = iterator.next().getArchitectureElement();
				
				if(element instanceof IdentityEntry) {
					identityExistence = true;
				}
			} 
			
			//First element is not needed
			if(!identityExistence) {
				addReport("For the component " + entry.getName() + " are roles defined. These roles must be authenticated by an identity link.", node.get_SourcePositionStart());
			}
			
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.RoleWithIdentity;
	}

}
