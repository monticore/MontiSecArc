/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.role;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponentBody;
import mc.umlp.arcd._ast.ASTArcElement;
import interfaces2.coco.ContextCondition;
import secarc._ast.ASTSecArcRole;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentBodyChecker;

/**
 * Check if the component role definition is unique
 * 
 * 
 * 
 */
public class UniqueComponentRole extends ContextCondition implements
		ISecComponentBodyChecker {

	public UniqueComponentRole() {
		super(MontiSecArcContextConditionConstants.UNIQUE_COMPONENT_ROLE);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentBodyChecker#check(mc.umlp.arcd._ast.ASTArcComponentBody)
	 */
	@Override
	public void check(ASTArcComponentBody node) {
		int count = 0;
		
		for(ASTArcElement children : node.getArcElement()) {
			if(children instanceof ASTSecArcRole) {
				count++;
			}
		}
		
		if(count > 1) {
			addReport("Components have a unique role definition.", node.get_SourcePositionStart());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.UniqueComponentRole;
	}

}
