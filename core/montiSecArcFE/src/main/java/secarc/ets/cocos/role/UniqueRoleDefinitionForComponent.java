/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.role;

import java.util.List;


import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import interfaces2.coco.ContextCondition;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentChecker;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Check if there are only unique ports
 * 
 * 
 * 
 */
public class UniqueRoleDefinitionForComponent extends ContextCondition
		implements ISecComponentChecker {

	public UniqueRoleDefinitionForComponent() {
		super(MontiSecArcContextConditionConstants.UNIQUE_ROLE_DEFINITION_FOR_COMPONENT);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentChecker#check(mc.umlp.arcd._ast.ASTArcComponent, secarc.ets.entries.SecComponentEntry)
	 */
	@Override
	public void check(ASTArcComponent node, SecComponentEntry entry, ArchitectureGraph graph) {
		List<RoleEntry> roles = entry.getRoles();
		
		String role = "";
		
		for(int i = 0; i< roles.size()-1; i++) {
			role = roles.get(i).getName();
			for(int j = i+1; j < roles.size(); j++) {
				if(role.equals(roles.get(j).getName())) {
					addReport("The role " + role + " exists twice for the component: " + NameHelper.getSimpleGenericNameFromComplexname(entry.getName()), node.get_SourcePositionStart());
					break;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.UniqueRoleDefinitionForComponent;
	}

}
