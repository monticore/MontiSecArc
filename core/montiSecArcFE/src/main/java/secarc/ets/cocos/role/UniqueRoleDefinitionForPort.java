/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.role;

import java.util.List;

import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcPort;
import interfaces2.coco.ContextCondition;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecPortChecker;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecPortEntry;

/**
 * Check if there are only unique ports
 * 
 * 
 * 
 */
public class UniqueRoleDefinitionForPort extends ContextCondition implements
		ISecPortChecker {

	public UniqueRoleDefinitionForPort() {
		super(MontiSecArcContextConditionConstants.UNIQUE_ROLE_DEFINTION_FOR_PORT);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecPortChecker#check(mc.umlp.arcd._ast.ASTArcPort, secarc.ets.entries.SecPortEntry)
	 */
	@Override
	public void check(ASTArcPort node, SecPortEntry entry) {
		List<RoleEntry> roles = entry.getRoles();
		
		String role = "";
		
		for(int i = 0; i < roles.size()-1; i++) {
			role = roles.get(i).getName();
			for(int j = i+1; j < roles.size(); j++) {
				if(role.equals(roles.get(j).getName())) {
					addReport("The role " + role + " exists twice for the port: " + NameHelper.getSimpleGenericNameFromComplexname(entry.getName()), node.get_SourcePositionStart());
					break;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.UniqueRoleDefinitionForPort;
	}

}
