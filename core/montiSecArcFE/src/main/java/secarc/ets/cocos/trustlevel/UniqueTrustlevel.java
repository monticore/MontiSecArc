/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.trustlevel;

import mc.IErrorCode;
import mc.helper.NameHelper;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcComponentBody;
import mc.umlp.arcd._ast.ASTArcElement;
import interfaces2.coco.ContextCondition;
import secarc._ast.ASTSecArcTrustLevel;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.checkers.ISecComponentBodyChecker;

/**
 * Checks if the trustlevel definition is unique
 * 
 * 
 * 
 */
public class UniqueTrustlevel extends ContextCondition implements
		ISecComponentBodyChecker {

	public UniqueTrustlevel() {
		super(MontiSecArcContextConditionConstants.UNIQUE_TRUSTLEVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecComponentBodyChecker#check(mc.umlp.arcd._ast.ASTArcComponentBody)
	 */
	@Override
	public void check(ASTArcComponentBody node) {
		int count = 0;
		
		for(ASTArcElement children : node.getArcElement()) {
			if(children instanceof ASTSecArcTrustLevel) {
				count++;
			}
		}
		
		ASTArcComponent parent = (ASTArcComponent) node.get_Parent();
		
		if(count > 1) {
			addReport(" The conponent " + NameHelper.getSimplenameFromComplexname(parent.getName()) + " has more than one trustlevel definition.", node.get_SourcePositionStart());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.UniqueTrustlevel;
	}

}
