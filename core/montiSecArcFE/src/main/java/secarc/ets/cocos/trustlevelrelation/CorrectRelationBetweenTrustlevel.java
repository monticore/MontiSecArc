/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.cocos.trustlevelrelation;

import com.google.common.base.Optional;

import mc.IErrorCode;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;
import interfaces2.resolvers.AmbigousException;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc.error.MontiSecArcErrorCodes;
import secarc.ets.check.CoCoHelper;
import secarc.ets.check.MontiSecArcContextConditionConstants;
import secarc.ets.cocos.common.ComponentExistence;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.graph.ArchitectureGraph;

/**
 * Checks if the relation between the two components is correct defined
 * regarding the trustlevel.
 * 
 * 
 * 
 */
public class CorrectRelationBetweenTrustlevel extends ComponentExistence {

	public CorrectRelationBetweenTrustlevel() {
		super(MontiSecArcContextConditionConstants.CORRECT_RELATION_BETWEEN_TRUSTLEVEL);
	}
	
	@Override
	public void check(ASTSecArcIdentity node, IdentityEntry entry, ArchitectureGraph graph)
			throws AmbigousException {
		
	}

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.cocos.checkers.ISecTrustlevelRelationChecker#check(secarc._ast.ASTSecArcTrustlevelRelation, secarc.ets.entries.TrustlevelRelationEntry)
	 */
	@Override
	public void check(ASTSecArcTrustlevelRelation node,
			TrustlevelRelationEntry entry) throws AmbigousException {
		String refLowerTrustlevelComponent = entry.getComponentWithLowerTrustlevel();
		String refHigherTrustlevelComponent = entry.getComponentWithHigherTrustlevel();
		
		SubComponentEntry lowerTrustlevelComoponent = innerCheck(node, node.getMainParent(), refLowerTrustlevelComponent);
		
		SubComponentEntry higherTrustlevelComoponent = innerCheck(node, node.getMainParent(), refHigherTrustlevelComponent);
		
		//Checked in super class
		if(lowerTrustlevelComoponent == null || higherTrustlevelComoponent == null) {
			return;
		} 
		
		Optional<TrustlevelEntry> lowerTrustlevel = ((SecComponentEntry) lowerTrustlevelComoponent.getComponentType().getBestKnownVersion()).getTrustlevel(); 
		
		Optional<TrustlevelEntry> higherTrustlevel = ((SecComponentEntry) higherTrustlevelComoponent.getComponentType().getBestKnownVersion()).getTrustlevel();
		
		String lowerTrustlevelAsString = "-1";
		int lowerTrustlevelAsInteger = -1;
		
		String higherTrustlevelAsString = "-1";
		int higherTrustlevelAsInteger = -1;
		
		if(lowerTrustlevel.isPresent()) {
			lowerTrustlevelAsString = CoCoHelper.getTrustlevelAsString((SecComponentEntry) lowerTrustlevelComoponent.getComponentType().getBestKnownVersion());
			lowerTrustlevelAsInteger = CoCoHelper.getTrustlevelAsInteger((SecComponentEntry) lowerTrustlevelComoponent.getComponentType().getBestKnownVersion());;
		} else {
			//Derived Trustlevel
			SecComponentEntry lowerTrustlevelSuperComponent = this.getTrustlevel((ASTArcComponent) lowerTrustlevelComoponent.getComponentType().getBestKnownVersion().getNode());
			if(lowerTrustlevelSuperComponent != null) {
				lowerTrustlevelAsString = CoCoHelper.getTrustlevelAsString((SecComponentEntry) lowerTrustlevelSuperComponent.getBestKnownVersion());
				lowerTrustlevelAsInteger = CoCoHelper.getTrustlevelAsInteger((SecComponentEntry) lowerTrustlevelSuperComponent.getBestKnownVersion());
			}
		}
		
		if(higherTrustlevel.isPresent()) {
			higherTrustlevelAsString = CoCoHelper.getTrustlevelAsString((SecComponentEntry) higherTrustlevelComoponent.getComponentType().getBestKnownVersion());
			higherTrustlevelAsInteger = CoCoHelper.getTrustlevelAsInteger((SecComponentEntry) higherTrustlevelComoponent.getComponentType().getBestKnownVersion());;
		} else {
			//Derived Trustlevel
			SecComponentEntry higherTrustlevelSuperComponent = this.getTrustlevel((ASTArcComponent) higherTrustlevelComoponent.getComponentType().getBestKnownVersion().getNode());
			if(higherTrustlevelSuperComponent != null) {
				higherTrustlevelAsString = CoCoHelper.getTrustlevelAsString((SecComponentEntry) higherTrustlevelSuperComponent.getBestKnownVersion());
				higherTrustlevelAsInteger = CoCoHelper.getTrustlevelAsInteger((SecComponentEntry) higherTrustlevelSuperComponent.getBestKnownVersion());;
			}
		}
		
		if(entry.isEqual() && higherTrustlevelAsInteger != lowerTrustlevelAsInteger) {
			addReport("The relation between the components " + refLowerTrustlevelComponent + " (trustlevel " +  lowerTrustlevelAsString + ") and " + refHigherTrustlevelComponent + " (trustlevel " + higherTrustlevelAsString + ") is defined as equals.", node.get_SourcePositionStart());
		} else if (!entry.isEqual() && higherTrustlevelAsInteger < lowerTrustlevelAsInteger) {
			addReport("The trustlevel of the component " + refLowerTrustlevelComponent + " (trustlevel " +  lowerTrustlevelAsString + ") must be lower than the trustlevel of component " + refHigherTrustlevelComponent + " (trustlevel " + higherTrustlevelAsString + ") regarding the defined relation.", node.get_SourcePositionStart());
		}
		
	}
	
	/**
	 * Search for trustlevel in super components 
	 * @param node
	 * @return trustlevel
	 * @throws AmbigousException
	 */
	private SecComponentEntry getTrustlevel(ASTArcComponent node) throws AmbigousException {
		if(node == null) {
			return null;
		}
		ASTArcComponent parent = node.getMainParent();
		if(parent != null) {
			SecComponentEntry componentParent = (SecComponentEntry) resolver.resolve(parent.getName(), ComponentEntry.KIND, getNameSpaceFor(parent));
			if(componentParent.getTrustlevel().isPresent()) {
				return componentParent;
			} else {
				return getTrustlevel(parent);
			}
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.coco.ContextCondition#getErrorCode()
	 */
	@Override
	public IErrorCode getErrorCode() {
		return MontiSecArcErrorCodes.CorrectRelationBetweenTrustlevel;
	}

}
