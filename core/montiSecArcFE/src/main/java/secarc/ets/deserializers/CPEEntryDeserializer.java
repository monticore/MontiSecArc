/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.inject.Inject;

import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.CPEEntry;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link CPEEntry}s. 
 * 
 */
public class CPEEntryDeserializer extends STEntryDeserializer {

	private final IArcdEntryFactory arcdFactory;
	
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public CPEEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return CPEEntry.KIND;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        CPEEntry cpe = ((SecEntryFactory) arcdFactory).createCPE();
        
        String name = object.<String> getAttributeValue("cpe").or("");
        
        cpe.setName(name);
        cpe.setEntryState(STEntryState.FULL, cpe);
        return cpe;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		CPEEntry version = (CPEEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            version.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.setCPE(version);
        }
        return version;
	}

}
