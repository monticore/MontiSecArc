/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.inject.Inject;


import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link ConfigurationEntry}s. 
 * 
 */
public class ConfigurationEntryDeserializer extends STEntryDeserializer {
	
	private final IArcdEntryFactory arcdFacotry;
	
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public ConfigurationEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFacotry = arcdFactory;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return ConfigurationEntry.KIND;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        ConfigurationEntry configuration = ((SecEntryFactory) arcdFacotry).createConfiguration();
        
        String name = object.<String> getAttributeValue("name").or("");
        
        configuration.setName(name);
        configuration.setEntryState(STEntryState.FULL, configuration);
        return configuration;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		ConfigurationEntry configuration = (ConfigurationEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            configuration.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.setConfiguration(configuration);
        }
        return configuration;
	}

}
