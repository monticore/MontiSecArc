/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.inject.Inject;


import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.SecPortEntry;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link FilterEntry}s. 
 * 
 */
public class FilterEntryDeserializer extends STEntryDeserializer {

	private final IArcdEntryFactory arcdFactory;
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public FilterEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return FilterEntry.KIND;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        FilterEntry filter = ((SecEntryFactory) arcdFactory).createFilter();
        
        String name = object.<String> getAttributeValue("name").or("");
        
        filter.setName(name);
        filter.setEntryState(STEntryState.FULL, filter);
        return filter;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		FilterEntry filter = (FilterEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            filter.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.setFilter(filter);
        } else if (parent instanceof SecPortEntry) {
        	SecPortEntry port = (SecPortEntry) parent;
        	filter.addKind(ArcdConstants.ST_KIND_PROTECTED);
        	port.setFilter(filter);
        }
        return filter;
	}

}
