/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link IdentityEntry}s. 
 * 
 */
public class IdentityEntryDeserializer extends STEntryDeserializer {
	
	private final IArcdEntryFactory arcdFactory;
	
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public IdentityEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return IdentityEntry.KIND;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        IdentityEntry identity = ((SecEntryFactory) arcdFactory).createIdentity();
        
        String source = object.<String> getAttributeValue("source").get();
        String target = object.<String> getAttributeValue("target").get();
        Optional<Boolean> isStrong = object.<Boolean> getAttributeValue("isStrong");
        
        identity.setSource(source);
        identity.setTarget(target);
        checkArgument(isStrong.isPresent());
        identity.setIdentityKind(isStrong.get());
        identity.setEntryState(STEntryState.FULL, identity);
        return identity;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		IdentityEntry identity = (IdentityEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            identity.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.addIdentity(identity);
        }
        return identity;
	}

}
