/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Optional;

import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link PEPEntry}s. 
 * 
 */
public class PEPEntryDeserializer extends STEntryDeserializer{

	private final IArcdEntryFactory arcdFactory;
	
	public PEPEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return PEPEntry.KIND;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        PEPEntry pep = ((SecEntryFactory) arcdFactory).createPEP();
        
        String name = object.<String> getAttributeValue("name").or("");
        Optional<Boolean> isOn = object.<Boolean> getAttributeValue("isOn");
        
        pep.setName(name);
        checkArgument(isOn.isPresent());
        pep.setAccessControl(isOn.get());
        pep.setEntryState(STEntryState.FULL, pep);
        return pep;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		PEPEntry pep = (PEPEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            pep.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.setPEP(pep);
        }
        return pep;
	}

}
