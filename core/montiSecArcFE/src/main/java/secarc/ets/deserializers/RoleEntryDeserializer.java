/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.inject.Inject;

import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.SecPortEntry;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link RoleEntry}s. 
 * 
 */
public class RoleEntryDeserializer extends STEntryDeserializer {

	private IArcdEntryFactory arcdFactory;
	
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public RoleEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}
	
	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return RoleEntry.KIND;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        RoleEntry role = ((SecEntryFactory) arcdFactory).createRole();
        
        String name = object.<String> getAttributeValue("role").get();
        
        role.setName(name);
        role.setEntryState(STEntryState.FULL, role);
        return role;
	}

	/* (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		RoleEntry role = (RoleEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            role.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.addRole(role);
        } else if(parent instanceof SecPortEntry) {
        	SecPortEntry port = (SecPortEntry) parent;
        	role.addKind(ArcdConstants.ST_KIND_PROTECTED);
        	port.addRole(role);
        }
        return role;
	}

}
