/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;
import secarc.ets.entries.SecComponentEntry;
import interfaces2.STEntry;


import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arc.ets.deserializer.MAComponentEntryDeserializer;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;

/**
 * Deserializer for {@link ComponentEntry}s. 
 * 
 */
public class SecComponentEntryDeserializer extends MAComponentEntryDeserializer {

	/**
	 * 
	 * @param compFactory
	 */
	public SecComponentEntryDeserializer(IArcdEntryFactory arcdFactory) {
		super(arcdFactory);
	}
	
	/*
     * (non-Javadoc)
     * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
     */
    @Override
    public STEntry deserialize(ASTNode node) {
        checkArgument(node instanceof ASTObject);
        
        return (SecComponentEntry) super.deserialize(node);
    }

}
