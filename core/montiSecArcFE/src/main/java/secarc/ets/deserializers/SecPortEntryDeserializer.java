/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Optional;

import secarc.ets.entries.SecPortEntry;
import interfaces2.ISTEntry;

import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ets.deserializers.PortEntryDeserializer;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * Deserializer for {@link PortEntry}s. 
 * 
 */
public class SecPortEntryDeserializer extends PortEntryDeserializer {

	/**
	 * 
	 * @param arcdFactory
	 */
	public SecPortEntryDeserializer(IArcdEntryFactory arcdFactory) {
		super(arcdFactory);
	}
	
	 /*
     * (non-Javadoc)
     * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
     */
    @Override
    public ISTEntry deserialize(ASTNode node) {
        checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        
        SecPortEntry port = (SecPortEntry) super.deserialize(node);
        
        Optional<Boolean> isCritical = object.getAttributeValue("isCritical");
        if (isCritical.isPresent()) {
        	port.setCritical(isCritical.get());
        }
        
        return port;
    }

}
