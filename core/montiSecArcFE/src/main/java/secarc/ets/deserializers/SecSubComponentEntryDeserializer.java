/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;
import interfaces2.ISTEntry;

import secarc.ets.entries.SecSubComponentEntry;

import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ets.deserializers.SubComponentEntryDeserializer;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;

public class SecSubComponentEntryDeserializer extends
		SubComponentEntryDeserializer {
	
	/**
	 * 
	 * @param arcdFactory
	 */
	public SecSubComponentEntryDeserializer(IArcdEntryFactory arcdFactory) {
		super(arcdFactory);
	}
	
	/*
     * (non-Javadoc)
     * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
     */
    @Override
    public ISTEntry deserialize(ASTNode node) {
    	checkArgument(node instanceof ASTObject);
        
		return (SecSubComponentEntry) super.deserialize(node);
    }

}
