/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.TrustlevelEntry;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link TrustlevelEntry}s. 
 * 
 */
public class TrustlevelEntryDeserializer extends STEntryDeserializer {

	private final IArcdEntryFactory arcdFactory;
	
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public TrustlevelEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return TrustlevelEntry.KIND;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        TrustlevelEntry trustlevel = ((SecEntryFactory) arcdFactory).createTrustlevel();
        
        Optional<Integer> value = object.<Integer> getAttributeValue("value");
        Optional<Boolean> isPositiv = object.<Boolean> getAttributeValue("isPositiv");
        Optional<String> reason = object.<String> getAttributeValue("reason");
        
        if(reason.isPresent()) {
        	trustlevel.setReason(reason.get());
        }
        checkArgument(isPositiv.isPresent());
        trustlevel.setRelative(isPositiv.get());
        checkArgument(value.isPresent());
        trustlevel.setValue(value.get());
        trustlevel.setEntryState(STEntryState.FULL, trustlevel);
        return trustlevel;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		TrustlevelEntry trustlevel = (TrustlevelEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            trustlevel.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.setTrustlevel(trustlevel);
        }
        return trustlevel;
	}

}
