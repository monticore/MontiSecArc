/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.deserializers;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.TrustlevelRelationEntry;
import mc.ast.ASTNode;
import mc.ets.serialization._ast.ASTObject;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.loaders.STEntryDeserializer;

/**
 * Deserializer for {@link TrustlevelRelationEntry}s. 
 * 
 */
public class TrustlevelRelationEntryDeserializer extends STEntryDeserializer {

	private final IArcdEntryFactory arcdFactory;
	
	/**
	 * 
	 * @param arcdFactory
	 */
	@Inject
	public TrustlevelRelationEntryDeserializer(IArcdEntryFactory arcdFactory) {
		this.arcdFactory = arcdFactory;
	}
	
	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#getKind()
	 */
	@Override
	public String getKind() {
		return TrustlevelRelationEntry.KIND;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode)
	 */
	@Override
	public ISTEntry deserialize(ASTNode node) {
		checkArgument(node instanceof ASTObject);
        ASTObject object = (ASTObject) node;
        TrustlevelRelationEntry trustlevelRelation = ((SecEntryFactory) arcdFactory).createTrustlevelRelation();
        
        Optional<String> componentWithLowerTrustlevel = object.<String> getAttributeValue("componentWithLowerTrustlevel");
        Optional<String> componentWithHigherTrustlevel = object.<String> getAttributeValue("componentWithHigherTrustlevel");
        Optional<Boolean> isEqual = object.<Boolean> getAttributeValue("isEqual");
        
        trustlevelRelation.setComponentWithLowerTrustlevel(componentWithLowerTrustlevel.get());
        trustlevelRelation.setComponentWithHigherTrustlevel(componentWithHigherTrustlevel.get());
        checkArgument(isEqual.isPresent());
        trustlevelRelation.setEqual(isEqual.get());
        trustlevelRelation.setEntryState(STEntryState.FULL, trustlevelRelation);
        return trustlevelRelation;
	}

	/*
	 * (non-Javadoc)
	 * @see interfaces2.loaders.STEntryDeserializer#deserialize(mc.ast.ASTNode, interfaces2.ISTEntry)
	 */
	@Override
	public ISTEntry deserialize(ASTNode description, ISTEntry parent) {
		TrustlevelRelationEntry trustlevelRelation = (TrustlevelRelationEntry) deserialize(description);
        if (parent instanceof SecComponentEntry) {
            SecComponentEntry comp = (SecComponentEntry) parent;
            trustlevelRelation.addKind(ArcdConstants.ST_KIND_PROTECTED);
            comp.addTrustlevelRelation(trustlevelRelation);
        }
        return trustlevelRelation;
	}

}
