/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc cpe entry.
 *
 *
 */
public class CPEEntry extends  AbstractSTEntry<CPEEntry>{

	/**
     * Entry kind of cpe.
     */
    public static final String KIND = "SecArcCPE";
    
    /**
     * cpe of 3rd party component
     */
    @Serialized
    private String cpe;
    
    /**
     * @param version Version of 3rd party component
     */
    public void setCPE(String cpe) {
    	this.cpe = cpe;
    }
	
    /**
     * @return Version of 3rd party component
     */
    public String getCPE() {
    	return cpe;
    }
    
    /**
     * Default constructor. Creates a new {@link CPEEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public CPEEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}

}
