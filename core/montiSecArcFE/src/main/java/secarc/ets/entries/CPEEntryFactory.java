/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;

/**
 * Creates Creates {@link CPEEntry}s.
 *
 *
 */
public class CPEEntryFactory implements ICPEEntryFactory {

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.entries.ICPEEntryFactory#createCPE()
	 */
	@Override
	public CPEEntry createCPE() {
		CPEEntry entry = new CPEEntry();
		entry.setName(MontiSecArcConstants.CPE_NAME);
		
		return entry;
	}

}
