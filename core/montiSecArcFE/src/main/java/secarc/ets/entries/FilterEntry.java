/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc filter entry.
 *
 *
 */
public class FilterEntry extends  AbstractSTEntry<FilterEntry>{
	
	/**
     * Entry kind of filter.
     */
    public static final String KIND = "SecArcFilter";

    /**
     * Default constructor. Creates a new {@link FilterEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public FilterEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}

}
