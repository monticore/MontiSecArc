/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link CPEEntry}s.
 *
 *
 */
public interface ICPEEntryFactory {

	/**
	 * Create a new {@link CPEEntry}
	 * 
	 * @return a new {@link CPEEntry}
	 */
	CPEEntry createCPE();
	
}
