/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link ConfigurationEntry}s.
 *
 *
 */
public interface IConfigurationEntryFactory {

	/**
	 * Create a new {@link ConfigurationEntry}
	 * 
	 * @return a new {@link ConfigurationEntry}
	 */
	ConfigurationEntry createConfiguration();
	
	/**
     * Creates a new {@link ConfigurationEntry} with the given name. 
     * 
     * @param name filter name. Name must not be null.
     * @return a new {@link ConfigurationEntry} with the given name.
     */
	ConfigurationEntry createConfiguration(String name);
	
}
