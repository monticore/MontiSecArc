/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link FilterEntry}s.
 *
 *
 */
public interface IFilterEntryFactory {

	/**
	 * Create a new {@link FilterEntry}
	 * 
	 * @return a new {@link FilterEntry}
	 */
	FilterEntry createFilter();
	
	/**
     * Creates a new {@link FilterEntry} with the given name. 
     * 
     * @param name filter name. Name must not be null.
     * @return a new {@link FilterEntry} with the given name.
     */
	FilterEntry createFilter(String name);
	
}
