/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link IdentityEntry}s.
 *
 *
 */
public interface IIdentityEntryFactory {

	/**
	 * Create a new {@link IdentityEntry}
	 * 
	 * @return a new {@link IdentityEntry}
	 */
	IdentityEntry createIdentity();
	
}
