/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link PEPEntry}s.
 *
 *
 */
public interface IPEPEntryFactory {

	/**
	 * Create a new {@link PEPEntry}
	 * 
	 * @return a new {@link PEPEntry}
	 */
	PEPEntry createPEP();
	
	
}
