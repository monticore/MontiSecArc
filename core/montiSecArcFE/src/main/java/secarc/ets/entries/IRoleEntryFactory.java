/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link RoleEntry}s.
 *
 *
 */
public interface IRoleEntryFactory {

	/**
	 * Create a new {@link RoleEntry}
	 * 
	 * @return a new {@link RoleEntry}
	 */
	RoleEntry createRole();
	
}
