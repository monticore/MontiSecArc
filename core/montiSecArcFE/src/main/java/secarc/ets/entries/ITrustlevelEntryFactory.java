/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link TrustlevelEntry}s.
 *
 *
 */
public interface ITrustlevelEntryFactory {

	/**
	 * Create a new {@link TrustlevelEntry}
	 * 
	 * @return a new {@link TrustlevelEntry}
	 */
	TrustlevelEntry createTrustlevel();
	
}
