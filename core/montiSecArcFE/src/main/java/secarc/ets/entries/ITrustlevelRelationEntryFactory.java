/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link TrustlevelRelationEntry}s.
 *
 *
 */
public interface ITrustlevelRelationEntryFactory {

	/**
	 * Create a new {@link TrustlevelRelationEntry}
	 * 
	 * @return a new {@link TrustlevelRelationEntry}
	 */
	TrustlevelRelationEntry createTrustlevelRelation();
	
}
