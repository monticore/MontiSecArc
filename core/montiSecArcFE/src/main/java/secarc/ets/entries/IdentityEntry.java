/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc identity entry.
 *
 *
 */
public class IdentityEntry extends AbstractSTEntry<IdentityEntry>{

	/**
     * Entry kind of identity.
     */
    public static final String KIND = "SecArcIndentity";
    
    /**
     * source of identity
     */
    @Serialized
    private String source;
    
    /**
     * target of identity
     */
    @Serialized
    private String target;
    
    /**
     * Maps strong identity to true
     */
    @Serialized
    private boolean isStrong = true;
    
    /**
     * @return Source of Identity
     */
    public String getSource() {
    	return source;
    }
    
    /**
     * @param source Source of Identity
     */
    public void setSource(String source) {
    	this.source = source;
    }
    
    /**
     * @return Target of Identity
     */
    public String getTarget() {
    	return target;
    }
    
    /**
     * @param target Target of Identity
     */
    public void setTarget(String target) {
    	this.target = target;
    }
    
    /**
     * @param strong = true, weak = false
     */
    public void setIdentityKind(boolean identityKind) {
    	this.isStrong = identityKind;
    }
    
    /**
     * @return true, if identity is strong, else false
     */
    public boolean isStrong() {
    	return isStrong;
    }
    
    /**
     * @return false, if identity is weak, else true
     */
    public boolean isWeak() {
    	return !isStrong;
    }
    
    /**
     * Default constructor. Creates a new {@link IdentityEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public IdentityEntry() {
		 super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}
	
	
	/*
	 * Override(non-Javadoc)
	 * @see mc.umlp.arcd.ets.entries.AbstractSTEntry#getName()
	 */
	@Override
	public String getName() {
		return getTarget();
	}
	
	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getSource());
        sb.append(" -> ");
        sb.append(getTarget());
        return sb.toString();
    }

}
