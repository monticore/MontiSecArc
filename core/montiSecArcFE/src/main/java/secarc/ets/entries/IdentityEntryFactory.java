/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link IdentityEntry}s.
 *
 *
 */
public class IdentityEntryFactory implements IIdentityEntryFactory {

	/* (non-Javadoc)
	 * @see secarc.ets.entries.IIdentityEntryFactory#createIdentity()
	 */
	@Override
	public IdentityEntry createIdentity() {
		return new IdentityEntry();
	}

}
