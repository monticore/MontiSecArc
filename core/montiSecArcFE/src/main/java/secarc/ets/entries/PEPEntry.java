/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc pep entry.
 *
 *
 */
public class PEPEntry extends AbstractSTEntry<PEPEntry>{
	
	/**
     * Entry kind of identity.
     */
    public static final String KIND = "SecArcPEP";
    
    /**
     * Maps on too true 
     */
    @Serialized
    private boolean isOn;
    
    /**
     * @param accessControl on = true, off = false
     */
    public void setAccessControl(boolean accessControl) {
    	this.isOn = accessControl;
    }
    
    /**
     * @return true, if accesscontrol is on, else false
     */
    public boolean isOn() {
    	return isOn;
    }
    
    /**
     * @return true, if accesscontrol is off, else false
     */
    public boolean isOff() {
    	return !isOn;
    }

    /**
     * Default constructor. Creates a new {@link PEPEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public PEPEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}

}
