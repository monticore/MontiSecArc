/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;

/**
 * 
 * Factory that created {@link PEPEntry}s.
 *
 *
 */
public class PEPEntryFactory implements IPEPEntryFactory{

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IPEPEntryFactory#create()
     */
	@Override
	public PEPEntry createPEP() {
		PEPEntry entry = new PEPEntry();
		entry.setName(MontiSecArcConstants.PEP_NAME);
		
		return entry;
	}

}
