/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc componentRole entry.
 *
 *
 */
public class RoleEntry extends  AbstractSTEntry<FilterEntry>{
	
	/**
     * Entry kind of component role
     */
    public static final String KIND = "SecArcRole";
    
    /**
     * role for component
     */
    @Serialized
    private String role;

    /**
     * @param role Role for component
     */
    public void setRole(String role) {
    	this.role = role;
    }
    
    /**
     * @return Role for component
     */
    public String getRole() {
    	return role;
    }
    
    /**
     * Default constructor. Creates a new {@link RoleEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visability.
     */
	public RoleEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.ets.entries.AbstractSTEntry#getName()
	 */
	@Override
	public String getName() {
		return getRole();
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.ets.entries.AbstractSTEntry#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.role = name;
	}

}
