/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * Creates Creates {@link RoleEntry}s.
 *
 *
 */
public class RoleEntryFactory implements IRoleEntryFactory {

	/* (non-Javadoc)
	 * @see secarc.ets.entries.IRoleEntryFactory#createRole()
	 */
	@Override
	public RoleEntry createRole() {
		return new RoleEntry();
	}

}
