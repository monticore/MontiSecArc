/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import interfaces2.loaders.STEntryDeserializer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Optional;



import mc.IModelloader;
import mc.ets.serialization.STEntrySerializer.EmbeddedEntry;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arc.ets.entries.MAComponentEntry;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * MontiSecArc component entry.
 *
 *
 */
public class SecComponentEntry extends MAComponentEntry {

	/**
	 * filter for component
	 */
	@Serialized
	@EmbeddedEntry
	private FilterEntry filter;
	
	/**
	 * Trustlevel for component
	 */
	@Serialized
	@EmbeddedEntry
	private TrustlevelEntry trustlevel;
	
	/**
	 * Identity links from client to server
	 */
	@Serialized
	@EmbeddedEntry
	private List<IdentityEntry> identities = new ArrayList<IdentityEntry>();
	
	/**
	 * Roles for components
	 */
	@Serialized
	@EmbeddedEntry
	private List<RoleEntry> roles = new ArrayList<RoleEntry>();
	
	/**
	 * PEP for component
	 */
	@Serialized
	@EmbeddedEntry
	private PEPEntry pep;
	
	/**
	 * CPE for 3rd party component
	 */
	@Serialized
	@EmbeddedEntry
	private CPEEntry cpe;
	
	/**
	 * Configuration for 3rd party component
	 */
	@Serialized
	@EmbeddedEntry
	private ConfigurationEntry configuration;
	
	/**
	 * Trustlevel relation 
	 */
	@Serialized
	@EmbeddedEntry
	private List<TrustlevelRelationEntry> trustlevelRelation = new ArrayList<TrustlevelRelationEntry>();
	
	/**
	 * @param filter Filter for component
	 */
	public void setFilter(FilterEntry filter) {
		this.filter = filter;
	}
	
	/**
	 * @return Filter for component
	 */
	public FilterEntry getFilter() {
		return filter;
	}
	
	/**
	 * @param trustlevel Trustlevel for component
	 */
	public void setTrustlevel(TrustlevelEntry trustlevel) {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            bestKnown.setTrustlevel(trustlevel);
        }
		this.trustlevel = trustlevel;
	}
	
	/**
	 * @return Trustlevel for Component
	 */
	public Optional<TrustlevelEntry> getTrustlevel() {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            return bestKnown.getTrustlevel();
        }
        return Optional.fromNullable(trustlevel);
	}
	
	/**
	 * @param pep PEP for component
	 */
	public void setPEP(PEPEntry pep) {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
		if (!bestKnown.equals(this)) {
            bestKnown.setPEP(pep);
        }
		this.pep = pep;
	}
	
	/**
	 * @return PEP for Component
	 */
	public PEPEntry getPEP() {
		return pep;
	}
	
	/**
	 * @param cpe for 3rd party component
	 */
	public void setCPE(CPEEntry cpe) {
		this.cpe = cpe;
	}
	
	/**
	 * @return CPE for 3rd party component
	 */
	public CPEEntry getCPE() {
		return cpe;
	}
	
	/**
	 * @param configuration Configuration for 3rd party component
	 */
	public void setConfiguration(ConfigurationEntry configuration) {
		this.configuration = configuration;
	}
	
	/**
	 * @return Configuration for 3rd party component
	 */
	public ConfigurationEntry getConfiguration() {
		return configuration;
	}
	
	/**
	 * @param identity Identity links from client to server
	 */
	public void addIdentity(IdentityEntry identity) {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            bestKnown.addIdentity(identity);
        }
        this.identities.add(identity);
	}
	
	/**
	 * @param componentRole Roles for component
	 */
	public void addRole(RoleEntry componentRole) {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            bestKnown.addRole(componentRole);
        }
        this.roles.add(componentRole);
	}
	
	/**
	 * @return Identity links from client to server
	 */
	public List<IdentityEntry> getIdentities() {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            return bestKnown.getIdentities();
        }
        return identities;
	}
	
	public IdentityEntry getIdentity(String target) {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            return bestKnown.getIdentity(target);
        }
        for(IdentityEntry entry : identities) {
        	if(entry.getTarget().endsWith(target)) {
        		return entry;
        	}
        }
        return null;
	}
	
	/**
	 * @return Roles for component
	 */
	public List<RoleEntry> getRoles() {
		SecComponentEntry bestKnown = (SecComponentEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            return bestKnown.getRoles();
        }
        return roles;
	}
		
	/**
     * Default constructor. Creates a new {@link SecComponentEntry}
     * with {@link ArcdConstants.ST_KIND_PUBLIC} visibility.
     */
	public SecComponentEntry() {
		super();
	}   
	
	/**
     * 
     * @param name port name
     * @return port with the given name, absent optional, if it does not exist
     */
	public Optional<PortEntry> getPort(String name) {
		Optional<PortEntry> portEntry = getIncomingPort(name);
		if(portEntry.isPresent()) {
			return portEntry;
		} else {
			return getOutgoingPort(name);
		}		
	}
	
	/**
	 * 
	 * @param loader
	 * @param deserializers
	 * @return
	 */
	public List<PortEntry> getAllPorts(IModelloader loader, Collection<STEntryDeserializer> deserializers) {
		List<PortEntry> ports = getAllIncomingPorts(loader, deserializers);
		ports.addAll(getAllOutgoingPorts(loader, deserializers));
		return ports;
	}
	
	/**
	 * 
	 * @param trustlevelRelation
	 */
	public void addTrustlevelRelation(TrustlevelRelationEntry trustlevelRelation) {
		this.trustlevelRelation.add(trustlevelRelation);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<TrustlevelRelationEntry> getAllTrustlevelRelation() {
		return this.trustlevelRelation;
	}
	
}
