/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntryFactory;

/**
 * Creates Creates {@link SecConnectorEntry}s.
 *
 *
 */
public class SecConnectorEntryFactory extends ConnectorEntryFactory {

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IPEPEntryFactory#create()
     */
	@Override
	public ConnectorEntry createConnector() {
		return new SecConnectorEntry();
	}

}
