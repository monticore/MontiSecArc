/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import mc.umlp.arcd.ets.entries.ArcdEntryFactory;

import com.google.inject.Inject;


public class SecEntryFactory extends ArcdEntryFactory {
	
	/**
	 * Factory for filters
	 */
	private IFilterEntryFactory filterFactory;
	
	/**
	 * Factory for peps
	 */
	private IPEPEntryFactory pepEntryFactory;

	/**
	 * Factory for trustlevel
	 */
	private ITrustlevelEntryFactory trustlevelFactory;
	
	/**
	 * Factory for configuration
	 */
	private IConfigurationEntryFactory configurationFactory;
	
	/**
	 * Factory for idnetities
	 */
	private IIdentityEntryFactory identityFactory;
	
	/**
	 * Factory for roles
	 */
	private IRoleEntryFactory roleFactory;
	
	/**
	 * Factory for cpe
	 */
	private ICPEEntryFactory cpeFactory;
	
	/**
	 * Factory for trustlevel relation
	 */
	private ITrustlevelRelationEntryFactory trustlevelRelationFactory;

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IFilterEntryFactory#createFilter()
     */
	public FilterEntry createFilter() {
		return filterFactory.createFilter();
	}

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IFilterEntryFactory#createFilter(String name)
     */
	public FilterEntry createFilter(String name) {
		return filterFactory.createFilter(name);
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IPEPEntryFactory#createPEP()
     */
	public PEPEntry createPEP() {
		return pepEntryFactory.createPEP();
	}
    
    /* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.ITrustlevelEntryFactory#createTrustlevel()
     */
	public TrustlevelEntry createTrustlevel() {
		return trustlevelFactory.createTrustlevel();
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IConfigurationEntryFactory#createConfiguration()
     */
	public ConfigurationEntry createConfiguration() {
		return configurationFactory.createConfiguration();
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IConfigurationEntryFactory#createConfiguration(String name)
     */
	public ConfigurationEntry createConfiguration(String name) {
		return configurationFactory.createConfiguration(name);
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IIdentityEntryFactory#createIdentity()
     */
	public IdentityEntry createIdentity() {
		return identityFactory.createIdentity();
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IRoleEntryFactory#createRole()
     */
	public RoleEntry createRole() {
		return roleFactory.createRole();
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.ICPEEntryFactory#createCPE()
     */
	public CPEEntry createCPE() {
		return cpeFactory.createCPE();
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.ITrustlevelRelationFactory#createTrustlevelRelation()
     */
	public TrustlevelRelationEntry createTrustlevelRelation() {
		return trustlevelRelationFactory.createTrustlevelRelation();
	}

	@Inject
	public void setFilterFactory(IFilterEntryFactory filterFactory) {
		this.filterFactory = filterFactory;
	}
	
	@Inject
	public void setPepEntryFactory(IPEPEntryFactory pepEntryFactory) {
		this.pepEntryFactory = pepEntryFactory;
	}
	
	@Inject
	public void setTrustlevelFactory(ITrustlevelEntryFactory trustlevelFactory) {
		this.trustlevelFactory = trustlevelFactory;
	}
	
	@Inject
	public void setConfigurationFactory(IConfigurationEntryFactory configurationFactory) {
		this.configurationFactory = configurationFactory;
	}
	
	@Inject
	public void setIdentityFactory(IIdentityEntryFactory identityFactory) {
		this.identityFactory = identityFactory;
	}
	
	@Inject
	public void setRoleFactory(IRoleEntryFactory roleFactory) {
		this.roleFactory = roleFactory;
	}
	
	@Inject
	public void setCPEFactory(ICPEEntryFactory cpeFactory) {
		this.cpeFactory = cpeFactory;
	}
	
	@Inject
	public void setTrustlevelRelationFactory(ITrustlevelRelationEntryFactory trustlevelRelationFactory) {
		this.trustlevelRelationFactory = trustlevelRelationFactory;
	}
    
}
