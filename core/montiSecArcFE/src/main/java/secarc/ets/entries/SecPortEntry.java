/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import java.util.ArrayList;
import java.util.List;

import mc.ets.serialization.STEntrySerializer.EmbeddedEntry;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * MontiSecArc port entry.
 *
 *
 */
public class SecPortEntry extends PortEntry{
	
	/**
	 * filter for Port
	 */
	@Serialized
	@EmbeddedEntry
	private FilterEntry filter;
	
	/**
	 * Roles for port
	 */
	@Serialized
	@EmbeddedEntry
	private List<RoleEntry> roles = new ArrayList<RoleEntry>();
	
	@Serialized
	private boolean isCritical = false;
	
	/**
	 * @param filter filter for Port
	 */
	public void setFilter(FilterEntry filter) {
		this.filter = filter;
	}
	
	/**
	 * @return filter for port
	 */
	public FilterEntry getFilter() {
		return filter;
	}
	
	/**
	 * adds Roles to port
	 * 
	 * @param role
	 */
	public void addRole(RoleEntry role) {
		SecPortEntry bestKnown = (SecPortEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            bestKnown.addRole(role);
        }
        this.roles.add(role);
	}
	
	/**
	 * port roles
	 * 
	 * @return roles
	 */
	public List<RoleEntry> getRoles() {
		SecPortEntry bestKnown = (SecPortEntry) getBestKnownVersion();
        if (!bestKnown.equals(this)) {
            return bestKnown.getRoles();
        }
        return roles;
	}
	
	/**
	 * @param isCritical critical = true, not critical = false
	 */
	public void setCritical(boolean isCritical) {
		this.isCritical = isCritical;
	}
	
	/**
	 * @return true, if port is critical, else false;
	 */
	public boolean isCritical() {
		return isCritical;
	}
	
	/**
     * Default constructor. Creates a new {@link SecPortEntry}
     * with {@link ArcdConstants.ST_KIND_PUBLIC} visibility.
     */
	public SecPortEntry(){
		super();
	}

}
