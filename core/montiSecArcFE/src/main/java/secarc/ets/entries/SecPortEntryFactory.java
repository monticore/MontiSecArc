/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.PortEntryFactory;

public class SecPortEntryFactory extends PortEntryFactory {

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.IPortEntryFactory#create()
     */
	@Override
	public PortEntry createPort() {
		return new SecPortEntry();
	}
	
}
