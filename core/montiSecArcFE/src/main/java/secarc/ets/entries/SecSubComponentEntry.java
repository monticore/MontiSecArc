/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import static com.google.common.base.Preconditions.checkArgument;
import interfaces2.ISTEntry;
import interfaces2.STEntryState;
import interfaces2.helper.EntryLoadingErrorException;
import interfaces2.loaders.InterfaceLoader;
import interfaces2.loaders.STEntryDeserializer;

import java.util.Collection;
import java.util.Set;

import mc.IModelloader;
import mc.umlp.arc.ets.entries.MASubComponentEntry;
import mc.umlp.arcd.ets.entries.ComponentEntry;

/**
 * MontiSecArc subConnector entry.
 *
 *
 */
public class SecSubComponentEntry extends MASubComponentEntry {

	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.SubComponentEntry#getComponentType()
     */
    @Override
    public SecComponentEntry getComponentType() {
        return (SecComponentEntry) super.getComponentType();
    }

    /* (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.SubComponentEntry#setComponentType(mc.umlp.arcd.ets.entries.ComponentEntry)
     */
    @Override
    public void setComponentType(ComponentEntry componentType) {
        checkArgument(componentType instanceof SecComponentEntry);
        super.setComponentType(componentType);
    }
    
    /* (non-Javadoc)
     * @see interfaces2.STEntry#loadFullVersion(mc.IModelloader, java.util.Collection)
     */
    @Override
    public void loadFullVersion(IModelloader loader, Collection<STEntryDeserializer> deserializers) throws EntryLoadingErrorException {
     // qualifies the component type
        if (getComponentType().getEntryState() == STEntryState.FULL) {
            return;
        }
        if (getComponentType().getEntryState() == STEntryState.UNQUALIFIED) {
            throw new IllegalStateException();
        }
        String componentTypeName = getComponentType().getName();

        Set<ISTEntry> entries = InterfaceLoader.loadExported(loader, componentTypeName, symtabKindToBeLoaded, deserializers,
                getExportedInterfaceKindToBeLoaded());
        if (entries != null) {
            for (ISTEntry e : entries) {
                if (e instanceof ComponentEntry) {
                    ComponentEntry te = (SecComponentEntry) e;
                    if (te.getName().equals(componentTypeName)) {
                        getComponentType().setEntryState(STEntryState.FULL, te);
                        setEntryState(STEntryState.FULL, this);
                        break;
                    }
                }
            }    
        }
        if (getComponentType().getEntryState() != STEntryState.FULL) {
            throw new EntryLoadingErrorException(this);
        }
    }

}
