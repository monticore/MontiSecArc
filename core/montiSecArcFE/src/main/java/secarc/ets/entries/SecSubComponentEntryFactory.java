/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import com.google.inject.Inject;

import mc.umlp.arc.ets.entries.MASubComponentEntryFactory;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.IArcdTypeEntryFactory;
import mc.umlp.arcd.ets.entries.IArcdValueEntryFactory;
import mc.umlp.arcd.ets.entries.IComponentEntryFactory;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

/**
 * 
 * Factory that created {@link SecSubComponentEntry}s.
 *
 *
 */
public class SecSubComponentEntryFactory extends MASubComponentEntryFactory {

	/**
     * @param componentFactory {@link IComponentEntryFactory} used to create
     *            {@link ComponentEntry}s.
     */
    @Inject
    public SecSubComponentEntryFactory(final IComponentEntryFactory componentFactory, IArcdValueEntryFactory arcdValueFactory, IArcdTypeEntryFactory arcdTypeFactory) {
        super(componentFactory, arcdValueFactory, arcdTypeFactory);
    }
    

    
    /*
     * (non-Javadoc)
     * @see mc.umlp.arcd.ets.entries.ISubComponentEntryFactory#create()
     */
    @Override
    public SubComponentEntry createSubComponent() {
        return new SecSubComponentEntry();
    }
	
}
