/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc trustlevel entry.
 *
 *
 */
public class TrustlevelEntry extends AbstractSTEntry<TrustlevelEntry> {
	
	/**
     * Entry kind of trustlevel.
     */
    public static final String KIND = "SecArcTrustlevel";
    
    /**
     * Level of trust
     */
    @Serialized
    private int value;
    
    /**
     * relative to super trustlevel
     */
    @Serialized
    private boolean isPositiv;
    
    /**
     * Reason for trustlevel
     */
    @Serialized
    private String reason;
    
    /**
     * @return Level of trust
     */
    public int getValue() {
    	return value;
    }
    
    /**
     * @param value Level of trust
     */
    public void setValue(int value) {
    	this.value = value;
    }
    
    /**
     * @return Reason for trustlevel
     */
    public String getReason() {
    	return reason;
    }
    
    /**
     * @param reason Reason for trustlevel
     */
    public void setReason(String reason) {
    	this.reason = reason;
    }
    
    /**
     * @param relative positive = true, negative = false
     */
    public void setRelative(boolean relative) {
    	this.isPositiv = relative;
    }
    
    /**
     * @return Positive, if true, else false
     */
    public boolean isPositive() {
    	return isPositiv;
    }
    
    /**
     * @return Negative, if true, else false
     */
    public boolean isNegative() {
    	return !isPositiv;
    }

    /**
     * Default constructor. Creates a new {@link TrustlevelEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public TrustlevelEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}
	
	/**
	 * Minimal constructor. Creates a new {@link TrustlevelEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility, value and isPositive.
	 * @param value
	 * @param isPositve
	 */
	public TrustlevelEntry(int value, boolean isPositve) {
		super(ArcdConstants.ST_KIND_PROTECTED);
		this.value = value;
		this.isPositiv = isPositve;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}

}
