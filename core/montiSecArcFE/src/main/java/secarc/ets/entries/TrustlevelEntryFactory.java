/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;

/**
 * 
 * Factory that created {@link TrustlevelEntry}s.
 *
 *
 */
public class TrustlevelEntryFactory implements ITrustlevelEntryFactory {

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.entries.ITrustlevelEntryFactory#createTrustlevel()
	 */
	@Override
	public TrustlevelEntry createTrustlevel() {
		TrustlevelEntry entry = new TrustlevelEntry();
		entry.setName(MontiSecArcConstants.TRUSTLEVEL_NAME);
		return entry;
	}

}
