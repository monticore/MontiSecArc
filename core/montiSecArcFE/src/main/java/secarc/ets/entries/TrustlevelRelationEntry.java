/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

import secarc.MontiSecArcConstants;
import mc.ets.serialization.STEntrySerializer.Serialized;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.AbstractSTEntry;

/**
 * MontiSecArc trustlevel relation entry.
 *
 *s
 */
public class TrustlevelRelationEntry extends AbstractSTEntry<TrustlevelRelationEntry>{

	/**
     * Entry kind of trustlevel relation.
     */
    public static final String KIND = "SecArcTrustlevelRelation";
    
    /**
     * Component with lower trustlevel
     */
    @Serialized
    private String componentWithLowerTrustlevel;
    
    /**
     * Component with higher trustlevel
     */
    @Serialized
    private String componentWithHigherTrustlevel;
    
    /**
     * If the trustlevels are equals
     */
    @Serialized
    private boolean isEqual;
    
    /**
     * 
     * @param componentWithHigherTrustlevel component with higher trustlevel
     */
    public void setComponentWithHigherTrustlevel(String componentWithHigherTrustlevel) {
    	this.componentWithHigherTrustlevel = componentWithHigherTrustlevel;
    }
    
    /**
     * 
     * @return component with higher trustlevel
     */
    public String getComponentWithHigherTrustlevel() {
    	return this.componentWithHigherTrustlevel;
    }
    
    /**
     * 
     * @param componentWithLowerTrustlevel component with lower trustlevel
     */
    public void setComponentWithLowerTrustlevel(String componentWithLowerTrustlevel) {
    	this.componentWithLowerTrustlevel = componentWithLowerTrustlevel;
    }
    
    /**
     * 
     * @return component with lower trustlevel
     */
    public String getComponentWithLowerTrustlevel() {
    	return this.componentWithLowerTrustlevel;
    }
    
    /**
     * 
     * @param equal the trustlevels are equal
     */
    public void setEqual(boolean equal) {
    	this.isEqual = equal;
    }
    
    /**
     * 
     * @return is equal
     */
    public boolean isEqual() {
    	return this.isEqual;
    }
    
    /**
     * Default constructor. Creates a new {@link TrustlevelRelationEntry}
     * with {@link ArcdConstants.ST_KIND_PROTECTED} visibility.
     */
	public TrustlevelRelationEntry() {
		super(ArcdConstants.ST_KIND_PROTECTED);
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getLanguage()
     */
	@Override
	public String getLanguage() {
		return MontiSecArcConstants.MONTI_SEC_ARC_LANGUAGE_ID;
	}

	/*
     * (non-Javadoc)
     * @see interfaces2.STEntry#getKind()
     */
	@Override
	public String getKind() {
		return KIND;
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.ets.entries.AbstractSTEntry#getName()
	 */
	@Override
	public String getName() {
		return this.getComponentWithHigherTrustlevel();
	}
	
	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getComponentWithLowerTrustlevel());
        if(isEqual) {
        	sb.append(" = ");
        } else {
        	sb.append(" < ");
        }
        sb.append(getComponentWithHigherTrustlevel());
        return sb.toString();
    }
	
}
