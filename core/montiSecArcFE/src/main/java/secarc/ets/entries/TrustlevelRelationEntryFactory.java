/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.entries;

/**
 * 
 * Factory that created {@link TrustlevelRelationEntry}s.
 *
 *
 */
public class TrustlevelRelationEntryFactory implements ITrustlevelRelationEntryFactory{

	/*
	 * (non-Javadoc)
	 * @see secarc.ets.entries.ITrustlevelRelationEntryFactory#createTrustlevelRelation()
	 */
	@Override
	public TrustlevelRelationEntry createTrustlevelRelation() {
		TrustlevelRelationEntry entry = new TrustlevelRelationEntry();
		return entry;
	}
	
}
