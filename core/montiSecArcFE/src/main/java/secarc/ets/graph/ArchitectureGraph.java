/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import interfaces2.IAdapter;
import interfaces2.ISTEntry;
import interfaces2.STEntry;
import interfaces2.STEntryState;

import java.util.Collection;

import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

import org.jgrapht.DirectedGraph;

import cc.clarc.commons.JGraphT;
import cc.clarc.commons.exceptions.RuntimeExceptions;

import com.google.common.collect.ImmutableSet;

/**
 * A representation of a {@link mc.umlp.arcd.ArcdLanguage}-based architecture
 * implemented using the JGraphT framework.
 * 
 */
public class ArchitectureGraph {
  
  /**
   * Factory method that returns a {@link ArchitectureGraphBuilder} for building
   * {@link ArchitectureGraph} instances for the given root component.
   */
  public static final ArchitectureGraphBuilder of(ComponentEntry rootComponentEntry) {
    checkArgument(rootComponentEntry.getEntryState().equals(STEntryState.FULL));
    return ArchitectureGraphBuilder.forArchitecture(rootComponentEntry);
  }
  
  /**
   * Factory method that returns a {@link ArchitectureGraphBuilder} for building
   * {@link ArchitectureGraph} instances for the given {@link STEntry}, assuming
   * that it is an instance of {@link ComponentEntry} or a respective
   * {@link interfaces2.ISTEntryAdapter}.
   */
  public static final ArchitectureGraphBuilder of(ISTEntry entry) {
    
    checkNotNull(entry);
    ComponentEntry rootComponentEntry = null;
    
    ISTEntry rootEntry = entry instanceof IAdapter
        ? ((IAdapter) entry).getAdaptee()
        : entry;
    
    if (rootEntry instanceof ComponentEntry) {
      rootComponentEntry = (ComponentEntry) entry;
    } else {
      RuntimeExceptions.throwIllegalArgumentException();
    }
    
    return ArchitectureGraphBuilder.forArchitecture(rootComponentEntry);
  }
  
  /**
   * The concrete graph.
   */
  private final DirectedGraph<Vertex<? extends STEntry>, Edge> graph;
  
  /**
   * A view on the concrete graph with all edges being reversed.
   */
  private final DirectedGraph<Vertex<? extends STEntry>, Edge> reversedGraph;
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ArchitectureGraph
   */
  ArchitectureGraph(DirectedGraph<Vertex<? extends STEntry>, Edge> graph) {
    this.graph = graph;
    this.reversedGraph = JGraphT.newReversedGraph(graph);
  }
  
  /**
   * Returns all sub components to which the given sub component is connected
   * to.
   */
  public final Collection<SubComponentEntry> getConnectedComponents(SubComponentEntry subComponent) {
    return ImmutableSet.<SubComponentEntry> builder()
        .addAll(getConnectedReceivingComponents(subComponent))
        .addAll(getConnectedSendingComponents(subComponent))
        .build();
  }
  
  /**
   * Returns the receiving sub components to which the given sub component is
   * connected to.
   */
  public final Collection<SubComponentEntry> getConnectedReceivingComponents(SubComponentEntry subComponent) {
    return ArchitectureNeighbours.forElement(subComponent, this.graph);
    
  }
  
  /**
   * Returns the sending sub components to which the given sub component is
   * connected to.
   */
  public final Collection<SubComponentEntry> getConnectedSendingComponents(SubComponentEntry subComponent) {
    return ArchitectureNeighbours.forElement(subComponent, this.reversedGraph);
  }
  
  /**
   * Returns the concrete graph. Its interface is part of the JGraphT framework.
   */
  public final DirectedGraph<Vertex<? extends STEntry>, Edge> getRawGraph() {
    return this.graph;
  }
  
  /**
   * Returns the concrete reversed graph. Its interface is part of the JGraphT framework.
   */
  public final DirectedGraph<Vertex<? extends STEntry>, Edge> getReversedRawGraph() {
    return this.reversedGraph;
  }
  
}
