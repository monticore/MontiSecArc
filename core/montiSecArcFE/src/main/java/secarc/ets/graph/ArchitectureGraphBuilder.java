/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import static com.google.common.base.Preconditions.checkNotNull;
import interfaces2.STEntry;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.entries.CPEEntry;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;

import de.se_rwth.commons.Splitters;

/**
 * A builder that builds instances of {@link ArchitectureGraph} based on a given
 * {@link ComponentEntry} representing the root component of an architecture.
 * 
 * 
 */
public class ArchitectureGraphBuilder {
  
  /**
   * Factory method for {@link ArchitectureGraphBuilder}.
   */
  public static final ArchitectureGraphBuilder forArchitecture(ComponentEntry rootComponentEntry) {
    return new ArchitectureGraphBuilder(rootComponentEntry);
  }
  
  /**
   * @see #includeHierarchy
   */
  private boolean includeHierarchy = false;
  
  /**
   * @see #collapseConnectors
   */
  private boolean collapseConnectors = false;
  
  /**
   * The root component on which basis the graph will be built.
   */
  private final ComponentEntry rootComponent;
  
  /**
   * The actual graph that we are building.
   */
  private DirectedGraph<Vertex<? extends STEntry>, Edge> graph;
    
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ArchitectureGraphBuilder
   */
  ArchitectureGraphBuilder(ComponentEntry rootComponent) {
    checkNotNull(rootComponent);
    this.rootComponent = rootComponent;
  }
  
  /**
   * Commits all settings and starts the actual build process.
   */
  public final ArchitectureGraph buildGraphConnectorEdge() {
    doBuildGraphConnectorEdge();
    return new ArchitectureGraph(this.graph);
  }
  
  /**
   * Commits all settings and starts the actual build process.
   */
  public final ArchitectureGraph buildGraphIdentityEdge() {
    doBuildGraphIdentityEdge();
    return new ArchitectureGraph(this.graph);
  }
  
  /**
   * Flag that determines whether the built graph should omit connectors
   * adjacent to forwarding ports and directly connect transitively connected
   * concrete ports instead.
   */
  public final ArchitectureGraphBuilder collapseConnectors() {
    this.collapseConnectors = true;
    return this;
  }
  
  /**
   * Flag that determines whether the built graph should also represent the
   * architecture's hierarchy of components.
   */
  public final ArchitectureGraphBuilder includeHierarchy() {
    this.includeHierarchy = true;
    return this;
  }
  
  /**
   * @return collapseConnectors
   */
  protected final boolean doesCollapseConnectors() {
    return this.collapseConnectors;
  }
  
  /**
   * @return includeHierarchy
   */
  protected final boolean doesIncludeHierarchy() {
    return this.includeHierarchy;
  }
  
  /**
   * Does the actual work for a given component. Calls itself recursively on all
   * eventual subcomponents.
   */
  protected void visitComponent(SubComponentEntry component, ComponentEntry componentParent) {
        
    ComponentEntry componentType = component.getComponentType().getBestKnownVersion();
    
    Vertex<ComponentEntry> componentVertex = Vertex.of(componentType);
    this.graph.addVertex(componentVertex);
    
    visitIncomingPorts(componentType, componentVertex);
    visitOutgoingPorts(componentType, componentVertex);
    visitFilter(componentType, componentVertex);
    visitRoleComponent(componentType, componentVertex);
    visitCPE(componentType, componentVertex);
    visitConfiguration(componentType, componentVertex);
    visitPEP(componentType, componentVertex);
    visitTrustlevel(componentType, componentVertex);
    
    /* Recursively visit all subcomponents and add them as well as their ports
     * to the graph before proceeding with connectors. */
    for (SubComponentEntry subComponent : componentType.getSubComponents()) {
      visitComponent(subComponent, componentType);
    }
    
  }
  
  /**
   * Does the actual work for a given component. Calls itself recursively on all
   * eventual subcomponents. For Connector
   * 
   * @param component
   * @param componentParent
   */
  protected void visitComponentForConnector(SubComponentEntry component, ComponentEntry componentParent) {
	  ComponentEntry componentType = component.getComponentType().getBestKnownVersion();
	  
	  /* Recursively visit all subcomponents and add them as well as their ports
	     * to the graph before proceeding with connectors. */
	  for (SubComponentEntry subComponent : componentType.getSubComponents()) {
	  	visitComponentForConnector(subComponent, componentType);
	  }
	    
	  visitConnectors(componentType, componentParent);
  }
  
  /**
   * Does the actual work for a given component. Calls itself recursively on all
   * eventual subcomponents. For Identity
   * 
   * @param component
   * @param componentParent
   */
  protected void visitComponentForIdentity(SubComponentEntry root, SubComponentEntry currentComponent) {
	  ComponentEntry componentType = currentComponent.getComponentType().getBestKnownVersion();
	  
	  /* Recursively visit all subcomponents and add them as well as their ports
	     * to the graph before proceeding with connectors. */
	  for (SubComponentEntry subComponent : componentType.getSubComponents()) {
	  	visitComponentForIdentity(root, subComponent);
	  }
	    
	  visitIdentity(root.getComponentType(), componentType, Vertex.of(currentComponent));
  }
  
  /**
   * Adds all roles of the given component type to the graph
   * 
   * @param componentType
   * @param componentVertex
   */
  protected void visitRoleComponent(ComponentEntry componentType, Vertex<ComponentEntry> componentVertex) {
	  if(((SecComponentEntry) componentType).getRoles() != null && !((SecComponentEntry) componentType).getRoles().isEmpty()) {
		  Vertex<RoleEntry> roleVertex = null;
		  for(RoleEntry role : ((SecComponentEntry) componentType).getRoles()) {
			  roleVertex = Vertex.lookup(role, this.graph);
			  if(roleVertex == null) {
				  roleVertex = Vertex.of(role);
				  this.graph.addVertex(roleVertex);
			  }
		  	this.graph.addEdge(componentVertex, roleVertex);
		  }
	  }
  }
  
  /**
   * Adds all roles of the given port to the graph
   * 
   * @param port
   * @param portVertex
   */
  protected void visitRole(PortEntry port, Vertex<PortEntry> portVertex) {
	  if(((SecPortEntry) port).getRoles() != null && !((SecPortEntry) port).getRoles().isEmpty()) {
		  Vertex<RoleEntry> roleVertex = null;
		  for(RoleEntry role : ((SecPortEntry) port).getRoles()) {
			  roleVertex = Vertex.lookup(role, this.graph);
			  if(roleVertex == null) {
				  roleVertex = Vertex.of(role);
				  this.graph.addVertex(roleVertex);
			  }
		  	this.graph.addEdge(portVertex, roleVertex);
		  }
	  }
  }
  
  /**
   * Adds the configuration of the given component type to the graph
   * 
   * @param componentType
   * @param componentVertex
   */
  protected void visitConfiguration(ComponentEntry componentType, Vertex<ComponentEntry> componentVertex) {
	  ConfigurationEntry configuration = ((SecComponentEntry) componentType).getConfiguration();
	  if(configuration != null) {
		  Vertex<ConfigurationEntry> configurationVertex = Vertex.of(configuration);
		  this.graph.addVertex(configurationVertex);
		  this.graph.addEdge(componentVertex, configurationVertex);
	  }
  }
  
  /**
   * Adds the pep of the given component type to the graph
   * 
   * @param componentType
   * @param componentVertex
   */
  protected void visitPEP(ComponentEntry componentType, Vertex<ComponentEntry> componentVertex) {
	  PEPEntry pep = ((SecComponentEntry) componentType).getPEP();
	  if(pep != null) {
		  Vertex<PEPEntry> pepVertex = Vertex.of(pep);
		  this.graph.addVertex(pepVertex);
		  this.graph.addEdge(componentVertex, pepVertex);
	  }
  }
  
  /**
   * Adds the trustlevel of the given component type to the graph
   * 
   * @param componentType
   * @param componentVertex
   */
  protected void visitTrustlevel(ComponentEntry componentType, Vertex<ComponentEntry> componentVertex) {
	  Optional<TrustlevelEntry> trustlevel = ((SecComponentEntry) componentType).getTrustlevel();
	  if(trustlevel.isPresent()) {
		  Vertex<TrustlevelEntry> trustlevelVertex = Vertex.of(trustlevel.get());
		  this.graph.addVertex(trustlevelVertex);
		  this.graph.addEdge(componentVertex, trustlevelVertex);
	  }
  }
  
  /**
   * Adds the version of the given component type to the graph
   * 
   * @param componentType
   * @param componentVertex
   */
  protected void visitCPE(ComponentEntry componentType, Vertex<ComponentEntry> componentVertex) {
	  CPEEntry cpe = ((SecComponentEntry) componentType).getCPE();
	  if(cpe != null) {
		  Vertex<CPEEntry> cpeVertex = Vertex.of(cpe);
		  this.graph.addVertex(cpeVertex);
		  this.graph.addEdge(componentVertex, cpeVertex);
	  }
  }
  
  /**
   * Adds all filters of the given component type to the graph
   * @param componentType
   * @param componentVertex
   */
  protected void visitFilter(ComponentEntry componentType, Vertex<ComponentEntry> componentVertex) {
	  if(((SecComponentEntry) componentType).getFilter() != null) {
		  Vertex<FilterEntry> filterVertex = Vertex.of(((SecComponentEntry) componentType).getFilter());
		  this.graph.addVertex(filterVertex);
		  this.graph.addEdge(componentVertex, filterVertex);
	  }
  }
  
  /**
   * Adds all fitlers of the given port to the graph
   * @param componentType
   * @param componentVertex
   */
  protected void visitFilter(PortEntry port, Vertex<PortEntry> portVertex) {
	  if(((SecPortEntry) port).getFilter() != null) {
		  Vertex<FilterEntry> filterVertex = Vertex.of(((SecPortEntry) port).getFilter());
		  this.graph.addVertex(filterVertex);
		  this.graph.addEdge(portVertex, filterVertex);
	  }
  }
  
  /**
   * Adds all connectors of the given component type to the graph
   */
  protected void visitConnectors(ComponentEntry componentType, ComponentEntry componentParent) {
    
    for (ConnectorEntry connector : componentType.getConnectors()) {
      Vertex<ConnectorEntry> connectorVertex = Vertex.of(connector);
      this.graph.addVertex(connectorVertex);
      
      Iterable<String> source = Splitters.DOT.split(connector.getSource());
      Iterable<String> target = Splitters.DOT.split(connector.getTarget());
      String sourcePort = Iterables.get(source, 1, Iterables.get(source, 0));
      String targetPort = Iterables.get(target, 1, Iterables.get(target, 0));
      String sourceComponent = Iterables.size(source) == 2 ? Iterables.getFirst(source, "") : "";
      String targetComponent = Iterables.size(target) == 2 ? Iterables.getFirst(target, "") : "";
      
      Vertex<PortEntry> sourcePortVertex = null;
      Vertex<PortEntry> targetPortVertex = null;
      
      if (sourceComponent.isEmpty()) {
    	  if(((SecComponentEntry) componentType).getPort(sourcePort).isPresent()) {
	        PortEntry port = ((SecComponentEntry) componentType).getPort(sourcePort).get().getBestKnownVersion();
	        if(port != null) {
	        	sourcePortVertex = Vertex.lookup(port, this.graph);
	        }
    	  } else if(componentParent != null && 
    			  ((SecComponentEntry) componentParent).getPort(sourcePort).isPresent()) {
    	    PortEntry port = ((SecComponentEntry) componentParent).getPort(sourcePort).get().getBestKnownVersion();
    		if(port != null) {
    			sourcePortVertex = Vertex.lookup(port, this.graph);
    		}    		
    	  }
      } else {
    	  SubComponentEntry subComponent = null;
    	  if(componentParent != null && componentParent.getSubComponent(sourceComponent).isPresent()) {
	        subComponent = componentParent.getSubComponent(sourceComponent).get();
    	  } else if(componentType!= null && componentType.getSubComponent(sourceComponent).isPresent()) {
    		subComponent = componentType.getSubComponent(sourceComponent).get();
    	  }
	      if(subComponent != null && ((SecComponentEntry) subComponent.getComponentType()).getPort(sourcePort).isPresent()) {
		      PortEntry port = ((SecComponentEntry) subComponent.getComponentType()).getPort(sourcePort).get().getBestKnownVersion();
		      if(port != null) {
		    	  sourcePortVertex = Vertex.lookup(port, this.graph);
		      }
		  }
      }
      
      if (targetComponent.isEmpty()) {
    	  if(((SecComponentEntry) componentType).getPort(targetPort).isPresent()) {
    		  PortEntry port = ((SecComponentEntry) componentType).getPort(targetPort).get().getBestKnownVersion();
    	      if(port != null) {
    	    	  targetPortVertex = Vertex.lookup(port, this.graph);
    	      }
    	  } else if(componentParent != null && 
    			  ((SecComponentEntry) componentParent).getPort(targetPort).isPresent()) {
    	      PortEntry port = ((SecComponentEntry) componentParent).getPort(targetPort).get().getBestKnownVersion();
    	      if(port != null) {
    	    	  sourcePortVertex = Vertex.lookup(port, this.graph);
    	      }    		
    	  }
      } else {
    	  SubComponentEntry subComponent = null;
    	  if(componentParent != null && componentParent.getSubComponent(targetComponent).isPresent()) {
	        subComponent = componentParent.getSubComponent(targetComponent).get();
    	  } else if(componentType != null && componentType.getSubComponent(targetComponent).isPresent()) {
    		subComponent = componentType.getSubComponent(targetComponent).get();
    	  }
    	  if(subComponent != null && ((SecComponentEntry) subComponent.getComponentType()).getPort(targetPort).isPresent()) {
		      PortEntry port = ((SecComponentEntry) subComponent.getComponentType()).getPort(targetPort).get().getBestKnownVersion();
		      if(port != null) {
		    	  targetPortVertex = Vertex.lookup(port, this.graph);
		      }
    	  }
      }
      
      if(sourcePortVertex != null) {
    	  this.graph.addEdge(sourcePortVertex, connectorVertex);
      }
      if(targetPortVertex != null) {
    	  this.graph.addEdge(connectorVertex, targetPortVertex);
      }
      
    }

  }
  
  /**
   * Adds all identity links of the given component type to the graph
   */
  protected void visitIdentity(ComponentEntry root, ComponentEntry componentType,
	      Vertex<SubComponentEntry> componentVertex) {
	  ComponentEntry fullQualifiedComponent = null;
	  
	  for(IdentityEntry identityEntry : ((SecComponentEntry) componentType).getIdentities()) {
		  Vertex<IdentityEntry> identityVertex = Vertex.of(identityEntry);
	      this.graph.addVertex(identityVertex);
	      fullQualifiedComponent = findFullQualifiedComponent(root, identityEntry.getSource());
	      if(fullQualifiedComponent != null) {
	    	  this.graph.addEdge(Vertex.lookup(fullQualifiedComponent.getBestKnownVersion(), this.graph), identityVertex);
	      }
	      fullQualifiedComponent = findFullQualifiedComponent(root, identityEntry.getTarget());
	      if(fullQualifiedComponent != null) {
	    	  this.graph.addEdge(identityVertex, Vertex.lookup(fullQualifiedComponent.getBestKnownVersion(), this.graph));
	      }
	  }	  
  }
  
  /**
   * Visit all incoming ports of this component and connect the component node
   * to them in the direction of the message flow.
   */
  protected void visitIncomingPorts(ComponentEntry componentType,
      Vertex<ComponentEntry> componentVertex) {
    for (PortEntry port : componentType.getIncomingPorts()) {
      Vertex<PortEntry> portVertex = Vertex.of(port.getBestKnownVersion());
      this.graph.addVertex(portVertex);
      this.graph.addEdge(portVertex, componentVertex);
      visitFilter(port, portVertex);
      visitRole(port, portVertex);
    }
  }
  
  /**
   * Visit all outgoing ports of this component and connect the component node
   * to them in the direction of the message flow.
   */
  protected void visitOutgoingPorts(ComponentEntry componentType,
      Vertex<ComponentEntry> componentVertex) {
    for (PortEntry port : componentType.getOutgoingPorts()) {
      Vertex<PortEntry> portVertex = Vertex.of(port.getBestKnownVersion());
      this.graph.addVertex(portVertex);
      this.graph.addEdge(componentVertex, portVertex);
      visitFilter(port, portVertex);
      visitRole(port, portVertex);
    }
  }
  
  /**
   * Find the full qualified component which is referenced in a identity link
   * @param root
   * @param fullQualifiedName
   * @return reference component
   */
  private ComponentEntry findFullQualifiedComponent(ComponentEntry root, String fullQualifiedName) {
	  	
	  int firstdot = fullQualifiedName.indexOf(".");
	  String nameSubcomponent = "";
	  	
	  if(firstdot == -1) {
		  return findRootFromFullQualifiedName(root, fullQualifiedName);
	  } else {
		  nameSubcomponent = fullQualifiedName.substring(0, firstdot);
		  fullQualifiedName = fullQualifiedName.substring(firstdot + 1);
	  }
	  
	  ComponentEntry rootFullQualifiedName = findRootFromFullQualifiedName(root, nameSubcomponent);
	  
	  //Check in coco
	  if(rootFullQualifiedName == null) {
		  return null;
	  }
	  
	  //search from rootFullQualifiedName
	  return findSubComponentFromRootFullQualifiedName(rootFullQualifiedName, fullQualifiedName);
  }
  
  /**
   * Find root element of the full qualified name
   * -> first part of the full qualified name
   * @param current
   * @param fullQualifiedName
   * @return root of full qualified name
   */
  private ComponentEntry findRootFromFullQualifiedName(ComponentEntry root, String name) {
	  for(SubComponentEntry subComponent : root.getSubComponents()) {
		  if(subComponent.getName().equals(name)) {
			  return subComponent.getComponentType();
		  } else {
			  ComponentEntry componentEntry = findRootFromFullQualifiedName(subComponent.getComponentType(), name);
			  if(componentEntry != null) {
				  return componentEntry;
			  }
		  }
	  }
	  return null;
  }
  
  /**
   * Find the full qualified component 
   * 
   * @param current
   * @param fullQualifiedName
   * @return refrenced component
   */
  private ComponentEntry findSubComponentFromRootFullQualifiedName(ComponentEntry current, String fullQualifiedName) {
	  int firstdot = fullQualifiedName.indexOf(".");
	  String nameSubcomponent = "";
	  	
	  if(firstdot == -1) {
		  nameSubcomponent = fullQualifiedName;
	  } else {
		  nameSubcomponent = fullQualifiedName.substring(0, firstdot);
		  fullQualifiedName = fullQualifiedName.substring(firstdot-1);
	  }
	  
	  for(SubComponentEntry subComponent : current.getSubComponents()) {
		  if(subComponent.getName().equals(nameSubcomponent)) {
			  if(fullQualifiedName.contains(".")) {
				  return findSubComponentFromRootFullQualifiedName(subComponent.getComponentType(), fullQualifiedName);
			  } else {
				  return subComponent.getComponentType();
			  }
		  }
	  }	  
	  
	  return null;
	  
  }
  
  /**
   * Starts the actual work. We create an artificial instance of the root
   * Connectors are the edges
   * component type named "ROOT".
   */
  private void doBuildGraphConnectorEdge() {
    this.graph = new DefaultDirectedGraph<Vertex<? extends STEntry>, Edge>(Edge.class);
    SubComponentEntry rootComponentEntry = new SubComponentEntry();
    rootComponentEntry.setComponentType(this.rootComponent);
    rootComponentEntry.setName("ROOT");
    visitComponent(rootComponentEntry, null);
    //connector must be integrated at the end because a port can be used in a connector before it is saved in the graph
    visitComponentForConnector(rootComponentEntry, null);
  }
  
  /**
   * Starts the actual work. We create an artificial instance of the root
   * Identity are the edges
   * component type named "ROOT".
   */
  private void doBuildGraphIdentityEdge() {
    this.graph = new DefaultDirectedGraph<Vertex<? extends STEntry>, Edge>(Edge.class);
    SubComponentEntry rootComponentEntry = new SubComponentEntry();
    rootComponentEntry.setComponentType(this.rootComponent);
    rootComponentEntry.setName("ROOT");
    visitComponent(rootComponentEntry, null);
    //identity must be integrated at the end because a port can be used in a connector before it is saved in the graph
    visitComponentForIdentity(rootComponentEntry, rootComponentEntry);    
  }
  
}
