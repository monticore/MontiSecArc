/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.Sets.newHashSet;
import interfaces2.STEntry;

import java.util.Collection;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graphs;

/**
 * TODO: Write me!
 * 
 * 
 */
public final class ArchitectureNeighbours {
  
  /**
   * TODO: Write me!
   * 
   * @param entry
   * @param graph
   * @return
   */
  public static <E extends STEntry> Collection<E> forElement(E entry,
      DirectedGraph<Vertex<? extends STEntry>, Edge> graph) {
    Vertex<E> vertex = Vertex.lookup(entry, graph);
    checkArgument(vertex != null);
    return getNext(vertex, entry.getClass(), graph);
  }
  
  /**
   * TODO: Write me!
   * 
   * @param vertex
   * @param graph
   * @return
   */
  @SuppressWarnings("unchecked")
  private static <E extends STEntry> Collection<E> getNext(Vertex<? extends STEntry> vertex,
      Class<? extends STEntry> entryClass,
      
      DirectedGraph<Vertex<? extends STEntry>, Edge> graph) {
    Collection<E> entries = newHashSet();
    for (Vertex<? extends STEntry> v : Graphs.successorListOf(graph, vertex)) {
      entries.addAll((Collection<? extends E>) getNext(v, vertex, entryClass, graph));
    }
    return entries;
  }
  
  /**
   * TODO: Write me!
   * 
   * @param vertex
   * @param graph
   * @return
   */
  @SuppressWarnings("unchecked")
  private static <E extends STEntry> Collection<E> getNext(Vertex<? extends STEntry> vertex,
      Vertex<? extends STEntry> startVertex,
      Class<? extends STEntry> entryClass,
      DirectedGraph<Vertex<? extends STEntry>, Edge> graph) {
    
    Collection<E> entries = newHashSet();
    
    if (!vertex.equals(startVertex)) {
      STEntry entry = vertex.getArchitectureElement();
      if (entryClass.isAssignableFrom(entry.getClass())) {
        entries.add((E) entry);
      } else {
        for (Vertex<? extends STEntry> v : Graphs.successorListOf(graph, vertex)) {
          entries.addAll((Collection<? extends E>) getNext(v, startVertex, entryClass, graph));
        }
      }
    }
    
    return entries;
  }
  
  /**
   * Private constructor permitting manual instantiation.
   */
  private ArchitectureNeighbours() {
  }

}
