/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import secarc.ets.entries.CPEEntry;


/**
 * Vertex for a {@link Version}.
 * 
 */
public class CPEVertex  extends Vertex<CPEEntry> {

	/**
	 * Constructor for cc.clarc.lang.architecture.graph.VersionVertex
	 * 
	 * @param architectureElementDescription
	 */
	  protected CPEVertex(CPEEntry architectureElementDescription) {
	    super(architectureElementDescription);
	  }
	
}
