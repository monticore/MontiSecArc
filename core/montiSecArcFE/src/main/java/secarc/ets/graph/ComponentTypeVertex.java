/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.ComponentEntry;

/**
 * TODO: Write me!
 * 
 * 
 */
final class ComponentTypeVertex extends Vertex<ComponentEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ComponentTypeVertex
   * 
   * @param architectureElementDescription
   */
  protected ComponentTypeVertex(ComponentEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
