/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.SubComponentEntry;

/**
 * Vertex for a {@link SubComponent}.
 * 
 */
final class ComponentVertex extends Vertex<SubComponentEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ComponentVertex
   * 
   * @param architectureElementDescription
   */
  protected ComponentVertex(SubComponentEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
