/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import secarc.ets.entries.ConfigurationEntry;

/**
 * Vertex for a {@link Configuration}.
 * 
 */
public class ConfigurationVertex extends Vertex<ConfigurationEntry> {
	
	/**
	 * Constructor for cc.clarc.lang.architecture.graph.ConfigurationVertex
	 * 
	 * @param architectureElementDescription
	 */
	  protected ConfigurationVertex(ConfigurationEntry architectureElementDescription) {
	    super(architectureElementDescription);
	  }

}
