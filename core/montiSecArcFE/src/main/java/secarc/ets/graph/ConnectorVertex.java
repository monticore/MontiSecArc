/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.ConnectorEntry;

/**
 * TODO: Write me!
 * 
 * 
 */
final class ConnectorVertex extends Vertex<ConnectorEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.ConnectorVertex
   * 
   * @param architectureElementDescription
   */
  protected ConnectorVertex(ConnectorEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
