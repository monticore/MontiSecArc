/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import org.jgrapht.graph.DefaultEdge;

/**
 * Represents an edge in an {@link ArchitectureGraph}.
 * 
 * 
 */
public class Edge extends DefaultEdge {
  
  private static final long serialVersionUID = 8312943492634005198L;
  
}
