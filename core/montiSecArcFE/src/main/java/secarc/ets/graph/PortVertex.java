/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.PortEntry;

/**
 * TODO: Write me!
 * 
 * 
 */
final class PortVertex extends Vertex<PortEntry> {
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.PortVertex
   * 
   * @param architectureElementDescription
   */
  protected PortVertex(PortEntry architectureElementDescription) {
    super(architectureElementDescription);
  }
  
}
