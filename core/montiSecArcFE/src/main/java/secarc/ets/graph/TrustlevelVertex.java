/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import secarc.ets.entries.TrustlevelEntry;

/**
 * Vertex for a {@link Trustlevel}.
 * 
 */
final class TrustlevelVertex extends Vertex<TrustlevelEntry> {

	/**
	 * Constructor for cc.clarc.lang.architecture.graph.TrustlevelVertex
	 * 
	 * @param architectureElementDescription
	 */
	  protected TrustlevelVertex(TrustlevelEntry architectureElementDescription) {
	    super(architectureElementDescription);
	  }
	
}
