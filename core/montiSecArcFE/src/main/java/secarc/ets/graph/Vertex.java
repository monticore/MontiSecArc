/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import interfaces2.STEntry;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

import org.jgrapht.DirectedGraph;

import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.entries.CPEEntry;

import com.google.common.base.Objects;

/**
 * Represents a vertex in an {@link ArchitectureGraph}. Vertices contain
 * references to the {@link STEntry}s they represent.
 * 
 * 
 */
public abstract class Vertex<E extends STEntry> {
  
  /**
   * Returns the vertex that contains the given {@link STEntry} or null if not
   * found.
   */
  @SuppressWarnings("unchecked")
  final static <E extends STEntry> Vertex<E> lookup(E entry,
      DirectedGraph<Vertex<?>, Edge> graph) {
    
    assert entry != null;
    
    Vertex<E> vertexForEntry = null;
    
    for (Vertex<?> vertex : graph.vertexSet()) {
      if (entry.equals(vertex.getArchitectureElement())) {
        vertexForEntry = (Vertex<E>) vertex;
        break;
      }
    }
    
    return vertexForEntry;
  }
  
  /**
   * Factory method creating a concrete {@link ComponentTypeVertex} subclass of
   * {@link Vertex}.
   */
  public static ComponentTypeVertex of(ComponentEntry component) {
    return new ComponentTypeVertex(component);
  }
  
  /**
   * Factory method creating a concrete {@link ConnectorVertex} subclass of
   * {@link Vertex}.
   */
  public static final ConnectorVertex of(ConnectorEntry connector) {
    return new ConnectorVertex(connector);
  }
  
  /**
   * Factory method creating a concrete {@link PortVertex} subclass of
   * {@link Vertex}.
   */
  public static final PortVertex of(PortEntry port) {
    return new PortVertex(port);
  }
  
  /**
   * Factory method creating a concrete {@link ComponentVertex} subclass of
   * {@link Vertex}.
   */
  public static final ComponentVertex of(SubComponentEntry subComponent) {
    return new ComponentVertex(subComponent);
  }
  
  /**
   * Factory method creating a concrete {@link FilterVertex} filter of
   * {@link Vertex}.
   */
  public static final FilterVertex of(FilterEntry filter) {
	  return new FilterVertex(filter);
  }
  
  /**
   * Factory method creating a concrete {@link RoleVertex} role of
   * {@link Vertex}.
   */
  public static final RoleVertex of(RoleEntry role) {
	  return new RoleVertex(role);
  }
  
  /**
   * Factory method creating a concrete {@link ConfigurationVertex} configuration of
   * {@link Vertex}.
   */
  public static final ConfigurationVertex of(ConfigurationEntry configuration) {
	  return new ConfigurationVertex(configuration);
  }
  
  /**
   * Factory method creating a concrete {@link PEPVertex} pep of
   * {@link Vertex}.
   */
  public static final PEPVertex of(PEPEntry pep) {
	  return new PEPVertex(pep);
  }
  
  /**
   * Factory method creating a concrete {@link IdentityVertex} identity of
   * {@link Vertex}.
   */
  public static final IdentityVertex of(IdentityEntry identity) {
	  return new IdentityVertex(identity);
  }
  
  /**
   * Factory method creating a concrete {@link TrustlevelVertex} trustlevel of
   * {@link Vertex}.
   */
  public static final TrustlevelVertex of(TrustlevelEntry trustlevel) {
	  return new TrustlevelVertex(trustlevel);
  }
  
  /**
   * Factory method creating a concrete {@link CPEVertex} cpe of
   * {@link Vertex}.
   */
  public static final CPEVertex of(CPEEntry cpe) {
	  return new CPEVertex(cpe);
  }
  
  /**
   * The reference to the represented {@link STEntry}
   */
  private final E architectureElementDescription;
  
  /**
   * Constructor for cc.clarc.lang.architecture.graph.Vertex
   */
  protected Vertex(E architectureElementDescription) {
    this.architectureElementDescription = architectureElementDescription;
  }
  
  /**
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Vertex<?> that = (Vertex<?>) obj;
    return Objects.equal(this.architectureElementDescription, that.architectureElementDescription);
  }
  
  /**
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hashCode(this.architectureElementDescription);
  }
  
  /**
   * @return the represented {@link STEntry}
   */
  public E getArchitectureElement() {
    return this.architectureElementDescription;
  }
  
}
