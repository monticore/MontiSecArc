/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.graph;

import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.ConnectorEntry;
import mc.umlp.arcd.ets.entries.PortEntry;
import mc.umlp.arcd.ets.entries.SubComponentEntry;

/**
 * Prints instances of {@link Vertex}.
 * 
 */
final class VertexPrinter {
  
  /**
   * Prints a single vertex representing a component type
   */
  static String printComponentTypeVertex(ComponentTypeVertex vertex) {
    ComponentEntry entry = vertex.getArchitectureElement();
    return "[Component Type] " + entry.getTypeName();
  }
  
  /**
   * Prints a single vertex representing a component instance
   */
  static String printComponentVertex(ComponentVertex vertex) {
    SubComponentEntry entry = vertex.getArchitectureElement();
    return "[Component] " + entry.getName() + " : " + entry.getComponentType().getTypeName();
  }
  
  /**
   * Prints a single vertex representing a connector
   */
  static String printConnectorVertex(ConnectorVertex vertex) {
    ConnectorEntry entry = vertex.getArchitectureElement();
    return "[Connector] " + entry.getSource() + " -> " + entry.getTarget();
  }
  
  /**
   * Prints a single vertex representing a port
   */
  static String printPortVertex(PortVertex vertex) {
    PortEntry entry = vertex.getArchitectureElement();
    String direction = entry.isIncoming() ? "Incoming" : "Outgoing";
    return "[Port] " + direction + " " + entry.getName();
  }
  
  /**
   * Prints a single vertex
   */
  static String printVertex(Vertex<?> vertex) {
    
    String print;
    
    if (vertex instanceof ComponentTypeVertex) {
      print = printComponentTypeVertex((ComponentTypeVertex) vertex);
    } else if (vertex instanceof ComponentVertex) {
      print = printComponentVertex((ComponentVertex) vertex);
    } else if (vertex instanceof PortVertex) {
      print = printPortVertex((PortVertex) vertex);
    } else if (vertex instanceof ConnectorVertex) {
      print = printConnectorVertex((ConnectorVertex) vertex);
    } else {
      print = vertex.getArchitectureElement().toString();
    }
    
    return print;
  }
  
  /**
   * Private constructor permitting manual instantiation.
   */
  private VertexPrinter() {
  }

}
