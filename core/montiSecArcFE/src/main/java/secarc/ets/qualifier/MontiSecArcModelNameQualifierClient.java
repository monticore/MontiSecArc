/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.qualifier;

import secarc.MontiSecArcConstants;
import mc.umlp.arc.ets.qualifier.MontiArcModelNameQualifierClient;
import mc.umlp.arcd.ArcdConstants;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.qualifier.ArcdModelNameQualifierClient;

/**
 * 
 * Component model name qualifier client.
 *
 *
 */
public class MontiSecArcModelNameQualifierClient extends
		MontiArcModelNameQualifierClient {

	 /**
     * Model name qualifier client id.
     */
    public static final String ID = "MontiSecArc";
    
    /* (non-Javadoc)
     * @see interfaces2.helper.AbstractModelNameQualifierClient#getFileEnding()
     */
    @Override
    public String getFileEnding() {
        return MontiSecArcConstants.FILE_ENDING;
    }
    
    /* (non-Javadoc)
     * @see interfaces2.helper.AbstractModelNameQualifierClient#getModelKindID()
     */
    @Override
    public String getModelKindID() {
        return ID;
    }
    
    /* (non-Javadoc)
     * @see interfaces2.helper.AbstractModelNameQualifierClient#getModelKindID()
     */
    @Override
    public String getSymbolKindID() {
      return ArcdModelNameQualifierClient.ID;
    }
    
    /* (non-Javadoc)
     * @see interfaces2.helper.AbstractModelNameQualifierClient#getModelKindIDOfCreateModelNameFileVisitor()
     */
    @Override
    public String getModelKindIDOfCreateModelNameFileVisitor() {
        return null;
    }

    @Override
    public String[] getExportedEntryKinds() {
      return new String[] {ComponentEntry.KIND};
    }
    
    @Override
    public String getSymbolTableKind() {
      return ArcdConstants.ST_KIND_PROTECTED;
    }
	
}
