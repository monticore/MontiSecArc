/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;
import secarc.ets.entries.CPEEntry;

/**
 * 
 * Resolver client for version entries.
 *
 *
 */
public class CPEResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return CPEEntry.KIND;
	}

}
