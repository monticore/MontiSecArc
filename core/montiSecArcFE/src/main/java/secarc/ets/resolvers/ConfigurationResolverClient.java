/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import secarc.ets.entries.ConfigurationEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
 * 
 * Resolver client for configuration entries.
 *
 *
 */
public class ConfigurationResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return ConfigurationEntry.KIND;
	}

}
