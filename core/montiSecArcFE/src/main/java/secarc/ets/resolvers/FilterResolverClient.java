/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import secarc.ets.entries.FilterEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
 * 
 * Resolver client for filter entries.
 *
 *
 */
public class FilterResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return FilterEntry.KIND;
	}

}
