/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import java.util.HashSet;
import java.util.Set;

import mc.umlp.arcd.ArcdConstants;
import secarc.ets.entries.IdentityEntry;
import interfaces2.ISTEntry;
import interfaces2.STEntry;
import interfaces2.SymbolTable;
import interfaces2.namespaces.NameSpace;
import interfaces2.resolvers.AbstractResolverClient;
import interfaces2.resolvers.AmbigousException;

/**
 * 
 * Resolver identity links.
 *
 *
 */
public class IdentityResolverClient extends AbstractResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return IdentityEntry.KIND;
	}

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#resolve(java.lang.String, interfaces2.namespaces.NameSpace, java.lang.Object[])
     */
    @Override
    public STEntry resolve(String name, NameSpace nsp, Object... params) throws AmbigousException {
        Set<IdentityEntry> found = new HashSet<IdentityEntry>();
        Set<SymbolTable> all = new HashSet<SymbolTable>();
        all.add(nsp.getEncapsulatedST());
        all.addAll(getSymTab(ArcdConstants.ST_KIND_PROTECTED, nsp.getExportedSTs()));
        all.addAll(getSymTab(ArcdConstants.ST_KIND_PROTECTED, nsp.getImportedSTs()));
        for (SymbolTable table : all) {
            for (ISTEntry entry : table.getEntries()) {
                if (entry instanceof IdentityEntry) {
                	IdentityEntry casted = (IdentityEntry) entry;
                    if (casted.getTarget().equals(name) && !nsp.isHidden(casted)) {
                        found.add(casted);
                    }
                }
            }
        }
        if (found.size() == 1) {
            return found.iterator().next();
        } else if (found.size() > 1) {
            throw new AmbigousException(found);
        } else {
            return null;    
        }
    }

}
