/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import secarc.ets.entries.PEPEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
 * 
 * Resolver client for pep entries.
 *
 *
 */
public class PEPResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return PEPEntry.KIND;
	}

}
