/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import secarc.ets.entries.TrustlevelRelationEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;

/**
* 
* Resolver client for trustlevel relation entries.
*
*
*/
public class TrustlevelRelationResolverClient extends
		AbstractArcdResolverClient {

	/*
	 * (non-Javadoc)
	 * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
	 */
	@Override
	public String getResponsibleKind() {
		return TrustlevelRelationEntry.KIND;
	}

}
