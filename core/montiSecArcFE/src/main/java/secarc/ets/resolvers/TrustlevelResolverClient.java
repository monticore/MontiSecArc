/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.resolvers;

import secarc.ets.entries.TrustlevelEntry;
import mc.umlp.arcd.ets.resolvers.AbstractArcdResolverClient;
/**
* 
* Resolver client for trustlevel entries.
*
*
*/
public class TrustlevelResolverClient extends AbstractArcdResolverClient {

	/* (non-Javadoc)
     * @see interfaces2.resolvers.IResolverClient#getResponsibleKind()
     */
	@Override
	public String getResponsibleKind() {
		return TrustlevelEntry.KIND;
	}

}
