/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform;

import secarc.ets.transform.transformators.SecAutoConnection;
import mc.DSLRoot;
import mc.umlp.arc.ets.transform.PreCoCoCheckMontiArcTransformationVisitor;
import mc.umlp.arc.ets.transform.transformators.AutoConnection;
import mc.umlp.arcd.ets.transform.transformators.ITransformator;

public class PreCoCoCheckMontiSecArcTransformationVisitor<T extends DSLRoot<?>> extends PreCoCoCheckMontiArcTransformationVisitor<T> {

	public PreCoCoCheckMontiSecArcTransformationVisitor(final T dslroot) {
        super(dslroot);
        
        createUsefulTransformators();
	}
	
	 /* (non-Javadoc)
     * @see mc.umlp.arcd.ets.transform.TransformationVisitor#register(mc.umlp.arcd.ets.transform.transformators.ITransformator)
     */
    @Override
    protected void register(ITransformator trafo) {
        super.register(trafo);
        //remove by SecAutoConnection
        if (trafo instanceof AutoConnection) {
        	autoConnectTransformators.remove(trafo);
        }
    }
    
    private void createUsefulTransformators() {
        register(new SecAutoConnection(getSymtab()));
    }
	
}
