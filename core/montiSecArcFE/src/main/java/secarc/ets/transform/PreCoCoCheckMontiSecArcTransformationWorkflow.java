/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.transform;

import secarc._tool.MontiSecArcRoot;
import mc.DSLWorkflow;
import mc.ast.InheritanceVisitor;

public class PreCoCoCheckMontiSecArcTransformationWorkflow<T extends MontiSecArcRoot> extends DSLWorkflow<T> {

	public PreCoCoCheckMontiSecArcTransformationWorkflow(
			Class<? extends T> responsibleClass) {
		super(responsibleClass);
	}

	@Override
	public void run(MontiSecArcRoot dslroot) {
		InheritanceVisitor.run(new PreCoCoCheckMontiSecArcTransformationVisitor<MontiSecArcRoot>(dslroot), dslroot.getAst());
	}

}
