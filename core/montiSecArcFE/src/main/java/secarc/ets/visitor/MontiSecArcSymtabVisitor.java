/* (c) https://github.com/MontiCore/monticore */
package secarc.ets.visitor;

import java.util.ArrayList;
import java.util.List;


import interfaces2.STEntryState;
import interfaces2.SymbolTable;
import interfaces2.namespaces.NameSpace;
import secarc.MontiSecArcConstants;
import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcPEP;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcTrustLevel;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc._ast.ASTSecArcCPE;
import secarc._ast.PrototypeASTSecArcConnector;
import secarc._ast.PrototypeASTSecArcFilterComponent;
import secarc._ast.PrototypeASTSecArcPort;
import secarc._ast.PrototypeASTSecArcSimpleConnector;
import secarc.ets.entries.RoleEntry;
import secarc.ets.entries.ConfigurationEntry;
import secarc.ets.entries.FilterEntry;
import secarc.ets.entries.IdentityEntry;
import secarc.ets.entries.PEPEntry;
import secarc.ets.entries.SecComponentEntry;
import secarc.ets.entries.SecConnectorEntry;
import secarc.ets.entries.SecEntryFactory;
import secarc.ets.entries.SecPortEntry;
import secarc.ets.entries.TrustlevelEntry;
import secarc.ets.entries.TrustlevelRelationEntry;
import secarc.ets.entries.CPEEntry;

import com.google.common.base.Optional;
import com.google.inject.Inject;

import mc.helper.NameHelper;
import mc.types._ast.ASTQualifiedName;
import mc.types._ast.ASTQualifiedNameList;
import mc.umlp.arc.ets.visitors.MontiArcSymtabVisitor;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd.ets.entries.ComponentEntry;
import mc.umlp.arcd.ets.entries.IArcdEntryFactory;
import mc.umlp.arcd.ets.entries.PortEntry;

/** 
 * 
 * Concrete Symbol table visitor for MontiSecARc.
 *
 *
 */
public class MontiSecArcSymtabVisitor extends MontiArcSymtabVisitor {
	
	/**
     * 
     * @param implementationCreator
     */
	@Inject
    public MontiSecArcSymtabVisitor(final IArcdEntryFactory arcdFactory) {
        super(arcdFactory);
        super.setPublicNodes(MontiSecArcConstants.MONTI_SEC_ARC_PUBLIC_NODES);
    }
	
	private FilterEntry createFilterEntry(ASTSecArcFilter node) {
		FilterEntry filterEntry = ((SecEntryFactory) this.getArcdFactory()).createFilter(node.getName());
		filterEntry.setEntryState(STEntryState.FULL, filterEntry);
        filterEntry.setNode(node);
        
        NameSpace ns = getNameSpaceFor(node);
        
        storage.addEntry(filterEntry, ns, node.get_SourcePositionStart());
        
        return filterEntry;
	}
	
	/**
	 * Visits componentRole
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcRole node) {
		List<RoleEntry> roles = new ArrayList<RoleEntry>();
        
        NameSpace ns = getNameSpaceFor(node);
        
        for (String role : node.getRoles()) {
        	RoleEntry roleEntry = ((SecEntryFactory) this.getArcdFactory()).createRole();
        	roleEntry.setEntryState(STEntryState.FULL, roleEntry);
        	roleEntry.setRole(role);
            
            storage.addEntry(roleEntry, ns, node.get_SourcePositionStart());
            //Add to currentComponent
            if (!storage.getEntryStack().isEmpty()) {
            	if(storage.getEntryStack().peek() instanceof ComponentEntry) {
	            	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
	                currentComponent.addRole(roleEntry);
            	}
            }
            roles.add(roleEntry);
        }
        
        //Add roles
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntries(roles);
        }
	}
	
	/**
	 * Visits configuration
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcConfiguration node) {
        ConfigurationEntry configurationEntry = ((SecEntryFactory) this.getArcdFactory()).createConfiguration();
        configurationEntry.setEntryState(STEntryState.FULL, configurationEntry);
        configurationEntry.setName(node.getName());
        configurationEntry.setNode(node);
        
        NameSpace ns = getNameSpaceFor(node);
        
        storage.addEntry(configurationEntry, ns, node.get_SourcePositionStart());
        
        //Add to currentComponent
        if (!storage.getEntryStack().isEmpty()) {
        	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
            currentComponent.setConfiguration(configurationEntry);
        }
        
        //Add configuration
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntry(configurationEntry);
        }
	}
	
	/**
	 * Visits identity
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcIdentity node) {
		List<IdentityEntry> identities = new ArrayList<IdentityEntry>();
        
        NameSpace ns = getNameSpaceFor(node);
        
        for (ASTQualifiedName target : node.getTargets()) {
            IdentityEntry identityEntry = ((SecEntryFactory) this.getArcdFactory()).createIdentity();
            identityEntry.setEntryState(STEntryState.FULL, identityEntry);
            identityEntry.setIdentityKind(node.isStrong());
            identityEntry.setSource(NameHelper.dotSeparatedStringFromList(node.getSource().getParts()));
            identityEntry.setTarget(NameHelper.dotSeparatedStringFromList(target.getParts()));
            identityEntry.setNode(node);
            
            storage.addEntry(identityEntry, ns, node.get_SourcePositionStart());
            //Add to currentComponent
            if (!storage.getEntryStack().isEmpty()) {
            	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
                currentComponent.addIdentity(identityEntry);
            }
            identities.add(identityEntry);
        }
        
        //Add Identity
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntries(identities);
        }
	}
	
	/**
	 * Visit pep
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcPEP node) {
        PEPEntry pepEntry = ((SecEntryFactory) this.getArcdFactory()).createPEP();
        pepEntry.setEntryState(STEntryState.FULL, pepEntry);
        pepEntry.setAccessControl(node.isOn());
        pepEntry.setNode(node);
        
        NameSpace ns = getNameSpaceFor(node);
        
        storage.addEntry(pepEntry, ns, node.get_SourcePositionStart());
        
        //Add to currentComponent
        if (!storage.getEntryStack().isEmpty()) {
        	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
            currentComponent.setPEP(pepEntry);
        }
        
        //Add PEP
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntry(pepEntry);
        }		
	}
	
	/**
	 * Visits Roles for Ports
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcRefRole node) {
		
		List<RoleEntry> roles = new ArrayList<RoleEntry>();
        
        NameSpace ns = getNameSpaceFor(node);
        
        for (String role : node.getRoles()) {
        	RoleEntry roleEntry = ((SecEntryFactory) this.getArcdFactory()).createRole();
        	roleEntry.setEntryState(STEntryState.FULL, roleEntry);
        	roleEntry.setRole(role);
            
            storage.addEntry(roleEntry, ns, node.get_SourcePositionStart());
            //Add to current component
            if (!storage.getEntryStack().isEmpty() && storage.getEntryStack().peek() instanceof ComponentEntry) {
            	
	            SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
	            	            
	            Optional<PortEntry> portEntry = currentComponent.getPort(node.getPortName());
	            if(portEntry.isPresent()) {
	            	((SecPortEntry) portEntry.get()).addRole(roleEntry);
	            }
            	
            }
            roles.add(roleEntry);
        }
        
        //Add Roles
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntries(roles);
        }
	}
	
	/**
	 * Visits trustlevel
	 * 
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcTrustLevel node) {
        TrustlevelEntry trustlevelEntry = ((SecEntryFactory) this.getArcdFactory()).createTrustlevel();
        trustlevelEntry.setEntryState(STEntryState.FULL, trustlevelEntry);
        trustlevelEntry.setReason(node.getReason());
        trustlevelEntry.setValue(node.getValue().getValue());
        trustlevelEntry.setRelative(node.isPLUS());
        trustlevelEntry.setNode(node);
        
        NameSpace ns = getNameSpaceFor(node);
        
        storage.addEntry(trustlevelEntry, ns, node.get_SourcePositionStart());
        
        //Add to currentComponent
        if (!storage.getEntryStack().isEmpty()) {
        	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
            currentComponent.setTrustlevel(trustlevelEntry);
        }
        
        //Add trustlevel
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntry(trustlevelEntry);
        }
	}
	
	/**
	 * Visits cpe
	 *  
	 * @param node ast node to visit
	 */
	public void visit(ASTSecArcCPE node) {
        CPEEntry cpeEntry = ((SecEntryFactory) this.getArcdFactory()).createCPE();
        cpeEntry.setEntryState(STEntryState.FULL, cpeEntry);
        cpeEntry.setCPE(node.getNameCPE().getValue());
        cpeEntry.setNode(node);
        
        NameSpace ns = getNameSpaceFor(node);
        
        storage.addEntry(cpeEntry, ns, node.get_SourcePositionStart());
        
        //Add to currentComponent
        if (!storage.getEntryStack().isEmpty()) {
        	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
            currentComponent.setCPE(cpeEntry);
        }
        
        //Add version
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntry(cpeEntry);
        }
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.visitors.ArcdSymtabVisitor#visit(ASTArcConnector node)
     */
	@Override
	public void visit(ASTArcConnector node) {
		super.visit(node);
		
		if(node instanceof PrototypeASTSecArcConnector && !storage.getEntryStack().isEmpty() && ((PrototypeASTSecArcConnector) node).isEncrypted()) {
			setConnectorEncrypted(node.getTargets());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.ets.visitors.ArcdSymtabVisitor#visit(mc.umlp.arcd._ast.ASTArcSimpleConnector)
	 */
	@Override
	public void visit(ASTArcSimpleConnector node) {
		super.visit(node);
		
		if(node instanceof PrototypeASTSecArcSimpleConnector && !storage.getEntryStack().isEmpty() && ((PrototypeASTSecArcSimpleConnector) node).isEncrypted()) {
			setConnectorEncrypted(node.getTargets());
		}
	}
	
	private void setConnectorEncrypted(ASTQualifiedNameList targets) {
		SecComponentEntry comp = (SecComponentEntry) storage.getEntryStack().peek();
		SecConnectorEntry connectorEntry = null;
		String name = "";
		for(ASTQualifiedName target : targets) {
			name = NameHelper.dotSeparatedStringFromList(target.getParts());
			if(comp.getConnector(name).isPresent()) {
				connectorEntry = (SecConnectorEntry) comp.getConnector(name).get();
				connectorEntry.setEncryption(true);
			}
		}
	}
	
	/* (non-Javadoc)
     * @see mc.umlp.arcd.ets.visitors.ArcdSymtabVisitor#visit(ASTArcPort node)
     */
	@Override
	public void visit(ASTArcPort node) {
		super.visit(node);
		
		if(node instanceof PrototypeASTSecArcPort && !storage.getEntryStack().isEmpty() && ((PrototypeASTSecArcPort) node).isCritical()) {
			SecComponentEntry comp = (SecComponentEntry) storage.getEntryStack().peek();
			Optional<PortEntry> port = comp.getPort(node.printName());
			if(port.isPresent()) {
				((SecPortEntry) port.get()).setCritical(true);
			}
		}
		if(node instanceof PrototypeASTSecArcPort && !storage.getEntryStack().isEmpty() && ((PrototypeASTSecArcPort) node).getSecArcFilter() != null) {
			ASTSecArcFilter filterNode = ((PrototypeASTSecArcPort) node).getSecArcFilter();
			FilterEntry filterEntry = createFilterEntry(filterNode);
			SecComponentEntry comp = (SecComponentEntry) storage.getEntryStack().peek();
			Optional<PortEntry> port = comp.getPort(node.printName());
			if(port.isPresent()) {
				((SecPortEntry) port.get()).setFilter(filterEntry);
			}
			//Add filter 
	        for (SymbolTable table : getAllExportedTables(filterNode)) {
	            table.addEntry(filterEntry);
	        }
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arc.ets.visitors.MontiArcSymtabVisitor#visit(mc.umlp.arcd._ast.ASTArcComponent)
	 */
	@Override
	public void visit(ASTArcComponent node) {
		super.visit(node);
		
		if(node instanceof PrototypeASTSecArcFilterComponent && !storage.getEntryStack().isEmpty() && ((PrototypeASTSecArcFilterComponent) node).getSecArcFilter() != null) {
			ASTSecArcFilter filterNode = ((PrototypeASTSecArcFilterComponent) node).getSecArcFilter();
			FilterEntry filterEntry = createFilterEntry(filterNode);
			SecComponentEntry comp = (SecComponentEntry) storage.getEntryStack().peek();
			comp.setFilter(filterEntry);
			//Add filter 
	        for (SymbolTable table : getAllExportedTables(filterNode)) {
	            table.addEntry(filterEntry);
	        }
		}
	}
	
	/**
	 * Visits trustlevel relation
	 * @param node node to visit
	 */
	public void visit(ASTSecArcTrustlevelRelation node) {
		TrustlevelRelationEntry trustlevelRelationEntry = ((SecEntryFactory) this.getArcdFactory()).createTrustlevelRelation();
		trustlevelRelationEntry.setEntryState(STEntryState.FULL, trustlevelRelationEntry);
		trustlevelRelationEntry.setEqual(node.isEQUALS());
		if(node.isGT()) {
			trustlevelRelationEntry.setComponentWithHigherTrustlevel(NameHelper.dotSeparatedStringFromList(node.getClient().getParts()));
			trustlevelRelationEntry.setComponentWithLowerTrustlevel(NameHelper.dotSeparatedStringFromList(node.getServer().getParts()));
		} else if(node.isLT() || node.isEQUALS()) {
			trustlevelRelationEntry.setComponentWithHigherTrustlevel(NameHelper.dotSeparatedStringFromList(node.getServer().getParts()));
			trustlevelRelationEntry.setComponentWithLowerTrustlevel(NameHelper.dotSeparatedStringFromList(node.getClient().getParts()));
		}
		trustlevelRelationEntry.setNode(node);
        
        NameSpace ns = getNameSpaceFor(node);
        
        storage.addEntry(trustlevelRelationEntry, ns, node.get_SourcePositionStart());
        
        //Add to currentComponent
        if (!storage.getEntryStack().isEmpty()) {
        	SecComponentEntry currentComponent = (SecComponentEntry) storage.getEntryStack().peek();
            currentComponent.addTrustlevelRelation(trustlevelRelationEntry);
        }
        
        //Add version
        for (SymbolTable table : getAllExportedTables(node)) {
            table.addEntry(trustlevelRelationEntry);
        }
	}

}
