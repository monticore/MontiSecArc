/* (c) https://github.com/MontiCore/monticore */
package secarc.prettyprint;

import secarc._ast.ASTMCCompilationUnit;
import secarc._ast.ASTSecArcCPE;
import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcConnector;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcFilterComponent;
import secarc._ast.ASTSecArcFilterSubComponent;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcPEP;
import secarc._ast.ASTSecArcPort;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcRoleInterface;
import secarc._ast.ASTSecArcSimpleConnector;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc._ast.ASTSecMontiArcAutoConnect;
import mc.ast.ASTNode;
import mc.ast.AbstractVisitor;
import mc.ast.ConcretePrettyPrinter;
import mc.ast.PrettyPrinter;
import mc.helper.IndentPrinter;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcComponentBody;
import mc.umlp.arcd._ast.ASTArcComponentHead;
import mc.umlp.arcd._ast.ASTArcComponentImplementation;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcElement;
import mc.umlp.arcd._ast.ASTArcElementList;
import mc.umlp.arcd._ast.ASTArcInterface;
import mc.umlp.arcd._ast.ASTArcParameter;
import mc.umlp.arcd._ast.ASTArcPort;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import mc.umlp.arcd._ast.ASTArcSubComponent;
import mc.umlp.arcd._ast.ASTArcSubComponentInstance;

/**
 * 
 * Concrete pretty printer for MontiSecArc.
 *
 *
 */
public class MontiSecArcConcretePrettyPrinter extends ConcretePrettyPrinter {
	
	private static final Class<?>[] RESPONSIBLE_CLASSES = new Class[] { 
        ASTMCCompilationUnit.class,
        ASTArcComponent.class, ASTArcComponentHead.class,
        ASTArcComponentBody.class, ASTArcElement.class, ASTArcElementList.class, 
        ASTArcSubComponent.class,
        ASTArcSubComponentInstance.class, ASTArcConnector.class,
        ASTArcInterface.class, ASTArcPort.class, ASTArcSimpleConnector.class,
        ASTArcParameter.class, ASTArcComponentImplementation.class,
        ASTSecArcIdentity.class, ASTSecArcTrustlevelRelation.class,
        ASTSecArcConfiguration.class, ASTSecArcCPE.class, ASTSecArcFilter.class,
        ASTSecArcPEP.class, ASTSecArcRole.class, ASTSecArcRefRole.class,
        ASTSecArcRoleInterface.class, ASTSecArcFilterComponent.class, ASTSecArcConnector.class,
        ASTSecArcSimpleConnector.class, ASTSecMontiArcAutoConnect.class, ASTSecArcFilterSubComponent.class,
        ASTSecArcPort.class,
    };

	@Override
	public Class<?>[] getResponsibleClasses() {
		return RESPONSIBLE_CLASSES;
	}

	@Override
	public void prettyPrint(ASTNode a, IndentPrinter printer) {
		PrettyPrinter parent = this.getPrettyPrinter();
		
		// Run visitor
        AbstractVisitor.run(new MontiSecArcPrettyPrinterConcreteVisitor(parent,
                printer), a);
	}

}
