/* (c) https://github.com/MontiCore/monticore */
package secarc.prettyprint;

import secarc._tool.MontiSecArcRoot;
import mc.DSLWorkflow;

public class MontiSecArcInterfacePrinterWorkflow extends DSLWorkflow<MontiSecArcRoot> {

	public MontiSecArcInterfacePrinterWorkflow() {
		super(MontiSecArcRoot.class);
	}

	@Override
	public void run(MontiSecArcRoot dslRoot) {
		//TODO
	}

}
