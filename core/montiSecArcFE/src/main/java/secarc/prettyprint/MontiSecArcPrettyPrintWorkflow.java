/* (c) https://github.com/MontiCore/monticore */
package secarc.prettyprint;

import java.io.File;

import secarc._tool.MontiSecArcRoot;
import mc.DSLWorkflow;
import mc.ast.PrettyPrinter;
import mc.helper.GeneratedFile;
import mc.helper.GeneratedFile.WriteTime;
import mc.javadsl.prettyprint.JavaDSLConcretePrettyPrinter;
import mc.umlp.arcd._ast.ASTMCCompilationUnit;
import mc.umlp.ocl.prettyprint.OCLConcretePrettyPrinter;

/**
 * 
 * Concrete pretty printer workflow for MontiSecArc.
 *
 *
 */
public class MontiSecArcPrettyPrintWorkflow extends DSLWorkflow<MontiSecArcRoot>{

	/**
     * Creates a new {@link MontiSecArcPrettyPrintWorkflow} for the responsible root class.
     * 
     * @param responsibleClass root class to use
     */
	public MontiSecArcPrettyPrintWorkflow(
			Class<? extends MontiSecArcRoot> responsibleClass) {
		super(responsibleClass);
	}

	@Override
	public void run(MontiSecArcRoot dslroot) {
		ASTMCCompilationUnit ast = dslroot.getAst();
        
        // Get name for output file
        String name = new File(dslroot.getFilename()).getName();
        
        // Create file
        GeneratedFile f = new GeneratedFile("", name, WriteTime.IMMEDIATELY);
        
        // Register it for writing
        dslroot.addFile(f);
        
        // init and run the acutal visitor
        PrettyPrinter p = new PrettyPrinter();
        p.addConcretePrettyPrinter(new MontiSecArcConcretePrettyPrinter());
//        p.addConcretePrettyPrinter(new JavaDSLConcretePrettyPrinter());
//        p.addConcretePrettyPrinter(new OCLConcretePrettyPrinter());
        
        p.prettyPrint(ast, f.getContent());
	}

}
