/* (c) https://github.com/MontiCore/monticore */
package secarc.prettyprint;

import de.monticore.lang.common._ast.ASTCVExpression;
import secarc._ast.ASTSecArcConfiguration;
import secarc._ast.ASTSecArcConnector;
import secarc._ast.ASTSecArcFilter;
import secarc._ast.ASTSecArcFilterComponent;
import secarc._ast.ASTSecArcFilterSubComponent;
import secarc._ast.ASTSecArcIdentity;
import secarc._ast.ASTSecArcPEP;
import secarc._ast.ASTSecArcPort;
import secarc._ast.ASTSecArcRefRole;
import secarc._ast.ASTSecArcRole;
import secarc._ast.ASTSecArcRoleInterface;
import secarc._ast.ASTSecArcSimpleConnector;
import secarc._ast.ASTSecArcTrustLevel;
import secarc._ast.ASTSecArcTrustlevelRelation;
import secarc._ast.ASTSecArcCPE;
import secarc._ast.ASTSecMontiArcAutoConnect;
import mc.ast.PrettyPrinter;
import mc.helper.IndentPrinter;
import mc.types._ast.ASTQualifiedName;
import mc.umlp.arc.prettyprint.MontiArcPrettyPrinterConcreteVisitor;
import mc.umlp.arcd._ast.ASTArcSubComponentInstance;

/**
 * 
 * Concrete pretty printer visitor for MontiSecArc.
 *
 *
 */
public class MontiSecArcPrettyPrinterConcreteVisitor extends MontiArcPrettyPrinterConcreteVisitor {

	public MontiSecArcPrettyPrinterConcreteVisitor(PrettyPrinter parent,
			IndentPrinter printer) {
		super(parent, printer);
	}

	public void ownVisit(ASTSecArcTrustLevel a) {
        printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print("trustlevel ");
        if(a.isPLUS()) {
        	this.printer.print("+");
        } else {
        	this.printer.print("-");
        }
        this.printer.print(a.getValue().getValue());
        this.printer.print(" ");
        this.printer.print(a.getReason());
        this.printer.println(";");
        printer.println();
        printComment(a.get_PostComments());
    }
	
	public void ownVisit(ASTSecArcIdentity a) {
        printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print("identity ");
        if(a.isStrong()) {
        	this.printer.print("strong ");
        } else {
        	this.printer.print("weak ");
        }
        this.startVisit(a.getSource());
        this.printer.print(" -> ");
        String sep = "";
        for (ASTQualifiedName name : a.getTargets()) {
            this.printer.print(sep);
            this.startVisit(name);
            sep = ", ";
        }
        this.printer.println(";");
        printer.println();
        printComment(a.get_PostComments());
    }
	
	public void ownVisit(ASTSecArcConnector a) {
        printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print("connect ");
        if(a.isEncrypted()) {
        	this.printer.print("encrypted ");
        } else {
        	this.printer.print("unencrypted ");
        }        
        this.startVisit(a.getSource());
        this.printer.print(" -> ");
        String sep = "";
        for (ASTQualifiedName name : a.getTargets()) {
            this.printer.print(sep);
            this.startVisit(name);
            sep = ", ";
        }
        this.printer.println(";");
        printer.println();
        printComment(a.get_PostComments());
    }
	
	public void ownVisit(ASTSecArcSimpleConnector sc) {
        visitExternal(sc.getStereotype());
        if(sc.isEncrypted()) {
        	this.printer.print("encrypted ");
        } else {
        	this.printer.print("unencrypted ");
        }   
        visitExternal(sc.getSource());
        printer.print(" -> ");
        String separator = "";
        for (ASTQualifiedName target : sc.getTargets()) {
            printer.print(separator);
            separator = ", ";
            visitExternal(target);
        }
        
    }
	
	public void ownVisit(ASTSecMontiArcAutoConnect completion) {
        printer.print("autoconnect ");
        if (completion.getStereotype() != null) {
            getVisitor().startVisit(completion.getStereotype());
            printer.print(" ");
        }
        if(completion.isEncrypted()) {
        	this.printer.print("encrypted ");
        } else {
        	this.printer.print("unencrypted ");
        }   
        if (completion.isPort()) {
            printer.print("port");
        }
        else if (completion.isType()) {
            printer.print("type");
        }
        else if (completion.isOff()) {
            printer.print("off");
        }
        printer.println(";");
    }
	
	public void ownVisit(ASTSecArcFilterComponent component) {
        printComment(component.get_PreComments());
        if (component.getStereotype() != null) {
            this.startVisit(component.getStereotype());
            this.printer.print(" ");
        }
        
        this.printer.print("component " + component.getName());
        if(component.getSecArcFilter() != null) {
        	this.printFilter((component).getSecArcFilter());
        }
        if (component.getInstanceName() != null) {
            printer.print(" ");
            printer.print(component.getInstanceName());
        }
        getVisitor().startVisit(component.getHead());
        getVisitor().startVisit(component.getBody());
        printComment(component.get_PostComments());
    }
	
	public void ownVisit(ASTSecArcFilterSubComponent ref) {
        if (ref.getStereotype() != null) {
            visitExternal(ref.getStereotype());
            printer.print(" ");
        }
        printer.print("component ");
        if(ref.getSecArcFilter() != null) {
        	this.printFilter(ref.getSecArcFilter());
        }
        getVisitor().startVisit(ref.getType());
        if (ref.getArguments() != null && !ref.getArguments().isEmpty()) {
            printer.print("(");
            String sep = "";
            for (ASTCVExpression arg : ref.getArguments()) {
                printer.print(sep);
                sep = ", ";
                getVisitor().startVisit(arg);
            }
            printer.print(")");
        }
        if (ref.getInstances() != null && ref.getInstances().size() > 0) {
            String sep = " ";
            for (ASTArcSubComponentInstance name : ref.getInstances()) {
                printer.print(sep);
                sep = ", ";
                getVisitor().startVisit(name);
            }
        }
        printer.println(";");
    }
	
	public void ownVisit(ASTSecArcPort a) {
        printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            getVisitor().startVisit(a.getStereotype());
        }
        if(a.isCritical()) {
        	this.printer.print("critcal ");
        }
        if(a.getSecArcFilter() != null) {
        	this.printFilter(a.getSecArcFilter());
        }
        if (a.isIncoming()) {
            this.printer.print("in ");
        }
        else if (a.isOutgoing()) {
            this.printer.print("out ");
        }
        else {
            this.printer.print("UNDEF_DIRECTION ");
        }
        this.startVisit(a.getType());
        
        if (a.getName() != null && !a.getName().isEmpty()) {
            printer.print(" " + a.getName());
        }
        printComment(a.get_PostComments());
    }
	
	public void ownVisit(ASTSecArcPEP a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            getVisitor().startVisit(a.getStereotype());
        }
        printer.print("accesscontrol ");
        if(a.isOn()) {
        	printer.print("on");
        } else {
        	printer.print("off");
        }
        printer.println(";");        
	}
	
	public void ownVisit(ASTSecArcCPE a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            getVisitor().startVisit(a.getStereotype());
        }
        printer.print("version \"");
        printer.print(a.getNameCPE());
        printer.println("\";");   
	}
	
	public void ownVisit(ASTSecArcConfiguration a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            getVisitor().startVisit(a.getStereotype());
        }
        printer.print("version \"");
        printer.print(a.getName());
        printer.println(";");     
	}
	
	public void ownVisit(ASTSecArcTrustlevelRelation a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print("trustlevelrelation ");
        this.startVisit(a.getClient());   
        if(a.isEQUALS()) {
        	this.printer.print(" = ");
        } else if(a.isGT()) {
        	this.printer.print(" > ");
        } else {
        	this.printer.print(" < ");
        }
        this.startVisit(a.getServer());
        this.printer.println(";");
        printer.println();
        printComment(a.get_PostComments());
	}
	
	public void ownVisit(ASTSecArcRole a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print("access ");
        for(String name : a.getRoles()) {
        	this.printer.print(name);
        	this.printer.print(", ");
        }
        this.printer.print(";");
        printer.println();
        printComment(a.get_PostComments());
	}
	
	public void ownVisit(ASTSecArcRoleInterface a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print("access ");
        for(ASTSecArcRefRole refRole : a.getSecArcRefRole()) {
        	printRefRole(refRole);
        }
        printer.println();
        printComment(a.get_PostComments());
	}
	
	protected void printRefRole(ASTSecArcRefRole a) {
		printComment(a.get_PreComments());
        if (a.getStereotype() != null) {
            visitExternal(a.getStereotype());
            printer.print(" ");
        }
        this.printer.print(a.getPortName());
        this.printer.print(" ");
        for(String name : a.getRoles()) {
        	this.printer.print(name);
        	this.printer.print(", ");
        }
        this.printer.print(", ");
        printer.println();
        printComment(a.get_PostComments());
	}
	
	protected void printFilter(ASTSecArcFilter filter) {
		this.printer.print("(filter ");
		this.printer.print(filter.getName());
		this.printer.print(") ");
	}
	
}
