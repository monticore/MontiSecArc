/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import mc.ProblemReport;

import org.junit.Test;

import secarc.error.MontiSecArcErrorCodes;


/**
 * Test for @{MyLanguageTool}.
 * 
 * 
 */
public class MontiSecArcCoCoTest extends TestWithSymbolTableSec<MontiSecArcTestTool> {

	
	public MontiSecArcCoCoTest() {
		super(MontiSecArcTestTool.class, new String[] { "gen/conv" });
	}

	/**
	 * Test for AccessControl CoCos
	 */
	@Test
	public void testAccessControl() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/accesscontrol" });
		tool.init();
		
		assertTrue(tool.run());
		
		List<MontiSecArcErrorCodes> errorCodes = new ArrayList<MontiSecArcErrorCodes>();
		errorCodes.add(MontiSecArcErrorCodes.RolesDefinedInSubcomponentsPEP);
		errorCodes.add(MontiSecArcErrorCodes.UniquePEP);
		errorCodes.add(MontiSecArcErrorCodes.RoleWithIdentity);
		errorCodes.add(MontiSecArcErrorCodes.AccessControlOff);

        assertEquals(4, handler.getErrors().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        
	}
	
	/**
	 * Test for Trustlevel CoCos 
	 */
	@Test
	public void testTrustlevel() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/trustlevel" });
		tool.init();
		
		assertTrue(tool.run());
		
		List<MontiSecArcErrorCodes> errorCodes = new ArrayList<MontiSecArcErrorCodes>();
		errorCodes.add(MontiSecArcErrorCodes.UniqueTrustlevel);

        assertEquals(1, handler.getErrors().size());
        for(ProblemReport error :  handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for Identity link Cocos
	 */
	@Test
	public void testIdentity() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/identity/" });
		tool.init();
		
		assertTrue(tool.run());
		
		List<MontiSecArcErrorCodes> errorCodes = new ArrayList<MontiSecArcErrorCodes>();
		errorCodes.add(MontiSecArcErrorCodes.ComponentExistence);
		errorCodes.add(MontiSecArcErrorCodes.UniqueDefinitionWithFullQualifiedName);
		errorCodes.add(MontiSecArcErrorCodes.DifferentSourceTarget);
		errorCodes.add(MontiSecArcErrorCodes.IdentityCycleCheck);

        assertEquals(10, handler.getErrors().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
	/**
	 * Test for Connector CoCos
	 */
	@Test
	public void testConnector() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/connector" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(0, handler.getErrors().size());
	}
	
	/**
	 * Test for AutoConnect CoCos
	 */
	@Test
	public void testAutoConnect() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/autoconnect" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(0, handler.getErrors().size());
	}
	
	/**
	 * Test for filter component CoCos
	 */
	@Test
	public void testFilterComponent() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/filtercomponent" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(1, handler.getWarnings().size());
        assertEquals(MontiSecArcErrorCodes.NamingConventions, handler.getWarnings().get(0).getErrorcode());
	}
	
	/**
	 * Test for filter port CoCos
	 */
	@Test
	public void testFilterPort() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/filterport" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(0, handler.getErrors().size());
	}
	
	/**
	 * Test for critical port CoCos
	 */
	@Test
	public void testCriticalPort() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/criticalport" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(0, handler.getErrors().size());
	}
	
	/**
	 * Test for role CoCos
	 */
	@Test
	public void testRole() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/role/" });
		tool.init();
		
		assertTrue(tool.run());
		
		List<MontiSecArcErrorCodes> errorCodes = new ArrayList<MontiSecArcErrorCodes>();
		errorCodes.add(MontiSecArcErrorCodes.UniqueComponentRole);
		errorCodes.add(MontiSecArcErrorCodes.RolesDefinedInSubcomponentsPEP);
		errorCodes.add(MontiSecArcErrorCodes.PortExistence);
		errorCodes.add(MontiSecArcErrorCodes.UniqueRoleDefinitionForComponent);
		errorCodes.add(MontiSecArcErrorCodes.UniqueRoleDefinitionForPort);
		errorCodes.add(MontiSecArcErrorCodes.RoleWithIdentity);
		
        assertEquals(7, handler.getErrors().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
        assertEquals(2, handler.getWarnings().size());
	}
	
	/**
	 * Test for cpe CoCos
	 */
	@Test
	public void testCPE() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/cpe" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(1, handler.getErrors().size());
        assertEquals(MontiSecArcErrorCodes.UniqueCPE, handler.getErrors().get(0).getErrorcode());
	}
	
	/**
	 * Test for configuration CoCos
	 */
	@Test 
	public void testConfiguration() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/configuration" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(1, handler.getErrors().size());
        assertEquals(MontiSecArcErrorCodes.UniqueConfiguration, handler.getErrors().get(0).getErrorcode());
        assertEquals(1, handler.getWarnings().size());
	}
	
	/**
	 * Test for component CoCos
	 */
	@Test
	public void testComponent() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/component" });
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(2, handler.getErrors().size());
        assertEquals(MontiSecArcErrorCodes.CompoletnessThirdPartyComponent, handler.getErrors().get(0).getErrorcode());
	}
	
	/**
	 * Test for trustlevel relation
	 */
	@Test
	public void testTrustlevelRelation() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/coco/trustlevelrelation/" });
		tool.init();
		
		assertTrue(tool.run());
		List<MontiSecArcErrorCodes> errorCodes = new ArrayList<MontiSecArcErrorCodes>();
		errorCodes.add(MontiSecArcErrorCodes.CorrectRelationBetweenTrustlevel);
		errorCodes.add(MontiSecArcErrorCodes.ComponentExistence);
		errorCodes.add(MontiSecArcErrorCodes.UniqueDefinitionWithFullQualifiedName);
		
        assertEquals(6, handler.getErrors().size());
        for(ProblemReport error : handler.getErrors()) {
        	assertTrue(errorCodes.contains(error.getErrorcode()));
        }
	}
	
}
