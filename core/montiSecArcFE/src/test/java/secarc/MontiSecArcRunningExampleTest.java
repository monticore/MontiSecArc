/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mc.dsltool.AmbigousException;
import mc.dsltool.CircluarDependencyException;

public class MontiSecArcRunningExampleTest extends TestWithSymbolTableSec<MontiSecArcTestTool>  {

	public MontiSecArcRunningExampleTest() {
		super(MontiSecArcTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Test for Running Example in MontiArc
	 * @throws AmbigousException 
	 * @throws CircluarDependencyException 
	 */
	@Test
	public void testRunningExample() throws CircluarDependencyException, AmbigousException {
		MontiSecArcTool tool = createTestToolWithAdditionalParameters(new String[] {"src/test/resources/secarc/cds/"}, new String[] {"src/test/resources"});
		tool.init();
		
		assertTrue(tool.run());
		
	}
	
	/**
	 * Test for Running Example in MontiSecArc
	 * @throws AmbigousException 
	 * @throws CircluarDependencyException 
	 */
	@Test
	public void testSecRunningExample() throws CircluarDependencyException, AmbigousException {
		MontiSecArcTool tool = createTestToolWithAdditionalParameters(new String[] {"src/test/resources/secarc/seccds/CashDeskSystem.secarc"}, new String[] {"src/test/resources/"});
		tool.init();
		
		assertTrue(tool.run());
		
	}
	
	/**
	 * Test with only one path for Running Example in MontiSecArc 
	 * @throws AmbigousException 
	 * @throws CircluarDependencyException 
	 */
	@Test
	public void testSecRunningExamplePath() throws CircluarDependencyException, AmbigousException {
		MontiSecArcTool tool = createTestToolWithAdditionalParameters(new String[] {"src/test/resources/secarc/seccdsE/CashDeskSystem.secarc"}, new String[] {"src/test/resources/"});
		tool.init();
		
		assertTrue(tool.run());
		
	}
		
}
