/* (c) https://github.com/MontiCore/monticore */
package secarc;

import mc.DSLRoot;
import mc.test.helpers.TestErrorHandler;


public class MontiSecArcTestTool extends MontiSecArcTool {

	public MontiSecArcTestTool(String[] args) {
		super(args);
		this.init();
	}
    
    @Override
    public void init() {
        super.init();
        this.addErrorHandler(TestErrorHandler.getInstance());
    }

    /**
     * @param string
     * @return
     */
    public DSLRoot<?> getRootByName(String filename) {
        return super.getModelloader().getRootByFileName(filename);
    }

}
