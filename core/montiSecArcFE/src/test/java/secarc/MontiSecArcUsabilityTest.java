/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mc.umlp.arc.exceptions.MontiArcErrorCodes;
import mc.umlp.arcd.TestWithSymtab;

/**
 * Test for @{MyLanguageTool}.
 * 
 * Usability examples
 * 
 * 
 */
public class MontiSecArcUsabilityTest extends TestWithSymtab<MontiSecArcTestTool>{

	public MontiSecArcUsabilityTest() {
		super(MontiSecArcTestTool.class, new String[] { "gen/conv" });
	}
	
	/**
	 * Test for usability examples
	 */
	@Test
	public void testUsability() {
		MontiSecArcTool tool = createTestToolWithoutJava(new String[] { "src/test/resources/secarc/usability/DataStorage.secarc",
				"src/test/resources/secarc/usability/Usability_Roles.secarc"});
		tool.init();
		
		assertTrue(tool.run());

        assertEquals(2, handler.getWarnings().size());
        assertEquals(MontiArcErrorCodes.UnusedPort, handler.getWarnings().get(0).getErrorcode());
        assertEquals(MontiArcErrorCodes.UnusedPort, handler.getWarnings().get(1).getErrorcode());
	}

}
