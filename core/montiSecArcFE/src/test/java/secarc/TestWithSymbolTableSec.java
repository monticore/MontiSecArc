/* (c) https://github.com/MontiCore/monticore */
package secarc;

import static mc.umlp.arcd.ToolConstants.ARG_ANALYSIS;
import static mc.umlp.arcd.ToolConstants.ARG_CONF;
import static mc.umlp.arcd.ToolConstants.ARG_MODELPATH;
import static mc.umlp.arcd.ToolConstants.ARG_OUT;
import static mc.umlp.arcd.ToolConstants.ARG_SYMTABDIR;
import static mc.umlp.arcd.ToolConstants.ARG_SYNTHESIS;
import static mc.umlp.arcd.ToolConstants.WF_CREATE_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_INIT_CHECK;
import static mc.umlp.arcd.ToolConstants.WF_INIT_SYMTAB;
import static mc.umlp.arcd.ToolConstants.WF_PARSE;
import static mc.umlp.arcd.ToolConstants.WF_PRE_CHECK_TRAFO;
import static mc.umlp.arcd.ToolConstants.WF_RUN_CHECK;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import interfaces2.language.ETSTool;
import mc.MCG;
import mc.Parameters;
import mc.ProblemReport.Type;
import mc.umlp.arcd.TestWithSymtab;

public class TestWithSymbolTableSec <T extends ETSTool> extends
TestWithSymtab<T> {
	
	public TestWithSymbolTableSec(final Class<T> toolClass, final String[] symbolFolders) {
		super(toolClass, symbolFolders);
	}
	
	/*
	 * (non-Javadoc)
	 * @see mc.umlp.arcd.TestWithSymtab#createTestToolWithoutJava(java.lang.String[], java.lang.String[])
	 */
	@Override
    protected T createTestToolWithoutJava(String[] files, String... additionalModelPath) {
		String[] args = new String[] {
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK };
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String file : files) {
            argsAsList.add(file);
        }
        
        T tool = null;
        try {
            Constructor<T> constructor = toolClass.getConstructor(String[].class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args);
            tool.setErrorLevel(Type.WARNING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }
	
	/**
     * Creates a DSL Tool from the type T that is configured to build the symbol
     * table for the given files being able to load models and symbols from the
     * src folder, the java bootstrap folder and the given additional model
     * paths.
     *
     * @param files files to process
     * @param additionalModelPath to expand the model path
     * @param additionalParameters add additional parameters here to configure the tool
     *
     * @return the created dsl tool
     */
	@Override
    protected T createTestToolWithAdditionalParameters(String[] files, String[] additionalModelPath, String... additionalParameters) {
        String[] args = new String[] { 
                ARG_MODELPATH, "src/main/java",
                ARG_CONF, "mc-test.cfg",
                ARG_OUT, OUTPUT_TEST_FOLDER,
                ARG_SYMTABDIR, SYMTAB_FOLDER,
                ARG_ANALYSIS, Parameters.ALL, WF_PARSE,
                ARG_ANALYSIS, "javadsl", "setname",
                ARG_ANALYSIS, "javadsl", "addImports",
                ARG_ANALYSIS, Parameters.ALL, WF_INIT_SYMTAB,
                ARG_ANALYSIS, Parameters.ALL, WF_CREATE_SYMTAB,
                ARG_SYNTHESIS, Parameters.ALL, WF_INIT_CHECK,
                ARG_SYNTHESIS, "secarc", WF_PRE_CHECK_TRAFO,
                ARG_SYNTHESIS, Parameters.ALL, WF_RUN_CHECK };
        List<String> argsAsList = new LinkedList<String>();
        argsAsList.addAll(Arrays.asList(args));
        for (String file : files) {
            argsAsList.add(file);
        }
        
        for (String mp : additionalModelPath) {
            argsAsList.add(ARG_MODELPATH);
            argsAsList.add(mp);
        }
        for (String param : additionalParameters) {
            argsAsList.add(param);
        }
        

        T tool = null;
        try {
            Constructor<T> constructor = toolClass
                    .getConstructor(String[].class);
            args = argsAsList.toArray(new String[argsAsList.size()]);
            tool = constructor.newInstance((Object) args);
            tool.addErrorHandler(handler);
            tool.setErrorLevel(Type.WARNING);
            MCG.addErrorHandler(handler);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return tool;
    }

}
