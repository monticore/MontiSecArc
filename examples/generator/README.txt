run mvn install to compile the example

generator-example:
the testcase executes the generator on the model supplied within the generator-example project

rte-example:
example for static code used within the use-example project

use-example:
uses the generator and executes it via maven on the model supplied within the use-example project
