/* (c) https://github.com/MontiCore/monticore */
package connectionBasic.client;

import hash.PasswordHash;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.apache.shiro.authc.UsernamePasswordToken;

 
public abstract class Client {
	     
  protected Socket openConnection(String in_ip, int in_port) 
	throws IOException{
	  //Verbindung herstellen und Socket in connectionList einfügen
	  Socket mySocket = new Socket(in_ip, in_port);
	  ObjectOutputStream out = new ObjectOutputStream(mySocket.getOutputStream());
	  UsernamePasswordToken token = new UsernamePasswordToken("defaultLogin", "defaultPassword");
	  out.writeObject(token);
	  out.flush();
		
	  ObjectInputStream in  = new ObjectInputStream(mySocket.getInputStream());
	  Boolean successfulLogin = in.readBoolean();
	  if (successfulLogin){
		  return mySocket;
	  } else {
		  throw new IOException();
	  }
  }
  
  protected Socket openConnection(String in_ip, int in_port, String inName, String inPassword) 
	throws IOException{
	  long startTime = System.currentTimeMillis();
	  //Verbindung herstellen
	  Socket mySocket = new Socket(in_ip, in_port);
		
	  //Credentials übertragen
	  try {
		  String hashed_pw = PasswordHash.createHash(inPassword, PasswordHash.toHex(inName.getBytes()));
		  //System.out.println(inPassword + "->" +hashed_pw);
		  ObjectOutputStream out = new ObjectOutputStream(mySocket.getOutputStream());
		  UsernamePasswordToken token = new UsernamePasswordToken(inName, hashed_pw);
		  out.writeObject(token);
		  out.flush();
	  } catch (NoSuchAlgorithmException e) {
		  e.printStackTrace();
	  } catch (InvalidKeySpecException e) {
		  e.printStackTrace();
	  }
	  
	  ObjectInputStream in  = new ObjectInputStream(mySocket.getInputStream());
	  Boolean successfulLogin = in.readBoolean();
	  if (successfulLogin){
		  long stopTime = System.currentTimeMillis();
		  long elapsedTime = stopTime - startTime;
		  System.out.println(elapsedTime);
		  return mySocket;
	  } else {
		  //System.out.println("Wrong Credentials!");
		  long stopTime = System.currentTimeMillis();
		  long elapsedTime = stopTime - startTime;
		  System.out.println(elapsedTime);
		  throw new IOException();
	  }
  }
  
  public boolean closeConnection(Socket closingSocket){
	  try{
		  if(!closingSocket.isClosed()){
			  closingSocket.close();
	//		  System.out.println("Connection Closed.");
		  } else {
			  System.out.println("Connection was already closed.");
		  }
		  return true;
	  } catch (IOException e) {
		  e.printStackTrace();
		  return false;
	  }
  }
}

