/* (c) https://github.com/MontiCore/monticore */
package connectionBasic.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;


//Thread bzw. Runnable zur Realisierung der Client-Anforderungen
class Handler implements Runnable {  //oder 'extends Thread'
private final Socket client;
private final PortHandler portHandler;
private final PortMapInterface pmi;

Handler(Socket client, PortHandler ph, PortMapInterface in_pmi) { //Server/Client-Socket
  this.client = client;
  this.portHandler = ph;
  this.pmi = in_pmi;
}

public void run(){
    Subject currentUser = SecurityUtils.getSubject();
    try{
  	  InputStream  clientInStream  = client.getInputStream();
  	  OutputStream clientOutStream = client.getOutputStream();
  	  //Nutzer für diesen Thread authentifizieren
  	  this.checkCredentials(clientInStream, clientOutStream, currentUser, client.getLocalPort());
  	            
  	  if(currentUser.isAuthenticated()){
 // 		  System.out.println("run handler");
          portHandler.handle(clientInStream, clientOutStream);
  		  currentUser.logout();
 // 		  System.out.println("user logged out");
  	  }
  	  clientInStream.close();
  	  clientOutStream.close();
		  client.close();
    } catch(IOException e){
  	  
    }
}

public void checkCredentials(InputStream income, OutputStream outcome, Subject currentUser, int port){
	  try {
		  if(!currentUser.isAuthenticated()){
			  ObjectInputStream in  = new ObjectInputStream(income);
			  // Anmeldedaten prüfen
			  UsernamePasswordToken token = (UsernamePasswordToken) in.readObject();
			  token.setRememberMe(true);
			  try{
				  currentUser.login(token);
			  } catch (UnknownAccountException uae) {
				  System.out.println("A user failed to log in!");
			  } catch (IncorrectCredentialsException ice) {
				  System.out.println("A user failed to log in!");
			  } catch (LockedAccountException lae) {
				  System.out.println("A user failed to log in!");
			  }	catch (AuthenticationException ae){
				  System.out.println("A user failed to log in!");
			  }
		  }
		  //Rollenüberprüfung
		  String portName = pmi.getName(port);
		  if(currentUser.isAuthenticated() && !currentUser.isPermitted(portName)){
			  currentUser.logout();
			  System.out.println("permission not given");
		  }

		  ObjectOutputStream out = new ObjectOutputStream(outcome);
		  boolean successfulLogin = false;
		  if(currentUser.isAuthenticated()){
			  successfulLogin = true;
			  System.out.println("Logged in! " +currentUser);
		  }
		  out.writeBoolean(successfulLogin);
		  out.flush();
	  } catch (IOException e) {
		  System.out.println("IOException, checkCredentials-run");
	  } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

}
