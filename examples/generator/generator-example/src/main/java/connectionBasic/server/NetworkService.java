/* (c) https://github.com/MontiCore/monticore */
package connectionBasic.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.shiro.subject.Subject;

//Thread bzw. Runnable zur Entgegennahme der Client-Anforderungen
class NetworkService implements Runnable { //oder extends Thread
	private final ServerSocket serverSocket;
	private final ExecutorService pool;
	private final PortHandler portHandler;
	private final PortMapInterface pmi;

	public NetworkService(ExecutorService pool, ServerSocket serverSocket, PortHandler ph, PortMapInterface in_pmi) {
		this.serverSocket = serverSocket; 
		this.pool = pool;
		this.portHandler = ph;
		this.pmi = in_pmi;
	}
	
	public void run() { // run the service
		try {
			while ( true ) {
				System.out.println(Thread.currentThread() + " waitForConnection");
				Socket cs = serverSocket.accept();							//warten auf Client-Anforderung
		    	Subject newUser = new Subject.Builder().buildSubject();		//neuen Nutzer erstellen
				pool.execute(newUser.associateWith(new Handler(cs, portHandler, pmi)));	//für diesen Nutzer den Handler aufrufen
			}
		} catch (IOException ex) {
			System.out.println("--- Interrupt NetworkService-run");
		}
		finally {
			System.out.println("--- Ende NetworkService(pool.shutdown)");
			pool.shutdown();  //keine Annahme von neuen Anforderungen
			try {
				//warte maximal 4 Sekunden auf Beendigung aller Anforderungen
				pool.awaitTermination(4L, TimeUnit.SECONDS);
				if ( !serverSocket.isClosed() ) {
					System.out.println("--- Ende NetworkService:ServerSocket close");
					serverSocket.close();
				}
			} catch ( IOException e ) { }
			catch ( InterruptedException ei ) { }
		}
	}
}
