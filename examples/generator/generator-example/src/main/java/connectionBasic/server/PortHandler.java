/* (c) https://github.com/MontiCore/monticore */
package connectionBasic.server;

import java.io.InputStream;
import java.io.OutputStream;

public interface PortHandler {
	  void handle(InputStream in, OutputStream out);
}
