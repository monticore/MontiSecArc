/* (c) https://github.com/MontiCore/monticore */
package connectionBasic.server;

public interface PortMapInterface {
	public int getPort(String PortName);
	public String getName(int portNumber);
}
