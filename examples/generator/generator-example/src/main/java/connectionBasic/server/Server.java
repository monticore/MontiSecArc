/* (c) https://github.com/MontiCore/monticore */
package connectionBasic.server;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.io.*; 
import java.net.*;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.util.Factory;

 
public abstract class Server {
	private Map<Integer, PortHandler> handlerList = new HashMap<Integer, PortHandler>();
	private Set<Integer> portList = new HashSet<Integer>();
	
	protected Set<Integer> listenPorts = new HashSet<Integer>();
	protected PortMapInterface pmi;
	
	public void start() throws IOException {
		if(!portList.equals(listenPorts)){
			System.out.println("Handler for Ports are missing!");
		} else {
			final ExecutorService pool            = Executors.newCachedThreadPool();
			final Set<ServerSocket> serverSockets = new HashSet<ServerSocket>();
			final Thread[] threads                = new Thread[portList.size()];

			//String finalPath = URI.create("/generator/src/main/shiro.ini").normalize().getPath();
			Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
			SecurityManager securityManager = factory.getInstance();
			SecurityUtils.setSecurityManager(securityManager);
    
			//für jeden Port einen Thread starten um Client Anfragen behandeln zu können
			int i=0;
			ServerSocket newSocket;
			for(Integer  port : portList){
				newSocket = new ServerSocket(port);
				serverSockets.add(newSocket);
			
				threads[i] = new Thread(new NetworkService(pool, newSocket, handlerList.get(port), pmi));
				System.out.println("Thread for Port " + port + " started!");
				threads[i].start();
				i++;
			}
			//
			//reagiert auf Strg+C, der Thread(Parameter) darf nicht gestartet sein
			Runtime.getRuntime().addShutdownHook(
				new Thread() {
					public void run() {
						System.out.println("Strg+C, pool.shutdown");
						pool.shutdown();  //keine Annahme von neuen Anforderungen
						try {
							//warte maximal 4 Sekunden auf Beendigung aller Anforderungen
							pool.awaitTermination(4L, TimeUnit.SECONDS);
							for(ServerSocket socket : serverSockets){
								if (!socket.isClosed()) {
									System.out.println("ServerSocket close");
									socket.close();
								}
							}
						} catch ( IOException e ) { }
						catch ( InterruptedException ei ) { }
					}
				}
			);
		}
	}
	
	//fügt den PortHandler für den entsprechenden Port dem Server hinzu
	//sollte der Port schon einen Handler haben, wird dieser überschrieben
	//wenn an den angegeben Port kein Handler erwartet wird, gibt die Methode false zurück
	public boolean addToHandlerlist(int port, PortHandler ph){
		if(listenPorts.contains(port)){
			handlerList.put(port, ph);
			portList.add(port);
			return true;
		}else{
			return false;
		}
	}
	
	//gibt alle Ports aus, die einen Handler erwaten
	public String getListenPorts(){
		return this.listenPorts.toString();
	}
	
	public boolean addPortMap(PortMapInterface in_pmi){
		pmi = in_pmi;
		return true;
	}
}
