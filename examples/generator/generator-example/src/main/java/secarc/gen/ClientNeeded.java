/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.ArrayList;
import java.util.List;

import mc.ast.ASTNode;
import mc.codegen.TemplateCalculator;
import mc.codegen.TemplateOperator;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import secarc._ast.ASTSecArcFilterComponent;

public class ClientNeeded extends TemplateCalculator<ASTNode> {

	@Override
	public boolean calc(ASTNode ast, TemplateOperator op) {
		boolean res = false;
		ASTSecArcFilterComponent c = (ASTSecArcFilterComponent) ast;
		List<ASTArcConnector> allConnections = new ArrayList<ASTArcConnector>();
		
		//in eigener Komponente nach ausgehenden Verbindungen suchen
		allConnections = c.getAllEncryptedConnectors();
		allConnections.addAll(c.getAllUnencryptedConnectors());
		for(ASTArcConnector connection : allConnections){
			if(!connection.getSource().toString().contains(".")){
				res = true;
			}
		}
		
		//in ober Komponente nach von "c" ausgehenden Verbindungen suchen
		if(c.getMainParent() != null){
			ASTSecArcFilterComponent parent = (ASTSecArcFilterComponent) c.getMainParent();
			allConnections = parent.getAllEncryptedConnectors();
			allConnections.addAll(parent.getAllUnencryptedConnectors());
			for(ASTArcConnector connection : allConnections){
				if(connection.getSource().toString().startsWith(c.getInstanceName()+".")){
					res = true;
				}
			}
			
			//in allen Komponenten auf der Ebene von "c" nach von "c" ausgehenden Verbindungen suchen
			for(ASTArcComponent child : parent.getInnerComponents()){
				ASTSecArcFilterComponent filterChild = (ASTSecArcFilterComponent) child;	
				allConnections = filterChild.getAllEncryptedConnectors();
				allConnections.addAll(filterChild.getAllUnencryptedConnectors());			
				for(ASTArcConnector connection : allConnections){
					if(connection.getSource().toString().startsWith(c.getInstanceName()+".")){
						res = true;
					}
				}				
			}
		}
		
		//in allen Subkomponenten nach von "c" ausgehenden Verbindungen suchen
		for(ASTArcComponent child : c.getInnerComponents()){
			ASTSecArcFilterComponent filterChild = (ASTSecArcFilterComponent) child;
			allConnections = filterChild.getAllEncryptedConnectors();
			allConnections.addAll(filterChild.getAllUnencryptedConnectors());		
			for(ASTArcConnector connection : allConnections){
				if(connection.getSource().toString().startsWith(c.getInstanceName()+".")){
					res = true;
				}
			}
		}
		
		op.setValue("clientName", c.printQualifiedName().replace('.','_'));
		return res;
	}
}
