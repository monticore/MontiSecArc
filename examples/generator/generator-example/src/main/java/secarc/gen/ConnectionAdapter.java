/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.List;

import mc.types._ast.ASTQualifiedName;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import secarc._ast.ASTSecArcConnector;
import secarc._ast.ASTSecArcSimpleConnector;

public class ConnectionAdapter {
	private ASTArcSimpleConnector simpleConnector;
	private ASTArcConnector connector;
	private boolean isSimple;
	
	public ConnectionAdapter(ASTArcSimpleConnector connection){
		this.simpleConnector = connection;
		this.isSimple = true;
	}
	public ConnectionAdapter(ASTArcConnector connection){
		this.connector = connection;
		this.isSimple = false;		
	}
	
	//----------------------------------
	public ASTQualifiedName getSource(){
		if(isSimple){
			return simpleConnector.getSource();
		}else{
			return connector.getSource();
		}
	}
	
	public List<String[]> printedTargets(){
		if(isSimple){
			return simpleConnector.printedTargets();
		} else {
			return connector.printedTargets();
		}
	}
	
	public boolean isEncrypted(){
		if(isSimple){
			if(this.simpleConnector instanceof ASTSecArcSimpleConnector){
				ASTSecArcSimpleConnector secConnection = (ASTSecArcSimpleConnector) this.simpleConnector;
				return secConnection.isEncrypted();
			} else {
				return false;
			}
		} else {
			if(this.connector instanceof ASTSecArcConnector){
				ASTSecArcConnector secConnection = (ASTSecArcConnector) this.connector;
				return secConnection.isEncrypted();
			} else {
				return false;
			}
		}
	}
}
