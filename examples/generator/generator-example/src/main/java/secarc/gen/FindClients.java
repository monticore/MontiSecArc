/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mc.ast.ASTNode;
import mc.codegen.TemplateCalculator;
import mc.codegen.TemplateOperator;
import mc.types._ast.ASTQualifiedName;
import mc.umlp.arcd._ast.ASTArcComponent;
import mc.umlp.arcd._ast.ASTArcConnector;
import mc.umlp.arcd._ast.ASTArcSimpleConnector;
import secarc._ast.ASTSecArcFilterComponent;
import secarc._ast.ASTSecArcIdentity;

public class FindClients extends TemplateCalculator<ASTNode> {

	@Override
	public boolean calc(ASTNode ast, TemplateOperator op) {
		List<ConnectionAdapter> allAdapterConnections = new ArrayList<>();
		
		Set<FullTargetInfo> allTargets = new HashSet<FullTargetInfo>();

		String ftiName = "", ftiIp = "", ftiFullPortName = "";

		ASTSecArcFilterComponent c = (ASTSecArcFilterComponent) ast;
		
		// ausgehende Verbindungen in eigener Komponente
		allAdapterConnections = setAllAdapters(c);
		for (ConnectionAdapter connection : allAdapterConnections) {
			String connectionSource = connection.getSource().toString();
			if (!connectionSource.contains(".")) {			// ausgehende Verbindungen aus eigener Komponente
				for (String[] target : connection.printedTargets()) {
					FullTargetInfo fti = new FullTargetInfo();
					if (target[0].equals("")) {				// Ziel in eigener Komponente
						ftiName = "Self";
						ftiIp = "127.0.0.1";
						ftiFullPortName = c.printQualifiedName().replace('.','_')+ "_" + target[1];
					} else {
						target[0] = target[0].replace(".", "");
						ftiName = target[0];
						// Ziel auf höherer Ebene
						if (c.getMainParent() != null) {
							ASTSecArcFilterComponent parent = (ASTSecArcFilterComponent) c.getMainParent();
							ftiFullPortName = parent.printQualifiedName().replace('.', '_') + "_" + target[1];
							ftiIp = parent.printQualifiedName();
							// Alle anderen Komponenten auf gleicher Ebene durchsuchen
							for (ASTArcComponent innerC : parent.getInnerComponents()) {
								ASTSecArcFilterComponent innerFilterC = (ASTSecArcFilterComponent) innerC;
								if (innerFilterC.getInstanceName().equals(target[0])) {
									ftiFullPortName = innerFilterC.printQualifiedName().replace('.','_')+ "_" + target[1];
									ftiIp = innerFilterC.printQualifiedName();
								}
							}
						}
						// Ziel auf unter Ebene
						for (ASTArcComponent innerC : c.getInnerComponents()) {
							ASTSecArcFilterComponent innerFilterC = (ASTSecArcFilterComponent) innerC;
							if (innerFilterC.getInstanceName().equals(target[0])) {
								ftiFullPortName = innerFilterC.printQualifiedName().replace('.', '_')+ "_" + target[1];
								ftiIp = innerFilterC.printQualifiedName();
							}
						}
					}
					fti.setName(ftiName);
					fti.setPort(target[1]);
					fti.setFullPortName(ftiFullPortName);
					fti.setIp(ftiIp);
					fti.setEncrypted(connection.isEncrypted());
					allTargets.add(fti);
				}
			}
		}

		// alle ausgehenden Verbindungen der höheren Ebene
		if (c.getMainParent() != null) {
			ASTSecArcFilterComponent parent = (ASTSecArcFilterComponent) c.getMainParent();
			allAdapterConnections = setAllAdapters(parent);
			for (ConnectionAdapter connection : allAdapterConnections) {
				String connectionSource = connection.getSource().toString();
				if (connectionSource.startsWith(c.getInstanceName() + ".")) { // ausgehende Verbindungen aus aktueller Komponente
					for (String[] target : connection.printedTargets()) {
						FullTargetInfo fti = new FullTargetInfo();
						if (target[0].equals("")) { // Ziel auf parent Ebene

							if (parent.getInstanceName() != null) {
								ftiName = parent.getInstanceName();
							} else {
								ftiName = parent.getName();
							}

							ftiIp = parent.printQualifiedName();
							ftiFullPortName = parent.printQualifiedName().replace('.', '_') + "_" + target[1];
						} else {
							target[0] = target[0].replace(".", "");
							// Ziel auf eigener Ebene
							for (ASTArcComponent innerC : parent.getInnerComponents()) {
								ftiName = target[0];
								ASTSecArcFilterComponent innerFilterC = (ASTSecArcFilterComponent) innerC;
								if (target[0].equals(c.getInstanceName())) {
									ftiName = "Self";
									ftiIp = "127.0.0.1";
									ftiFullPortName = c.printQualifiedName().replace('.', '_')+ "_"+ target[1];
								} else if(target[0].equals(innerFilterC.getInstanceName())) {
									ftiFullPortName = innerFilterC.printQualifiedName().replace('.','_')+ "_" + target[1];
									ftiIp = innerFilterC.printQualifiedName();
								}
							}
						}
						fti.setName(ftiName);
						fti.setPort(target[1]);
						fti.setFullPortName(ftiFullPortName);
						fti.setIp(ftiIp);
						fti.setEncrypted(connection.isEncrypted());
						allTargets.add(fti);
					}
				}
			}
			
			// alle Subkomponenten der Parent Component durchsuchen (also alle Komponenten auf der Ebene der aktuellen Komponente)
			for (ASTArcComponent innerC : parent.getInnerComponents()) {
				ASTSecArcFilterComponent innerFilterC = (ASTSecArcFilterComponent) innerC;
				allAdapterConnections = setAllAdapters(innerFilterC);
				for (ConnectionAdapter connection : allAdapterConnections) {
					String connectionSource = connection.getSource().toString();
					if (connectionSource.startsWith(c.getInstanceName() + ".")) { // ausgehende Verbindungen auf eigener Ebene
						for (String[] target : connection.printedTargets()) {
							FullTargetInfo fti = new FullTargetInfo();
							if (target[0].equals("")) { // Ziel in eigener Komponente
								if (innerFilterC.getInstanceName() != null) {
									ftiName = innerFilterC.getInstanceName();
								} else {
									ftiName = innerFilterC.getName();
								}

								ftiIp = innerFilterC.printQualifiedName();
								ftiFullPortName = innerFilterC.printQualifiedName().replace('.', '_')+ "_" + target[1];
							} else {
								target[0] = target[0].replace(".", "");
								ftiFullPortName = parent.printQualifiedName().replace('.', '_') + "_" + target[1];
								ftiIp = parent.printQualifiedName();
								// Ziel auf gleicher Ebene
								for (ASTArcComponent innerTargetC : parent.getInnerComponents()) {
									ftiName = target[0];
									ASTSecArcFilterComponent innerTargetFilterC = (ASTSecArcFilterComponent) innerTargetC;
									if (target[0].equals(c.getInstanceName())) {
										ftiName = "Self";
										ftiIp = "127.0.0.1";
										ftiFullPortName = c.printQualifiedName().replace('.', '_')+ "_" + target[1];
									} else if (innerTargetFilterC.getInstanceName().equals(target[0])) {
										ftiFullPortName = innerTargetFilterC.printQualifiedName().replace('.', '_')+ "_" + target[1];
										ftiIp = innerTargetFilterC.printQualifiedName();
									}
								}
							}
							fti.setName(ftiName);
							fti.setPort(target[1]);
							fti.setFullPortName(ftiFullPortName);
							fti.setIp(ftiIp);
							fti.setEncrypted(connection.isEncrypted());
							allTargets.add(fti);
						}
					}
				}
			}

		}

		// Ausgehende Verbindungen aller Subkomponenten
		for (ASTArcComponent innerC : c.getInnerComponents()) {
			ASTSecArcFilterComponent innerFilterC = (ASTSecArcFilterComponent) innerC;
			allAdapterConnections = setAllAdapters(innerFilterC);
			for (ConnectionAdapter connection : allAdapterConnections) { // ausgehende Verbindungen der Subkomponenten
				String connectionSource = connection.getSource().toString();
				if (connectionSource.startsWith(c.getInstanceName() + ".")) { // ausgehende Verbindungen der aktuellen Komonente auf unter Ebene
					for (String[] target : connection.printedTargets()) { // alle Ziele dieser Verbindung
						FullTargetInfo fti = new FullTargetInfo();
						if (target[0].equals("")) { // Ziel auf eigener Ebene
							ftiName = innerFilterC.getInstanceName();
							ftiIp = innerFilterC.printQualifiedName();
							ftiFullPortName = innerFilterC.printQualifiedName().replace('.', '_') + "_" + target[1];
						} else {
							ftiName = "Self";
							ftiIp = "127.0.0.1";
							ftiFullPortName = c.printQualifiedName().replace('.', '_')+ "_" + target[1];
						}
						fti.setName(ftiName);
						fti.setPort(target[1]);
						fti.setFullPortName(ftiFullPortName);
						fti.setIp(ftiIp);
						fti.setEncrypted(connection.isEncrypted());
						allTargets.add(fti);
					}

				}
			}
		}

		// Doppelte Connections entfernen
		//in allTargets können sich auch doppelte Verbindungen befinden.
		Set<FullTargetInfo> noDubbleTarget = new HashSet<FullTargetInfo>();
		for (FullTargetInfo oneFTI : allTargets) {
			boolean addIt = true;
			for (FullTargetInfo dubbleTarget : noDubbleTarget) {
				if (dubbleTarget.getFullPortName().equals(oneFTI.getFullPortName())) addIt = false;
			}
			if (addIt) noDubbleTarget.add(oneFTI);
		}
		//noDoubbleTarget enthält jetzt alle Verbindungen der Komponente genau einmal
		
		// Identity Links bearbeitetn
		//Elemente aus noDoubbleTarget bekommem bei Bedarf angefügt, dass die Verbindung mit Identity Link ist
		
		// eigene ausgehende Identities
		List<ASTSecArcIdentity> identities = c.getAllIdentities();
		for (ASTSecArcIdentity identity : identities) {
			if (identity.getSource().toString().equals(c.getInstanceName().toString())) {
				for (ASTQualifiedName target : identity.getTargets()) {
					for (FullTargetInfo oneFTI : noDubbleTarget) {
						if (target.toString().equals(oneFTI.getName())) oneFTI.setIdentity(true);
					}
				}
			}
		}
		// ausgehende Identities auf höherer Ebene
		if (c.getMainParent() != null) {
			ASTSecArcFilterComponent parent = (ASTSecArcFilterComponent) c.getMainParent();
			List<ASTSecArcIdentity> parentIds = parent.getAllIdentities();
			for (ASTSecArcIdentity identity : parentIds) {
				if (identity.getSource().toString().equals(c.getInstanceName().toString())) {
					for (ASTQualifiedName target : identity.getTargets()) {
						for (FullTargetInfo oneFTI : noDubbleTarget) {
							if (target.toString().equals(oneFTI.getName())) oneFTI.setIdentity(true);
						}
					}
				}
			}
		}

		op.setValue("packageName", c.printQualifiedName());
		op.defineValue("allTargets", noDubbleTarget);
		return true;
	}
	
	private List<ConnectionAdapter> setAllAdapters(ASTSecArcFilterComponent component){
		List<ASTArcConnector> allConnections = new ArrayList<ASTArcConnector>();
		List<ASTArcSimpleConnector> allSimpleConnections = new ArrayList<ASTArcSimpleConnector>();
		List<ConnectionAdapter> res = new ArrayList<ConnectionAdapter>();
		
		allConnections = component.getAllEncryptedConnectors();
		allConnections.addAll(component.getAllUnencryptedConnectors());
		for(ASTArcConnector connection : allConnections){
			res.add(new ConnectionAdapter(connection));
		}
		allSimpleConnections = component.getAllEncryptedSimpleConnectors();
		allSimpleConnections.addAll(component.getAllUnencryptedSimpleConnectors());
		for(ASTArcSimpleConnector connection : allSimpleConnections){
			res.add(new ConnectionAdapter(connection));
		}
		return res;
	}
}
