/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import mc.ast.ASTNode;
import mc.codegen.TemplateCalculator;
import mc.codegen.TemplateOperator;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc._ast.ASTSecArcFilterComponent;
import secarc._ast.ASTSecArcPort;

public class FindIniInfo  extends TemplateCalculator<ASTNode> {
	  
	  @Override
	  public boolean calc(ASTNode ast, TemplateOperator op) {
		  
	      ASTSecArcFilterComponent c = (ASTSecArcFilterComponent)ast;
	      Map<String,Set<String>> globalRoles = new HashMap<String,Set<String>>();
	      Map<String,Set<String>> portRoles = new HashMap<String,Set<String>>();
	      
		  Set<String> allPorts = new HashSet<String>();
		  Set<String> noRoleDefined = new HashSet<String>();
		  
		  String qualifiedName = c.printQualifiedName().replace('.', '_')+"_";
		  
		  //globale Rollen mit allen eingehenden Ports zuordnen
		  if(c.getAllRoles() != null){
			  for(ASTArcPort port : c.getPorts()){
				  if(port.isIncoming()){
					  allPorts.add(qualifiedName+port.printName());
				  }
			  }
			  for(String role : c.getAllRoles().getRoles()){
				  globalRoles.put(role, allPorts);
			  }
			  noRoleDefined.add("nothing");
		  }
		  
		  //Ports mit individuellen Rollen aus den globalen Rollen löschen und portbezogene Rollen erstellen 
	      for(ASTArcPort port : c.getPorts()){
	    	  ASTSecArcPort secPort ;
	    	  if(port instanceof ASTSecArcPort && port.isIncoming()){
	    		  secPort = (ASTSecArcPort) port;
		    	  if(secPort.getRoles().isEmpty()){
		    		  //keine Globale Rollen -> Ports ohne Rollen haben "no Role Defined"
		    		  if(c.getAllRoles() == null) noRoleDefined.add(qualifiedName+port.getName()); 
		    	  } else {
		    		  //Port Namen aus den Globalen Rollen wieder entfernen
		    		  for(Map.Entry<String, Set<String>> gRole : globalRoles.entrySet()){
		    			  gRole.getValue().remove(qualifiedName+port.getName());
		    		  }
		    		  //Port wird allen entsprechenden Rollen zugeordnet
		    		  for(String portRole : secPort.getRoles()){
	    				  Set<String> currentPorts = new HashSet<String>();
		    			  if(portRoles.containsKey(portRole)) currentPorts = portRoles.get(portRole);
		    			  currentPorts.add(qualifiedName+port.getName());
		    			  portRoles.put(portRole, currentPorts);
		    		  }
		    	  }
	    	  }
	      }
	      
	      if(noRoleDefined.isEmpty()) noRoleDefined.add("nothing");
		  globalRoles.put("noRoleDefined", noRoleDefined);
	      
		  //----------------------------------------------
		  
	      Set<String> allRoles = new HashSet<String>();

	      //alle globalen Rollen in String-Set für Generator umwandeln
	      for(Map.Entry<String, Set<String>> gRole : globalRoles.entrySet()){
	    	  String komma = "";
	    	  String roleAsString = gRole.getKey() + " = ";
	    	  for(String portAsString : gRole.getValue()){
	    		  roleAsString = roleAsString + komma + portAsString;
	    		  komma = ", ";
	    	  }
	    	  allRoles.add(roleAsString);
	      }
	      
	      //alle portbezogenen Rollen in String-Set für Generator umwandeln
	      for(Map.Entry<String, Set<String>> pRole : portRoles.entrySet()){
	    	  String komma = "";
	    	  String roleAsString = pRole.getKey() + " = ";
	    	  for(String portAsString : pRole.getValue()){
	    		  roleAsString = roleAsString + komma + portAsString;
	    		  komma = ", ";
	    	  }
	    	  allRoles.add(roleAsString);
	      }
		  
	      op.defineValue("roles", allRoles);
	      
		  return true;
	  }

}
