/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import mc.ast.ASTNode;
import mc.ast.Visitor;
import mc.codegen.TemplateCalculator;
import mc.codegen.TemplateOperator;

public class FindPorts extends TemplateCalculator<ASTNode> {
	
	@Override
	public boolean calc(ASTNode ast, TemplateOperator op) {
	    PortVisitor vis = new PortVisitor();
	    Visitor.run(vis, ast);
	    op.setValue("portList",vis.getPortList());
	    return true;
	}
}
