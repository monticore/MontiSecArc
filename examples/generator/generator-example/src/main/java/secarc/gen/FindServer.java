/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mc.ast.ASTNode;
import mc.codegen.TemplateCalculator;
import mc.codegen.TemplateOperator;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc._ast.ASTSecArcFilterComponent;

public class FindServer extends TemplateCalculator<ASTNode> {
	  
	  @Override
	  public boolean calc(ASTNode ast, TemplateOperator op) {
		  
		  Set<String> incommingPorts = new HashSet<String>();
	      ASTSecArcFilterComponent component = (ASTSecArcFilterComponent)ast;
	      String qualifiedName = component.printQualifiedName().replace('.', '_');  //qualifizierender Name für Port Variablen
	      
	      //alle eingehenden Ports werden der PortListe des Servers Hinzugefügt
	      List<ASTArcPort> ports = component.getPorts();
	      for(ASTArcPort port : ports){
	    	  if(port.isIncoming()){
	    		  incommingPorts.add(qualifiedName+"_"+port.getName());
	    	  }
	      }
	      
	      // packageName ist der qualifiedName
	      op.setValue("packageName", component.printQualifiedName());
	      op.setValue("incommingPorts", incommingPorts);
	    return true;
	  }
}
