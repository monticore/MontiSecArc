/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

public class FullTargetInfo{
	private String name;
	private String port;
	private String ip;
	private String fullPortName;
	private boolean identity = false;
	private boolean encrypted;
	
	public void setName(String in_name){
		this.name = in_name.replace(".", "");
	}
	public String getName(){
		return name;
	}
	
	public void setPort(String in_port){
		this.port = in_port;
	}
	public String getPort(){
		return port;
	}
	
	public void setFullPortName(String in_fullPort){
		this.fullPortName = in_fullPort;
	}
	public String getFullPortName(){
		return fullPortName;
	}
	
	public void setIp(String in_ip){
		this.ip = in_ip;
	}
	public String getIp(){
		return ip;
	}
	
	public void setIdentity(boolean in_ID){
		this.identity = in_ID;
	}
	public boolean getIdentity(){
		return this.identity;
	}
	
	public void setEncrypted(boolean in_encrypt){
		this.encrypted = in_encrypt;
	}
	public boolean getEncrypted(){
		return this.encrypted;
	}
}
