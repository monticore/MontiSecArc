/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mc.ast.ConcreteVisitor;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc._ast.ASTSecArcFilterComponent;

public class PortVisitor extends ConcreteVisitor{
	
    private static Set<String> portList = new HashSet<String>();
	
	public void visit(ASTSecArcFilterComponent c){
		List<ASTArcPort> ports = c.getPorts();
	    for(ASTArcPort port : ports){
	    	if(port.isIncoming()) portList.add(c.printQualifiedName().replace('.', '_')+"_"+port.printName());
	    }
	}
	
	public Set<String> getPortList(){
		return portList;
	}

}
