/* (c) https://github.com/MontiCore/monticore */
package secarc.gen;

import java.util.List;

import mc.ast.ASTNode;
import mc.codegen.TemplateCalculator;
import mc.codegen.TemplateOperator;
import mc.umlp.arcd._ast.ASTArcPort;
import secarc._ast.ASTSecArcFilterComponent;

public class ServerNeeded extends TemplateCalculator<ASTNode> {

	@Override
	public boolean calc(ASTNode ast, TemplateOperator op) {
		boolean res = false;
		ASTSecArcFilterComponent c = (ASTSecArcFilterComponent) ast;
		List<ASTArcPort> ports = c.getPorts();
		for(ASTArcPort port : ports){
			if(port.isIncoming()){
				res = true;
			}
		}
		op.setValue("serverName", c.printQualifiedName().replace('.','_'));
		return res;
	}
}
