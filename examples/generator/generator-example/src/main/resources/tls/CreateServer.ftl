<#-- (c) https://github.com/MontiCore/monticore -->
<#if op.callCalculator("secarc.gen.FindServer")></#if>
package ${packageName};

import secarc.rte.Server;

public class ${ast.getName()}_Server extends Server {

	public ${ast.getName()}_Server(){
	<#list incommingPorts as port>
		listenPorts.add("${port}");
    </#list>
	}
}
