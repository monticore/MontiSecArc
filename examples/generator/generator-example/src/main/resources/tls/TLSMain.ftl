<#-- (c) https://github.com/MontiCore/monticore -->
<#-- Disable Java Comments, when not generating Java
${op.setTracing(false)}-->

<#if op.callCalculator("secarc.gen.ServerNeeded")>
${op.callTemplates(
    "tls.CreateServer",
    ast.printQualifiedName()+"."+ast.getName()+"_Server",
    "java",
    ast)}
</#if>
