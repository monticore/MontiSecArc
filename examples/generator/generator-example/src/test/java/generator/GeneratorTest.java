/* (c) https://github.com/MontiCore/monticore */
package generator;

import static org.junit.Assert.*;

import java.io.File;

//import mc.umlp.arc.MontiArcConstants;
//import mc.umlp.arc.MontiArcGeneratorTool;



import mc.MCG;

import org.junit.BeforeClass;
import org.junit.Test;

import secarc.MontiSecArcGeneratorTool;
import secarc.MontiSecArcTool;

/**
 * TODO: Write me!
 *
 *
 */
public class GeneratorTest {
  
  private static final String 
      TEST_INPUT_FOLDER = "src/test/resources",
      TEST_GEN_DIR = "target/generated-test-sources";
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }
  
  @Test
  public void testExecuteDefault() {
    MCG.initMonticoreGlobals();
    
    MontiSecArcGeneratorTool tool = new MontiSecArcGeneratorTool(new String[] {
              TEST_INPUT_FOLDER + "/executeDefault",
      //"src/main/models", 
      "-out", "target/generated-test-sources", 
      "-gentarget", "target/generated-test-sources", 
//      "-conf", "mc.cfg", 
      "-symtabdir", "target/generated-test-sources/symboltable", 
      //"-check ALL default",
      "-analysis", "ALL", "parse",
      "-analysis", "ALL", "init", 
      "-analysis", "ALL", "createExported", 
      "-synthesis", "ALL", "prepareCheck",
      "-synthesis", "ALL", "check",
      //"-generator", "tls.IdentityMain", "secarc._ast.ASTSecArcIdentity",
      //"-generator", "tls.TLSMain", "secarc._ast.ASTSecArcFilterComponent",
      "-generator", "tls.TLSMain", "secarc._ast.ASTSecArcFilterComponent"
      
      
      });
      tool.init();
      tool.run();
//      File expectedFactory = new File(MontiArcConstants.DEFAULT_GEN_DIR + "/a/gen/factories/MyModelFactory.java");
//      File expectedInterface = new File(MontiArcConstants.DEFAULT_GEN_DIR + "/a/gen/interfaces/IMyModel.java");
//      File expectedComp = new File(MontiArcConstants.DEFAULT_GEN_DIR + "/a/gen/AMyModel.java");
//      assertTrue(expectedFactory.exists());
//      assertTrue(expectedInterface.exists());
//      assertTrue(expectedComp.exists());
  }
  
}
