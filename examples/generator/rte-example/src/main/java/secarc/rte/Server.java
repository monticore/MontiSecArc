/* (c) https://github.com/MontiCore/monticore */
package secarc.rte;

import java.util.*;
 
public abstract class Server {
	private Set<Integer> portList = new HashSet<Integer>();
	
	protected Set<String> listenPorts = new HashSet<String>();
	
	public void start() {
	}
	
}
