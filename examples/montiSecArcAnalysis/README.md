<!-- (c) https://github.com/MontiCore/monticore -->
#MontiSecArc Analysis Example
Start the analysis tool by executing the script startExample.sh.
This will analyze the MontiSecArc Model in src/test/resources/secarc/seccds/fe/Store.secarc, which uses different files in src/test/resources/.

The impact of the individual analysis can be adjusted in src/main/conf/Analysis_Conf.txt

#Build
In case you want to change the analysises implemented in MontiSecArcAnalysis, run mvn install in the ../../core folder and change the script startExample.sh to point at the resulting jar ../../core/montiSecArcAnalysis/target/montiSecArcAnalysis.jar.

## Further Information

* [Project root: MontiCore @github](https://github.com/MontiCore/monticore)
* [MontiCore documentation](http://www.monticore.de/)
* [**List of languages**](https://github.com/MontiCore/monticore/blob/dev/docs/Languages.md)
* [**MontiCore Core Grammar Library**](https://github.com/MontiCore/monticore/blob/dev/monticore-grammar/src/main/grammars/de/monticore/Grammars.md)
* [Best Practices](https://github.com/MontiCore/monticore/blob/dev/docs/BestPractices.md)
* [Publications about MBSE and MontiCore](https://www.se-rwth.de/publications/)
* [Licence definition](https://github.com/MontiCore/monticore/blob/master/00.org/Licenses/LICENSE-MONTICORE-3-LEVEL.md)

