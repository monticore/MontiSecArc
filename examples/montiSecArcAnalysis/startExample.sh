#!/bin/bash
# (c) https://github.com/MontiCore/monticore  


MSAA="montiSecArcAnalysis-0.0.16.jar"
#MSAA="../../core/montiSecArcAnalysis/target/montiSecArcAnalysis.jar"

./startMontiSecArc.sh $MSAA src/test/resources/ mc-test.cfg target/out target/symtab -analysisconfpath src/main/conf/Analysis_Conf.txt secAnalysisBeginners src/test/resources/de/rwth/se/supermarket/SupermarketTradingSystem.secarc
