#!/bin/bash
# (c) https://github.com/MontiCore/monticore  
standard_command="java -jar $1 -mp $2 -conf $3 -out $4 -symtabdir $5 -analysis ALL parse -analysis ALL init -analysis ALL createExported -synthesis ALL prepareCheck -synthesis arcd preCheckTransformation -synthesis ALL check"

conf_command=""

if [ "-analysisconfpath" == "$6" ]
  then 
    conf_command="$6 $7"
    shift
    shift
fi

shift
shift
shift 
shift 
shift

trustlevel_command="-synthesis secarc secPreParameter -synthesis secarc secFilterTrustlevel"

analysis_command="-synthesis secarc $1"
shift

while [ "$1" != '' ] 
  do
    if [[ $1 =~ .*/.* ]] 
      then
         files+=("$1") 
         shift
    elif [ "-critical" == $1 ]
      then 
        critical_command="-synthesis secarc secFilterCriticalPort" 
        shift
    else
       trustlevel_command+=( "$1" )
       shift
    fi
  done

$standard_command $conf_command ${trustlevel_command[*]} $critical_command $analysis_command ${files[*]}
